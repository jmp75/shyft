time\_series.view\_container.legend
===================================

.. automodule:: shyft.dashboard.time_series.view_container.legend
   :members:
   :undoc-members:
   :show-inheritance:
