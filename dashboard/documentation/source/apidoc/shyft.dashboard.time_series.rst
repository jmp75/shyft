
shyft.dashboard.time\_series
============================


.. automodule:: shyft.dashboard.time_series
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.time_series.attr_callback_manager
   shyft.dashboard.time_series.axes
   shyft.dashboard.time_series.axes_handler
   shyft.dashboard.time_series.bindable
   shyft.dashboard.time_series.data_utility
   shyft.dashboard.time_series.ds_view_handle
   shyft.dashboard.time_series.ds_view_handle_registry
   shyft.dashboard.time_series.dt_selector
   shyft.dashboard.time_series.formatter
   shyft.dashboard.time_series.layouts
   shyft.dashboard.time_series.renderer
   shyft.dashboard.time_series.state
   shyft.dashboard.time_series.ts_viewer
   shyft.dashboard.time_series.view

**Subpackages**

.. toctree::

   shyft.dashboard.time_series.sources
   shyft.dashboard.time_series.tools
   shyft.dashboard.time_series.view_container
