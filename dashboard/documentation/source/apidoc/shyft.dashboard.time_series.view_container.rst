
shyft.dashboard.time\_series.view\_container
============================================


.. automodule:: shyft.dashboard.time_series.view_container
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.time_series.view_container.figure
   shyft.dashboard.time_series.view_container.legend
   shyft.dashboard.time_series.view_container.table
   shyft.dashboard.time_series.view_container.view_container_base
