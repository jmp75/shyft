/* tslint:disable:variable-name */
export class TsInfo {
  name: string;
  pfx: boolean;
  delta_t: number; // time_
  olson_tz_id: string;
  data_period: number[]; // period_
  created: number; // time_
  modified: number; // time_
}

export class TimeSeries {
  pfx: boolean;
  data: number[][];
}

export class ReadRequest {
  request_id: number;
  read_period: number[];
  clip_period: number[];
  cache: boolean;
  ts_ids: string[];
}
