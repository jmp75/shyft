import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TsTableComponent } from './ts-table.component';

describe('TsTableComponent', () => {
  let component: TsTableComponent;
  let fixture: ComponentFixture<TsTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
