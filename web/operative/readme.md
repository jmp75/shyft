### An example for starting the server:

a] open a conda prompt
b] activate your conda enviroment used for shyft development
    - requires the usual shyft packages, and the qt package (PySide2)
c] start server:
    python start_config_server.py --doc-root ./db-config-files