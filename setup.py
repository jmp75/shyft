import glob
import os
import platform
import shutil
import subprocess
from os import path, chdir, getcwd

import sys
from setuptools import setup, find_packages, Distribution
from sysconfig import get_platform

shyft_root = path.dirname(path.realpath(__file__))
chdir(shyft_root)
print(f'Building Shyft Open Source in {shyft_root}, cwd={getcwd()}')


class BinaryDistribution(Distribution):
    """Distribution which always forces a binary package with platform name"""

    def has_ext_modules(self):
        return True


# OK version stuff: Basic idea: if file VERSION exists, use that otherwise git
if path.exists('VERSION'):
    VERSION = open('VERSION').read().strip()
    print(f'VERSION file:{VERSION}')
else:
    VERSION = '4.8.' + subprocess.check_output('git rev-list HEAD --count', shell=True, universal_newlines=True).strip()
    print(f'VERSION git:{VERSION}')

ext_s: str = '.pyd' if 'Windows' in platform.platform() else '.so'

# Allow build/setup time-series only part of shyft
ts_only: bool = False
ts_only_opt: str = '--ts-only'
if ts_only_opt in sys.argv:
    ts_only = True
    sys.argv.remove(ts_only_opt)  # have to remove it from opts to avoid setup.py complain
# Allow build/setup of dashboard ++ onnly part of shyft
dashboard_only: bool = False
dashboard_only_opt: str = '--dashboard-only'
# Conda does not allow same name for conda-pkg and pypi-pkg
# so we need a post-fix to the name of it
conda_pip: str = ''
conda_pip_opt: str = '--conda-pip'

if conda_pip_opt in sys.argv:
    conda_pip = '_pip'
    sys.argv.remove(conda_pip_opt)  # have to remove it from opts to avoid setup.py complain

if dashboard_only_opt in sys.argv:
    dashboard_only = True
    sys.argv.remove(dashboard_only_opt)  # have to remove it from opts to avoid setup.py complain

if ts_only and dashboard_only:
    ts_only = False
    print("Flags ts_only and dashboard_only are set, using just dashboard_only flag!")


if ts_only or dashboard_only:
    ext_names = ['shyft/time_series/_time_series' + ext_s]
else:
    ext_names = ['shyft/api/_api' + ext_s,
                 'shyft/energy_market/core/_core' + ext_s,
                 'shyft/energy_market/ltm/_ltm' + ext_s,
                 'shyft/energy_market/stm/_stm' + ext_s,
                 'shyft/time_series/_time_series' + ext_s,
                 'shyft/api/pt_st_k/_pt_st_k' + ext_s,
                 'shyft/api/pt_gs_k/_pt_gs_k' + ext_s,
                 'shyft/api/pt_hs_k/_pt_hs_k' + ext_s,
                 'shyft/api/pt_hps_k/_pt_hps_k' + ext_s,
                 'shyft/api/pt_ss_k/_pt_ss_k' + ext_s,
                 'shyft/api/r_pm_gs_k/_r_pm_gs_k' + ext_s,
                 'shyft/api/hbv_stack/_hbv_stack' + ext_s,
                 'shyft/energy_market/stm/shop/_shop' + ext_s]

needs_build_ext = not all([path.exists(ext_name) for ext_name in ext_names])

if needs_build_ext:
    print('One or more extension modules needs build, attempting auto build')
    for f in ext_names:
        if not path.exists(f):
            print(f'{f}: needs rebuild')

    if "Windows" in platform.platform():
        try:
            print(subprocess.check_output(["call build_support/win_build_shyft.cmd"], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT))
        except:
            print('\nbuild_support/win_build_shyft.cmd build FAILED.\nplease inspect log and build preconditions on gitlab.com/shyft-os/shyft')
            exit()
    elif "Linux" in platform.platform():
        try:
            # For Linux, use the cmake approach for compiling the extensions
            print(subprocess.check_output("sh build_support/build_shyft.sh", shell=True))
        except:
            print("Problems compiling shyft, try building with the build_api.sh "
                  "or build_api_cmake.sh (Linux only) script manually...")
            exit()
    else:
        print("Only windows and Linux supported")
        exit()
else:
    print('Extension modules are already built in place')

if ts_only:
    setup(
        name=f'shyft.time_series{conda_pip}',
        version=VERSION,
        author='shyft-os',
        author_email='sigbjorn.helset@gmail.com',
        url='https://gitlab.com/shyft-os/shyft',
        description='An OpenSource toolbox providing tools for advanced time-series',
        license='LGPL v3',
        packages=['shyft.time_series'],
        package_data={'shyft.time_series': ['../time_series/*.so', '../time_series/*.pyd', '../lib/*.dll', '../lib/*.s*']},
        entry_points={},
        requires=["numpy"],
        install_requires=["numpy"],
        tests_require=['pytest'],
        platforms=[get_platform()],
        zip_safe=False,
        distclass=BinaryDistribution
    )
elif dashboard_only:
    packages = ['shyft.time_series']
    packages.extend([f'shyft.{p}' for p in find_packages('shyft') if p.startswith('dashboard') or p.startswith('util')])
    setup(
        name=f'shyft.dashboard{conda_pip}',
        version=VERSION,
        author='shyft-os',
        author_email='sigbjorn.helset@gmail.com',
        url='https://gitlab.com/shyft-os/shyft',
        description='An OpenSource toolbox providing tools for energy-market, hydrological forecasting and advanced time-series',
        license='LGPL v3',
        packages=packages,
        package_data={'shyft.time_series': ['../time_series/*.so', '../time_series/*.pyd', '../lib/*.dll', '../lib/*.s*', '../util/layoutgraph/layout_configuration.yml']},
        requires=["numpy"],
        install_requires=["numpy"],
        tests_require=['pytest'],
        zip_safe=False,
        platforms=[get_platform()],
        extras_require={
            'dashboard': ['bokeh', 'pint', 'pydot']
        },
        distclass=BinaryDistribution,
        entry_points={
            'console_scripts': [
                'shyft-dashboard-examples = shyft.dashboard.entry_points.start_bokeh_examples:main',
                'shyft-dashboard-visualisation = shyft.dashboard.entry_points.visualize_apps:main'
            ],
            'shyft_dashboard_apps': [
                'dtss_viewer_app = shyft.dashboard.apps.dtss_viewer.dtss_viewer_app:DtssViewerApp'
            ]
        }
    )
else:
    # Full setup of everything
    setup(
        name=f'shyft{conda_pip}',
        version=VERSION,
        author='shyft-os',
        author_email='sigbjorn.helset@gmail.com',
        url='https://gitlab.com/shyft-os/shyft',
        description='An OpenSource toolbox providing tools for energy-market, hydrological forecasting and advanced time-series',
        license='LGPL v3',
        packages=[f'shyft.{p}' for p in find_packages('shyft')],
        package_data={'': ['../energy_market/service/install/*.s*', '*.so', '*.pyd', '../lib/*.dll', '../lib/*.s*', '../lib/SHOP_license.dat','../util/layoutgraph/layout_configuration.yml']},
        requires=["numpy"],
        install_requires=["numpy"],
        tests_require=['pytest'],
        zip_safe=False,
        platforms=[get_platform()],
        extras_require={
            'repositories': ['netcdf4', 'shapely', 'pyyaml', 'pyproj'],
            'dashboard': ['bokeh', 'pint', 'pydot'],
            'viz': ['matplotlib'],
            'notebooks': ['jupyter'],
            'energy_market_ui':['pyside2']
        },
        distclass=BinaryDistribution,
        entry_points={
            'console_scripts': [
                'shyft-dashboard-examples = shyft.dashboard.entry_points.start_bokeh_examples:main',
                'shyft-dashboard-visualisation = shyft.dashboard.entry_points.visualize_apps:main'
            ],
            'shyft_dashboard_apps': [
                'dtss_viewer_app = shyft.dashboard.apps.dtss_viewer.dtss_viewer_app:DtssViewerApp'
            ]
        }
    )
