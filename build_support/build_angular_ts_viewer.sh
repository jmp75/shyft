#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
set -o nounset # exit on undeclared vars set -u 
set -o pipefail # exit status of the last command that threw non-zero exit code returned

# Debugging set -x 
# set -o xtrace

exec 3>&1 4>&2
SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/..)}

cd $SHYFT_WORKSPACE/web/demo/angular-ts-viewer

echo "Running 'npm install'"
npm install --silent --no
echo "Runnning 'ng build'"
ng build


