#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
set -o nounset # exit on undeclared vars set -u 
set -o pipefail # exit status of the last command that threw non-zero exit code returned

# Debugging set -x 
# set -o xtrace

exec 3>&1 4>&2
SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/..)}

cd $SHYFT_WORKSPACE/web/demo/angular-ts-viewer
echo "Make coverage directory (otherwise there will be no coverage report)"
mkdir coverage

echo "Running 'npm install'"
npm install --silent --no

export CHROMIUM_BIN=/usr/bin/chromium

echo "Runnning 'ng test'"
ng test --no-watch --code-coverage --karma-config=karma-ci.conf.js

