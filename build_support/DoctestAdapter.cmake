#
# Script: DoctestAdapter.cmake
#
# CMake functionality for automatically adding CMake tests for each of the test cases
# in a doctest application, available with function add_doctests.
#
# The name of the individual CTests created by this script will be in form
# "<target>[.<suite>].<case>[.<subcase>]", which Visual Studio's Test Explorer will display
# as hierarchy. Visual Studio only interprets test names into at most 3 hierarchical levels,
# from bottom up, so that names in form "<target>.<suite>.<case>.<subcase>" will get
# "<target>.<suite>" as the name of top level node, and then "<case>" on child nodes, and
# "<subcase>" on grand-children.
# 
# Since suite and subcase are optional, then hierarchy might be either 2 or 3 levels depp. If
# mixing tests with and without suite and/or subcase the result will be inconsistent levels:
# For example with two tests "target1.test1" and "target1.suiteA.testA" then test1 and suiteA will
# both be immediate children of a node "target1". To make sure all test cases are at the same level
# and all subcases at the same level, there is an argument "consistent_hierarchy" that can
# be specified to enforce all 3 levels, with default values for any missing suite and subcase names.
# Then all targets.suites are at level 1, cases at level 2 and subcases at level 3. But if the test
# code is consistently not using suites or subcases, then this will make the hierarchy a unnecessary
# verbose with dummy entries in all paths, so then it might be a good idea to set argument
# consistent_hierarchy to false.
#
# Sample usage:
#
#   enable_testing()
#   find_file(doctest_ADAPTER NAMES "DoctestAdapter.cmake")
#   if(doctest_ADAPTER)
#       include(${doctest_ADAPTER})
#       add_doctests(ShopLibTest "tests1.cpp;tests2.cpp;tests3.cpp" TRUE)
#   else()
#       message(WARNING "Doctest adapter CMake script not found, CMake tests not generated!")
#   endif()
#
# The sample above will try to search for this utility file, but you can also directly specify
# its path in variable doctest_ADAPTER, for example using -D argument on the CMake command line.
#
# Note: This function will only run during the CMake generate step, so after modifying
# the test source code the generate step must forced to run again for the CMake tests
# to be correct according to the test implementation!
#
# Note: Using fairly naive regex matching on doctest keywords, lines commented out
# with single line comment will be skipped, but if they are within multi-line
# comment or preprocessor #if directives they will still be included.
#
# Note: The length of suite/case/subcase names must be limited! Depending on the length of
# the build directory path, the limit for the total combined test names could be perhaps
# 100 or 150 characters. The reason is that Visual Studio generates a json file in the build
# folder for each test, and the file name includes the complete test name, as well as a
# prefix "CMakeLists.Tests." and some unique numbers. If the total path to such a file
# exceeds the path length limit on the system (259 characters on Windows), Visual Studio
# will not be able to create it and the result is often that the Test Explorer hangs.
#

#
# Helper function: make_identifier 
#
# Transform the value of a specified string variable into a valid CMake identifier,
# which means it must match the regex "^[A-Za-z_][A-Za-z0-9_]*$".
# Arguments are name of variable to store results and the string to process,
# to perform in-place modification then supply name of the string variable
# as first argument: make_identifier(string_variable ${string_variable})
#
function(make_identifier output_variable input_string)
	# Replace all sequences of illegal characters with one underscore.
	string(REGEX REPLACE "[^A-Za-z0-9_]+" "_" result "${input_string}")
	# First character has additional restriction, prepend name with underscore if first
	# character is not valid (after replacing characters that are illegal everywhere).
	string(REGEX REPLACE "(^[^A-Za-z_].*)" "_\\1" result "${result}")
	# Store result back to variable supplied by caller.
	set(${output_variable} ${result} PARENT_SCOPE)
endfunction()

#
# Helper function: unescape_string
#
# Transform an escaped C-style string ("string with possible escaped quotes \" or other \\ special characters")
# into unescaped form ("string with possible escaped quotes " or other \ special characters").
# Useful when using an escaped C-style string in CMake commands such as add_test where CMake does automatically
# perform escaping on its own, to avoid double escaping.
# Arguments are name of variable to store results and the string to process,
# to perform in-place modification then supply name of the string variable
# as first argument: unescape_string(string_variable ${string_variable})
#
function(unescape_string output_variable input_string)
	string(REGEX REPLACE "\\\\(.)" "\\1" result "${input_string}")
	set(${output_variable} ${result} PARENT_SCOPE)
endfunction()

#
# Helper function: add_doctest
#
# Given a doctest application (target) and name of a test suite (optional),
# name of a test case, and name of subcase (optional), then generate a
# single CMake add_test command with appropriate name and doctest command
# line filters.
#
function(add_doctest test_application suite_name case_name subcase_name consistent_hierarchy)
	# Part 1: Application
	get_filename_component(application_name "${test_application}" NAME_WE)
	set(test_name ${application_name})
	# Part 2: Suite
	if(suite_name)
		unescape_string(suite_name_unescaped ${suite_name})
		string(REGEX MATCH "[,*?]" illegal_characters_match "${suite_name_unescaped}")
		if(illegal_characters_match)
			message(WARNING "Suite name \"${suite_name_unescaped}\" contains illegal character [,*?]") # Must avoid special characters in command line filters, and there is no escaping (https://github.com/onqtam/doctest/issues/135)
		endif()
		set(suite_filter "--test-suite=${suite_name_unescaped}")
		make_identifier(suite_name ${suite_name}) # After adding real suite_name as filter we transform it into a valid CMake test name. The rule is that the test name may not contain spaces, quotes, or other characters special in CMake syntax. Not sure exactly what this means, but restricting it to a valid CMake identifier is definitely enough. At least we want to avoid "." or "::" which VS Test Explorer uses to create hierarchy.
	elseif(consistent_hierarchy)
		set(suite_name "${application_name}") # Dummy suite with same name as application
	endif()
	if(suite_name)
		if(test_name)
			string(APPEND test_name ".${suite_name}")
		else()
			set(test_name ${suite_name})
		endif()
	endif()
	# Part 3: Case
	if(case_name)
		unescape_string(case_name_unescaped ${case_name})
		string(REGEX MATCH "[,*?]" illegal_characters_match "${case_name_unescaped}")
		if(illegal_characters_match)
			message(WARNING "Case name \"${case_name_unescaped}\" contains illegal character [,*?]") # Must avoid special characters in command line filters, and there is no escaping (https://github.com/onqtam/doctest/issues/135)
		endif()
		set(case_filter "--test-case=${case_name_unescaped}")
		make_identifier(case_name ${case_name})
		if(test_name)
			string(APPEND test_name ".${case_name}")
		else()
			set(test_name ${case_name})
		endif()
	endif()
	# Part 4: Subcase
	if (subcase_name)
		unescape_string(subcase_name_unescaped ${subcase_name})
		string(REGEX MATCH "[,*?]" illegal_characters_match "${subcase_name_unescaped}")
		if(illegal_characters_match)
			message(WARNING "Subcase name \"${subcase_name_unescaped}\" contains illegal character [,*?]") # Must avoid special characters in command line filters, and there is no escaping (https://github.com/onqtam/doctest/issues/135)
		endif()
		set(subcase_filter "--subcase=${subcase_name_unescaped}")
		make_identifier(subcase_name ${subcase_name})
	elseif(consistent_hierarchy)
		set(subcase_name "${case_name}") # Dummy subcase with same name as parent case
	endif()
	if (subcase_name)
		if(test_name)
			string(APPEND test_name ".${subcase_name}")
		else()
			set(test_name ${subcase_name})
		endif()
	endif()
	message(DEBUG "Adding test \"${test_name}\" for command \"${test_application}\" and arguments \"${suite_filter}\" \"${case_filter}\" \"${subcase_filter}\"")
	add_test(NAME "${test_name}" COMMAND "${test_application}" "${suite_filter}" "${case_filter}" "${subcase_filter}" WORKING_DIRECTORY $<TARGET_FILE_DIR:${test_application}>)
endfunction()

#
# Function add_doctests
#
# Given a doctest application find all test suites/cases/subcases and generate
# CMake add_test commands with appropriate filters to make a CMake test of each
# of them, running them individually.
#
# Note: Using fairly naive regex matching on doctest keywords, lines commented out
# with single line comment will be skipped, but if they are within multi-line
# comment or preprocessor #if directives they will still be included.
#
function(add_doctests test_application file_paths consistent_hierarchy)
	message(STATUS "Adding doctests for ${test_application}")
	foreach (file_path ${file_paths})
		get_filename_component(file_path ${file_path} ABSOLUTE) # Resolve relative paths since behavior of EXISTS is well-defined only for full paths.
		if (EXISTS ${file_path})
			set(test_count 0)
			# Read all lines from the file that contains a relevant doctest keyword
			file(STRINGS ${file_path} test_items REGEX "^[ \t]*(TEST_SUITE|TEST_CASE|TEST_CASE_FIXTURE|SUBCASE)[ \t]*[(]")
			if (test_items)
				list(LENGTH test_items num_test_items)
				math(EXPR last_index "${num_test_items}-1")

				set(test_suite_name "")
				set(test_case_name "")
				set(test_subcase_name "")

				foreach(index RANGE 0 ${last_index})
					list(GET test_items ${index} test_item)
					# Regex match: <doctest-keyword><space>(<space>"string with possible escaped quotes \" or other \\ special characters"<anything...>)
					string(REGEX MATCH "^[ \t]*(TEST_SUITE|TEST_CASE|SUBCASE)[ \t]*[(][ \t]*[\"](([^\"]|\\\")*)[\"].*$" test_item_match "${test_item}")
					if (NOT test_item_match)
						string(REGEX MATCH "^[ \t]*(TEST_CASE_FIXTURE)[ \t]*[(][^,]*[,][ \t]*[\"](([^\"]|\\\")*)[\"].*$" test_item_match "${test_item}")
					endif()
					if (NOT test_item_match)
						message(WARNING "Unable to match test item: ${test_item}")
					else()
						set(test_item_type ${CMAKE_MATCH_1})
						set(test_item_name ${CMAKE_MATCH_2})
						if(test_item_type STREQUAL "TEST_SUITE")
							set(test_suite_name ${test_item_name})
							set(test_case_name "")
							set(test_subcase_name "")
						elseif(test_item_type STREQUAL "TEST_CASE" OR test_item_type STREQUAL "TEST_CASE_FIXTURE")
							if(NOT test_subcase_name AND test_case_name)
								add_doctest(${test_application} "${test_suite_name}" "${test_case_name}" "" "${consistent_hierarchy}")
								MATH(EXPR test_count "${test_count}+1")
							endif()
							set(test_case_name ${test_item_name})
							set(test_subcase_name "")
						elseif(test_item_type STREQUAL "SUBCASE")
							add_doctest(${test_application} "${test_suite_name}" "${test_case_name}" "${test_item_name}" "${consistent_hierarchy}")
							MATH(EXPR test_count "${test_count}+1")
							set(test_subcase_name ${test_item_name})
						endif()
					endif()
				endforeach()
				if(NOT test_subcase_name AND test_case_name)
					add_doctest(${test_application} "${test_suite_name}" "${test_case_name}" "" "${consistent_hierarchy}")
					MATH(EXPR test_count "${test_count}+1")
				endif()
				message(VERBOSE "Added ${test_count} tests from file ${file_path}")
			else()
				message(VERBOSE "File ${file_path} did not contain any doctests")
			endif()
		else()
			message(WARNING "Failed to add doctests from file ${file_path}: File does not exist")
		endif()
	endforeach()
endfunction()
