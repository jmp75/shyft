from .utility import create_hydro_power_system
from shyft.energy_market.core import HydroPowerSystem,HydroPowerSystemBuilder
from shyft.energy_market.core import Model, ModelBuilder,Gate
import pytest

class TestWithHydroPowerSystem:
    def test_can_create_hps(self):
        a = create_hydro_power_system(hps_id=1, name='hps')
        assert a is not None
        assert len(a.reservoirs) >0
        assert a.reservoirs[0].hps is not None
        assert a.reservoirs[0].hps.name == a.name
        assert a.units[0].power_plant
        mb = ModelBuilder(Model(1, "m1"))
        ma = mb.create_model_area(1, "a")
        ma.detailed_hydro = a
        assert ma.detailed_hydro is not None

    def test_robust_add_unit(self):
        s1=HydroPowerSystem(1,'s1')
        s2=HydroPowerSystem(2,'s2')
        pp=s1.create_power_plant(1,'pp')
        u2 =s2.create_unit(2,'u2')
        u1= s1.create_unit(1,'u1')
        with pytest.raises(RuntimeError): # refuse to add objects from two different hps systems
            pp.add_unit(u2)
        pp.add_unit(u1)
        with pytest.raises(RuntimeError):  # refuse to add duplicate
            pp.add_unit(u1)
        p2=s1.create_power_plant(2,'p2')
        with pytest.raises(RuntimeError):  # refuse to add unit that is already owned by other
            p2.add_unit(u1)

    def test_robust_add_gate(self):
        s1 = HydroPowerSystem(1, 's1')
        s2 = HydroPowerSystem(2, 's2')
        b1=HydroPowerSystemBuilder(s1)
        w1 = b1.create_waterway(1,'w1','xx')
        g1 = s1.create_gate(1, "Gate1", "{type:'binary'}")
        w1.add_gate(g1)
        assert g1 and g1.waterway
        with pytest.raises(RuntimeError):
            w1.add_gate(g1)


    def test_equal_structure_with_test_system(self):
        a = create_hydro_power_system(hps_id=1, name='a')
        b = create_hydro_power_system(hps_id=2, name='b')
        assert a.equal_structure(b)

    def test_equal_structure_different_ids(self):
        """Create two systems each containing a single reservoir with different names and ids.
        Check that the two systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(0, 'rsv 0')

        hps1 = HydroPowerSystem(1, 'hps 1')
        hps1.create_reservoir(1, 'rsv 1')

        assert hps0.equal_structure(hps1)

    def test_equal_structure_permuted_components(self):
        """Create two systems containing the same components, but added to the system in different order.
        Check that the two systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(0, 'rsv 0')
        hps0.create_reservoir(1, 'rsv 1')

        hps1 = HydroPowerSystem(1, 'hps 1')
        hps1.create_reservoir(1, 'rsv 1')
        hps1.create_reservoir(0, 'rsv 0')

        assert hps0.equal_structure(hps1)


    def test_equal_structure_missing_connection(self):
        """Create two systems having the same components, but only one has connected components.
        Check that the systems are not structurally equal"""

        hps0 = HydroPowerSystem(0, 'hps 0')
        hps0.create_reservoir(0, 'rsv 0')
        hps0.create_tunnel(0, 'water 0')

        hps1 = HydroPowerSystem(1, 'hps 1')
        reservoir = hps1.create_reservoir(0, 'rsv 0')
        waterway = hps1.create_tunnel(0, 'water 0')
        reservoir.output_to(waterway)

        assert not hps0.equal_structure(hps1)

    def test_equal_structure_reversed_connection(self):
        """Create two systems having the same components, but in one system a connection is reversed.
        Check that the systems are not structurally equal"""

        hps0 = HydroPowerSystem(0, 'hps 0')
        reservoir = hps0.create_reservoir(0, 'rsv 0')
        waterway = hps0.create_tunnel(0, 'water 0')
        waterway.output_to(reservoir)

        hps1 = HydroPowerSystem(1, 'hps 1')
        reservoir = hps1.create_reservoir(0, 'rsv 0')
        waterway = hps1.create_tunnel(0, 'water 0')
        reservoir.output_to(waterway)

        assert not hps0.equal_structure(hps1)

    def test_equal_structure_permuted_connections(self):
        """Create two systems having the same components and connections, but with connections added in
        a different order. Check that the systems are structurally equal."""
        hps0 = HydroPowerSystem(0, 'hps 0')
        rsv0 = hps0.create_reservoir(0, 'rsv 0')
        tun0 = hps0.create_tunnel(0, 'tun 0')
        tun1 = hps0.create_tunnel(1, 'tun 1')
        rsv0.output_to(tun0).output_to(tun1)

        hps1 = HydroPowerSystem(0, 'hps 0')
        rsv0 = hps1.create_reservoir(0, 'rsv 0')
        tun0 = hps1.create_tunnel(0, 'tun 0')
        tun1 = hps1.create_tunnel(1, 'tun 1')
        rsv0.output_to(tun1).output_to(tun0)


        assert hps0.equal_structure(hps1)

    def test_serialize_and_deserialize_xml(self):
        b = create_hydro_power_system(hps_id=1, name='hps')
        blob_of_b = b.to_blob()
        b_2 = HydroPowerSystem.from_blob(blob_of_b)
        assert b.equal_structure(b_2)

if __name__ == '__main__':
    t = TestWithHydroPowerSystem()
    t.test_equal_structure()
