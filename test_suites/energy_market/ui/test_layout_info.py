from shyft.energy_market import ui

def test_layout_info():
    li = ui.LayoutInfo(1, "name", "{misc.}")
    assert hasattr(li, "id")
    assert hasattr(li, "name")
    assert hasattr(li, "json")
    assert li.id == 1
    assert li.name == "name"
    assert li.json == "{misc.}"
