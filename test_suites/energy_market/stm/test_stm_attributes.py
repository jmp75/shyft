import pytest
from functools import reduce

from shyft.energy_market.stm import (Reservoir, HydroPowerSystem, StmSystem, t_xy, StmSystemContext, ModelState,
                                     MessageList)
from shyft.energy_market.core import Point, XyPointCurve,XyPointCurveWithZ, PointList
from shyft.energy_market.stm.utilities import create_t_turbine_description
from shyft.time_series import (TimeSeries,TimeAxis,time,point_interpretation_policy as pfx)

# noinspection PyUnresolvedReferences
from test_suites.energy_market.stm.testing_utilities import (
    fixture_reservoir_factory, fixture_unit_factory, fixture_waterway_factory,
    fixture_gate_factory, fixture_power_plant_factory, fixture_hps_factory,
)

def test_reservoir_attributes():
    hps = HydroPowerSystem(1, "Reservoir hps")
    res = hps.create_reservoir(2, "Reservoir one", "")
    assert hasattr(res, "volume_level_mapping")
    assert hasattr(res, "endpoint_desc")

    assert hasattr(res, "level")
    assert hasattr(res.level, "regulation_min")
    assert hasattr(res.level, "regulation_max")
    assert hasattr(res.level, "realised")
    assert hasattr(res.level, "schedule")
    assert hasattr(res.level, "constraint")
    assert hasattr(res.level, "result")
    assert hasattr(res.level.constraint, "min")
    assert hasattr(res.level.constraint, "max")

    assert hasattr(res, "volume")
    assert hasattr(res.volume, "static_max")
    assert hasattr(res.volume, "result")
    assert hasattr(res.volume, "schedule")
    assert hasattr(res.volume, "constraint")
    assert hasattr(res.volume.constraint, "min")
    assert hasattr(res.volume.constraint, "max")
    assert hasattr(res.volume.constraint, "tactical")
    assert hasattr(res.volume.constraint.tactical, "min")
    assert hasattr(res.volume.constraint.tactical, "max")
    assert hasattr(res.volume, "slack")
    assert hasattr(res.volume.slack, "lower")
    assert hasattr(res.volume.slack, "upper")

    assert hasattr(res, "inflow")
    assert hasattr(res.inflow, "schedule")
    assert hasattr(res.inflow, "realised")
    assert hasattr(res.inflow, "result")

    assert hasattr(res, "ramping")
    assert hasattr(res.ramping, "level_up")
    assert hasattr(res.ramping, "level_down")

    assert hasattr(res, "tag")
    assert res.tag == "/HPS1/R2"


def test_aggregate_attributes():
    hps = HydroPowerSystem(1, "Aggregate hps")
    agg = hps.create_aggregate(2, "Aggregate one", "")

    assert hasattr(agg, "generator_description")
    assert hasattr(agg, "turbine_description")
    assert hasattr(agg, "unavailability")

    assert hasattr(agg, "production")
    assert hasattr(agg.production, "constraint")
    assert hasattr(agg.production.constraint, "min")
    assert hasattr(agg.production.constraint, "max")
    assert hasattr(agg.production, "schedule")
    assert hasattr(agg.production, "result")
    assert hasattr(agg.production, "static_min")
    assert hasattr(agg.production, "static_max")

    assert hasattr(agg, "discharge")
    assert hasattr(agg.discharge, "constraint")
    assert hasattr(agg.discharge.constraint, "min")
    assert hasattr(agg.discharge.constraint, "max")
    assert hasattr(agg.discharge, "schedule")
    assert hasattr(agg.discharge, "result")

    assert hasattr(agg, "cost")
    assert hasattr(agg.cost, "start")
    assert hasattr(agg.cost, "stop")

    assert hasattr(agg, "tag")
    assert agg.tag == "/HPS1/U2"


def test_power_station_attributes():
    hps = HydroPowerSystem(1, "Power station hps")
    pwr_station = hps.create_power_station(2, "Power station", "")
    assert hasattr(pwr_station, "outlet_level")
    assert hasattr(pwr_station, "mip")
    assert hasattr(pwr_station, "unavailability")

    assert hasattr(pwr_station, "production")
    assert hasattr(pwr_station.production, "constraint_min")
    assert hasattr(pwr_station.production, "constraint_max")
    assert hasattr(pwr_station.production, "schedule")

    assert hasattr(pwr_station, "discharge")
    assert hasattr(pwr_station.discharge, "constraint_min")
    assert hasattr(pwr_station.discharge, "constraint_max")
    assert hasattr(pwr_station.discharge, "schedule")

    assert hasattr(pwr_station, "tag")
    assert pwr_station.tag == "/HPS1/P2"


def test_water_route_attributes():
    hps = HydroPowerSystem(1, "Water route hps")
    wtr_route = hps.create_water_route(2, "Water route", "")
    assert hasattr(wtr_route, "head_loss_coeff")
    assert hasattr(wtr_route, "head_loss_func")

    assert hasattr(wtr_route, "geometry")
    assert hasattr(wtr_route.geometry, "length")
    assert hasattr(wtr_route.geometry, "diameter")
    assert hasattr(wtr_route.geometry, "z0")
    assert hasattr(wtr_route.geometry, "z1")

    assert hasattr(wtr_route, "discharge")
    assert hasattr(wtr_route.discharge, "static_max")
    assert hasattr(wtr_route.discharge, "result")

    assert hasattr(wtr_route, "tag")
    assert wtr_route.tag == "/HPS1/W2"


def test_gate_attributes():
    hps = HydroPowerSystem(1, "Water route hps")
    gate = hps.create_water_route(2, "Water route", "").add_gate(3, "Gate", "")
    assert hasattr(gate, "flow_description")

    assert hasattr(gate, "opening")
    assert hasattr(gate.opening, "schedule")

    assert hasattr(gate, "discharge")
    assert hasattr(gate.discharge, "schedule")
    assert hasattr(gate.discharge, "result")
    assert hasattr(gate.discharge, "static_max")

    assert hasattr(gate, "tag")
    assert gate.tag == "/HPS1/G3"


def test_stm_sys_attributes():
    sys = StmSystem(1, "A", "{misc}")
    assert hasattr(sys, "run_parameters")
    rp = sys.run_parameters
    assert hasattr(rp, "n_inc_runs")
    assert hasattr(rp, "n_full_runs")
    assert hasattr(rp, "head_opt")
    assert hasattr(rp, "run_time_axis")
    assert hasattr(rp, "fx_log")


def test_t_xy_call():
    """Test that ``t_xy.__call__`` returns the latest value available at the given time."""
    xy = t_xy()
    xy[2] = XyPointCurve([Point(0.0, 0.0)])
    xy[1] = XyPointCurve([Point(1.0, 1.0)])
    xy[3] = XyPointCurve([Point(2.0, 2.0)])

    assert xy(0.5) == None
    assert xy(1.5) == xy[1]
    assert xy(2.5) == xy[2] == xy(2)
    assert xy(3.5) == xy[3]


def test_proxy_attribute_url():
    """Test the various versions of a proxy-attribute's url-funciton"""
    hps = HydroPowerSystem(1, "url hps")
    rsvx = hps.create_reservoir(2, "reservoir", "")
    rsv=hps.reservoirs[0]
    assert rsv.inflow.schedule.url() == "/HPS1/R2.inflow.schedule"

    assert rsv.inflow.schedule.url("", 0,-1) == ".inflow.schedule"
    assert rsv.inflow.schedule.url("", 0, 0) == ".${attr_id}"
    assert rsv.inflow.schedule.url("", 0, 1) == ".inflow.schedule"

    assert rsv.inflow.schedule.url("", 1) == "/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 1, 0) == "/R${rsv_id}.${attr_id}"
    assert rsv.inflow.schedule.url("", 1, 1) == "/R${rsv_id}.inflow.schedule"
    assert rsv.inflow.schedule.url("", 1, 2) == "/R2.inflow.schedule"

    assert rsv.inflow.schedule.url("", 2) == "/HPS1/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 0) == "/HPS${hps_id}/R${rsv_id}.${attr_id}"
    assert rsv.inflow.schedule.url("", 2, 1) == "/HPS${hps_id}/R${rsv_id}.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 2) == "/HPS${hps_id}/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 3) == "/HPS1/R2.inflow.schedule"

def test_proxy_attribute_unit_url():
    """Test the various versions of a proxy-attribute's url-funciton"""
    hps = HydroPowerSystem(1, "url hps")
    u = hps.create_unit(2, "unit", "")

    assert u.unavailability.url() == "/HPS1/U2.unavailability"

    assert u.unavailability.url("", 0) == ".unavailability"
    assert u.unavailability.url("", 0, 0) == ".${attr_id}"
    assert u.unavailability.url("", 0, 1) == ".unavailability"

    assert u.unavailability.url("", 1) == "/U2.unavailability"
    assert u.unavailability.url("", 1, 0) == "/U${unit_id}.${attr_id}"
    assert u.unavailability.url("", 1, 1) == "/U${unit_id}.unavailability"
    assert u.unavailability.url("", 1, 2) == "/U2.unavailability"

    assert u.unavailability.url("", 2) == "/HPS1/U2.unavailability"
    assert u.unavailability.url("", 2, 0) == "/HPS${hps_id}/U${unit_id}.${attr_id}"
    assert u.unavailability.url("", 2, 1) == "/HPS${hps_id}/U${unit_id}.unavailability"
    assert u.unavailability.url("", 2, 2) == "/HPS${hps_id}/U2.unavailability"
    assert u.unavailability.url("", 2, 3) == "/HPS1/U2.unavailability"


def test_proxy_attribute_unit_fx():
    """Test the various versions of a proxy-attribute's url-function"""
    hps = HydroPowerSystem(1, "url hps")
    u = hps.create_unit(2, "u1", "")

    assert not u.unavailability.exists
    a = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    b = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    c = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [0, 1, 0], pfx.POINT_AVERAGE_VALUE)
    assert a == b
    u.unavailability = a  # assign directly to the proxy (conversion goes magic between proxy<T>->T)
    assert u.unavailability.exists  # verify it's there
    u.unavailability.remove()
    assert not u.unavailability.exists  # verify it's gone
    u.unavailability.value = a  # ensure we can assign to value  by ref
    assert u.unavailability.exists  # verify it's there again
    astr = str(u.unavailability)
    v = hps.create_unit(3,"u2")
    assert v.unavailability != u.unavailability  # ensure it compares not equal by value
    v.unavailability=b
    assert v.unavailability == u.unavailability  # ensure it compares equal by value
    u.unavailability.remove()  # remove it
    assert not u.unavailability.exists  # verify it's not there any more
    v.unavailability.remove()
    assert v.unavailability == u.unavailability  # ensure it compares equal by value(null== null) in this case
    u.unavailability = a
    assert u.unavailability == b  # now compare a ts to a proxy<ts>
    assert u.unavailability != c  # now compare a ts to a proxy<ts>
    # test time-dependent turbine-description at main object.
    td= u.turbine_description
    assert not td.exists
    t0=time('2020-12-06T00:00:00Z')
    u.turbine_description= create_t_turbine_description(t0,
        [XyPointCurveWithZ(
            XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])),
            400.0)
        ]
    )
    assert u.turbine_description.exists
    td=u.turbine_description
    u.turbine_description.remove()
    assert not u.turbine_description.exists
    # test TimeSeries attribute in a nested Unit._Production.result
    upr= u.production.result
    assert not upr.exists
    u.production.result = a
    assert u.production.result.exists
    upr_url= u.production.result.url()
    assert upr_url == '/HPS1/U2.production.result'


def test_stm_run():
    mdl = StmSystem(1, "A", "{misc}")
    ctx = StmSystemContext(ModelState.IDLE, mdl)
    assert not ctx.system.run_parameters.fx_log.exists
    ctx.message("test message")
    assert ctx.system.run_parameters.fx_log.exists
    assert len(ctx.system.run_parameters.fx_log.value) == 1
    assert ctx.system.run_parameters.fx_log.value[0].message == "test message"


def test_stm_gate_idents_unique():
    """Test that adding that gate id uniqueness is enforced"""
    hps = HydroPowerSystem(0, "test")
    ww0 = hps.create_waterway(0, "ww0")
    ww1 = hps.create_waterway(1, "ww1")

    _ = ww0.add_gate(0, "gate_0")
    _ = ww1.add_gate(1, "gate_1")

    with pytest.raises(RuntimeError, match="Gate id must be unique"):
        _ = ww1.add_gate(0, "gate_3")


def test_stm_gate_names_unique():
    """Test that adding that gate name uniqueness is enforced"""
    hps = HydroPowerSystem(0, "test")
    ww0 = hps.create_waterway(0, "ww0")
    ww1 = hps.create_waterway(1, "ww1")

    _ = ww0.add_gate(0, "gate_0")
    _ = ww1.add_gate(1, "gate_1")

    with pytest.raises(RuntimeError, match="Gate name must be unique"):
        _ = ww1.add_gate(2, "gate_0")


# noinspection DuplicatedCode
class TestFlattenedAttributes:
    def test_unit_attributes_flattened(self, unit_factory):
        """Test that 'unit.flattened_attributes' has correct paths and attributes."""
        unit = unit_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_reservoir_attributes_flattened(self, reservoir_factory):
        """Test that 'reservoir.flattened_attributes' has correct paths and attributes."""
        unit = reservoir_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_waterway_attributes_flattened(self, waterway_factory):
        """Test that 'waterway.flattened_attributes' has correct paths and attributes."""
        unit = waterway_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_gate_attributes_flattened(self, gate_factory):
        """Test that 'gate.flattened_attributes' has correct paths and attributes."""
        unit = gate_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_power_plant_attributes_flattened(self, power_plant_factory):
        """Test that 'power_plant.flattened_attributes' has correct paths and attributes."""
        unit = power_plant_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()
