from .models import create_test_hydro_power_system
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, StmSystemContext, ModelState, SharedMutex
from shyft.energy_market.stm import t_xy
from shyft.energy_market.core import Point, PointList, XyPointCurve
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE


def test_can_create_hps():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)


# TODO: .. important for validity of other tests.
# def test_equal_structure():
#    pass  # not started yet
# a = create_hydro_power_system(hps_id=1, name='a')
# b = create_hydro_power_system(hps_id=2, name='b')
# assert a.equal_structure(b)


def test_serialize_and_deserialize_blob():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)


def test_can_set_rsv_storage_description():
    a = HydroPowerSystem(1, 'a')
    r = a.create_reservoir(1, "x", "json{'a':1}")
    # reservoirs[0] is now in python
    #       equivalent to .find_reservoir_by_id(1)..
    #a_r= a.find_reservoir_by_id(1)
    a_r= a.reservoirs[0]
    assert not a_r.volume_level_mapping.exists, 'no storage_description set yet'
    v = t_xy()
    v[time('2018-10-17T00:00:00Z')] = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    a_r.volume_level_mapping = v
    assert a_r.volume_level_mapping.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    assert b.reservoirs
    b_r = b.find_reservoir_by_id(1)
    assert b_r
    assert b_r.volume_level_mapping
    assert b_r.volume_level_mapping.exists
    # TODO: Equality shall work, robust, to 2 x eps or similar
    # assert b_r.volume_level_mapping == a_r.volume_level_mapping


def test_rsv_plan_mm3():
    a = HydroPowerSystem(1, 'a')
    r = a.create_reservoir(1, "x", "json{'a':1}")
    assert not r.level.schedule.exists, 'no level_schedule set yet'
    r.level.schedule = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_INSTANT_VALUE)
    assert r.level.schedule.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    b_r = b.reservoirs[0]
    assert b_r.level.schedule.exists


def test_can_create_stm_system():
    a = StmSystem(1, "A", "json{}")
    assert a
    assert a.id == 1
    assert a.name == 'A'
    assert a.json == 'json{}'
    assert len(a.hydro_power_systems) == 0
    assert len(a.market_areas) == 0


def test_can_blobify_stm_system():
    """ verify serialization roundtrip for blob"""
    a = StmSystem(1, "A", "json{}")
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.id == a.id
    assert b.name == a.name
    assert b.json == a.json
    assert len(b.hydro_power_systems) == len(a.hydro_power_systems)
    assert len(b.market_areas) == len(a.market_areas)


def test_stm_system_proxy_attributes():
    """ Verify the proxy attributes in StmSystem"""
    a = StmSystem(1, "A", "{misc}")
    rp = a.run_parameters
    assert rp.n_inc_runs == 0
    assert not rp.run_time_axis.exists
    rp.n_inc_runs = 2
    rp.run_time_axis = TimeAxis(time('2018-01-01T00:00:00Z'), time(3600), 240)
    assert rp.n_inc_runs == 2
    assert rp.run_time_axis.exists
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.run_parameters.n_inc_runs == rp.n_inc_runs
    assert b.run_parameters.run_time_axis == rp.run_time_axis


def test_hps_can_find_by_name():
    s = create_test_hydro_power_system(hps_id=1, name='hps')
    w=s.find_waterway_by_id(1)
    assert w and 'saurdal' in w.name
    g1 = w.add_gate(11,'tunx_g1') # we can add gate to a model
    assert g1
    assert not g1.opening.schedule.exists, 'check that we got stm gate'
    p=s.find_power_plant_by_id(1)
    r=s.find_reservoir_by_id(1)
    u=s.find_unit_by_id(165061)
    assert p and r and u
    assert p.id == s.find_power_plant_by_name(p.name).id
    assert r.id == s.find_reservoir_by_name(r.name).id
    assert u.id == s.find_unit_by_name(u.name).id
    # ensure we got real stm objects back
    assert not p.outlet_level.exists
    assert not r.level.regulation_max.exists
    assert not u.production.result.exists
    assert not w.discharge.result.exists


def test_stm_system_context():
    s = StmSystem(1, "A", "")
    ctx = StmSystemContext(ModelState.IDLE, s)
    assert hasattr(ctx, "state")
    assert ctx.state == ModelState.IDLE
    assert hasattr(ctx, "system")
    assert s.name == ctx.system.name
    assert hasattr(ctx, "mutex")
    assert isinstance(ctx.mutex, SharedMutex)


# def test_attribute_group_keep_alive():
#     """Test that the attribute data is kept alive while the Python attribute group object exists"""
#     lvl = create_test_hydro_power_system(hps_id=1, name='hps').reservoirs[0].level
#     assert lvl.regulation_min.exists


def test_attribute_keep_alive():
    """Test that the attribute data is kept alive while the Python attribute object exists"""
    # a = create_test_hydro_power_system(hps_id=1, name='hps')
    # lrl = a.reservoirs[0].level.regulation_min

    rsv = create_test_hydro_power_system(hps_id=1, name='hps').reservoirs[0]
    pass
