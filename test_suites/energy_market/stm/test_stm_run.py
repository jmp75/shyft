from shyft.energy_market.stm import StmModelRef, ModelRefList, StmRun, StmSession
from shyft import time_series as sa


def test_model_ref():
    ri = StmModelRef("host", 123, 456, "key")
    assert ri == StmModelRef("host", 123, 456, "key")
    assert ri != StmModelRef("guest", 123, 456, "key")

    assert hasattr(ri, "host")
    assert ri.host == "host"
    assert hasattr(ri, "port_num")
    assert ri.port_num == 123
    assert ri.api_port_num == 456
    assert hasattr(ri, "model_key")
    assert ri.model_key == "key"

    ris = ModelRefList()
    assert len(ris) == 0
    ris.append(ri)
    assert len(ris) == 1
    assert ris[0] == ri


def test_stm_run():
    def verify_stm_run(run: StmRun):
        assert hasattr(run, "id")
        assert hasattr(run, "name")
        assert hasattr(run, "created")
        assert hasattr(run, "json")
        assert hasattr(run, "labels")
        assert hasattr(run, "model_refs")
    # 1. Default run:
    run1 = StmRun()
    verify_stm_run(run1)
    assert run1.id == 0
    assert run1.name == ""
    assert run1.json == ""
    assert run1.created == sa.no_utctime
    assert len(run1.labels) == 0
    assert len(run1.model_refs) == 0

    # 2. Add a model reference:
    mr = StmModelRef("host", 12, 34, "testkey")
    run1.add_model_ref(mr)
    assert len(run1.model_refs) == 1
    assert run1.model_refs[0] == mr
    assert run1.get_model_ref("testkey") == mr
    assert run1.get_model_ref("nonkey") is None
    assert not run1.remove_model_ref("nonkey")
    assert run1.remove_model_ref("testkey")
    assert len(run1.model_refs) == 0

    # 2. Constructor with default parameters:
    t0 = sa.utctime_now()
    run2 = StmRun(2, "testrun", t0)
    verify_stm_run(run2)
    assert run2.id == 2
    assert run2.name == "testrun"
    assert run2.created == t0
    assert len(run2.labels) == 0
    assert len(run2.model_refs) == 0
    run2.add_model_ref(StmModelRef())
    run2.add_model_ref(StmModelRef("host", 12, 34, "key"))
    assert len(run2.model_refs) == 2

    # 3. Constructor with non-default parameters:
    model_refs = ModelRefList()
    model_refs.append(StmModelRef())
    model_refs.append(StmModelRef("host", 123, 456, "key"))
    run3 = StmRun(3, "testrun2", t0, json="misc.", labels=["Tag1", "Tag2"], model_refs=model_refs)
    verify_stm_run(run3)
    assert run3.id == 3
    assert run3.name == "testrun2"
    assert run3.created == t0
    assert run3.json == "misc."
    assert len(run3.labels) == 2
    assert run3.labels[0] == "Tag1"
    assert run3.labels[1] == "Tag2"
    assert len(run3.model_refs) == 2
    assert run3.model_refs[0] == StmModelRef()
    assert run3.model_refs[1] == StmModelRef("host", 123, 456, "key")
    assert run3 != StmRun(3, "testrun2", t0, json="misc.")


def test_stm_session():
    # 0. Create a session:
    session = StmSession(1, "testsession", sa.utctime_now())
    assert hasattr(session, "id")
    assert hasattr(session, "name")
    assert hasattr(session, "created")
    assert hasattr(session, "json")
    assert hasattr(session, "labels")
    assert hasattr(session, "runs")
    assert hasattr(session, "base_model")
    assert hasattr(session, "task_name")

    assert session.id == 1
    assert session.name == "testsession"
    assert session.json == ""
    assert len(session.labels) == 0
    assert len(session.runs) == 0
    assert isinstance(session.base_model, StmModelRef)
    assert session.task_name == ""

    # We add a run:
    run = StmRun(2, "testrun", sa.utctime_now())
    session.add_run(run)
    assert len(session.runs) == 1
    assert run == session.runs[0]
    run2 = StmRun(3, "testrun2", sa.utctime_now())
    session.add_run(run2)
    assert len(session.runs) == 2
    assert run2 == session.runs[1]

    # Get run:
    assert run == session.get_run(2)
    assert session.get_run(4) is None
    assert run2 == session.get_run("testrun2")
    assert session.get_run("norun") is None

    # Remove runs using id or name:
    assert session.remove_run(2)
    assert len(session.runs) == 1
    assert not session.remove_run(2)
    assert session.remove_run("testrun2")
    assert len(session.runs) == 0
    assert not session.remove_run("not a run")

