from time import sleep

from shyft.energy_market.stm import DStmClient, DStmServer,ModelState
from shyft.time_series import Calendar, TimeAxis, time, deltahours
from tempfile import TemporaryDirectory
import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
import shutil

if not shop.shyft_with_shop():
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r

def test_remote_shop_logging(port_no, web_api_port, system_to_optimize):
    assert port_no != web_api_port
    td = TemporaryDirectory() # TODO, when shop properly releases its file handle, replace with: with TemporaryDirectory() as td:
    srv = DStmServer(td.name)
    srv.set_listening_port(port_no)
    srv.start_server()
    # Add a
    mid="optimize"
    srv.do_add_model(mid, system_to_optimize)
    host = "127.0.0.1"
    client = DStmClient(f"{host}:{port_no}", 1000)
    try:
        assert len(client.get_model_ids()) == 1
        # optimize model remotely:
        t_begin = time('2018-10-17T10:00:00Z')
        t_end = time('2018-10-18T10:00:00Z')
        t_step = deltahours(1)
        n_steps = (t_end - t_begin) / t_step
        ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)
        shop_commands = create_test_optimization_commands(1, False)
        assert client.get_log(mid) == "Unable to find log."
        assert client.optimize(mid, ta, shop_commands)
        while client.get_state(mid) != ModelState.IDLE:  # poll until it's done (we resolve this later!)
            sleep(0.2)
        assert client.get_model(mid) # This will not wait until optimization is complete
        assert len(client.get_log(mid)) > 200
    finally:
        client.close()
        srv.close()
    # TODO: Remove below here when reintroducing use of "with TemporaryDirectory() as td:"
    try:
        shutil.rmtree(td.name.name)
    except:
        print(f"WARNING: Failed to delete temporary directory \"{td.name}\"")


