import pytest
from tempfile import TemporaryDirectory
import sys

websockets = pytest.importorskip("websockets")
import asyncio
import socket
from shyft.energy_market import stm
from shyft import time_series as sa

if sys.version_info < (3, 7):
    pytest.skip("requires Python3.7 or higher", allow_module_level=True)


def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"


def get_response(req: str, port_no: int):
    async def wrap(wreq):
        async with websockets.connect(uri(port_no)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(wrap(req))


def test_web_api(port_no, web_api_port):
    import time
    # ensure there is an event loop for this thread
    loop = asyncio.new_event_loop()
    with TemporaryDirectory() as root_dir:
        srv = stm.StmRunServer(str(root_dir))
        srv.set_listening_port(port_no)
        port = srv.start_server()
        web_api_port
        c = stm.StmRunClient(host_port=f"localhost:{port}", timeout_ms=1000)
        assert srv
        assert c
        # Store a simple session
        session = stm.StmSession(1, "s1", sa.time(10))
        session.labels.append("test")
        session.labels.append("web")
        mi = stm.ModelInfo(1, "s1", session.created)
        c.store_model(session, mi)
        # Start web API:
        host = "127.0.0.1"
        srv.start_web_api(host, web_api_port, str(root_dir + "/web"), 1, 1)
        time.sleep(0.5)
        qa_tuples = [
            ("""get_model_infos {"request_id": "1"}""", """{"request_id":"1","result":[{"id":1,"name":"s1","created":10.0,"json":""}]}"""),
            ("""read_model {"request_id": "2", "model_id": 1}""", """{"request_id":"2","result":{"id":1,"name":"s1","created":10.0,"json":"","labels":["test","web"],"runs":[],"base_model":{"host":"","port_num":-1,"api_port_num":-1,"model_key":""},"task_name":""}}""")
        ]
        try:
            for qa in qa_tuples:
                response = get_response(qa[0], web_api_port)
                assert response == qa[1], 'expected response'
        finally:
            srv.stop_web_api()

