from shyft.time_series import Calendar, deltahours, TimeAxis, TimeSeries, point_interpretation_policy, integral
import math


def test_integral():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    fill_value = 1.0
    ts = TimeSeries(ta=ta, fill_value=fill_value, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')*1.0 + 0.0  # expression, needing bind
    tsb = TimeSeries('b')*1.0 + 0.0  # another expression, needing bind for different ts
    ts_i1 = tsa.integral(ta)
    ts_i2 = integral(tsb, ta)
    # circulate through serialization
    ts_i1_blob = ts_i1.serialize()
    ts_i2_blob = ts_i2.serialize()
    ts_i1 = TimeSeries.deserialize(ts_i1_blob)
    ts_i2 = TimeSeries.deserialize(ts_i2_blob)

    for ts_i in [ts_i1, ts_i2]:
        assert ts_i.needs_bind()
        tsb = ts_i.find_ts_bind_info()
        assert len(tsb) == 1
        tsb[0].ts.bind(ts)
        ts_i.bind_done()
        assert not ts_i.needs_bind()

    ts_i1_values = ts_i1.values
    for i in range(n):
        expected_value = float(dt*fill_value)
        assert round(abs(expected_value - ts_i1.value(i)), 4) == 0, "expect integral of each interval"
        assert round(abs(expected_value - ts_i2.value(i)), 4) == 0, "expect integral of each interval"
        assert round(abs(expected_value - ts_i1_values[i]), 4) == 0, "expect integral of each interval"


def test_integral_fine_resolution():
    """ Case study for last-interval bug from python"""
    utc = Calendar()
    ta = TimeAxis(utc.time(2017, 10, 16), deltahours(24*7), 219)
    tf = TimeAxis(utc.time(2017, 10, 16), deltahours(3), 12264)
    src = TimeSeries(ta, fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    ts = src.integral(tf)
    assert ts is not None
    for i in range(len(tf)):
        if not math.isclose(ts.value(i), 1.0*deltahours(3)):
            assert round(abs(ts.value(i) - 1.0*deltahours(3)), 7) == 0
