import numpy as np
from shyft.time_series import IcePackingRecessionParameters, Calendar, deltaminutes, deltahours, TimeAxis, TimeSeries, POINT_INSTANT_VALUE, IcePackingParameters, ice_packing_temperature_policy, utctime_now, POINT_AVERAGE_VALUE


def test_ice_packing_ts_parameters():
    p = IcePackingParameters(3600*24*7, -15.0)
    assert p.threshold_window == 3600*24*7
    assert round(abs(p.threshold_temperature - -15.0), 7) == 0
    q = IcePackingParameters(threshold_temperature=-10.2, threshold_window=3600*24*10)
    assert q.threshold_window == 3600*24*10
    assert round(abs(q.threshold_temperature - -10.2), 7) == 0
    assert not (q == p)
    assert p == p
    assert p != q
    q.threshold_temperature = p.threshold_temperature
    q.threshold_window = p.threshold_window
    assert p == q
    assert not (p != q)


def test_ice_packing_recession_parameters():
    iprp = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert round(abs(iprp.alpha - 0.0001), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25), 7) == 0
    iprp.alpha *= 2
    iprp.recession_minimum *= 4
    assert round(abs(iprp.alpha - 0.0001*2), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25*4), 7) == 0
    iprp2 = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert iprp != iprp2
    assert not (iprp == iprp2)
    assert iprp == iprp
    assert not (iprp != iprp)
    assert iprp != iprp2


def test_ice_packing_ts():
    t0 = Calendar().time(2018, 1, 1)

    ts_dt = deltaminutes(30)
    window = deltahours(1)

    ta = TimeAxis(t0, ts_dt, 5*2)
    data = np.linspace(-5, 5, len(ta))
    data[len(ta)//2] = float('nan')  # introduce missing value
    ts = TimeSeries(ta, data, POINT_INSTANT_VALUE)  # notice that this implicate linear between point
    ipp = IcePackingParameters(threshold_window=window, threshold_temperature=0.0)
    source = TimeSeries("a")
    ip_source = source.ice_packing(ipp, ice_packing_temperature_policy.ALLOW_INITIAL_MISSING)

    ip_source_blob = ip_source.serialize()
    ip_source_2 = TimeSeries.deserialize(ip_source_blob)

    assert ip_source_2.needs_bind()
    fbi = ip_source_2.find_ts_bind_info()
    assert len(fbi) == 1
    fbi[0].ts.bind(ts)
    ip_source_2.bind_done()
    assert not ip_source_2.needs_bind()

    assert len(ip_source_2) == len(ts)
    assert ip_source_2.time_axis == ts.time_axis  # time-axis support equality
    expected = np.array([0, 1, 1, 1, 1, float('nan'), float('nan'), float('nan'), 0, 0], dtype=np.float64)
    assert np.allclose(expected, ip_source_2.values.to_numpy(), equal_nan=True)


def test_ice_packing_recession_ts():
    t0 = utctime_now()

    ts_dt = deltaminutes(30)
    window = deltahours(2)

    ta = TimeAxis(t0, ts_dt, 48*2 + 1)
    # flow data
    x0 = int(ta.total_period().start)
    x1 = int(ta.total_period().end)
    x = np.linspace(x0, x1, len(ta))
    flow_data = -0.0000000015*(x - x0)*(x - x1) + 1
    flow_ts = TimeSeries(ta, flow_data, POINT_AVERAGE_VALUE)
    # temperature data
    temperature_data = np.linspace(-5, 5, ta.size())
    temperature_data[48] = float('nan')  # introduce missing value
    temp_ts = TimeSeries(ta, temperature_data, POINT_INSTANT_VALUE)

    ipp = IcePackingParameters(threshold_window=window, threshold_temperature=0.0)
    iprp = IcePackingRecessionParameters(alpha=0.0001, recession_minimum=0.25)
    assert round(abs(iprp.alpha - 0.0001), 7) == 0
    assert round(abs(iprp.recession_minimum - 0.25), 7) == 0

    # unbound data sources
    source_t = TimeSeries("temperature")
    source_f = TimeSeries("flow")

    # unbound computed series
    ip_ts_ub = source_t.ice_packing(ipp, ice_packing_temperature_policy.ALLOW_ANY_MISSING)
    ipr_ts_ub = source_f.ice_packing_recession(ip_ts_ub, iprp)

    # serialize and deserialize
    ip_blob = ip_ts_ub.serialize()
    ip_ts = TimeSeries.deserialize(ip_blob)
    # -----
    ipr_blob = ipr_ts_ub.serialize()
    ipr_ts = TimeSeries.deserialize(ipr_blob)

    # bind the deserialized series
    assert ip_ts.needs_bind()
    assert ipr_ts.needs_bind()
    # -----
    fbi_ip = ip_ts.find_ts_bind_info()
    assert len(fbi_ip) == 1
    assert fbi_ip[0].id == 'temperature'
    fbi_ip[0].ts.bind(temp_ts)
    # -----
    ip_ts.bind_done()
    assert not ip_ts.needs_bind()
    # -----
    fbi_ipr = ipr_ts.find_ts_bind_info()
    assert len(fbi_ipr) == 2
    for i in range(len(fbi_ipr)):
        if fbi_ipr[i].id == 'flow':
            fbi_ipr[i].ts.bind(flow_ts)
        elif fbi_ipr[i].id == 'temperature':
            fbi_ipr[i].ts.bind(temp_ts)
    # -----
    ipr_ts.bind_done()
    assert not ipr_ts.needs_bind()

    prev = None
    assert len(ipr_ts) == len(flow_ts)
    for i in range(ipr_ts.size()):
        ice_packing = (ip_ts.value(i) == 1.)
        value = ipr_ts.value(i)
        if ice_packing:
            if prev is not None:
                assert value <= prev  # assert monotonic decreasing
            prev = value
        else:
            assert value == flow_ts.value(i)
    #
    #     u.assertEqual(ip_source_2.get(i).t, ts.get(i).t)
    #     if math.isnan(expected):
    #         u.assertTrue(math.isnan(ip_source_2.get(i).v))
    #     else:
    #         u.assertEqual(ip_source_2.get(i).v, expected)
