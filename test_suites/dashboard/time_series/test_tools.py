import pytest

from bokeh.plotting import figure as bokeh_figure
from bokeh.layouts import row
from bokeh.models import Range1d, WheelZoomTool
from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.tools.figure_tools import (ResetYRange, TimePeriodSelectorInFigure, WheelZoomDirection,
                                                            ActiveScroll, FigureToolError, Modes, Actions)
from shyft.dashboard.time_series.tools.view_time_axis_tools import (ViewPeriodSelector, DeltaTSelectorTool,
                                                                    )
from shyft.dashboard.time_series.tools.ts_viewer_tools import ResetTool
from shyft.dashboard.time_series.view_container.figure import Figure
import shyft.time_series as sa

# ----------------------------------------
# FIGURE TOOLS


def test_reset_y_range(mock_parent):
    reset_y_range = ResetYRange()
    reset_y_range.bind(parent=mock_parent)
    assert mock_parent.count_update_y_range == 0
    reset_y_range.on_click()
    assert mock_parent.count_update_y_range == 1
    # test state handling
    reset_y_range.state_port.receive_state(States.ACTIVE)
    reset_y_range.state_port.receive_state(States.DEACTIVE)
    assert reset_y_range._state == States.DEACTIVE

    reset_y_range.on_click()
    assert mock_parent.count_update_y_range == 1

    row(reset_y_range.layout)
    reset_y_range.layout_components


def test_wheel_zoom_direction():
    class Parent:
        def __init__(self):
            self.bokeh_figure = bokeh_figure(plot_width=100, plot_height=100, x_axis_type="datetime",
                                             toolbar_location=None,
                                             x_range=Range1d(start=0, end=100000, min_interval=1, max_interval=100),
                                             y_range=Range1d(1, 2))
            self.wheel_zoom = WheelZoomTool(dimensions="width")

    parent = Parent()
    wheel_zoom_direction = WheelZoomDirection()

    row(wheel_zoom_direction.layout)
    wheel_zoom_direction.layout_components

    wheel_zoom_direction.state_port.receive_state(States.ACTIVE)
    assert wheel_zoom_direction.scroll_button.active == 0
    wheel_zoom_direction.on_bind(parent=parent)
    assert wheel_zoom_direction.wheel_zooms
    wheel_zoom_direction.scroll_button.disabled = False

    wheel_zoom_direction.set_active_scroll(active_scroll=ActiveScroll.Y_axis)
    assert wheel_zoom_direction.wheel_zooms[0].dimensions == "height"
    wheel_zoom_direction.set_scroll_button_active(1)

    # test button clicks
    wheel_zoom_direction.scroll_button.active = 0
    assert wheel_zoom_direction.wheel_zooms[0].dimensions == "width"
    wheel_zoom_direction.scroll_button.active = 1
    assert wheel_zoom_direction.wheel_zooms[0].dimensions == "height"

    # test if deactive policy is working, resetting it to old state
    wheel_zoom_direction.state_port.receive_state(States.DEACTIVE)
    wheel_zoom_direction.scroll_button.active = 0
    assert wheel_zoom_direction.scroll_button.active == 1
    assert wheel_zoom_direction.wheel_zooms[0].dimensions == "height"
    wheel_zoom_direction.state_port.receive_state(States.ACTIVE)

    # test set_active_scroll
    wheel_zoom_direction.set_active_scroll(active_scroll=5)
    assert wheel_zoom_direction.bokeh_figures[0].toolbar.active_scroll is None

    # check buttonbehaviour if disabled
    wheel_zoom_direction.scroll_button.disabled = True
    wheel_zoom_direction.change_active_scroll(attr="", old=0, new=1)
    assert wheel_zoom_direction.scroll_button.active is None

    # test init
    with pytest.raises(FigureToolError):
        wheel_zoom_direction.on_bind(parent=parent)

    with pytest.raises(FigureToolError):
        parent2 = Parent()
        parent2.wheel_zoom = parent.wheel_zoom
        wheel_zoom_direction.on_bind(parent=parent2)


def test_time_period_selector(mock_parent):

    class PanEvent:
        def __init__(self, x):
            self.x = x

    class ParentParent:
        def connect_to_dt_selector(self, dt_selector):
            pass

    class Parent:
        def __init__(self):
            self.bokeh_figure = bokeh_figure(plot_width=100, plot_height=100, x_axis_type="datetime",
                                             toolbar_location=None, x_range=Range1d(start=0, end=100000, min_interval=1,
                                                                                    max_interval=100),
                                             y_range=Range1d(1, 2))
            self.parent = ParentParent()

    # test bind working
    time_period_selector = TimePeriodSelectorInFigure()
    parent = Parent()
    time_period_selector.on_bind(parent=parent)
    # should be a quad renderer
    assert time_period_selector.quad_renderer
    row(time_period_selector.layout)
    time_period_selector.layout_components

    assert time_period_selector.bottom == 0
    assert time_period_selector.top == 10

    # already bound to a figure
    with pytest.raises(FigureToolError):
        parent2 = Parent()
        time_period_selector.on_bind(parent=parent2)

    # figure with no time axis
    with pytest.raises(FigureToolError):
        parent2 = Parent()
        parent2.bokeh_figure = bokeh_figure(plot_width=100, plot_height=100,
                                             toolbar_location=None, x_range=Range1d(start=0, end=100000, min_interval=1,
                                                                                    max_interval=100),
                                             y_range=Range1d(1, 2))
        time_period_selector2 = TimePeriodSelectorInFigure()
        time_period_selector2.on_bind(parent=parent2)

    # test visibility
    time_period_selector.receive_visibility(True)
    time_period_selector.receive_visibility(False)
    assert not time_period_selector.visible
    assert not time_period_selector.quad_renderer.visible

    # change modes without snap_to_dt should end up in free range
    time_period_selector.mode_pres.set_selector_value(Modes.snap_to_dt, callback=True)
    assert time_period_selector.mode == Modes.free_range
    time_period_selector.mode_pres.set_selector_value(Modes.fixed_dt, callback=True)
    assert time_period_selector.mode == Modes.free_range
    time_period_selector.mode_pres.set_selector_value(Modes.free_range, callback=True)
    assert time_period_selector.mode == Modes.free_range

    time_period_selector.receive_dt(1)
    time_period_selector.mode_pres.set_selector_value(Modes.snap_to_dt, callback=True)
    assert time_period_selector.mode == Modes.snap_to_dt
    time_period_selector.receive_dt(2)
    time_period_selector.mode_pres.set_selector_value(Modes.fixed_dt, callback=True)
    assert time_period_selector.mode == Modes.fixed_dt
    time_period_selector.mode_pres.set_selector_value(Modes.free_range, callback=True)
    assert time_period_selector.mode == Modes.free_range

    # test manipulate period calls
    # test if no figure is bound
    time_period_selector2 = TimePeriodSelectorInFigure()
    time_period_selector2.manipulate_period(clicked=True)
    # should be not set to true
    assert time_period_selector2.edit_button.active == 0
    assert not time_period_selector2.visible

    time_period_selector.state_port.receive_state(States.DEACTIVE)
    time_period_selector.manipulate_period(clicked=True)
    assert time_period_selector.edit_button.active == 0
    assert not time_period_selector.visible

    time_period_selector.state_port.receive_state(States.ACTIVE)
    time_period_selector.state_port.receive_state(States.ACTIVE)
    assert time_period_selector.bottom == 1
    assert time_period_selector.top == 2

    # manipulate period
    time_period_selector.manipulate_period(clicked=True)
    assert time_period_selector.bottom == 1
    assert time_period_selector.top == 2
    assert time_period_selector.left_x == 50
    assert time_period_selector.right_x == 75
    assert time_period_selector.visible is True
    time_period_selector.manipulate_period(clicked=False)
    assert time_period_selector.visible is False

    # test on_y_range change
    time_period_selector.receive_visibility(False)
    parent.bokeh_figure.y_range.start = -10
    assert time_period_selector.bottom == 1
    time_period_selector.receive_visibility(True)
    parent.bokeh_figure.y_range.start = 0
    assert time_period_selector.bottom == 0
    parent.bokeh_figure.y_range.end = 10
    assert time_period_selector.top == 10

    # pan start callback
    time_period_selector.manipulate_period(clicked=True)
    event = PanEvent(10000)
    time_period_selector.pan_start_event_callback(event)
    assert time_period_selector.pan_start_x == 10
    assert time_period_selector.action == Actions.extend_left

    time_period_selector.manipulate_period(clicked=False)
    event = PanEvent(100000)
    time_period_selector.pan_start_event_callback(event)
    assert time_period_selector.deactivated
    assert time_period_selector.pan_start_x == 10

    time_period_selector.manipulate_period(clicked=True)
    event = PanEvent(80000)
    time_period_selector.pan_start_event_callback(event)
    assert time_period_selector.pan_start_x == 80
    assert time_period_selector.action == Actions.extend_right

    time_period_selector.manipulate_period(clicked=True)
    event = PanEvent(60000)
    time_period_selector.pan_start_event_callback(event)
    assert time_period_selector.pan_start_x == 60
    assert time_period_selector.action == Actions.move

    # TODO: missing test pan event, pan end event

# ----------------------------------------
# VIEW TIME AXIS TOOLS


def test_dt_selector_tool():
    dt_tool = DeltaTSelectorTool(title="test")
    row(dt_tool.layout)
    dt_tool.layout_components


def test_view_period_selector():

    class Parent:
        def __init__(self):
            self.view_range = sa.UtcPeriod(10000, 20000)
            self.set_periods = []

        def set_view_range(self, view_range, padding=False):
            self.set_periods.append(view_range)

    cal = sa.Calendar()
    view_period_selector = ViewPeriodSelector()
    parent = Parent()
    view_period_selector.bind(parent=parent)

    row(view_period_selector.layout)
    view_period_selector.layout_components

    # check if dt_period is set correctly
    week = view_period_selector.select_periods['Week']
    view_period_selector.period_selector_presenter.set_selector_value('Week', callback=True)
    assert view_period_selector.dt_period == week

    # test next period
    view_period_selector.jump_to_next_period()
    assert len(parent.set_periods) == 1
    assert parent.set_periods[-1] == sa.UtcPeriod(cal.add(parent.view_range.start, week, 1),
                                                 cal.add(parent.view_range.end, week, 1))

    # test previous period week
    view_period_selector.jump_to_previous_period()
    assert len(parent.set_periods) == 2
    assert parent.set_periods[-1] == sa.UtcPeriod(cal.add(parent.view_range.start, -week, 1),
                                                 cal.add(parent.view_range.end, -week, 1))

    view_period_selector.period_selector_presenter.set_selector_value('Auto', callback=True)
    assert view_period_selector.dt_period == 0

    # test next period auto
    time_span = parent.view_range.timespan()
    view_period_selector.jump_to_next_period()
    assert len(parent.set_periods) == 3
    assert parent.set_periods[-1] == sa.UtcPeriod(cal.add(parent.view_range.start, time_span, 1),
                                                 cal.add(parent.view_range.end, time_span, 1))

    # test previous period auto
    view_period_selector.jump_to_previous_period()
    assert len(parent.set_periods) == 4
    assert parent.set_periods[-1] == sa.UtcPeriod(cal.add(parent.view_range.start, time_span, -1),
                                                 cal.add(parent.view_range.end, time_span, -1))

    view_period_selector.state_port.receive_state(States.DEACTIVE)
    view_period_selector.jump_to_next_period()
    assert len(parent.set_periods) == 4
    view_period_selector.jump_to_previous_period()
    assert len(parent.set_periods) == 4


# ------------------------------------
# TS VIEWER TOOLS


def test_reset_tool():
    class Parent:
        def __init__(self):
            self.plot_count = 0

        def plot(self):
            self.plot_count += 1

    parent = Parent()
    reset_tool = ResetTool()
    reset_tool.state_port.receive_state(States.ACTIVE)
    row(reset_tool.layout)
    reset_tool.layout_components
    reset_tool.bind(parent)
    # check if plot func is called
    assert parent.plot_count == 0
    reset_tool.on_click()
    assert parent.plot_count == 1
    # deactivate and verify that the plot_count is not increasing
    reset_tool.state_port.receive_state(States.DEACTIVE)
    assert reset_tool._state == States.DEACTIVE
    reset_tool.on_click()
    assert parent.plot_count == 1

