import pytest
from shyft.time_series import UtcPeriod
import numpy as np


from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.view_container.table import Table
from shyft.dashboard.time_series.ts_viewer import TsViewer
from shyft.dashboard.time_series.axes_handler import BokehViewTimeAxis
from shyft.dashboard.time_series.view import TableView
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle
from shyft.dashboard.time_series.sources.source import DataSource
from shyft.dashboard.time_series.axes_handler import DsViewTimeAxisType
from shyft.dashboard.time_series.state import State, Unit, Quantity
from shyft.dashboard.time_series.sources.ts_adapter import TsAdapter


def test_table(mock_bokeh_document, test_ts_line, mock_ts_vector, logger_and_list_handler):
    logger, log_list = logger_and_list_handler
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    table_title = "test"
    table = Table(viewer=viewer, tools=[], title=table_title)
    table_view = TableView(view_container=table, columns={0: 'tull'}, label='bakvendtland', unit='MW')

    assert len(table.views) == 0
    table.add_view(view=table_view)
    assert len(table.views) == 1

    table.clear_views(specific_views=[table_view])
    assert len(table.views) == 0

    table.add_view(view=table_view)
    table.add_view(view=table_view)
    assert len(table.views) == 1

    table.clear()
    assert len(table.views) == 0
    table.add_view(view=table_view)

    time_range = UtcPeriod(0, 24*3600)
    data_source = DataSource(ts_adapter=test_ts_line, unit='MW',
                             request_time_axis_type=DsViewTimeAxisType.padded_view_time_axis,
                             time_range=time_range)

    da_view_handle = DsViewHandle(data_source=data_source, views=[table_view])

    viewer.add_ds_view_handles(ds_view_handles=[da_view_handle])

    table.update_view_data(view_data={table_view: mock_ts_vector})
    assert len(table.table_columns) and len(table.data) == 2
    assert [key for key in table.bokeh_data_source.data.keys()][0] == "Time"
    assert [key for key in table.bokeh_data_source.data.values()][0][0] == "Unit"

    table_view.visible = False
    assert len(table.table_columns) and len(table.data) == 1
    table_view.visible = True
    assert table.view_range_callback() is None
    new_title = "new title"
    table.update_title(new_title)
    assert table.bokeh_title_div.text == f"{table_title}: {new_title}"

    table.visible = True
    table.visible = False
    assert len(table.table_columns) and len(table.data) == 1
    table.visible = True
    assert len(table.table_columns) and len(table.data) == 2


def test_table_timezone(mock_bokeh_document):
    time_zone = 'Europe/Oslo'
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer", time_zone=time_zone)
    table = Table(viewer=viewer)
    assert table.parent.time_zone == time_zone


def test_table_states(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    table_title = "test"
    table = Table(viewer=viewer, tools=[], title=table_title)
    assert table.visible
    assert table._visible_state
    assert table._receive_state(States.DEACTIVE) is None
    assert not table.visible
    assert table._visible_state
    assert table._receive_state(States.ACTIVE) is None
    assert table.visible


# TODO: editable table testing


