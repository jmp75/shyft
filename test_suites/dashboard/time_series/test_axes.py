import pytest

import bokeh.models
from shyft.dashboard.time_series.axes import FigureYAxis, YAxis, YAxisError, YAxisSide
from shyft.dashboard.time_series.state import State


def test_y_axis():
    y_axis = YAxis(label='test', unit='Mw')
    y_axis2 = eval(repr(y_axis))
    assert y_axis.label == y_axis2.label
    assert y_axis.unit == y_axis2.unit
    assert y_axis.side == y_axis2.side


def test_figure_y_axis_init():
    # correct inint
    y_axis = YAxis(label='test', unit='MW', side=YAxisSide.RIGHT)
    fig_axis = FigureYAxis(axis=y_axis, unit_registry=State.unit_registry)
    assert fig_axis.side == YAxisSide.RIGHT.value

    # already bound YAxis should raise error
    with pytest.raises(YAxisError):
        fig_axis = FigureYAxis(axis=y_axis, unit_registry=State.unit_registry)

    # wrong unit
    y_axis2 = YAxis(label='test', unit='no_unit')
    with pytest.raises(YAxisError):
        fig_axis = FigureYAxis(axis=y_axis2, unit_registry=State.unit_registry)

    # init default y axis

    bokeh_axis = bokeh.models.LinearAxis()
    y_axis = YAxis(label='test', unit='MW')
    fig_axis = FigureYAxis(axis=y_axis, unit_registry=State.unit_registry, bokeh_axis=bokeh_axis)
    assert fig_axis.is_default_axis
    assert fig_axis.uid == 'default'
    assert fig_axis.side == YAxisSide.LEFT.value


def test_figure_y_axis_color_callback():
    old_color = 'green'
    y_axis = YAxis(label='test', unit='MW', color=old_color)
    fig_axis = FigureYAxis(axis=y_axis, unit_registry=State.unit_registry)
    assert fig_axis.bokeh_axis.axis_line_color == old_color

    new_color = 'blue'
    y_axis.color = new_color
    assert fig_axis.bokeh_axis.axis_line_color == new_color
    assert fig_axis.bokeh_axis.major_label_text_color == new_color
    assert fig_axis.bokeh_axis.axis_label_text_color == new_color
    assert fig_axis.bokeh_axis.major_tick_line_color == new_color
    assert fig_axis.bokeh_axis.minor_tick_line_color == new_color

