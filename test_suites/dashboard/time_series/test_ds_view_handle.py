from typing import List

import pytest
import numpy as np
from pint import UnitRegistry

from shyft.dashboard.time_series.axes import YAxis

from shyft.dashboard.base.ports import connect_ports, Receiver

from shyft.dashboard.time_series.view_container.legend import Legend
from shyft.dashboard.time_series.view_container.table import Table

from shyft.dashboard.time_series.view_container.figure import Figure

from shyft.dashboard.time_series.ts_viewer import TsViewer

from shyft.time_series import (TsVector, TimeSeries, TimeAxis, UtcTimeVector, DoubleVector, point_interpretation_policy,
                               min_utctime, max_utctime, UtcPeriod)
from shyft.dashboard.time_series.sources.source import DataSource
from shyft.dashboard.time_series.view import Line, LegendItem, TableView, FillInBetween
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle, DsViewHandleError, BasicDsViewHandleCreator


def test_ds_view_handle(test_ts_line):
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # different unit in view but same dimensionality
    fig_view = Line(unit='W', color='blue', label='test', view_container=1, index=0)
    # unit same dimensionality with different unit registry
    temp_ureg = UnitRegistry()
    fig_view2 = Line(unit=temp_ureg.Unit('GW'), color='blue', label='test', view_container=1, index=1)
    table_view = TableView(view_container=1, label='test', unit='MW')
    legend_view = LegendItem(views=[fig_view, table_view], view_container=1, label='test')

    dsvh = DsViewHandle(data_source=ds, views=[fig_view, fig_view2, legend_view, table_view])

    assert dsvh.data_source == ds
    assert dsvh.views == [fig_view, fig_view2, legend_view, table_view]

    # new ds_view_handle with bound source
    fig_view3 = Line(unit='MW', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view3])

    # new ds_view_handle with bound view
    ds2 = DataSource(ts_adapter=test_ts_line, unit='MW')
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds2, views=[fig_view])

    # new ds_view_handle with wrong unit definitionce
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # incompatible units
    fig_view = Line(unit='s', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])

    # new ds_view_handle with wrong unit definition with different unit registry
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # incompatible units
    fig_view = Line(unit=temp_ureg.Unit('s'), color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])

    # incompatible unit registry
    ds = DataSource(ts_adapter=test_ts_line, unit='euro')
    # incompatible units
    fig_view = Line(unit='euro', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])


def test_ds_view_handle_creator(mock_bokeh_document, mock_ts_vector):
    class DsvhRegistry:
        def __init__(self):
            self.dsvhs = []
            self.receive_dsvhs = Receiver(parent=self, name='test dsvh receiver',
                                          signal_type=List[DsViewHandle],
                                          func=self.dsvh_receiver)

        def dsvh_receiver(self, dsvhs: List[DsViewHandle]):
            self.dsvhs = dsvhs

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title='test')
    y_axis = YAxis(unit="MW", label="MW")
    fig1 = Figure(viewer=viewer)
    fig2 = Figure(viewer=viewer, y_axes=y_axis)
    table = Table(viewer=viewer)
    legend = Legend(viewer=viewer)
    dsvh_creator = BasicDsViewHandleCreator(figure_container=[fig1, fig2],
                                            legend_container=legend,
                                            table_container=table)
    assert dsvh_creator.figure_container == [fig1, fig2]
    assert dsvh_creator.table_container == [table]
    assert dsvh_creator.legend_container == [legend]

    dsvhs_registry = DsvhRegistry()
    connect_ports(dsvh_creator.send_ds_view_handles, dsvhs_registry.receive_dsvhs)
    assert dsvhs_registry.dsvhs == []

    mock_ts = mock_ts_vector.magnitude[0]
    mock_tsv = TsVector([mock_ts, mock_ts])
    mock_qts = dsvh_creator.unit_registry.Quantity(mock_ts, "")
    mock_qtsv = dsvh_creator.unit_registry.Quantity(mock_tsv, "")
    data = [mock_ts, mock_tsv, mock_qts, mock_qtsv]
    dsvh_creator.receive_data(data)

    assert dsvhs_registry.dsvhs[0].data_source.ts_adapter.data == data[0]
    assert dsvhs_registry.dsvhs[1].data_source.ts_adapter.data == data[1]
    assert dsvhs_registry.dsvhs[2].data_source.ts_adapter.data == data[2]
    assert dsvhs_registry.dsvhs[3].data_source.ts_adapter.data == data[3]

    # Test labels
    test_label = "test_label"
    dsvh_creator.receive_data([(mock_tsv, test_label)])
    for view in dsvhs_registry.dsvhs[0].views:
        assert view.label == test_label


def test_dsvh_y_axis(mock_bokeh_document):
    y_axis_1 = YAxis(label='y_axis_1', unit='mm')
    y_axis_2 = YAxis(label='y_axis_2', unit='cm/hr')
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title='test')
    figure = Figure(viewer=viewer, y_axes=[y_axis_1, y_axis_2])

    dsvh_creator = BasicDsViewHandleCreator(figure_container=[figure])

    with pytest.raises(RuntimeError):
        dsvh_creator.get_y_axis_by_unit(unit='kg')

    assert dsvh_creator.get_y_axis_by_unit(unit='mm') == y_axis_1
    assert dsvh_creator.get_y_axis_by_unit(unit='mm/hr') == y_axis_2
    assert dsvh_creator.get_y_axis_by_label(label='y_axis_1') == y_axis_1
    assert dsvh_creator.get_y_axis_by_label(label='y_axis_2') == y_axis_2

    with pytest.raises(RuntimeError):
        dsvh_creator.get_y_axis_by_label(label='y_axis_99')


def test_dsvh_percentiles_views(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title='test')
    figure = Figure(viewer=viewer)

    percentiles = [0, 10, 25, 50, 75, 90, 100]
    fill_views = BasicDsViewHandleCreator.get_percentiles_views(percentiles=percentiles,
                                                                view_container=figure,
                                                                color='black',
                                                                label='label',
                                                                unit='1')

    fill_count = len([i for i in fill_views if isinstance(i, FillInBetween)])
    assert fill_count == 3


def test_dsvh_get_time_range():
    t1 = np.array([1, 2, 3, 4, 5, 6, 7, 8])
    t2 = np.array([6, 7, 8, 9, 10, 11, 12, 13])
    ta1 = TimeAxis(UtcTimeVector.from_numpy(t1))
    ta2 = TimeAxis(UtcTimeVector.from_numpy(t2))
    d = np.array([10, 20, 30, 40, 50, 60, 70])
    ts1 = TimeSeries(ta1, DoubleVector.from_numpy(d), point_interpretation_policy.POINT_AVERAGE_VALUE)
    ts2 = TimeSeries(ta2, DoubleVector.from_numpy(d), point_interpretation_policy.POINT_AVERAGE_VALUE)
    empty_ts = TimeSeries()
    dsvhc = BasicDsViewHandleCreator()

    assert dsvhc.get_time_range(empty_ts) == UtcPeriod(min_utctime, max_utctime)
    assert dsvhc.get_time_range(TsVector()) == UtcPeriod(min_utctime, max_utctime)
    assert dsvhc.get_time_range(TsVector([empty_ts])) == UtcPeriod(min_utctime, max_utctime)
    assert dsvhc.get_time_range(ts1) == UtcPeriod(1, 8)
    assert dsvhc.get_time_range(ts2) == UtcPeriod(6, 13)
    assert dsvhc.get_time_range(TsVector([ts1])) == UtcPeriod(1, 8)
    assert dsvhc.get_time_range(TsVector([ts2])) == UtcPeriod(6, 13)
    assert dsvhc.get_time_range(TsVector([ts1, ts2])) == UtcPeriod(1, 13)


