from shyft.time_series import (Calendar, deltahours, deltaminutes, TimeAxisFixedDeltaT, YMDhms)
from shyft.repository.netcdf.time_conversion import convert_netcdf_time

try:
    from cftime import utime
except ModuleNotFoundError as mnf:
    from netcdftime import utime

import numpy as np


def test_extract_conversion_factors_from_string():
    u = utime('hours since 1970-01-01 00:00:00')
    u_tzoffset=getattr(u,'tzoffset',0)  # fix problem with cf time that does not have tzoffset after version 1.0.x
    t_origin = Calendar(int(u_tzoffset)).time(
        YMDhms(int(u.origin.year), int(u.origin.month), int(u.origin.day), int(u.origin.hour), int(u.origin.minute), int(u.origin.second)))
    delta_t_dic = {'days': deltahours(24), 'hours': deltahours(1), 'minutes': deltaminutes(1)}
    delta_t = delta_t_dic[u.units]
    assert u is not None
    assert delta_t == deltahours(1)
    assert t_origin == 0


def test_unit_conversion():
    utc = Calendar()
    t_num = np.arange(-24, 24, 1, dtype=np.float64)  # we use both before and after epoch to ensure sign is ok
    t_converted = convert_netcdf_time('hours since 1970-01-01 00:00:00', t_num)
    t_axis = TimeAxisFixedDeltaT(utc.time(1969, 12, 31, 0, 0, 0), deltahours(1), 2*24)
    for i in range(t_axis.size()):
        assert t_converted[i] == t_axis(i).start
