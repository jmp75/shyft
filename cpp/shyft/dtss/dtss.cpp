#include <shyft/dtss/dtss.h>
#include <shyft/core/dlib_utils.h>

#include <shyft/time_series/dd/compute_ts_vector.h>
#include <boost/serialization/map.hpp>
#include <shyft/web_api/web_api_generator.h> // looking for the geo ts url generator
#include <boost/dynamic_bitset.hpp>
#include <boost/thread/sync_bounded_queue.hpp>
#include <fstream>


//#include <boost/thread/concurrent_queues/queue_views.hpp>

namespace shyft::dtss {
    
    using shyft::time_series::dd::ts_as;
    using shyft::web_api::generator::geo_ts_url_generator;
    using shyft::time_series::dd::ats_vector;
    using std::ifstream;
    using std::ofstream;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_nvp;
    using shyft::core::core_arch_flags;
    
    void server::add_container(
            const std::string & container_name, const std::string & root_path,
            std::string container_type 
        ) {
        unique_lock<mutex> sl(c_mx);
        // factory.. CD::create_container(container_name, container_type, root_dir, *this);
        std::string server_container_name;
        if ( container_type.empty() || container_type == "ts_db" ) {
            server_container_name = container_name;
            container[server_container_name] = std::make_unique<ts_db>( root_path );
            if(server_container_name.size()==0) { // if root path, then scan for geo-cfg data-bases
                auto found_geo=geo_ts_db_scan(root_path);// and
                for(auto kv:found_geo) // *update/replace* existing, allowing other external to co-exist
                    geo[kv.first]=kv.second;
            }
                
        } else if ( container_type == "krls" ) {
            server_container_name = std::string{"KRLS_"} + container_name;
            container[server_container_name] = std::make_unique<krls_pred_db>(
                    root_path,
                    [this](const std::string & tsid, utcperiod period, bool use_ts_cached_read, bool update_ts_cache) -> ts_vector_t {
                        id_vector_t id_vec{ tsid };
                        return do_read(id_vec, period, use_ts_cached_read, update_ts_cache);
                    }
                );
        } else {
            throw std::runtime_error{ std::string{"Cannot construct unknown container type: "} + container_type };
        }
    }
    
    server::container_t::iterator server::container_find(const std::string & container_name, const std::string & container_query) {
        container_t::iterator f;
        if ( container_query.empty() || container_query == "ts_db" ) {
            f = container.find(container_name);
            if(f == std::end(container)) {
                f = container.find("");// try to find default container
            }
        } else if ( container_query == "krls" ) {
            f = container.find(std::string{"KRLS_"} + container_name);
        }
        return f;
    }
    
    its_db & server::internal(const std::string & container_name, const std::string & container_query) {
        auto f=container_find(container_name,container_query);
        if( f == std::end(container) )
            throw std::runtime_error(std::string("Failed to find shyft container: ")+container_name);
        return *f->second;
    }


    ts_info_vector_t server::do_find_ts(const std::string& search_expression) {
        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(search_expression);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(search_expression);
            auto container_query_it = queries.find(container_query);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern, container_query).find(extract_shyft_url_path(search_expression,pattern), queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern).find(extract_shyft_url_path(search_expression,pattern), queries);
            }
        } else if (find_ts_cb) {
            return find_ts_cb(search_expression);
        } else {
            return ts_info_vector_t();
        }
    }


    ts_info server::do_get_ts_info(const std::string & ts_name) {
        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(ts_name);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(ts_name);
            auto container_query_it = queries.find(container_query);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                 filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern, container_query).get_ts_info(extract_shyft_url_path(ts_name,pattern), queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern).get_ts_info(extract_shyft_url_path(ts_name,pattern), queries);
            }
        } else {
            return ts_info{};
        }
    }


    /** if overwrite on write, then flush the cache prior to writing */
    void server::do_cache_update_on_write(const ts_vector_t&tsv,bool overwrite_on_write) {
        vector<string> ts_ids;ts_ids.reserve(tsv.size());
        ts_vector_t tss;
        for (std::size_t i = 0; i < tsv.size(); ++i) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            ts_ids.emplace_back(rts->id);
            tss.emplace_back(apoint_ts(rts->rep));
        }
        ts_cache.add(ts_ids,tss,overwrite_on_write);
    }


    void server::do_store_ts(const ts_vector_t & tsv, bool overwrite_on_write, bool cache_on_write) {
        if(tsv.size()==0) return;
        // 1. filter out all shyft://<container>/<ts-path> elements
        //    and route these to the internal storage controller (threaded)
        //    std::map<std::string, ts_db> shyft_internal;
        //
        std::vector<std::size_t> other;
        other.reserve(tsv.size());
        std::vector<string> subs;
        bool sub_active= sm && sm->is_active();
        if(sub_active)
            subs.reserve(tsv.size());
            
        for( std::size_t i = 0; i < tsv.size(); ++i ) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            if ( ! rts )
                throw std::runtime_error("dtss store: require ts with url-references");
            if(sub_active) subs.push_back(rts->id);
            auto c = extract_shyft_url_container(rts->id);
            if( c.size() > 0 ) {
                auto queries = extract_shyft_url_query_parameters(rts->id);
                auto container_query_it = queries.find(container_query);
                if ( ! queries.empty() && container_query_it != queries.end() ) {
                    auto container_query = container_query_it->second;
                    filter_shyft_url_parsed_queries(queries, remove_queries);
                    internal(c, container_query).save(
                        extract_shyft_url_path(rts->id,c),  // path
                        rts->core_ts(),      // ts to save
                        overwrite_on_write,  // should do overwrite instead of merge
                        queries              // query key/values from url
                    );
                } else {
                    filter_shyft_url_parsed_queries(queries, remove_queries);
                    internal(c).save(
                        extract_shyft_url_path(rts->id,c),  // path
                        rts->core_ts(),      // ts to save
                        overwrite_on_write,  // should do overwrite instead of merge
                        queries              // query key/values from url
                    );
                }
                if ( cache_on_write ) { // ok, this ends up in a copy, and lock for each item(can be optimized if many)
                    if(overwrite_on_write)
                        ts_cache.remove(rts->id);//invalidate previous defs. if any
                    ts_cache.add(rts->id, apoint_ts(rts->rep));
                }
            } else {
                other.push_back(i); // keep idx of those we have not saved
            }
        }

        // 2. for all non shyft:// forward those to the
        //    store_ts_cb
        if(store_ts_cb && other.size()) {
            if(other.size()==tsv.size()) { //avoid copy/move if possible
                store_ts_cb(tsv);
                if (cache_on_write) do_cache_update_on_write(tsv,overwrite_on_write);
            } else { // have to do a copy to new vector
                ts_vector_t r;
                for(auto i:other) r.push_back(tsv[i]);
                store_ts_cb(r);
                if (cache_on_write) do_cache_update_on_write(r,overwrite_on_write);
            }
        }
        if(sub_active)
            sm->notify_change(subs);
    }


    void server::do_merge_store_ts(const ts_vector_t& tsv, bool cache_on_write) {
        if ( tsv.size() == 0 )
            return;

        //
        // 0. check & prepare the read time-series in tsv for the specified period of each ts
        //    (we optimize a little bit grouping on common period, and reading in batches with equal periods)
        //
        id_vector_t ts_ids; ts_ids.reserve(tsv.size());
        std::unordered_map<utcperiod, id_vector_t, detail::utcperiod_hasher> read_map;
        std::unordered_map<std::string, apoint_ts> id_map;

        for ( std::size_t i=0; i < tsv.size(); ++i ) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            if ( ! rts )
                throw std::runtime_error("dtss store merge: require ts with url-references");
            // sanity check
            if ( id_map.find(rts->id) != end(id_map) )
                throw std::runtime_error("dtss store merge requires distinct set of ids, first duplicate found:" + rts->id);
            id_map[rts->id] = apoint_ts(rts->rep);
            // then just build up map[period] = list of time-series to read
            auto rp = rts->rep->total_period();
            if ( read_map.find(rp) != end(read_map) ) {
                read_map[rp].push_back(rts->id);
            } else {
                read_map[rp] = id_vector_t{ rts->id };
            }
        }

        //
        // 1. do the read-merge for each common period, append to final minimal write list
        //
        ts_vector_t tsv_store; tsv_store.reserve(tsv.size());
        for ( auto rr = read_map.begin(); rr != read_map.end(); ++rr ) {
            auto read_ts = do_read(rr->second, rr->first, false, cache_on_write);
            // read_ts is in the order of the ts-id-list rr->second
            for ( std::size_t i = 0; i < read_ts.size(); ++i ) {
                auto ts_id = rr->second[i];
                read_ts[i].merge_points(id_map[ts_id]);
                tsv_store.push_back(apoint_ts(ts_id, read_ts[i]));
            }
        }

        //
        // 2. finally write the merged result back to whatever store is there
        //
        do_store_ts(tsv_store, false, cache_on_write);

    }

    ts_vector_t server::do_read(const id_vector_t & ts_ids, utcperiod p, bool use_ts_cached_read, bool update_ts_cache) {
        if( ts_ids.size() == 0 )
            return ts_vector_t{};

        // should cache?
        bool cache_read_results = update_ts_cache || cache_all_reads;

        // 0. filter out ts's we can get from cache, given we are allowed to use cache
        std::unordered_map<std::string, apoint_ts> cc;  // cached series
        if( use_ts_cached_read )
            cc = ts_cache.get(ts_ids, p);

        ts_vector_t results(ts_ids.size());
        std::vector<std::size_t> external_idxs;
        if ( cc.size() == ts_ids.size() ) {
            // if we got all ts's from cache -> map in the results
            for( std::size_t i = 0; i < ts_ids.size(); ++i )
                results[i] = cc[ts_ids[i]];
        } else {
            // 1. filter out shyft://
            //    if all shyft: return internal read
            external_idxs.reserve(ts_ids.size()); // only reserve space when needed
            for ( std::size_t i = 0; i < ts_ids.size(); ++i ) {
                if ( cc.find(ts_ids[i]) == cc.end() ) {
                    auto c = extract_shyft_url_container(ts_ids[i]);
                    if ( c.size() > 0 ) {
                        // check for queries in shyft:// url's
                        auto queries = extract_shyft_url_query_parameters(ts_ids[i]);
                        auto container_query_it = queries.find(container_query);
                        if ( ! queries.empty() && container_query_it != queries.end() ) {
                            auto container_query = container_query_it->second;
                            filter_shyft_url_parsed_queries(queries, remove_queries);
                            results[i] = apoint_ts(make_shared<const gpoint_ts>(internal(c, container_query).read(
                                extract_shyft_url_path(ts_ids[i],c), p, queries)));
                        } else {
                            filter_shyft_url_parsed_queries(queries, remove_queries);
                            results[i] = apoint_ts(make_shared<const gpoint_ts>(internal(c).read(
                                extract_shyft_url_path(ts_ids[i],c), p, queries)));
                        }
                        // caching?
                        if ( cache_read_results )
                            ts_cache.add(ts_ids[i], results[i]);
                    } else
                        external_idxs.push_back(i);
                } else {
                    results[i] = cc[ts_ids[i]];
                }
            }
        }

        // 2. if other/more than shyft get all those
        if( external_idxs.size() > 0 ) {
            if( ! bind_ts_cb )
                throw std::runtime_error("dtss: read-request to external ts, without external handler");

            // only externaly handled series => return only external result
            if( external_idxs.size() == ts_ids.size() ) {
                auto rts = bind_ts_cb(ts_ids,p);
                if( cache_read_results )
                    ts_cache.add(ts_ids,rts);

                return rts;
            }

            // collect & handle external references
            vector<string> external_ts_ids; external_ts_ids.reserve(external_idxs.size());
            for( auto i : external_idxs )
                external_ts_ids.push_back(ts_ids[i]);
            auto ext_resolved = bind_ts_cb(external_ts_ids, p);

            // caching?
            if( cache_read_results )
                ts_cache.add(external_ts_ids, ext_resolved);

            // merge external results into output results
            for(std::size_t i=0;i<ext_resolved.size();++i)
                results[external_idxs[i]] = ext_resolved[i];
        }

        return results;
    }

    void server::do_bind_ts(utcperiod bind_period, ts_vector_t& atsv, bool use_ts_cached_read, bool update_ts_cache) {

        using shyft::time_series::dd::ts_bind_info;

        std::unordered_map<std::string, std::vector<ts_bind_info>> ts_bind_map;
        std::vector<std::string> ts_id_list;

        // step 1: bind not yet bound time-series (ts with only symbol, needs to be resolved using bind_cb)
        for ( auto & ats : atsv ) {
            auto ts_refs = ats.find_ts_bind_info();
            for ( const auto & bi : ts_refs ) {
                if ( ts_bind_map.find(bi.reference) == ts_bind_map.end() ) { // maintain unique set
                    ts_id_list.push_back(bi.reference);
                    ts_bind_map[bi.reference] = std::vector<ts_bind_info>();
                }
                ts_bind_map[bi.reference].push_back(bi);
            }
        }

        // step 2: (optional) bind_ts callback should resolve symbol time-series with content
        if ( ts_bind_map.size() > 0 ) {
            auto bts = do_read(ts_id_list, bind_period, use_ts_cached_read, update_ts_cache);
            if ( bts.size() != ts_id_list.size() )
                throw std::runtime_error(std::string{"failed to bind all of "} + std::to_string(bts.size()) + std::string{" ts"});

            for ( std::size_t i = 0; i < ts_id_list.size(); ++i ) {
                for ( auto & bi : ts_bind_map[ts_id_list[i]] )
                    bi.ts.bind(bts[i]);
            }
        }

        // step 3: after the symbolic ts are read and bound, we iterate over the
        //         expression tree and calls .do_bind() so that
        //         the new information is taken into account and the expression tree are
        //         ready for evaluate with everything const so threading is safe.
        for ( auto & ats : atsv )
            ats.do_bind();
    }


    ts_vector_t server::do_evaluate_ts_vector(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache,utcperiod clip_period) {
        do_bind_ts(bind_period, atsv, use_ts_cached_read, update_ts_cache);
        if(clip_period.valid())
            return clip_to_period(ts_vector_t{ shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv) },clip_period);
        else
            return ts_vector_t{ shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv) };
    }


    ts_vector_t server::do_evaluate_percentiles(utcperiod bind_period, ts_vector_t& atsv, gta_t const&ta, std::vector<int64_t> const& percentile_spec,bool use_ts_cached_read,bool update_ts_cache) {
        do_bind_ts(bind_period, atsv,use_ts_cached_read,update_ts_cache);
        std::vector<int64_t> p_spec;for(const auto p:percentile_spec) p_spec.push_back(int(p));// convert
        return percentiles(atsv, ta, p_spec);// we can assume the result is trivial to serialize
    }


    void server::do_remove_ts(const std::string & ts_url) {
        if ( ! can_remove ) {
            throw std::runtime_error("dtss::server: server does not support removing");
        }

        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(ts_url);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(ts_url);
            auto container_query_it = queries.find(container_query);
            auto shyft_ts_url = extract_shyft_url_path(ts_url, pattern);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                filter_shyft_url_parsed_queries(queries, remove_queries);
                internal(pattern, container_query).remove(shyft_ts_url, queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                internal(pattern).remove(shyft_ts_url, queries);
            }
            ts_cache.remove(shyft_ts_url);// remove it from cache as well!
        } else {
            throw std::runtime_error("dtss::server: server does not allow removing for non shyft-url type data");
        }
    }


    void server::on_connect(
        std::istream & in,
        std::ostream & out,
        const std::string & foreign_ip,
        const std::string & local_ip,
        unsigned short foreign_port,
        unsigned short local_port,
        dlib::uint64 /*connection_id*/
    ) {

        using shyft::core::core_iarchive;
        using shyft::core::core_oarchive;
        using shyft::time_series::dd::expression_decompressor;
        using shyft::time_series::dd::compressed_ts_expression;
        using shyft::core::scoped_count;
        scoped_count _{alive_connections};// count the alive connections
        try {
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                    //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                    switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                    case message_type::EVALUATE_TS_VECTOR:
                    case message_type::EVALUATE_TS_VECTOR_CLIP:
                    case message_type::EVALUATE_EXPRESSION:
                    case message_type::EVALUATE_EXPRESSION_CLIP:{
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;utcperiod clip_period;
                        ts_vector_t rtsv;
                        core_iarchive ia(in,core_arch_flags);
                        ia>>bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }
                        ia>>use_ts_cached_read>>update_ts_cache;
                        if(msg_type==message_type::EVALUATE_TS_VECTOR_CLIP || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            ia>>clip_period;
                        }
                        auto result=do_evaluate_ts_vector(bind_period, rtsv,use_ts_cached_read,update_ts_cache,clip_period);//first get result
                        msg::write_type(message_type::EVALUATE_TS_VECTOR,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;

                    } break;
                    case message_type::EVALUATE_EXPRESSION_PERCENTILES:
                    case message_type::EVALUATE_TS_VECTOR_PERCENTILES: {
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;
                        ts_vector_t rtsv;
                        std::vector<int64_t> percentile_spec;
                        gta_t ta;
                        core_iarchive ia(in,core_arch_flags);
                        ia >> bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION_PERCENTILES) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }

                        ia>>ta>>percentile_spec>>use_ts_cached_read>>update_ts_cache;

                        auto result = do_evaluate_percentiles(bind_period, rtsv,ta,percentile_spec,use_ts_cached_read,update_ts_cache);//{
                        msg::write_type(message_type::EVALUATE_TS_VECTOR_PERCENTILES, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::FIND_TS: {
                        std::string search_expression; //{
                        search_expression = msg::read_string(in);// >> search_expression;
                        auto find_result = do_find_ts(search_expression);
                        msg::write_type(message_type::FIND_TS, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << find_result;
                    } break;
                    case message_type::GET_TS_INFO: {
                        std::string ts_url;
                        ts_url = msg::read_string(in);
                        auto result = do_get_ts_info(ts_url);
                        msg::write_type(message_type::GET_TS_INFO, out);
                        core_oarchive oa(out, core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::STORE_TS: {
                        ts_vector_t rtsv;
                        bool overwrite_on_write{ true };
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> overwrite_on_write >> cache_on_write;
                        do_store_ts(rtsv, overwrite_on_write, cache_on_write);
                        msg::write_type(message_type::STORE_TS, out);
                    } break;
                    case message_type::MERGE_STORE_TS: {
                        ts_vector_t rtsv;
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> cache_on_write;
                        do_merge_store_ts(rtsv, cache_on_write);
                        msg::write_type(message_type::MERGE_STORE_TS, out);
                    } break;
                    case message_type::REMOVE_TS: {
                        std::string ts_url = msg::read_string(in);
                        do_remove_ts(ts_url);
                        msg::write_type(message_type::REMOVE_TS, out);
                    } break;
                    case message_type::CACHE_FLUSH: {
                        flush_cache();
                        clear_cache_stats();
                        msg::write_type(message_type::CACHE_FLUSH,out);
                    } break;
                    case message_type::CACHE_STATS: {
                        auto cs = get_cache_stats();
                        msg::write_type(message_type::CACHE_STATS,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<cs;
                    } break;
                    case message_type::GET_GEO_INFO: {
                        auto r = do_get_geo_info();
                        msg::write_type(message_type::GET_GEO_INFO,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;
                    case message_type::EVALUATE_GEO: {
                        geo::eval_args geo_args;
                        bool use_cache;
                        bool update_cache;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_args>>use_cache>>update_cache;
                        auto result=do_geo_evaluate(geo_args,use_cache,update_cache);
                        msg::write_type(message_type::EVALUATE_GEO, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result;
                    }break;
                    
                    case message_type::STORE_GEO: {
                        geo::ts_matrix tsm;
                        bool cache,replace;
                        string geo_db_name;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_db_name>>tsm>>replace>>cache;
                        do_geo_store(geo_db_name,tsm,replace,cache);
                        msg::write_type(message_type::STORE_GEO, out);
                    } break;
                    
                    case message_type::ADD_GEO_DB: {
                        geo::ts_db_config_ gdb;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>gdb;
                        add_geo_ts_db(gdb);
                        msg::write_type(message_type::ADD_GEO_DB, out);
                    } break;

                    case message_type::REMOVE_GEO_DB: {
                        string geo_db_name;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_db_name;
                        remove_geo_ts_db(geo_db_name);
                        msg::write_type(message_type::REMOVE_GEO_DB, out);
                    } break;
                    
                    default:
                        throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string(static_cast<int>(msg_type)));
                    }
                } catch(dlib::socket_error const &) {
                    // silent ignore, later log. It means we got trouble read/write socket, we do not crash on that, just 
                    // continue, as its most likely the connection to remote that went away
                    // either because connection was broken, or client was killed
                }  catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        } catch(...) { // when we reach here.. we are going to close down the socket
            // so we just log to std err, then return.
            // the dlib thread, will forcibly close the socket
            // but survive the server it self.
            std::cerr<< "dtss: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
        }
    }
    
    /** detects if a geo db is using internal time-series, using the prefix shyft:// */
    static bool is_internal_geo(string const& prefix) {
        return boost::starts_with(prefix,"shyft://");
    }
    
    geo::geo_ts_matrix server::do_geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache ) {
        using geo::detail::ix_calc;
        using geo::detail::geo_ix;
        

        std::shared_ptr<geo::ts_db_config> gdb;
        
        // copy gdb, consider: make multiread-singlewrite lock
        /** protected access on lockup, but should we take a reader-lock, or just copy the gdb-structure ? */{
            unique_lock<mutex> sl(c_mx);
            auto f=geo.find(ea.geo_ts_db_id);
            if( f==geo.end()) throw std::runtime_error("geo_evaluate: no geo-db configured for " + ea.geo_ts_db_id);
            gdb=std::make_shared<geo::ts_db_config>(*f->second);// make a copy we keep during the evaluation, considered cheap relative  other work
        }
        auto internal_gdb=is_internal_geo(gdb->prefix);
        if(!internal_gdb && !geo_read_cb) 
            throw runtime_error("dtss: no geo read callback set, geo-extensions not available");
        
        // validate variables and build the geo_ts_set
        // refactor to function taking query params, gdb.cfg -> geo_ts_set or similar
        auto gx=gdb->compute(ea);// gx is a fully specified slice into the geo ts-db
        if(gx.size()==0)
            return {};
        
        //
        //-- (1) create the indexed result structure, with empty time-series (to be filled in later)
        //

        auto rv=gdb->create_geo_ts_matrix(gx);// keep this as result structure, supporting pure-indexing 
        //
        //-- (2) do the cache-lookups and fill in references from ts-cache, while
        //       we at the same time build the __residual gross query__
        //
        //       that we will use to query the backend-store to fill in
        //       those not matched.
        //
        //       the cache.get(...) uses a linear indexing approach, 
        //       doing a callback to us for each of the
        //         ts_id(i),period(i) we search
        //       and for after lookup, it will call us with
        //         found_cb(i,bool found, ts_t const&ts_found_ref)
        //
        //       so we need a fast computed size_t ix -> tuple of (t0,v,e,g) indicies matching the above structure
        //        (lets generate it in the loop above)
        //    -- then
        //       our plan is to use the
        //         found_cb(i,found=false,ts=none) -> extend the __residual gross query__
        //        and
        //         found_cb(i,found=true,ts=found_ts) -> i->tuple of(t0,v,e,g) rv[t0][v][e][g].ts=found_ts
        //
        // Invoke the ts_cache with suitable callbacks to us.
        
        ix_calc rx(gx.t.size(),gx.v.size(),gx.e.size(),gx.g.size());//rx result index flat<-->ndim conversions by math
        auto t0_ta=gdb->t0_time_axis();
        vector<size_t> cache_misses;cache_misses.reserve(rx.size());
        auto t0_span=ea.ts_dt>utctime(0)?ea.ts_dt:gdb->dt;// get out the expected/defined range of 'forecasts'
        string ts_url;ts_url.reserve(500);// assumed reasonable max-limit(not hard, only performance)
        auto  ts_url_sink=std::back_inserter(ts_url);
        geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(gdb->prefix);
        geo::ts_id url_ix;url_ix.geo_db=ea.geo_ts_db_id;//assign only once, invariant for this query
        if(use_cache) {
            ts_cache.get(
                rx.size(),//  this is the number of items we request,
                [&gx,&rx,&geo_ts_url_,&ts_url,&ts_url_sink,&url_ix,&t0_ta](size_t i)->string { // this will be how we generate the ts-url
                    auto const ix=rx.ix(i);
                    ts_url.resize(0);//resize,keep memory
                    url_ix.v=gx.v[ix.v];// important, translate to cfg.referenced indexes
                    url_ix.e=gx.e[ix.e];//  --!--
                    url_ix.g=gx.g[ix.g];//  --!--
                    url_ix.t=t0_ta.time(ix.t0);// get the real-time from ix.t0, so no translate to cfg needed
                    boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                    return ts_url;// here we want a single copy string op(it has to be one)
                    
                },
                [&rx,&t0_ta,t0_span](size_t i)->utcperiod {// this will return the wanted (minimum) period requested from cache
                    auto t0=t0_ta.time(rx.ix(i).t0);// the start of this 'forecast', using the t0_ta as the correct reference
                    return utcperiod(t0,t0+t0_span);// the total covering period for this 'forecast'
                },
                [&rx,&rv,&cache_misses](size_t i, bool found,apoint_ts const& f_ts)->void { // we get called back for each item here.
                    if(found) {
                        auto const  ix=rx.ix(i);
                        rv._set_ts(ix.t0,ix.v,ix.e,ix.g,f_ts); // notice that here we use ix. directly to correctly address the resulting structure
                    } else {
                        cache_misses.emplace_back(i);
                    }
                }
            );
        } else {
            for(size_t i=0;i<rx.size();++i)//fill in all misses
                cache_misses.emplace_back(i);
        }
        
        // -- (3) Analyze the result(hits,misses) from above, and if needed call to backend to refill the missing.
        //    
        //    pre:The call to backend should return exactly what is requested,
        //        so the request need to be precise, and let's say we 
        //        use indicies referenced to the geo cfg (so that t0-idx is i'th t0 =cfg.t0[t0_idx],
        //            and v-idx is ref to the i'th variables as cfg.variables[v_idx] etc.)
        //        then the request is minimal, precise, and well defined.
        //        
        //        For the returned result-type, In python, it could be convinient to adress this as
        //        GeoTsRequestResult that maps to c++ geo_ts_request_result
        //        E.g:
        //        
        //          r=GeoTsRequestResult(n_t0,n_var,n_ens,n_geo)
        //          then
        //          r.assign_ts(t0,var,ens,geo,ts), where t0,var,ens,geo are all indexes as pr. result-indexing space
        //          r.ts(t0,var,ens,geo,ts) -> gives the TimeSeries at specified result index.
        //                 .. so we can range-check t0,var,ens,geo against the specified range at construction.
        //                    to minimize mixing up indexes-errors.
        //
        //        internally rep as ats_vector[n_t0*n_var*n_ens*n_geo]
        //          ats_vector[i], where i is computed as (n_t0*n_var*n_ens*n_geo)*t0 + (n_var*n_ens*n_geo)*v + (n_ens*n_geo)*e + g
        //                          reversed compute is g= i%n_geo, .. e=.., v=.., t0=..
        // 
        //   post:When handling the result of the callback request, we need to map
        //        the python GeoTsResult (alias c++ geo_ts_request_result)
        //        back into the result structure as provided/returned by this routine.
        //
        //        recall r[t0-ix][var-ix][ens_idx]::type geo_ts_vector, where the i'th member of geo_ts-vector represents ith point'
        //
        //        The most efficient manner is index-lookup-translations:
        //          r_map_t0[t0_request_idx] gives directly ix that goes into the rv[ix]
        //          r_map_v[v_request_idx] gives directly the ix that goes into the rv[t0_ix][v_ix]
        //
        if(cache_misses.size()>0) {
            // prep the call to backend assume we should read all:
            auto b_gx=gx;
            auto b_ta=ea.ta.size()?ea.ta:gdb->t0_time_axis();// empty means all
            // for speed, we need a direct lookup map from callback indicies to result-set indicies
            vector<size_t> t_map;// if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
            vector<size_t> v_map;// if all  variables then v_map[i] would be 0,1,..n-1 etc. 
            vector<size_t> e_map;
            vector<size_t> g_map;
            
            if(cache_misses.size()<rx.size()) { // optimize away if all missing, in this case: reduce what we read into strictly needed
                using boost::dynamic_bitset;
                t_map.reserve(gx.t.size());// if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
                v_map.reserve(gx.v.size());// if all  variables then v_map[i] would be 0,1,..n-1 etc. 
                e_map.reserve(gx.e.size());
                g_map.reserve(gx.g.size());
                b_gx.size_to_zero();
                
                // scan and register missing, using bit-map approach, note ix positions refers to rx indexing space (result, not cfg)
                // TODO: we could keep these, and be 100% exact on fetch/store, but
                // for now we accept we get the union of the slices missing(.e.g if one var is missing tx, and another missing ty, then we read both vars at tx,ty)
                dynamic_bitset<> xvar{gdb->variables.size()};
                dynamic_bitset<> xgeo{gdb->grid.points.size()};
                dynamic_bitset<> xens{(size_t)gdb->n_ensembles};
                dynamic_bitset<> xt0{t0_ta.size()};
                for(auto cm:cache_misses) {
                    auto const x=rx.ix(cm);
                    xvar[x.v]=true;
                    xens[x.e]=true;// i'th index ref to arg ens_ix[i]
                    xgeo[x.g]=true;
                    xt0[x.t0]=true;// i'th index referring to t0_ta time-points
                }
                // construct arguments to pass to geo read callback routine
                // they are either by value, or by ix-ref to cfg. tables
                for(size_t i=0;i<xvar.size();++i) if(xvar[i]) {b_gx.v.push_back(gx.v[i]);v_map.push_back(i);}//gx.v[i] ref to cfg.variables[i]
                for(size_t i=0;i<xens.size();++i) if(xens[i]) {b_gx.e.push_back(gx.e[i]);e_map.push_back(i);}//gx.e[i] ref to arg ens_idx, by value.
                for(size_t i=0;i<xgeo.size();++i) if(xgeo[i]) {b_gx.g.push_back(gx.g[i]);g_map.push_back(i);}//gx.g[i] ref. to positions found by  geo-query
                for(size_t i=0;i<xt0.size();++i) if(xt0[i]) {b_gx.t.push_back(t0_ta.time(i));t_map.push_back(i);}
                b_ta=gta_t(b_gx.t,t0_ta.total_period().end);// the end-point is not important
            } 
            //
            // here we perform the call to backend, getting the missing results back in a dense ats_vector format
            //                           cfg.v    cfg.points   by val   ta
            
            auto br= internal_gdb?do_internal_geo_read(gdb,b_gx): geo_read_cb(gdb, b_gx);// backend result
            
            // unwind the result from  the backend, and put it into the missing entries of the result structure
            // br[var_idx][geo_idx][ens_idx]::type ats_vector, keeping all results, ordered by t0
            // __x means final result-index reference
            // _i  means cb result reference (they might differ)
            
            vector<string> c_id;// cache these ts urls with ts below
            vector<apoint_ts> c_ts;// cache these ts
            if(update_cache) {// ensure to make room for result to cache
                size_t c_size=b_gx.size();
                c_id.reserve(c_size);
                c_ts.reserve(c_size);
            }
            for(size_t ti=0;ti<b_ta.size();++ti) {
                size_t tix=t_map.size()?t_map[ti]:ti;// if all t, then 1-1 map, else pick result-index from t_map.
                for(size_t vi=0;vi<b_gx.v.size();++vi) {
                    size_t vix=v_map.size()?v_map[vi]:vi;// if all v, then 1-1 map, else pick result index from v_map.
                    for(size_t ei=0;ei<b_gx.e.size();++ei) {
                        size_t eix=e_map.size()?e_map[ei]:ei;// if all e, then 1-1 map, else pick result index from e_map.
                        for(size_t gi=0;gi<b_gx.g.size();++gi) {
                           size_t gix=g_map.size()?g_map[gi]:gi;// if all e, then 1-1 map, else pick result index from e_map.
                           auto const & rts=br._ts(ti,vi,ei,gi);
                           rv._set_ts(tix,vix,eix,gix,rts);
                           if(update_cache) {// collect stuff to cache here.(avoid multiple calls for mutex)
                               ts_url.resize(0);//resize,keep memory
                               url_ix.v=gx.v[vix];// important, translate to cfg.referenced indexes
                               url_ix.e=gx.e[eix];//  --!--
                               url_ix.g=gx.g[gix];//  --!--
                               url_ix.t=t0_ta.time(tix);// get the real-time from ix.t0, so no translate to cfg needed
                               boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                               c_id.push_back(ts_url);
                               c_ts.emplace_back(rts);
                           }
                        }
                    }
                }
            }
            if(update_cache &&  c_id.size()) {
                ts_cache.add(c_id,c_ts);
            }
        }
            
        if(!ea.concat)
            return rv;//we are done
        //    
        // (4) if concat, execute that here
        //
        return rv.concatenate(ea.cc_dt0,ea.ta.period(0).timespan());
    }

    vector<geo::ts_db_config_> server::do_get_geo_info() {
        unique_lock<mutex> sl(c_mx);// ensure we got geo stable
        vector<geo::ts_db_config_> r;r.reserve(geo.size());
        for(auto const &kv:geo)
            r.emplace_back(make_shared<geo::ts_db_config>(*kv.second));
        return r;
    }
    
    void server::add_geo_ts_db(geo::ts_db_config_ const &cfg) {
        if(!cfg) throw runtime_error("add_gs_ts_db must have a non-null argument");
        unique_lock<mutex> sl(c_mx);// ensure we got geo stable
        if(is_internal_geo(cfg->prefix)) {
            auto f=container_find(cfg->name,string(""));
            if(f == container.end()) 
                throw runtime_error("dtss.add_geo_ts_db: no internal ts-store container configured for name="+cfg->name);
            do_geo_ts_cfg_store(f->second->root_dir(),cfg);
        }
        geo[cfg->name] = std::make_shared<geo::ts_db_config>(*cfg);
    }
    
    void server::remove_geo_ts_db(std::string const& geo_db_name) {
        unique_lock<mutex> sl(c_mx);// ensure we got geo stable
        auto f=geo.find(geo_db_name);
        if( f!=geo.end()) {
            // remove any cache-entries
            flush_cache();// for now just empty all, later be specific, remove items selective
            auto gdb=f->second;//keep a ref
            geo.erase(geo_db_name);// get rid of the entry
            if(is_internal_geo(gdb->prefix)) { // possibly cleanup internal stuff, could take a loong time if a lot of files!
                its_db & ts_db=internal(f->second->name);
                fs::path gdb_root= fs::path(ts_db.root_dir())/f->second->name;
                fs::remove_all(gdb_root);
            }
        }// silently ignore attemt to remove non-existing geo_db_name ? The post-condition is the same
    }
    
    
    void server::do_geo_ts_cfg_store(std::string const&root,geo::ts_db_config_ const &gdb) {
        fs::path cfg_root_dir=fs::path(root)/gdb->name;
        if(!fs::exists(cfg_root_dir)) {
            fs::create_directory(cfg_root_dir);
        } else if(!fs::is_directory(cfg_root_dir)) {
            throw runtime_error("dtss::do_geo_ts_cfg_store: a file with name='"+gdb->name+"' blocks creation of geo root dir");
        }
        auto cfg_path= cfg_root_dir/geo_cfg_file;
        ofstream o(cfg_path.string(),std::ios::binary);
        core_oarchive oa(o,core_arch_flags);
        oa<<core_nvp("cfg",gdb);
    }
    
    void server::do_geo_store(std::string const& geo_db_name,geo::ts_matrix const& tsm, bool replace, bool cache) {
        using std::to_string;
        
        if(tsm.shape.size()==0)
            throw runtime_error("dtss geo store: no geo matrix data supplied");
        if(geo_db_name.size()==0)
            throw runtime_error("dtss geo store: no geo db-name is specified");
        shared_ptr<geo::ts_db_config> gdb;
        /** protected access on lockup,  copy the gdb-structure */{
            unique_lock<mutex> sl(c_mx);
            auto f=geo.find(geo_db_name);
            if( f==geo.end()) // here we could scan & load internal geo-db, instead of failing
                throw std::runtime_error("geo_evaluate: no geo-db configured for " + geo_db_name);
            
            gdb=make_shared<geo::ts_db_config>(*f->second);// make a copy we keep during the evaluation, considered cheap relative  other work
        }
        auto internal_gdb=is_internal_geo(gdb->prefix);
        if(!internal_gdb && !geo_store_cb) 
            throw runtime_error("dtss: no geo store callback set, geo-extensions not available");
        // verify correct shape, we need it twice, so a local lambda helps us doing that:
        auto verify_shape_vs_gdb=[](geo::detail::ix_calc const&shape,geo::ts_db_config const&gdb) {
            if(shape.n_v != gdb.variables.size())
                throw runtime_error("dtss geo store: variable dimension " + to_string(shape.n_v) + " differs from required " + to_string(gdb.variables.size()));
            if(int64_t(shape.n_e) != gdb.n_ensembles)
                throw runtime_error("dtss geo store: ensemble dimension " + to_string(shape.n_e) + " differs from required " + to_string(gdb.n_ensembles));
            if(shape.n_g != gdb.grid.points.size())
                throw runtime_error("dtss geo store: geo point  dimension " + to_string(shape.n_g) + " differs from required " + to_string(gdb.grid.points.size()));
        };
        verify_shape_vs_gdb(tsm.shape,*gdb);
        if(internal_gdb) do_internal_geo_store(gdb,tsm,replace);
        else             geo_store_cb(gdb,tsm,replace);// do the store operation here
        // figure out if we got more t0-versions, and update accordingly
        vector<utctime> t0x;t0x.reserve(tsm.shape.n_t0);
        auto t0_ta=gdb->t0_time_axis();
        for(size_t i=0;i<tsm.shape.n_t0;++i) {
            auto t0= gdb->get_associated_t0( tsm._ts(i,0,0,0).time(0),!replace); // important if concat (alias !replace)
            size_t t0_idx=t0_ta.index_of(t0);
            if(t0_idx==string::npos || t0_ta.time(t0_idx)!= t0) {
                t0x.push_back(t0);
            }
        }

        unique_lock<mutex> sl(c_mx);
        if(t0x.size()) { // new points arrived, we update the t0 of  geo-db using scoped lock
            auto f=geo.find(geo_db_name);
            if( f==geo.end()) throw std::runtime_error("geo_evaluate: geo-db removed during write for " + geo_db_name);
            // here we could compare f->second vs gdb  to ensure the defs is consistent
            verify_shape_vs_gdb(tsm.shape,*f->second);// relax requirement, just  verify equal in v,e,g dimensions, allow t0 changes.
            auto need_sort= f->second->t0_times.size()? t0x.front()<f->second->t0_times.back():false;
            for(auto t:t0x) f->second->t0_times.push_back(t);
            if(need_sort) {
                auto &t0v=f->second->t0_times;
                std::sort(t0v.begin(),t0v.end());
                auto last=std::unique(t0v.begin(),t0v.end());
                if(last!=t0v.end()) {
                    t0v.erase(last,t0v.end()); // get rid of possible duplicates when updating existing t0
                }
            }
            if(internal_gdb) {// if it's internal we take responsibility to store the most recent geo_ts_db_config
                do_geo_ts_cfg_store(internal(f->second->name).root_dir(),f->second);
            }
        }
        if(cache ||replace) { // if replace, flush old items.
            // update/replace cache items stored. 
            //  this might take some time for large payloads (like million)
            string ts_url;ts_url.reserve(500);// assumed reasonable max-limit(not hard, only performance)
            auto  ts_url_sink=std::back_inserter(ts_url);
            geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(gdb->prefix);
            geo::ts_id url_ix;url_ix.geo_db=geo_db_name;//assign only once, invariant for this query

            ts_cache.update(
                tsm.shape.size(),//  this is the number of items we request,
                [&tsm,&geo_ts_url_,&ts_url,&ts_url_sink,&url_ix,replace,&gdb](size_t i)->string { // this will be how we generate the ts-url
                    auto ix=tsm.shape.ix(i);
                    ts_url.resize(0);//resize,keep memory
                    url_ix.v=ix.v;
                    url_ix.e=ix.e;
                    url_ix.g=ix.g;
                    url_ix.t=gdb->get_associated_t0(tsm.tsv[i].time(0),!replace); // again .. ensure we use correct t0 in case of concat
                    boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                    return ts_url;// here we want a single copy string op(it has to be one)
                    
                },
                [&tsm](size_t i)->apoint_ts const& {// 
                    return tsm.tsv[i];
                },
                cache,
                false // hmm. we use replace as concat, so  let's just add to cache for now
            );

        }

    }

    /** simple pod */
    namespace {
    struct ts_store_pkg {
        string ts_name;
        gpoint_ts const * gts{nullptr};// not owning, scope-life time keept by store-worker
        bool replace{false};
    };
    }
    static size_t compute_n_workers(size_t n_ts) {
        auto hw_threads=std::thread::hardware_concurrency();
        if(hw_threads <2) hw_threads=2;
        return n_ts<hw_threads?n_ts:hw_threads;
    }
    
    void server::do_internal_geo_store(geo::ts_db_config_ cfg, geo::ts_matrix const&tsm,bool replace) {
        string ts_path;ts_path.reserve(100);
        auto  sink=std::back_inserter(ts_path);
        geo::ts_id tidx{cfg->name,0,0,0,utctime(0)};//reuse this in loop, reset ts_path
        geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
        its_db & ts_db=internal(cfg->name);//kept ref. hmm. could consider shared something here
        calendar utc;
        using boost::queue_op_status;
        using s_queue=boost::sync_bounded_queue<ts_store_pkg>;
        using std::async;
        using std::launch;
        size_t n_in_flight=compute_n_workers(tsm.shape.size());
        s_queue wq(n_in_flight*2);// add some slack in the queue 
        vector<std::future<int>> wt;
        for(size_t i=0;i<n_in_flight;++i) {
            wt.emplace_back(
                async(launch::async,
                    [&wq,&ts_db]()->int {// note, this is the logic for the queue worker thread
                        int error_count=0;
                        bool got_exception{false};
                        runtime_error re{""};
                        ts_store_pkg pkg;
                        while (queue_op_status::closed != wq.wait_pull_front(pkg)) {
                            
                            if(pkg.gts) {
                                try {// avoid harakiri and writer wait-forever on run-time errors, continue as long as possible
                                    ts_db.save(pkg.ts_name,pkg.gts->rep,pkg.replace);
                                } catch(runtime_error const &e) {
                                    ++error_count;
                                    re=e;
                                }
                            } else
                                ++error_count;// not very likely,
                        }
                        if(got_exception)
                            throw re;
                        return error_count;
                    }
                )
            );
        }
        
        for(size_t i=0;i<tsm.shape.n_t0;++i) {
            for(size_t v=0;v<tsm.shape.n_v;++v) {
                tidx.v=v;
                for(size_t e=0;e<tsm.shape.n_e;++e) {
                    tidx.e=e;
                    for(size_t g=0;g<tsm.shape.n_g;++g) {
                        ts_path.resize(0);// reset to 0, keep mem
                        tidx.g=g;
                        apoint_ts const &ts=tsm._ts(i,v,e,g);
                        auto gts = dynamic_cast<gpoint_ts const *>(ts.ts.get());
                        tidx.t=cfg->get_associated_t0(ts.time(0),!replace);// pr. def
                        generate(std::back_inserter(ts_path),url_gen,tidx);
                        wq.push_back(ts_store_pkg{ts_path,gts,replace});
                        //TODO: possible lock above,e.g. if the workers all quit!
                        //ts_db.save(ts_path,gts->rep,replace);
                    }
                }
            }
        }
        wq.close();// signal end of story
        // ensure to collect all errors/threads
        int sum_errors{0};
        bool got_exception{false};
        runtime_error re{""};// We accept that this one might be overwritten, last exception counts
        for(auto &w:wt) {
            try {
                sum_errors+=w.get();
            } catch( runtime_error const &e) { // is this ok for now?
                re=e;
                got_exception=true;
            } catch( std::exception const &e) {// if we get another exception.. should we just collect it, and 
                got_exception=true;
                re=runtime_error(e.what());
            } catch(...) {
                got_exception=true;
                re=runtime_error("unknown exception from storage-worker");
            }
        }
        if(got_exception)
            throw re;
        
    }
    namespace {
        /** simple pod for read queue operation */
        struct ts_read_pkg {
            string ts_name;
            apoint_ts * ats{nullptr};// target read assign, like *ats=read-result 
            utcperiod read_period;
        };
    }
    
    geo::ts_matrix server::do_internal_geo_read(geo::ts_db_config_ const &cfg,geo::slice const&gs) {
        geo::ts_matrix r=cfg->create_ts_matrix(gs);// create the result.
        string ts_path;ts_path.reserve(100);// we will regenerate this mill times, avoid malloc between
        auto  sink=std::back_inserter(ts_path);
        geo::ts_id tidx{cfg->name,0,0,0,utctime(0)};//reuse this in loop, reset ts_path
        geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
        its_db &ts_db=internal(cfg->name);// assume it succeeds here
        
        
        using boost::queue_op_status;
        using r_queue=boost::sync_bounded_queue<ts_read_pkg>;
        using std::async;
        using std::launch;
        size_t n_in_flight=compute_n_workers(gs.size());
        r_queue rq(n_in_flight*2);// add some slack in the queue 
        vector<std::future<int>> wt;
        for(size_t i=0;i<n_in_flight;++i) {
            wt.emplace_back(
                async(launch::async,
                    [&rq,&ts_db]()->int {
                        int error_count=0;
                        bool got_exception{false};
                        runtime_error re{""};
                        ts_read_pkg pkg;
                        while (queue_op_status::closed != rq.wait_pull_front(pkg)) {
                            
                            if(pkg.ats) {
                                try {// no harakiri on run-time errors, continue as long as possible
                                    apoint_ts rts(make_shared<const gpoint_ts>(ts_db.read(pkg.ts_name,pkg.read_period)));
                                    *pkg.ats = rts;// maybe direct inline expression above, or use move.
                                } catch(runtime_error const &e) {
                                    ++error_count;
                                    re=e;
                                }
                            } else
                                ++error_count;// not very likely,
                        }
                        if(got_exception)
                            throw re;
                        return error_count;
                    }
                )
            );
        }
        auto ts_dt=gs.ts_dt>utctime(0)?gs.ts_dt:cfg->dt;
        for(size_t i=0;i<gs.t.size();++i) {
            utcperiod read_period{gs.t[i],gs.t[i]+ts_dt};//maybe utc.add(ti,cfg->dt,1) to support yearly/monthly?
            // v,e,g dimensions share read-period
            tidx.t=gs.t[i];
            for(size_t v=0;v<gs.v.size();++v) {
                tidx.v=gs.v[v];
                for(size_t e=0;e<gs.e.size();++e) {
                    tidx.e=gs.e[e];
                    for(size_t g=0;g<gs.g.size();++g) {
                        ts_path.resize(0);// reset to 0, keep mem
                        tidx.g=gs.g[g];
                        generate(std::back_inserter(ts_path),url_gen,tidx);
                        rq.push_back(ts_read_pkg{ts_path,r._ts_pointer(i,v,e,g),read_period});
                    }
                }
            }
        }
        rq.close();// signal end of story, so that our read-workers quits the loop
        // ensure to collect all errors/threads
        int sum_errors{0};
        bool got_exception{false};
        runtime_error re{""};// We accept that this one might be overwritten, last exception counts
        for(auto &w:wt) {
            try {
                sum_errors+=w.get();
            } catch( runtime_error const &e) { // is this ok for now?
                re=e;
                got_exception=true;
            } catch( std::exception const &e) {// if we get another exception.. should we just collect it, and 
                got_exception=true;
                re=runtime_error(e.what());
            } catch(...) {
                got_exception=true;
                re=runtime_error("unknown exception from read-worker");
            }
        }
        if(got_exception)
            throw re;

        return r;
    }
    
    std::unordered_map<std::string,geo::ts_db_config_ > server::geo_ts_db_scan(fs::path root) const {
        std::unordered_map<std::string,geo::ts_db_config_ > r;
        vector<fs::path> bad_cfg_files;
        if(fs::exists(root) && fs::is_directory(root)) {
            for(auto x:fs::directory_iterator(root)) {
                if(fs::is_directory(x.path())) {
                    auto cfg_file=x.path()/geo_cfg_file;
                    if(fs::exists(cfg_file) && fs::is_regular_file(cfg_file)) {
                        try {
                            ifstream is(cfg_file.c_str(),std::ios::binary);
                            core_iarchive ia(is,core_arch_flags);
                            geo::ts_db_config_ cfg;
                            ia>>cfg;
                            if(cfg)
                                r[cfg->name]=cfg;
                            else
                                bad_cfg_files.push_back(cfg_file);// can read it, but it's content is null
                        } catch(...) {
                            bad_cfg_files.push_back(cfg_file);// just collect the bad-stuff
                        }
                    }
                }
            }
        }
        //TODO: figure out what to do (rename, report?) with the bad_cfg_files
        return r;
    }

}
