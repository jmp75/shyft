/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

//#include <shyft/py/energy_market/stm/expose_str.h> // No; gcc reports "specialization after instantiation"
#include <shyft/py/api/expose_str.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <boost/format.hpp>

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;

    using std::string;
    using std::to_string;

    using boost::format;

    template<>
    string str_(utctime const& o) {
        return calendar().to_string(o);
    }

    template<>
    string str_(utcperiod const& per) {
        return per.to_string();
    }

    template<>
    string str_(const generic_dt& ta) {
        string r("TimeAxis(");
        switch (ta.gt) {
        case generic_dt::generic_type::FIXED:
            r += "fixed,";
            break;
        case generic_dt::generic_type::CALENDAR:
            r += "calendar,";
            break;
        case generic_dt::generic_type::POINT:
            r += "point,";
            break;
        }
        r += str_(ta.total_period());
        return r + ")";
    }

    template<> string str_(apoint_ts const& o) {
        return o.stringify();
    }
    template<>
    string str_(const hydro_power::point &p) {
        return "("+to_string(p.x)+","+to_string(p.y)+")";
    }
    template<>
    string str_(const hydro_power::xy_point_curve& m) {
        return str_(m.points);
    }
    
    template<>
    string str_(const stm::t_xy_::element_type& m) {
        string r("{");
        calendar utc;
        for(const auto&i:m) {
            r+= "\n\t"+utc.to_string(i.first) + ": ";
            if(i.second->points.size()>0) {
                r+="[";
                for(const auto&p:i.second->points)
                    r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                r+="],";
            } else {
                r+="[],";
            }
        }
        return r+(r.size()>1?"\n}":"}");
    }

    template<>
    string str_(const stm::t_xyz_::element_type& m) {
        string r("{");
        calendar utc;
        for(const auto&i:m) {
            r+= "\n\t"+utc.to_string(i.first) + ": z"+to_string(i.second->z);
            if(i.second->xy_curve.points.size()>0) {
                r+="[";
                for(const auto&p:i.second->xy_curve.points)
                    r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                r+="],";
            } else {
                r+="[],";
            }
        }
        return r+(r.size()>1?"\n}":"}");
    }

    template<>
    string str_(stm::xyz_point_curve_list::value_type const& o) {
        string r;
        r += "{z@"+to_string(o.z) + ": ";
        if (o.xy_curve.points.size()>0) {
            r += "[";
            for(const auto&p:o.xy_curve.points)
                r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
            r += "]";
        } else {
            r += "[]";
        }
        return r+"}";
    }

    template<>
    string str_(const stm::t_xyz_list_::element_type& m) {
        string r("{");
        calendar utc;
        for(const auto&i:m) {
            r += "\n\t" + utc.to_string(i.first) + ": {";
            auto s = r.size();
            for(const auto&j:*i.second) {
                r += "\n\t\tz@"+to_string(j.z) + ": ";
                if (j.xy_curve.points.size()>0) {
                    r += "[";
                    for(const auto&p:j.xy_curve.points)
                        r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                    r += "],";
                } else {
                    r += "[],";
                }
            }
            r += r.size() > s ? "\n\t}" : "}";
        }
        return r+(r.size()>1?"\n}":"}");
    }
    template<> 
    string str_(const hydro_power::turbine_description &m) {
        string r("{");
        if(m.efficiencies.size()>1) {
            for (auto j=0u;j<m.efficiencies.size();++j) {
                const auto& eff = m.efficiencies[j];
                r += "\n\t\t" + to_string(j+1) + ": {";
                r += "\n\t\t\tmin@" + to_string(eff.production_min) + ",";
                r += "\n\t\t\tmax@" + to_string(eff.production_max) + ",";
                for(const auto&ec:eff.efficiency_curves) {
                    r += "\n\t\t\tz@"+to_string(ec.z) + ": ";
                    if (ec.xy_curve.points.size()>0) {
                        r += "[";
                        for(const auto&p:ec.xy_curve.points)
                            r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                        r += "],";
                    } else {
                        r += "[],";
                    }
                }
                r += "\n\t\t},";
            }
        } else if(m.efficiencies.size()>0) {
            const auto& eff = m.efficiencies.front();
            for(const auto&ec:eff.efficiency_curves) {
                r += "\n\t\tz@"+to_string(ec.z) + ": ";
                if (ec.xy_curve.points.size()>0) {
                    r += "[";
                    for(const auto&p:ec.xy_curve.points)
                        r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                    r += "],";
                } else {
                    r += "[],";
                }
            }
        }

        r+="}";
        return r;
    }
    
    template<>
    string str_(const stm::t_turbine_description_::element_type& m) {
        string r("{");
        calendar utc;
        for (const auto& i : m) {
            r += "\n\t" + utc.to_string(i.first) + ": {";
            auto s = r.size();
            if(i.second->efficiencies.size()>1) {
                for (auto j=0u;j<i.second->efficiencies.size();++j) {
                    const auto& eff = i.second->efficiencies[j];
                    r += "\n\t\t" + to_string(j+1) + ": {";
                    r += "\n\t\t\tmin@" + to_string(eff.production_min) + ",";
                    r += "\n\t\t\tmax@" + to_string(eff.production_max) + ",";
                    for(const auto&ec:eff.efficiency_curves) {
                        r += "\n\t\t\tz@"+to_string(ec.z) + ": ";
                        if (ec.xy_curve.points.size()>0) {
                            r += "[";
                            for(const auto&p:ec.xy_curve.points)
                                r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                            r += "],";
                        } else {
                            r += "[],";
                        }
                    }
                    r += "\n\t\t},";
                }
            } else if(i.second->efficiencies.size()>0) {
                const auto& eff = i.second->efficiencies.front();
                for(const auto&ec:eff.efficiency_curves) {
                    r += "\n\t\tz@"+to_string(ec.z) + ": ";
                    if (ec.xy_curve.points.size()>0) {
                        r += "[";
                        for(const auto&p:ec.xy_curve.points)
                            r+= "("+to_string(p.x)+","+to_string(p.y)+"),";
                        r += "],";
                    } else {
                        r += "[],";
                    }
                }
            }
            r += r.size() > s ? "\n\t}" : "}";
        }
        return r + (r.size() > 1 ? "\n}" : "}");
    }

    template<>
    string str_(const absolute_constraint& m) {
        return (format("AbsoluteConstraint(limit=%1%, flag=%2%)")%m.limit.stringify()%m.flag.stringify()).str();
    }

    template<>
    string str_(const penalty_constraint& m) {
        return (format("PenaltyConstraint(limit=%1%, flag=%2%, cost=%3%, penalty=%4%)")%m.limit.stringify()%m.flag.stringify()%m.cost.stringify()%m.penalty.stringify()).str();
    }

}
