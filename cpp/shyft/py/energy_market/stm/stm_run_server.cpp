#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/scoped_gil.h>

#include <shyft/energy_market/stm/srv/run/stm_run.h>
#include <shyft/energy_market/stm/srv/run/client.h>
#include <shyft/energy_market/stm/srv/run/server.h>
#include <shyft/web_api/energy_market/stm/run/request_handler.h>

namespace shyft::energy_market::stm::srv {
    using shyft::py::scoped_gil_release;
    using shyft::py::scoped_gil_aquire;
    using boost::python::class_;
    using boost::python::bases;
    namespace py=boost::python;

    using std::mutex;
    using std::unique_lock;
    using shyft::py::energy_market::py_server_with_web_api;
    using shyft::py::energy_market::py_client;
    using shyft::web_api::energy_market::stm::run::request_handler;

    struct py_run_client : py_client<run::client> {
        using super = py_client<run::client>;
        py_run_client(const std::string& host_port, int timeout_ms): super(host_port, timeout_ms) {}

        void add_run(int64_t mid, const stm_run_& run) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.add_run(mid, run);
        }

        bool remove_run(int64_t mid, int64_t rid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_run(mid, rid);
        }

        bool remove_run(int64_t mid, const std::string& rname) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_run(mid, rname);
        }

        stm_run_ get_run(int64_t mid, int64_t rid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_run(mid, rid);
        }

        stm_run_ get_run(int64_t mid, const string& rname) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_run(mid, rname);
        }

        void add_model_ref(int64_t mid, int64_t rid, const model_ref_& mr) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.add_model_ref(mid, rid, mr);
        }

        bool remove_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model_ref(mid, rid, mkey);
        }

        model_ref_ get_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_ref(mid, rid, mkey);
        }
    };

    struct py_run_server : py_server_with_web_api<run::server, request_handler> {
        using super = py_server_with_web_api<run::server, request_handler>;

        py_run_server(const string& root_dir): super(root_dir) {}

    };
}

namespace expose {
    namespace py = boost::python;

    using std::string;
    using std::shared_ptr;

    using namespace shyft::energy_market::stm::srv;

    void stm_run_server() {
        // Run info
        class_<model_ref, bases<>, shared_ptr<model_ref>>("StmModelRef",
            doc_intro("Reference to a model, and where to find it"),
            py::init<string, int, int, string>((py::arg("self"), py::arg("host"), py::arg("port_num"), py::arg("api_port_num"), py::arg("model_key")),
                doc_intro("Create a run info")
                doc_parameters()
                doc_parameter("host", "str", "Where the referenced model is stored")
                doc_parameter("port_num", "int", "At what port number to interface with the server")
                doc_parameter("api_port_num", "int", "At what port number to interface with the server using the web API.")
                doc_parameter("model_key", "str", "The model key the referenced model is stored under"))
            )
            .def(py::init<>((py::arg("self")), doc_intro("Default constructor for StmRunInfo")))
            .def_readwrite("host", &model_ref::host, "Where model is stored")
            .def_readwrite("port_num", &model_ref::port_num, "Port number to interface with server.")
            .def_readwrite("api_port_num", &model_ref::api_port_num, "Port number to interface with server through web API.")
            .def_readwrite("model_key", &model_ref::model_key, "The model key the referenced model is stored under")
            .def(py::self == py::self)
            .def(py::self != py::self)
            ;

        using ModelRefList = vector<shared_ptr<model_ref>>;
        class_<ModelRefList>("ModelRefList", "A strongly typed list of StmModelRef")
            .def(py::vector_indexing_suite<ModelRefList, true>())
            ;

        // Expose StmRun:
        class_<stm_run, py::bases<>, shared_ptr<stm_run>>(
            "StmRun",
            doc_intro("Provided a run concept for Stm. Can contain a number of models, as well as input- and result dataset related to e.g. a task")
            )
            .def(py::init<int64_t, string const&, utctime, py::optional<string, vector<string>, vector<model_ref_>>>(
                (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("created"),
                    py::arg("json")=string{""}, py::arg("labels")=vector<string>(), py::arg("model_refs")=vector<shared_ptr<model_ref>>()),
                doc_intro("Create an stm run")
                )
            )
            .def_readwrite("id", &stm_run::id, "The unique run ID")
            .def_readwrite("name", &stm_run::name, "any useful name of description")
            .def_readwrite("created", &stm_run::created, "the time of creation, or last modification")
            .def_readwrite("json", &stm_run::json, "a json formatted string with miscellaneous data")
            .def_readwrite("labels", &stm_run::labels, "A set of labels for the run")
            .def_readonly("model_refs", &stm_run::model_refs, "Set of model references that are related to run")
            .def(py::self == py::self)
            .def(py::self != py::self)
            .def("add_model_ref", &stm_run::add_model_ref, (py::arg("self"), py::arg("mr")),
                doc_intro("Add a model reference to the run")
                doc_parameters()
                doc_parameter("mr", "StmModelRef", "The model reference to add")
            )
            .def("remove_model_ref", &stm_run::remove_model_ref, (py::arg("self"), py::arg("mkey")),
                doc_intro("Remove a model reference from the run")
                doc_parameters()
                doc_parameter("mkey", "str", "The model key the reference has stored. Cf. StmModelRef.model_key")
                doc_returns("success", "Boolean", "True if model reference was successfully removed. False if run did not contain model reference with given model key")
            )
            .def("get_model_ref", &stm_run::get_model_ref, (py::arg("self"), py::arg("mkey")),
                doc_intro("Get model reference in run based on model key")
                doc_parameters()
                doc_parameter("mkey", "str", "The model key the reference has stored. Cf. StmModelRef.model_key")
                doc_returns("mr", "StmModelRef", "Requested model reference. None if not found in run.")
            )
            ;

        using StmRunVector = vector<shared_ptr<stm_run>>;
        class_<StmRunVector>("StmRunVector", "A strongly typed list, vector, of StmRuns")
            .def(py::vector_indexing_suite<StmRunVector, true>())
        ;

        // Expose StmSession:
        class_<stm_session, py::bases<>, shared_ptr<stm_session>>(
            "StmSession",
            doc_intro("Session concept for Stm. Can contain a number of runs.")
            doc_intro("Defined through its set of labels, a base model, which should relate to child runs and model_refs,")
            doc_intro("and a task name.")
            )
            .def(py::init<int64_t, const string&, utctime, py::optional<string, vector<string>, vector<stm_run_>, model_ref, string>>(
                (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("created"), py::arg("json")=string{""},
                    py::arg("labels")=vector<string>(), py::arg("runs")=vector<stm_run_>(), py::arg("base_model")=model_ref(),
                    py::arg("task_name")=string{""}),
                doc_intro("Create an stm session.")
                )
            )
            .def_readwrite("id", &stm_session::id, "The unique session ID")
            .def_readwrite("name", &stm_session::name, "Any useful name or description")
            .def_readwrite("created", &stm_session::created, "The time of creation, or last modification")
            .def_readwrite("json", &stm_session::json, "a json formatted string with miscellaneous data")
            .def_readwrite("labels", &stm_session::labels, "A set of labels for the session")
            .def_readonly("runs", &stm_session::runs, "Set of runs connected to session")
            .def_readwrite("base_model", &stm_session::base_mdl, "Base model, which should correspond to models used in runs.")
            .def_readwrite("task_name", &stm_session::task_name, "Name of task connected to session")
            .def(py::self == py::self)
            .def(py::self != py::self)
            .def("add_run", &stm_session::add_run, (py::arg("self"), py::arg("run")),
                doc_intro("Add a run to the session")
                doc_parameters()
                doc_parameter("run", "StmRun", "The run instance to add to the session")
                doc_returns("None", "None", "Returns nothing"))
            .def("remove_run", static_cast<bool (stm_session::*)(int64_t)>(&stm_session::remove_run),
                 (py::arg("self"), py::arg("id")), doc_intro("Remove a run from session based on id"))
            .def("remove_run", static_cast<bool (stm_session::*)(const string&)>(&stm_session::remove_run),
                 (py::arg("self"), py::arg("name")), doc_intro("Remove a run from session based on name"))
            .def("get_run", static_cast<stm_run_ (stm_session::*)(int64_t)>(&stm_session::get_run),
                 (py::arg("self"), py::arg("rid")),
                 doc_intro("Get run from session based on ID")
                 doc_parameters()
                 doc_parameter("rid", "int", "Id of run you want to get.")
                 doc_returns("run", "StmRun", "Request run, None if not found"))
            .def("get_run", static_cast<stm_run_ (stm_session::*)(const string&)>(&stm_session::get_run),
                 (py::arg("self"), py::arg("rname")),
                 doc_intro("Get run based on name")
                 doc_parameters()
                 doc_parameter("rname", "str", "Name of run you want to get")
                 doc_returns("run", "StmRun", "Request run, None if not found."))
            ;
        using StmSessionVector = vector<shared_ptr<stm_session>>;
        class_<StmSessionVector>("StmSessionVector", "A strongly typed list, vector, of StmSessions")
            .def(py::vector_indexing_suite<StmSessionVector, true>())
            ;

        // Server- and client:
        auto session_client = shyft::py::energy_market::expose_client<py_run_client>("StmRunClient",
            "The client api for the Stm run repository");
        session_client.def("add_run", &py_run_client::add_run,
                (py::arg("self"),py::arg("mid"),py::arg("run")),
                doc_intro("Add a run to a session on server")
                doc_parameters()
                doc_parameter("mid", "int", "session id to add a run to")
                doc_parameter("run", "StmRun", "The run to add to the session")
                doc_raises()
                doc_raise("RuntimeError", "If provided mid is an invalid session id")
                doc_raise("RuntimeError", "If provided run has the same id or name as a run already in session")
            )
            .def("remove_run", static_cast< bool(py_run_client::*)(int64_t, int64_t) >(&py_run_client::remove_run),
                (py::arg("self"),py::arg("mid"), py::arg("rid")),
                doc_intro("Remove a run from a session on server")
                doc_parameters()
                doc_parameter("mid", "int", "session id to remove run from")
                doc_parameter("rid", "int", "id of run to remove")
                doc_returns("success", "Boolean", "True if run was successfully removed from session")
            )
            .def("remove_run", static_cast< bool(py_run_client::*)(int64_t, const string&) >(&py_run_client::remove_run),
                 (py::arg("self"), py::arg("mid"), py::arg("rname")),
                 doc_intro("Remove run from a session on server based on run name")
                 doc_parameters()
                 doc_parameter("mid", "int", "session id to remove run from")
                 doc_parameter("rname", "str", "name of run to remove")
                 doc_returns("success", "Boolean", "True if run was successfully removed from session")
            )
            .def("get_run", static_cast< stm_run_ (py_run_client::*)(int64_t, int64_t)>(&py_run_client::get_run),
                 (py::arg("self"), py::arg("mid"), py::arg("rid")),
                 doc_intro("Get run from session based on run's id")
                 doc_parameters()
                 doc_parameter("mid", "int", "session ID")
                 doc_parameter("rid", "int", "run ID")
                 doc_returns("run", "StmRun", "Requested run, or None if not found")
            )
            .def("get_run", static_cast< stm_run_ (py_run_client::*)(int64_t, const string&)>(&py_run_client::get_run),
                 (py::arg("self"), py::arg("mid"), py::arg("rname")),
                 doc_intro("Get from session based on run's name")
                 doc_parameters()
                 doc_parameter("mid", "int", "session ID")
                 doc_parameter("rname", "str", "run name")
                 doc_parameter("run", "StmRun", "Requested run, or None if not found.")
            )
            .def("add_model_ref", &py_run_client::add_model_ref,
                 (py::arg("self"), py::arg("mid"), py::arg("rid"), py::arg("mr")),
                 doc_intro("Add a model reference to a run on server")
                 doc_parameters()
                 doc_parameter("mid", "int", "Session ID")
                 doc_parameter("rid", "int", "run ID")
                 doc_parameter("mr", "StmModelRef", "The model reference to add to run with ID=rid, contained in session with ID=mid")
            )
            .def("remove_model_ref", &py_run_client::remove_model_ref,
                (py::arg("self"), py::arg("mid"), py::arg("rid"), py::arg("mkey")),
                doc_intro("Remove a model reference from a run contained in a session")
                doc_parameters()
                doc_parameter("mid", "int", "session ID")
                doc_parameter("rid", "int", "run ID")
                doc_parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key")
                doc_returns("success", "Boolean", "True if model reference was successfully removed, False otherwise")
            )
            .def("get_model_ref", &py_run_client::get_model_ref,
                (py::arg("self"), py::arg("mid"), py::arg("rid"), py::arg("mkey")),
                doc_intro("Get a model reference stored in a run contained in a session")
                doc_parameters()
                doc_parameter("mid", "int", "session ID")
                doc_parameter("rid", "int", "run ID")
                doc_parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key")
                doc_returns("mr", "StmModelRef", "Requested model reference. None if run wasn't found or didn't contain requested model reference")
            )
            ;

        //using run_srv_t = shyft::py::energy_market::py_server_with_web_api<shyft::energy_market::stm::srv::run::server,
        //    shyft::web_api::energy_market::stm::run::request_handler>;
        shyft::py::energy_market::expose_server_with_web_api<py_run_server>("StmRunServer",
            "The server-side component for the STM run repository.");
    }
}
