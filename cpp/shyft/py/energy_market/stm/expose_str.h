/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/py/api/expose_str.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;

    using std::string;

    extern template string str_(utctime const& o);
    extern template string str_(utcperiod const& per);
    extern template string str_(const generic_dt& ta);
    extern template string str_(apoint_ts const& o);
    extern template string str_(const hydro_power::xy_point_curve& m);
    extern template string str_(const hydro_power::point &m);
    extern template string str_(const hydro_power::turbine_description &m);
    extern template string str_(const stm::t_xy_::element_type& m);
    extern template string str_(const stm::t_xyz_::element_type& m);
    extern template string str_(stm::xyz_point_curve_list::value_type const& o);
    extern template string str_(const stm::t_xyz_list_::element_type& m);
    extern template string str_(stm::xyz_point_curve_list const& o);
    extern template string str_(const stm::t_turbine_description_::element_type& m);
    extern template string str_(const absolute_constraint& m);
    extern template string str_(const penalty_constraint& m);
}
