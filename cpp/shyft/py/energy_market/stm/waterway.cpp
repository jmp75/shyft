/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::waterway::discharge_ const& o) {
        return (format("Waterway._Discharge(result=%1%, static_max=%2%)")
            %str_(o.result)
            %str_(o.static_max)
        ).str();
    }

    template<> string str_(stm::waterway::geometry_ const& o) {
        return (format("Waterway._Geometry(length=%1%, diameter=%2%, z0=%3%, z1=%4%)")
            %str_(o.length)
            %str_(o.diameter)
            %str_(o.z0)
            %str_(o.z1)
        ).str();
    }

    template<> string str_(stm::waterway const& o) {
        return (format("Waterway(id=%1%, name=%2%, geometry=%3%)")
            %str_(o.id)
            %str_(o.name)
            %str_(o.geometry)
        ).str();
    }

    void stm_waterway() {
        auto w=py::class_<
            stm::waterway,
            py::bases<hydro_power::waterway>,
            shared_ptr<stm::waterway>,
            boost::noncopyable
        >("Waterway", "Stm waterway.", py::no_init);
        w
            .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                "Create waterway with unique id and name for a hydro power system."))
            .def_readonly("geometry", &stm::waterway::geometry, "Geometry attributes")
            .def_readonly("discharge", &stm::waterway::discharge, "Discharge attributes")
            .add_property("tag", +[](const stm::waterway& self){return url_tag(self);}, "url tag")
            .def("add_gate",
                +[](std::shared_ptr<stm::waterway>& self, int uid, const string& name, const string& json) -> stm::gate_ {
                    auto hps = std::static_pointer_cast<stm::stm_hps>(self->hps.lock());
                    stm::gate_ gate = stm::stm_hps_builder(hps).create_gate(uid, name, json);
                    stm::waterway::add_gate(self, gate);
                    return gate;
                },
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                "Create and add a new gate to the waterway.")
            .def("add_gate",
                +[](std::shared_ptr<stm::waterway>& self, std::shared_ptr<stm::gate>& gate) -> stm::gate_ {
                    stm::waterway::add_gate(self, gate);
                    return gate;
                },
                (py::arg("self"), py::arg("gate")),
                "Add an existing gate to the waterway.")

            .def("__eq__", &stm::waterway::operator==)
            .def("__ne__", &stm::waterway::operator!=)

            .def("flattened_attributes", +[](stm::waterway& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes")
        ;
        expose_str_repr(w);
        add_proxy_property(w,"head_loss_coeff", stm::waterway,head_loss_coeff, "Loss factor, time-dependent attribute.")
        add_proxy_property(w,"head_loss_func", stm::waterway,head_loss_func, "Loss function, time-dependent attribute.")
        {
            py::scope w_scope=w;
            auto wd=py::class_<stm::waterway::discharge_, boost::noncopyable>("_Discharge", py::no_init);
            expose_str_repr(wd);
            _add_proxy_property(wd,"static_max", stm::waterway::discharge_,static_max, "[m3/s] Discharge maximum, time-dependent attribute.")
            _add_proxy_property(wd,"result", stm::waterway::discharge_,result, "[m3/s] Discharge result, time series.")
            

            auto wg=py::class_<stm::waterway::geometry_, boost::noncopyable>("_Geometry", py::no_init);
            expose_str_repr(wg);
            _add_proxy_property(wg, "length", stm::waterway::geometry_, length, "[m] Tunnel length, time-dependent attribute.")
            _add_proxy_property(wg, "diameter", stm::waterway::geometry_, diameter, "[m] Tunnel diameter, time-dependent attribute.")
            _add_proxy_property(wg, "z0", stm::waterway::geometry_, z0, "[masl] Tunnel inlet level, time-dependent attribute.")
            _add_proxy_property(wg, "z1", stm::waterway::geometry_, z1, "[masl] Tunnel outlet level, time-dependent attribute.")
            ;
        }
    }
}
