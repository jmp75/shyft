/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>

#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::energy_market_area const& o) {
        return (format("MarketArea(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    template<> string str_(stm::stm_system const& o) {
        return (format("StmSystem(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    struct stm_sys_ext {
        static std::vector<char> to_blob(const  stm::stm_system_& m) {
            auto s=shyft::energy_market::stm::stm_system::to_blob(m);
            return std::vector<char>(s.begin(),s.end());
        }
        static stm::stm_system_ from_blob(std::vector<char>& blob) {
            std::string s(blob.begin(),blob.end());
            return shyft::energy_market::stm::stm_system::from_blob(s);
        }
    };

    
    void stm_system() {
        auto ma=py::class_<
            stm::energy_market_area,
            py::bases<>,
            shared_ptr<stm::energy_market_area>,
            boost::noncopyable
        >("MarketArea", 
            doc_intro("A market area, with load/price and other properties.")
            doc_details("Within the market area, there are zero or more energy-\n"
                        "producing/consuming units.")
            , py::no_init);
        ma
            .def(py::init<int, const string&, const string&, stm::stm_system_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("stm_sys")),
                "Create market area for a stm system."))
            .def_readwrite("id",&stm::energy_market_area::id,"Unique id for this object.")
            .def_readwrite("name",&stm::energy_market_area::name,"Name for this object.")
            .def_readwrite("json",&stm::energy_market_area::json,"Json keeping any extra data for this object.")
        ;
        expose_str_repr(ma);
        add_proxy_property(ma,"load", stm::energy_market_area,load, "[W] Load, time-series.")
        add_proxy_property(ma,"price", stm::energy_market_area,price, "[Money/J] Price, time series.")
        add_proxy_property(ma,"max_buy", stm::energy_market_area,max_buy, "[W] Maximum buy, time series.")
        add_proxy_property(ma,"max_sale", stm::energy_market_area,max_sale, "[W] Maximum sale, time series.")
        add_proxy_property(ma,"buy_result", stm::energy_market_area,buy, "[W] Buy result, time-series.")
        add_proxy_property(ma,"sale_result", stm::energy_market_area,sale, "[W] Sale result, time series.")

        using  MarketAreaList=std::vector<stm::energy_market_area_>;
        auto mal=py::class_<MarketAreaList>("MarketAreaList", "A strongly typed list of MarketArea.")
            .def(py::vector_indexing_suite<MarketAreaList, true>())
        ;
        expose_str_repr(mal);

        auto ss= py::class_<
            stm::stm_system,
            py::bases<>,
            shared_ptr<stm::stm_system>,
            boost::noncopyable
        >("StmSystem", "A complete stm system, with market areas, and hydro power systems.", py::no_init);
        ss
            .def(py::init<int, const string&, const string&>(
                (py::arg("uid"), py::arg("name"), py::arg("json")), "Create stm system."))
            .def_readwrite("id",&stm::stm_system::id,"Unique id for this object.")
            .def_readwrite("name",&stm::stm_system::name,"Name for this object.")
            .def_readwrite("json",&stm::stm_system::json,"Json keeping any extra data for this object.")
            .def_readonly("market_areas",&stm::stm_system::market,"List of market areas.")
            .def_readonly("hydro_power_systems",&stm::stm_system::hps,"List of hydro power systems.")
            .def_readonly("run_parameters", &stm::stm_system::run_params, "Run parameters")
            .def("to_blob",&stm_sys_ext::to_blob,(py::arg("self")),
                doc_intro("Serialize the model to a blob.")
                doc_returns("blob","ByteVector","Blob form of the model.")
            )
            .def("from_blob",&stm_sys_ext::from_blob,(py::arg("blob")),
                doc_intro("Re-create a stm system from a previously create blob.")
                doc_returns("stm_sys","StmSystem","A stm system including hydro-power-systems and markets.")
            ).staticmethod("from_blob")
        ;
        expose_str_repr(ss);

        using StmSystemList=std::vector<shared_ptr<stm::stm_system>>;
        auto ssl=py::class_<StmSystemList>("StmSystemList")
            .def(py::vector_indexing_suite<StmSystemList, true>())
        ;
        expose_str_repr(ssl);
   }
}
