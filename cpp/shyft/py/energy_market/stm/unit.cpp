/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>

#include <boost/format.hpp>


namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::unit::discharge_::constraint_ const& o) {
        return (format("Unit._Discharge._Constraint(min=%1%, max=%2%)")
            %str_(o.min)
            %str_(o.max)
        ).str();
    }

    template<> string str_(stm::unit::cost_ const& o) {
        return (format("Unit._Cost(start=%1%, stop=%2%)")
            %str_(o.start)
            %str_(o.stop)
        ).str();
    }

    template<> string str_(stm::unit::production_::constraint_ const& o) {
        return (format("Unit._Production._Constraint(min=%1%, max=%2%)")
            %str_(o.min)
            %str_(o.max)
        ).str();
    }
  
    template<> string str_(stm::unit::discharge_ const& o) {
        return (format("Unit._Discharge(schedule=%1%, result=%2%, constraint=%3%)")
            %str_(o.schedule)
            %str_(o.result)
            %str_(o.constraint)
        ).str();
    }

    template<> string str_(stm::unit::production_ const& o) {
        return (format("Unit._Production(schedule=%1%, result=%2%, constraint= %3%, realised= %4%, static_min=%5%, static_max=%6%)")
            %str_(o.schedule)
            %str_(o.result)
            %str_(o.constraint)
            %str_(o.realised)
            %str_(o.static_min)
            %str_(o.static_max)
        ).str();
    }

    template<> string str_(stm::unit const& o) {
        return (format("Unit(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

 

    
    /**
     */
    
    //-- end play-ground
    
    void stm_unit() {
        // 
        // get type set from accessors ->
        // for_each(...)

        
        auto u=py::class_<
            stm::unit,
            py::bases<hydro_power::unit>,
            shared_ptr<stm::unit>,
            boost::noncopyable
        >("Unit", "Stm unit(turbine and generator assembly).", py::no_init);
        u
            .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                "Create unit with unique id and name for a hydro power system."))
            .def_readonly("discharge", &stm::unit::discharge, "Discharge attributes")
            .def_readonly("production", &stm::unit::production, "Production attributes")
            .def_readonly("cost", &stm::unit::cost, "Cost attributes")
            .add_property("tag", +[](const stm::unit& self){return url_tag(self);}, "url tag")

            .def("__eq__", &stm::unit::operator==)
            .def("__ne__", &stm::unit::operator!=)

            .def("flattened_attributes", +[](stm::unit& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes")
        ;
        expose_str_repr(u);
        add_proxy_property(u,"generator_description",stm::unit,generator_description, 
            doc_intro("Generator efficiency curve, time-dependent attribute.")
        )
        add_proxy_property(u,"turbine_description",stm::unit,turbine_description,
            doc_intro("This is the turbine time-dependent description of turbine efficiency") 
        )
        add_proxy_property(u,"unavailability",stm::unit,unavailability,
            doc_intro("1 if not available, nan,0 means available?")
        )
        

        using UnitList=std::vector<stm::unit_>;
        auto ul=py::class_<UnitList>("UnitList", "A strongly typed list of Units.");
        ul
            .def(py::vector_indexing_suite<UnitList, true>())
        ;
        expose_str_repr(ul);
        {
            py::scope scope_unit=u;
            auto ud=py::class_<stm::unit::discharge_, py::bases<>, boost::noncopyable>(
                "_Discharge", 
                doc_intro("Unit.Discharge attributes, flow[m3/s]"),
                py::no_init
            );
            ud
                .def_readonly("constraint", &stm::unit::discharge_::constraint, "Constraint group")
            ;
            expose_str_repr(ud);
            _add_proxy_property(ud,"schedule", stm::unit::discharge_,schedule, "Discharge schedule, time series.")
            _add_proxy_property(ud,"result", stm::unit::discharge_,result, "Discharge result, time series.")

            auto up=py::class_<stm::unit::production_, py::bases<>, boost::noncopyable>(
                "_Production",
                doc_intro("Unit.Production attributes, effect[W]"), 
                py::no_init
            );
            up
                .def_readonly("constraint", &stm::unit::production_::constraint, "Constraint group")
            ;
            expose_str_repr(up);

            _add_proxy_property(up,"result",stm::unit::production_,result,
                    doc_intro("[W] Production result, time series.")
            );
            _add_proxy_property(up,"schedule", stm::unit::production_,schedule, "Production schedule, time series.")
            _add_proxy_property(up,"static_min", stm::unit::production_,static_min, "Production minimum, time-dependent attribute.")
            _add_proxy_property(up,"static_max", stm::unit::production_,static_max, "Production maximum, time-dependent attribute.")
            _add_proxy_property(up,"realised", stm::unit::production_,realised, "Historical production, time series.")

            

            auto uc=py::class_<stm::unit::cost_, py::bases<>, boost::noncopyable>(
                "_Cost",
                doc_intro("Unit.Cost contain the start/stop costs"),
                py::no_init
            );
            expose_str_repr(uc);
            _add_proxy_property(uc,"start", stm::unit::cost_,start, "Start cost, time series.")
            _add_proxy_property(uc,"stop", stm::unit::cost_,stop, "Stop cost, time series.")
            {
                py::scope ud_scope=ud;
                auto udc=py::class_<stm::unit::discharge_::constraint_, py::bases<>, boost::noncopyable>(
                    "_Constraint", 
                    doc_intro("Constraints and limitations to the Unit-flow"),
                    py::no_init
                );
                expose_str_repr(udc);
                _add_proxy_property(udc,"min", stm::unit::discharge_,constraint_::min, "Discharge constraint minimum, time-series")
                _add_proxy_property(udc,"max", stm::unit::discharge_,constraint_::max, "Discharge constraint maximum, time-series")
            }

            {
                py::scope up_scope=up;
                auto  upc=py::class_<stm::unit::production_::constraint_, py::bases<>,boost::noncopyable>(
                    "_Constraint", 
                    doc_intro("Contains the effect constraints to the unit"),
                    py::no_init
                );
                expose_str_repr(upc);
                _add_proxy_property(upc,"min", stm::unit::production_,constraint_::min, "Production constraint minimum, time-series")
                _add_proxy_property(upc,"max", stm::unit::production_,constraint_::max, "Production constraint maximum, time-series")
            }
        }
    }
}
