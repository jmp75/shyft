#include <string>
#include <memory>
#include <vector>
#include <sstream>

#ifdef SHYFT_WITH_SHOP
#include <shyft/energy_market/stm/shop/shop_system.h>

#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/api/py_vector.h>
#include <shyft/py/energy_market/py_model_client_server.h>

#include "py_shop_command.h"
#include "py_shop_commander.h"


//TODO: later #include <boost/python/exception_translator.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <shyft/core/utctime_utilities.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/srv/db.h>

namespace py=boost::python;
using std::shared_ptr;
using std::string;

namespace shyft::py::energy_market::stm::shop {

using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::utcperiod;
using shyft::time_axis::generic_dt;
using shyft::time_series::dd::apoint_ts;
using shyft::energy_market::stm::stm_system_;

// Utility functions for Python wrapping of generic shop_system functions that streams
// data into an std::ostream reference (by default std::cout) given as last argument,
// to collect the data and return as a string, which is easier to work with from Python.
template<class T, class Func, class... Args>
std::string string_wrap_streaming_function(const T& obj, Func func, Args... args) {
    std::ostringstream stream; (obj.*func)(args..., stream); return stream.str();
}
template<class Func, class... Args>
std::string string_wrap_streaming_function(Func func, Args... args) {
    std::ostringstream stream; func(args..., stream); return stream.str();
}

struct py_shop_log_entry {
    utctime time;
    string severity;
    int code;
    string message;
    py_shop_log_entry(const ::shop::data::shop_log_entry& entry) {
        time = shyft::core::from_seconds(entry.time);
        severity = entry.severity;
        code = entry.code;
        message = entry.message;
        message.erase(message.begin(), std::find_if(message.begin(), message.end(), [](int ch) { return !std::isspace(ch); })); // some shop messages include line endings at beginning which is not wanted here (e.g. 3006: "End overview over input data:")
        message.erase(std::find_if(message.rbegin(), message.rend(), [](int ch) { return !std::isspace(ch); }).base(), message.end()); // many (all?) shop messages include line endings at the end which we is not wanted here
    }
    bool operator==(const py_shop_log_entry& other) const {
        return time == other.time
            && severity == other.severity
            && code == other.code
            && message == other.message;
    }
};

static std::string py_str(const py_shop_log_entry& a) {
    shyft::core::calendar utc;
    return string("ShopLogEntry('") + utc.to_string(a.time) + string("', '") + a.severity + string("', ") + std::to_string(a.code) + string(", '") + a.message + string("')");
}
static std::string vector_py_str(const vector<py_shop_log_entry>& v) {
    return expose::v_py_str<py_shop_log_entry,std::string(*)(const py_shop_log_entry&)>(v,&py_str);
}

void expose_shop_optimize(shyft::energy_market::stm::stm_system& stm, utctime t0, utctime t_end, utctimespan dt, const vector<shyft::energy_market::stm::shop::shop_command>& cmds, bool log_to_stdstream, bool log_to_files) {
    return shyft::energy_market::stm::shop::shop_system::optimize(stm, t0, t_end, dt, cmds, log_to_stdstream, log_to_files);
}

void expose_shop_optimize_ta(shyft::energy_market::stm::stm_system& stm, shyft::time_axis::generic_dt ta, const vector<shyft::energy_market::stm::shop::shop_command>& cmds, bool log_stream, bool log_file) {
    return shyft::energy_market::stm::shop::shop_system::optimize(stm, ta, cmds, log_stream, log_file);
}
// Exposing stm::shop::shop_system
static void expose_shop_system() {

    py::class_<
        py_shop_log_entry,
        py::bases<>
    >("ShopLogEntry", "A shop log entry, produced by the shop core.", py::no_init)
        .def_readonly("time",&py_shop_log_entry::time,"The timestamp for when the entry was produced.")
        .def_readonly("severity",&py_shop_log_entry::severity,"The severity of the message, as a string 'OK', 'Diagnosis OK', 'Warning' etc.")
        .def_readonly("code",&py_shop_log_entry::code,"The numerical identifier of the log message type.")
        .def_readonly("message",&py_shop_log_entry::message,"The textual message, or description.")
        .def<string(*)(const py_shop_log_entry&)>("__str__", &py_str, "Provide easy to read string representation of the object.")
        .def<string(*)(const py_shop_log_entry&)>("__repr__", &py_str, "Provide easy to read string representation of the object.")
        .def(py::self == py::self)
    ;

    expose::py_vector<py_shop_log_entry>(vector_py_str,"ShopLogEntryList", "A strongly typed list of ShopLogEntry.");

    py::class_<
        shyft::energy_market::stm::shop::shop_system,
        py::bases<>,
        shared_ptr<shyft::energy_market::stm::shop::shop_system>,
        boost::noncopyable
    >("ShopSystem", "A shop system, managing a session to the shop core.", py::no_init)
        .def(py::init<utcperiod, utctimespan>(
            (py::arg("time_period"), py::arg("time_step")),
            "Create shop system, initialize with fixed time resolution."))
        .def(py::init<const generic_dt&>(
            (py::arg("time_axis")),
            "Create shop system, initialize with time axis."))
        .def_readonly("commander",&shyft::energy_market::stm::shop::shop_system::commander,
            doc_intro("Get shop commander object.")
            doc_details("Use it to send individual commands to the shop core."))

        .def("set_logging_to_stdstreams",&shyft::energy_market::stm::shop::shop_system::set_logging_to_stdstreams,(py::arg("self"),
            py::arg("on")),
            doc_intro("Turn on or off logging from the shop core to standard output and error streams (console).")
            doc_details("Default is off.")
            doc_parameters()
            doc_parameter("on", "bool", "Turn on."))
        .def("set_logging_to_files",&shyft::energy_market::stm::shop::shop_system::set_logging_to_files,(py::arg("self"),
            py::arg("on")),
            doc_intro("Turn on or off logging from the shop core to files.")
            doc_details("When enabled shop will write its execution log file (shop_messages.log)\n"
                        "and optimization log file (cplex.log) to current directory. Custom log\n"
                        "files specified with the 'log_file' command will also not be written\n"
                        "unless enabled.\n"
                        "Default is off.")
            doc_parameters()
            doc_parameter("on", "bool", "Turn on."))
        .def<vector<py_shop_log_entry>(*)(shyft::energy_market::stm::shop::shop_system&, int)>("get_log_buffer", //&shyft::energy_market::stm::shop::shop_system::get_log_buffer,(py::arg("self"),
            [](shyft::energy_market::stm::shop::shop_system& o, int limit)->vector<py_shop_log_entry>{
                auto list = o.get_log_buffer(limit);
                vector<py_shop_log_entry> py_list;
                py_list.reserve(list.size());
                for (const auto& v : list) {
                    py_list.emplace_back(v);
                }
                return py_list;
            },
            (py::arg("self"), py::arg("limit")),
            "Get content of log entry buffer.")
        .def("emit",&shyft::energy_market::stm::shop::shop_system::emit,(py::arg("self"),
            py::arg("stm_system")),
            "Emit a stm system into the shop core.")
        .def("command",&shyft::energy_market::stm::shop::shop_system::command,(py::arg("self"),
            py::arg("commands")),
            "Send a set of commands to the shop core.")
        .def("collect",&shyft::energy_market::stm::shop::shop_system::collect,(py::arg("self"),
            py::arg("stm_system")),
            doc_intro("Collect results from shop core into stm system.")
            doc_details("The stm system should be the one previously emitted."))
        .def("optimize2",&expose_shop_optimize,
            (py::arg("stm_system"),py::arg("time_begin"),py::arg("time_end"),py::arg("time_step"),py::arg("commands"),py::arg("logging_to_stdstreams"),py::arg("logging_to_files")),
            doc_intro("Run one-step optimization with fixed time resolution.")
            doc_details("This will initialize a shop system with fixed time resolution,\n"
                        "emit stm system, send commands and collect results.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to optimize.")
            doc_parameter("time_begin","time","The start time for the optimization.")
            doc_parameter("time_end", "time", "The end time for the optimization.")
            doc_parameter("time_step", "utctime", "The time step of the optimization.")
            doc_parameter("commands", "ShopCommandList", "The commands to send to the shop api core.")
            doc_parameter("logging_to_stdstreams", "bool", "Allow logging from the shop core to standard output and error streams (console).")
            doc_parameter("logging_to_files", "bool", "Allow logging from the shop core to log files.")).staticmethod("optimize2")
        .def("optimize", &expose_shop_optimize_ta,
            (py::arg("stm_system"),py::arg("time_axis"),py::arg("commands"),py::arg("logging_to_stdstreams"),py::arg("logging_to_files")),
            doc_intro("Run one-step optimization with time axis.")
            doc_details("This will initialize a shop system with time axis,\n"
                        "emit stm system, send commands and collect results.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to optimize.")
            doc_parameter("time_axis","TimeAxis","The time axis for the optimization.")
            doc_parameter("commands", "ShopCommandList", "The commands to send to the shop api core.")
            doc_parameter("logging_to_stdstreams", "bool", "Allow logging from the shop core to standard output and error streams (console).")
            doc_parameter("logging_to_files", "bool", "Allow logging from the shop core to log files.")).staticmethod("optimize")
        .def<string(*)(const shyft::energy_market::stm::shop::shop_system&,bool,bool)>("export_topology_string",
            [](const shyft::energy_market::stm::shop::shop_system& o,bool all,bool raw)->string{return string_wrap_streaming_function(o,&shyft::energy_market::stm::shop::shop_system::export_topology,all,raw);},
            (py::arg("self"), py::arg("all")=false, py::arg("raw")=false),
            doc_intro("Export emitted topology as a DOT formatted string value.")
            doc_details("Use the Graphviz package to generate picture file from it.")
            doc_parameters()
            doc_parameter("all","bool","Include all objects, also non-topological.")
            doc_parameter("all","bool","Include all objects, also non-topological.")
            doc_parameter("raw", "bool", "Raw export showing all details.")
            doc_paramcont("This disables the default mangling, intended to give nicer output")
            doc_paramcont("at the risk of hiding away some important details.")
            doc_returns("DOT","str","Topology as DOT formatted string."))
        .def<void(*)(const shyft::energy_market::stm::shop::shop_system&,bool,bool)>("export_topology",
            [](const shyft::energy_market::stm::shop::shop_system& o,bool all,bool raw){o.export_topology(all,raw);},
            (py::arg("self"), py::arg("all")=false, py::arg("raw")=false),
            doc_intro("Export emitted topology in DOT format to standard output.")
            doc_details("Copy and paste into your favorite Graphviz editor.")
            doc_parameters()
            doc_parameter("all","bool","Include all objects, also non-topological.")
            doc_parameter("raw", "bool", "Raw export showing all details.")
            doc_paramcont("This disables the default mangling, intended to give nicer output")
            doc_paramcont("at the risk of hiding away some important details."))
        .def<string(*)(const shyft::energy_market::stm::shop::shop_system&,bool)>("export_data_string",
            [](const shyft::energy_market::stm::shop::shop_system& o,bool all)->string{return string_wrap_streaming_function(o,&shyft::energy_market::stm::shop::shop_system::export_data,all);},
            (py::arg("self"), py::arg("all")=false),
            doc_intro("Export emitted data objects and attributes as a JSON formatted string value.")
            doc_details("Use a JSON package to decode it.")
            doc_parameters()
            doc_parameter("all","bool","Include all attributes, even non-existing ones.")
            doc_returns("JSON","str","Data objects and attributes as JSON formatted string."))
        .def<void(*)(const shyft::energy_market::stm::shop::shop_system&,bool)>("export_data",
            [](const shyft::energy_market::stm::shop::shop_system& o,bool all){o.export_data(all);},
            (py::arg("self"), py::arg("all")=false),
            doc_intro("Export emitted data objects and attributes in JSON format to standard output.")
            doc_details("Copy and paste into your favorite JSON editor.")
            doc_parameters()
            doc_parameter("all","bool","Include all attributes, even non-existing ones."))

        .def<string(*)()>("environment_string",[]()->string{return string_wrap_streaming_function(&shyft::energy_market::stm::shop::shop_system::environment);},
            "Get all environment variables as a newline delimited string value.").staticmethod("environment_string")
        .def<void(*)()>("environment",[](){shyft::energy_market::stm::shop::shop_system::environment();},
            "Print all environment variables to standard output.").staticmethod("environment")
    ;
}

}

BOOST_PYTHON_MODULE(_shop) {
    py::scope().attr("__doc__") = "Statkraft Energy Market short term model Shop adapter";
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    shyft::py::energy_market::stm::shop::expose_shop_system();
    shyft::py::energy_market::stm::shop::expose_shop_command();
    shyft::py::energy_market::stm::shop::expose_shop_commander();
    py::def("shyft_with_shop",+[]()->bool{return true;});
    py::def("shop_api_version",+[]()->string{return BOOST_PP_STRINGIZE(SHOP_API_VERSION);});
}
#else
#include <shyft/py/api/boostpython_pch.h>
#include <boost/python/module_init.hpp>
namespace py=boost::python;
BOOST_PYTHON_MODULE(_shop) {
    py::scope().attr("__doc__") = "Dummy Stub for: Statkraft Energy Market short term model Shop adapter";
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    py::def("shyft_with_shop",+[]()->bool{return false;});
}
#endif
