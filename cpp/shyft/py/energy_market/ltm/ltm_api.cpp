#include <shyft/py/energy_market/py_object_ext.h>
#include <memory>


namespace py=boost::python;

namespace expose {

    void ltm() {
        
    }

}

BOOST_PYTHON_MODULE(_ltm) {
    py::scope().attr("__doc__") = "Shyft Open Source Energy Market long term models";
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    expose::ltm();
}
