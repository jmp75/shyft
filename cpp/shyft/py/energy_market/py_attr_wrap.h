#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <string>
#include <memory>
#include <type_traits>

#include <boost/hana.hpp>
#include <shyft/mp.h>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_str.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/a_wrap.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <boost/format.hpp>



namespace expose {
    namespace py=boost::python;
    using boost::format;
    using shyft::energy_market::url_fx_t;
    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::a_wrap;

    namespace mp = shyft::mp;
    namespace hana = boost::hana;



    /** def_a_wrap python exposes a wrapped attribute
     *
     * @details
     * Register the proxy-attribute class as returned by the energy_market model python-layer,
     * the a_wrap<A> type.
     * Typically the python exposed methods are there to make it easy to work with, so
     * that the proxy, a_wrap<A> behaves like A, but with additional functions like
     * .exists,.value, .remove() and .url(...).
     *
     * Notice that this class assumes the the str_(A const&)->std::string is available.
     *
     * @tparam A the attribute type to wrap, like stm-type attributes, apoint_ts, t_xy_ etc.
     * @param name of the exposed class.
     *
     */
    template <typename A>
    void def_a_wrap (char const*name) {
        using wt=a_wrap<A>;

        struct attribute_from_wrapped { // class to support automagic a_wrap<A> -> A in python
            static void construct( a_wrap<A>& w, A* a) {
                // place w.a into where a is pointing to.
                // py_wrap<T,A> -> A,  A= shared_ptr<X>
                new (a) A(w.a); // this will inc-ref a shared ptr, copy-construct a value-type, like we want.
            };
        };

        auto  c_=py::class_<wt>(name,
            doc_intro("A wrapped attribute class"),
            py::no_init
        );
        c_.def("url",+[](wt*self,std::string prefix, int levels,int template_levels)->std::string {return self->url(prefix,levels,template_levels);},
            (py::arg("self"),
                py::arg("prefix")=std::string(""),
                py::arg("levels")=-1,
                py::arg("template_levels")=-1),
            doc_intro("Generate an almost unique, url-like string for a proxy attribute.\n"
                      "The string will be based on the attribute's ID, the owning object's type and ID,\n"
                      "and the owning object's parent, if present.")
            doc_parameters()
            doc_parameter("prefix", "str", "What the resulting string starts with")
            doc_parameter("levels", "int", "How many levels of the url to include. "
                                           "levels == 0 includes only this level. Use level < 0 to include all levels")
            doc_parameter("template_levels", "int", "From what level, and onwards, to use templates instead of identifying string. "
                                                    "Use template_levels < 0 to ensure no use of templates.")
            doc_returns("attr_url", "str", "url-type string for the attribute")
        );
        c_.add_property("exists",&wt::exists,
            doc_intro("Check if attribute is available/filled in.")
            doc_returns("","bool","True if the attribute exists, otherwise False")
        );
        c_.def("remove",&wt::remove,
            (py::arg("self")),
            doc_intro("Remove the attribute.")
            doc_details("After calling this the .exists returns False.")
            doc_returns("removed_item","bool","True if removed.")
            doc_retcont("False if it was already away when invoking the method.")
        );
        c_.add_property("value",
            +[](wt*self)->A {return self->a;},
            +[](wt*self,A&v)->void {self->a=v;},
            doc_intro("Access value to get or set it.")
            doc_raises()
            doc_raise("runtime_error", "If .exists == False when you try to read it.")
        );
        // minimal: we do not need this : c_.def_readonly("name",&wt::a_name);
        // no.. we do not aim for this : c_.add_property("parent",+[](wt*self)->O {return self->o;});
        c_.def("__str__",+[](wt *self)->std::string {return (format("%1%")%str_(self->a)).str();});
        c_.def("__repr__",+[](wt *self)->std::string {return (format("%1%")%str_(self->a)).str();});
        c_.def(py::self==py::self);
        c_.def(py::self!=py::self);
        py::fx_implicitly_convertible<a_wrap<A>,A,attribute_from_wrapped>();
    }

    namespace detail {
        /** given  M= A T::*, how to extract A and T ?
        *
        * using  minfo=member_ptr_info<&T::some_member>;
        * and then
        * minfo::A is the type of the attribute
        * minfo::T is the type of the containing class
        */
        template <auto m> // c++ allows template auto types for all constexpr/integral constants
        struct member_ptr_info {

            //- helper class to extract out in case U= A T::*, a member pointer type
            template <class U>
            struct m_ptr_; // the purpose of this is to extract the A T::* out out U in the tempplated specialization below

            template <class T,class A>
            struct m_ptr_<A T::*>{ // specialize the above m_ptr_ class with  with U= A T::*
                using a_=A;// the attribute/member type
                using c_=T;// the class type, keeping the a_
            };

            using h = m_ptr_<decltype(m)>; // the helper class allow us to extract attribute type and its containing class
            using A= typename h::a_;
            using T= typename h::c_;
            //static A& apply(T& o) noexcept { return o.*m;} // example how to apply a member ptr to a ref to object T
        };
    }

    // root type of the path, e.g. reservoir
    template<typename A>
    using root_t = typename decltype(mp::accessor_ptr_struct(hana::front(std::declval<A>())))::type;

    // leaf nest type of the path, e.g. reservoir::level_
    template<typename A>
    using leaf_t = typename decltype(mp::accessor_ptr_struct(hana::back(std::declval<A>())))::type;

    // attribute type, e.g. apoint_ts
    template<typename A>
    using attr_t = typename decltype(mp::accessor_ptr_type(hana::back(std::declval<A>())))::type;

    /** @brief construct python wrapper for attribute
     *
     * @details
     * Creates a python wrapper object (type a_wrap<T>) for an attribute,
     * taking as input the root object and the full path to the attribute.
     *
     * @param root, the starting point for the dereference chain, e.g. stm::reservoir instance
     * @param a_path, the full path of accessors from the root to the attribute.
     */
    template<typename A>
    auto make_py_wrap(root_t<A>& root, A a_path) -> a_wrap<attr_t<A>> {
        static_assert(hana::size(a_path));

        auto attr_name = hana::first(hana::back(a_path)).c_str();  // const char*
        attr_t<A>& attr = mp::leaf_access(root, a_path);

        if constexpr(hana::size(a_path) == hana::size_c<1>) {
            // root is owner of the attribute
            auto url_fx = [&root](auto &so,int levels, int template_levels,std::string_view) {
                if(levels) root.generate_url(so, levels-1, template_levels>0 ? template_levels-1 : template_levels);
            };

            return { url_fx, attr_name, attr };
        }
        else {
            // We have to get the leaf struct instance owning the attribute
            leaf_t<A>& leaf = mp::leaf_access(root, hana::drop_back(a_path));

            auto url_fx = [&leaf](auto& so,int levels, int template_levels, std::string_view sv) {
                leaf.url_fx(so, levels, template_levels, sv);
            };

            return { url_fx, attr_name, attr };
        }
    }

    /** @brief flat dict of attributes for a component
     *
     * @details
     * Returns a flat Python dict mapping leaf attribute keys (str) to wrapped
     * attributes (a_wrap<T>).
     *
     * @param root the root object, an stm component, e.g. a stm::reservoir instance
     */
    template<typename T>
    auto make_flat_attribute_dict(T& root) {
        py::dict attr_dict;
        hana::for_each(mp::leaf_accessor_map(hana::type_c<T>), [&attr_dict, &root] (auto p) {
            attr_dict[hana::first(p).c_str()] = make_py_wrap(root, hana::second(p));
        });

        return attr_dict;
    }

    /** @brief add_proxy_property to python exposed class
     *
     * @details
     * Exposes as a property, a_wrap<A> type.
     * To be used for non-nested 'main' objects like unit,reservoir etc.
     * where the held_type is a shared_ptr.
     *
     * For nested exposure, use the _add_proxy_property macro.
     *
     * @note the reason for the macro, and not template fx,  is due to
     * the requirement for the function passed to add-property to be
     * context-less. Have not figured out a way to do this(yet).
     *
     *
     * @param c the instance of boost::python::class_<...>
     * @param name the name of the attribute/property in python
     * @param C the class of the owning c++ class of the attribute
     * @param attr the name of the attribute in the c++ class
     * @param doc_str the documentation to be presented to the end-user in python
     */
    #define add_proxy_property(c,name,C, attr,doc_str) {\
        using self_type= decltype(c)::metadata::wrapped;\
        using mi = detail::member_ptr_info<&C::attr>;\
        using a_type = typename mi::A;\
        using pyw_t = a_wrap<a_type>;\
        c.add_property(name,\
                +[](self_type* o)->pyw_t {\
                    return pyw_t([o](sbi_t&so,int levels, int template_levels,std::string_view){\
                            if(levels) o->generate_url(so,levels-1,template_levels>0?template_levels-1:template_levels);\
                    },\
                    name,o->attr);\
                },\
                +[](self_type* o, a_type av) { o->attr=av;},\
                doc_str\
        );\
    }

    /** @brief _add_proxy_property to nested members of python exposed class
     *
     * @details
     * Exposes as a property, a_wrap<A> type.
     * To be used for nested 'main' objects like unit.producton_,reservoir.volume_ etc.
     * For exposure of 'main' non-nested classes use the add_proxy_property macro.
     *
     * @param c the instance of boost::python::class_<...>
     * @param name the name of the attribute/property in python
     * @param C the class of the owning c++ class of the attribute
     * @param attr the name of the attribute in the c++ class
     * @param doc_str the documentation to be presented to the end-user in python
     */
    #define _add_proxy_property(c,name,C, attr,doc_str) {\
        using self_type= decltype(c)::metadata::wrapped;\
        using mi = detail::member_ptr_info<&C::attr>;\
        using a_type = typename mi::A;\
        using pyw_t = a_wrap<a_type>;\
        c.add_property(name,\
                +[](self_type& o)->pyw_t {\
                    return pyw_t([&o](sbi_t&so,int levels, int template_levels,std::string_view sv){\
                            o.url_fx(so,levels,template_levels,sv);\
                    },\
                    name,o.attr);\
                },\
                +[](self_type& o, a_type av) { o.attr=av;},\
                doc_str\
        );\
    }

}
