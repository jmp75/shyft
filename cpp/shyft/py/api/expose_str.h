#pragma once

#include <string>
#include <memory>
#include <vector>
#include <map>

namespace expose {

    /** str_(T const*) that every exposed class should specialize
     *
     * If you forget to do this, we typically fail to load the py-shared library due to some missing symbol class-name str_ ..
     * TODO: Is this a good idea? Or should we use ostream& operator<<(ostream&o,T const&self); (we then get basic types for free).
     *
     * @tparam T the real type (not the held pointer type) of the exposed c++ object
     * @param self the const T* of the object to provide a readable string
     * @return the string that best represent a human readable rep of the object
     */
    template<class T>
    std::string str_(T const& self);

    // some basics need to go here if this is going to work nicely for keys in maps
    // a kind of annoying, we have to be precise for any basic-type
    // template<class T> std::string str_(typename std::enable_if<std::is_integral<T>::value>::type const& o) { return std::to_string(o);}, would require explicit instantiation somewhere
    template<> inline std::string str_(std::int8_t const&o) {return std::to_string(o);}
    template<> inline std::string str_(std::int16_t const&o) {return std::to_string(o);}
    template<> inline std::string str_(std::int32_t const&o) {return std::to_string(o);}
    template<> inline std::string str_(std::int64_t const&o) {return std::to_string(o);}
    template<> inline std::string str_(double const&o) {return std::to_string(o);}
    template<> inline std::string str_(float const&o) {return std::to_string(o);}
    template<> inline std::string str_(std::string const&o) {return o;}



    template <class T, int max_items=20>
    inline std::string str_(std::vector<T> const&v);
    template <class K,class T, int max_items=10>
    inline std::string str_(std::map<K,T> const&v);

    /** ensure str_(any_pointer) -> None or str_(*any_pointer) */
    template <class T> inline std::string str_(T * o) {return o?str_(*static_cast<const T*>(o)):std::string("None");} // T=const U*, or T= U* 
    template <class T> inline std::string str_(std::shared_ptr<const T> const&o) { return str_(o.get());}
    template <class T> inline std::string str_(std::shared_ptr<T> const&o) {return str_(o.get());}

    /** provide str_ of vector<T> */
    template <class T, int max_items>
    inline std::string str_(std::vector<T> const&v) {
        std::string r;
        r+='[';
        size_t n=v.size()<max_items?v.size():max_items;
        for(size_t i=0;i<n;++i) {
            r+= i!=0?",":"";
            r+=str_(v[i]);
        }
        r+= v.size()<max_items?"]":",...]"; //  maybe add ...+n
        return r;
    }

    /** provide str_ of vector<T>
     *
     *  TODO: consider implementing construct_from( py::dict)
     */
    template <class K,class T, int max_items>
    inline std::string str_(std::map<K,T> const&v) {
        std::string r;
        r+='{';
        size_t n=v.size()<max_items?v.size():max_items;
        size_t i=0;
        for(auto const& kv:v) {
            r+= i!=0?",":"";
            r+=str_(kv.first);// we could invoke py __str__ for this.
            r+=":";
            r+=str_(kv.second);
            if(++i >=n)
                break;
        }
        r+= v.size()<max_items?"}":",...}"; //  maybe add ...+n
        return r;
    }


    /** oneliner to to expose __str__ and __repr__
     *
     *
     * @tparam PyCls the py::class_<...> type 
     * @param c the instance, eg. auto c=py::class_<....>;
     * @return the py class to support chaining.
     */
    template <class PyCls>
    PyCls& expose_str_repr(PyCls&c) {
        using w_tp=typename PyCls::wrapped_type;
        using str_fx= std::string (*)(w_tp const&);
        c.template def<str_fx>("__str__",&str_);
        c.template def<str_fx>("__repr__",&str_);
        return c;
    }
}
