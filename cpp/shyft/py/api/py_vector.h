/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/py_convertible.h>

namespace expose {

    template<class T, class Fstr> 
    static std::string v_py_str(const std::vector<T>& v, Fstr&& f_str) {
        if (!v.empty()) {
            std::string r{"["};
            for (const auto& i : v) {
                r += f_str(i);
                r += ",\n ";
            }
            r.replace(r.size()-3, std::string::npos, "]");
            return r;
        }
        else {
            return std::string{"[]"};
        }
    }

    template<class ValueType, class Fstr, bool NoProxy = false>
    static void py_vector(Fstr&& py_str_fx, const char* name, const char* doc) {
        using VectorType=std::vector<ValueType>;
        py::class_<VectorType>(name, doc)
            .def(py::vector_indexing_suite<VectorType, NoProxy>()) // To be able to appear as python list. By default indexed elements have Python reference semantics and are returned by proxy, this can be disabled by supplying true in the NoProxy template parameter (e.g. if type already have reference semantics, such as shared_ptr).
            .def("__str__", &py_str_fx, "Provide easy to read string representation of the object.")
            .def("__repr__", &py_str_fx, "Provide easy to read string representation of the object.")
            .def(py::init<const VectorType&>(py::args("clone"), doc_intro("Create a clone."))) // Required by iterable_converter.
        ;
        py_api::iterable_converter().from_python<VectorType>(); // To be able to create from plain Python list.
    }

}
