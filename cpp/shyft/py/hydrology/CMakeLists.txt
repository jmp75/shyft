

#
# define the shyft core api that exposes all common classes and methods
#
    set(core_api  "api")
    set(core_api_sources 
        api_actual_evapotranspiration.cpp     api_kirchner.cpp
        api_cell_environment.cpp              api_precipitation_correction.cpp
        api_priestley_taylor.cpp              api_gamma_snow.cpp
        api_pt_gs_k.cpp                       api_geo_cell_data.cpp
        api_region_environment.cpp            api_r_pm_gs_k.cpp
                             api_routing.cpp
        api_glacier_melt.cpp                  api.cpp
        api_skaugen.cpp                       api_hbv_actual_evapotranspiration.cpp
        api_state.cpp                         api_kalman.cpp
        api_hbv_physical_snow.cpp             api_target_specification.cpp
        api_hbv_snow.cpp                      api_hbv_soil.cpp
        api_hbv_tank.cpp                      api_penman_monteith.cpp
        api_interpolation.cpp                 api_hydrology_vectors.cpp
        api_radiation.cpp                     drms.cpp
        api_snow_tiles.cpp                    api_r_pt_gs_k.cpp
        )


    add_library(${core_api} SHARED ${core_api_sources})
    target_include_directories(${core_api} PRIVATE  ${python_include} ${python_numpy_include})
    set_target_properties(${core_api} PROPERTIES 
        OUTPUT_NAME ${core_api}
        PREFIX "_"
        INSTALL_RPATH "$ORIGIN/../lib"
        VISIBILITY_INLINES_HIDDEN TRUE
    )
    if(MSVC)
        set_target_properties(${core_api} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    elseif(APPLE)
        set_target_properties(${core_api} PROPERTIES SUFFIX ".so")
    endif()
    set_property(TARGET ${core_api} APPEND PROPERTY COMPILE_DEFINITIONS SHYFT_EXTENSION)
    set_property(TARGET ${core_api} APPEND PROPERTY COMPILE_DEFINITIONS  BOOST_BIND_GLOBAL_PLACEHOLDERS) # silence boost.py warning
    target_link_libraries(${core_api} shyft_core shyft_hydrology dlib::dlib ${boost_py_link_libraries} ${arma_libs})
    install(TARGETS ${core_api} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/api)


#
# define each shyft method-stack that exposes complete stacks
#
    foreach(shyft_stack  "r_pt_gs_k" "r_pm_gs_k" "pt_gs_k" "pt_ss_k" "pt_hs_k" "pt_st_k" "hbv_stack" "pt_hps_k")
        add_library(${shyft_stack} SHARED  ${shyft_stack}.cpp)
        target_include_directories(${shyft_stack} PRIVATE  ${python_include} ${python_numpy_include} )
        set_target_properties(${shyft_stack} PROPERTIES 
            OUTPUT_NAME ${shyft_stack}
            PREFIX "_"
            INSTALL_RPATH "$ORIGIN/../../lib"
            VISIBILITY_INLINES_HIDDEN TRUE
        )
        if(MSVC)
            set_target_properties(${shyft_stack} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
        elseif(APPLE)
            set_target_properties(${shyft_stack} PROPERTIES SUFFIX ".so")
        endif()
        set_property(TARGET ${shyft_stack} APPEND PROPERTY COMPILE_DEFINITIONS SHYFT_EXTENSION)
        set_property(TARGET ${shyft_stack} APPEND PROPERTY COMPILE_DEFINITIONS  BOOST_BIND_GLOBAL_PLACEHOLDERS) # silence boost.py warning
        target_link_libraries(${shyft_stack} shyft_core shyft_hydrology  dlib::dlib ${boost_py_link_libraries} ${arma_libs})
        install(TARGETS ${shyft_stack} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/api/${shyft_stack})
    endforeach(shyft_stack)

