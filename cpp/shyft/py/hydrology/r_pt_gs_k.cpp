/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/time/utctime_utilities.h>
#include  <shyft/hydrology/methods/radiation.h>
#include  <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/actual_evapotranspiration.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/hydrology/methods/gamma_snow.h>
#include <shyft/hydrology/methods/kirchner.h>
#include <shyft/hydrology/stacks/r_pt_gs_k.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/stacks/r_pt_gs_k_cell_model.h>
#include <shyft/hydrology/region_model.h>
#include  <shyft/hydrology/model_calibration.h>
#include  <shyft/py/hydrology/expose_statistics.h>
#include  <shyft/py/hydrology/expose.h>

static char const* version() {
   return "v1.0";
}

namespace expose {
    namespace r_pt_gs_k {
        using namespace boost::python;
        namespace py = boost::python;
        using namespace shyft::core;
        using namespace shyft::core::r_pt_gs_k;
        using std::string;
        using std::vector;

        typedef vector<state> RPTGSKStateVector;

        static void
        parameter_state_response() {

            class_<parameter,bases<>,std::shared_ptr<parameter>>("RPTGSKParameter",
                              "Contains the parameters to the methods used in the RPTGSK assembly\n"
                              "radiation, priestley taylor, gamma_snow,actual_evapotranspiration,precipitation_correction,kirchner\n"
                )
                .def(init<radiation::parameter,priestley_taylor::parameter,gamma_snow::parameter,actual_evapotranspiration::parameter,kirchner::parameter,precipitation_correction::parameter,py::optional<glacier_melt::parameter,routing::uhg_parameter,mstack_parameter>>(args("rad","pt","gs","ae","k","p_corr","gm","routing","msp"),"create object with specified parameters"))
                .def(init<const parameter&>(args("p"),"clone a parameter"))
                .def_readwrite("rad",&parameter::rad,"radiation parameter")
                .def_readwrite("pt",&parameter::pt,"priestley taylor parameter")
                .def_readwrite("gs",&parameter::gs,"gamma-snow parameter")
                .def_readwrite("gm", &parameter::gm, "glacier melt parameter")
				.def_readwrite("ae",&parameter::ae,"actual evapotranspiration parameter")
                .def_readwrite("kirchner",&parameter::kirchner,"kirchner parameter")
                .def_readwrite("p_corr",&parameter::p_corr,"precipitation correction parameter")
                .def_readwrite("routing",&parameter::routing,"routing cell-to-river catchment specific parameters")
                .def_readwrite("msp",&parameter::msp,"contains the method stack parameters")
                .def("size",&parameter::size,"returns total number of calibration parameters")
                .def("set",&parameter::set,args("p"),"set parameters from vector/list of float, ordered as by get_name(i)")
                .def("get",&parameter::get,args("i"),"return the value of the i'th parameter, name given by .get_name(i)")
                .def("get_name",&parameter::get_name,args("i"),"returns the i'th parameter name, see also .get()/.set() and .size()")
                .def("serialize",&serialize_to_bytes<parameter>,(py::arg("self")),"serializes the parameters to a blob, that later can be passed in to .deserialize()")
                .def("deserialize",&deserialize_from_bytes<parameter>,(py::arg("blob"))).staticmethod("deserialize")
                .def(self==self)
                .def(self!=self)
                ;

            typedef std::map<int,parameter> RPTGSKParameterMap;
            class_<RPTGSKParameterMap>("RPTGSKParameterMap","dict (int,parameter)  where the int is the catchment_id")
                .def(map_indexing_suite<RPTGSKParameterMap>())
            ;

            class_<state>("RPTGSKState")
                .def(init<gamma_snow::state,kirchner::state>(args("gs","k"),"initializes state with gamma-snow gs and kirchner k"))
                .def_readwrite("gs",&state::gs,"gamma-snow state")
                .def_readwrite("kirchner",&state::kirchner,"kirchner state")
                ;


            class_<RPTGSKStateVector,bases<>,std::shared_ptr<RPTGSKStateVector> >("RPTGSKStateVector")
                .def(vector_indexing_suite<RPTGSKStateVector>())
                ;


            class_<response>("RPTGSKResponse","This struct contains the responses of the methods used in the RPTGSK assembly")
                .def_readwrite("rad",&response::rad,"radiation response")
                .def_readwrite("pt",&response::pt,"priestley_taylor response")
                .def_readwrite("gs",&response::gs,"gamma-snnow response")
                .def_readwrite("gm_melt_m3s", &response::gm_melt_m3s, "glacier melt response[m3s]")
                .def_readwrite("ae",&response::ae,"actual evapotranspiration response")
                .def_readwrite("kirchner",&response::kirchner,"kirchner response")
                .def_readwrite("total_discharge",&response::total_discharge,"total stack response")
                ;
        }

        static void
        collectors() {
            typedef shyft::core::r_pt_gs_k::all_response_collector RPTGSKAllCollector;
            class_<RPTGSKAllCollector>("RPTGSKAllCollector", "collect all cell response from a run")
                .def_readonly("destination_area",&RPTGSKAllCollector::destination_area,"a copy of cell area [m2]")
                .def_readonly("avg_discharge",&RPTGSKAllCollector::avg_discharge,"Kirchner Discharge given in [m^3/s] for the timestep")
                .def_readonly("snow_sca",&RPTGSKAllCollector::snow_sca," gamma snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
                .def_readonly("snow_swe",&RPTGSKAllCollector::snow_swe,"gamma snow swe, [mm] over the cell sca.. area, - at the end of timestep")
                .def_readonly("snow_outflow",&RPTGSKAllCollector::snow_outflow," gamma snow output [m^3/s] for the timestep")
                .def_readonly("glacier_melt", &RPTGSKAllCollector::glacier_melt, " glacier melt (outflow) [m3/s] for the timestep")
                .def_readonly("ae_output",&RPTGSKAllCollector::ae_output,"actual evap mm/h")
                .def_readonly("pe_output",&RPTGSKAllCollector::pe_output,"pot evap mm/h")
                .def_readonly("rad_sw_t_output",&RPTGSKAllCollector::rad_sw_t_output,"sw translatedrad [W/m2]")
                .def_readonly("rad_sw_cs_p_output",&RPTGSKAllCollector::rad_sw_cs_p_output,"sw clear sky predicted [W/m2]")
                .def_readonly("end_response",&RPTGSKAllCollector::end_reponse,"end_response, at the end of collected")
                .def_readonly("avg_charge",&RPTGSKAllCollector::charge_m3s,"average charge in [m^3/s]")
            ;

            typedef shyft::core::r_pt_gs_k::discharge_collector RPTGSKDischargeCollector;
            class_<RPTGSKDischargeCollector>("RPTGSKDischargeCollector", "collect all cell response from a run")
                .def_readonly("cell_area",&RPTGSKDischargeCollector::cell_area,"a copy of cell area [m2]")
                .def_readonly("avg_discharge",&RPTGSKDischargeCollector::avg_discharge,"Kirchner Discharge given in [m^3/s] for the timestep")
                .def_readonly("snow_sca",&RPTGSKDischargeCollector::snow_sca," gamma snow covered area fraction, sca.. 0..1 - at the end of timestep (state)")
                .def_readonly("snow_swe",&RPTGSKDischargeCollector::snow_swe,"gamma snow swe, [mm] over the cell sca.. area, - at the end of timestep")
                .def_readonly("end_reponse",&RPTGSKDischargeCollector::end_response,"end_response, at the end of collected")
                .def_readwrite("collect_snow",&RPTGSKDischargeCollector::collect_snow,"controls collection of snow routine")
                .def_readonly("avg_charge", &RPTGSKDischargeCollector::charge_m3s, "average charge in [m^3/s]")
                ;
            typedef shyft::core::r_pt_gs_k::null_collector RPTGSKNullCollector;
            class_<RPTGSKNullCollector>("RPTGSKNullCollector","collector that does not collect anything, useful during calibration to minimize memory&maximize speed")
                ;

            typedef shyft::core::r_pt_gs_k::state_collector RPTGSKStateCollector;
            class_<RPTGSKStateCollector>("RPTGSKStateCollector","collects state, if collect_state flag is set to true")
                .def_readwrite("collect_state",&RPTGSKStateCollector::collect_state,"if true, collect state, otherwise ignore (and the state of time-series are undefined/zero)")
                .def_readonly("kirchner_discharge",&RPTGSKStateCollector::kirchner_discharge,"Kirchner state instant Discharge given in m^3/s")
                .def_readonly("gs_albedo",&RPTGSKStateCollector::gs_albedo,"")
                .def_readonly("gs_lwc",&RPTGSKStateCollector::gs_lwc,"")
                .def_readonly("gs_surface_heat",&RPTGSKStateCollector::gs_surface_heat,"")
                .def_readonly("gs_alpha",&RPTGSKStateCollector::gs_alpha,"")
                .def_readonly("gs_sdc_melt_mean",&RPTGSKStateCollector::gs_sdc_melt_mean,"")
                .def_readonly("gs_acc_melt",&RPTGSKStateCollector::gs_acc_melt,"")
                .def_readonly("gs_iso_pot_energy",&RPTGSKStateCollector::gs_iso_pot_energy,"")
                .def_readonly("gs_temp_swe",&RPTGSKStateCollector::gs_temp_swe,"")
            ;

        }

        static void
        cells() {
              typedef shyft::core::cell<parameter, environment_t, state, state_collector, all_response_collector> RPTGSKCellAll;
              typedef shyft::core::cell<parameter, environment_t, state, null_collector, discharge_collector> RPTGSKCellOpt;
              expose::cell<RPTGSKCellAll>("RPTGSKCellAll","tbd: RPTGSKCellAll doc");
              expose::cell<RPTGSKCellOpt>("RPTGSKCellOpt","tbd: RPTGSKCellOpt doc");
              expose::statistics::gamma_snow<RPTGSKCellAll>("RPTGSKCell");//it only gives meaning to expose the *All collect cell-type
              expose::statistics::actual_evapotranspiration<RPTGSKCellAll>("RPTGSKCell");
              expose::statistics::priestley_taylor<RPTGSKCellAll>("RPTGSKCell");
              expose::statistics::radiation<RPTGSKCellAll>("RPTGSKCell");
              expose::statistics::kirchner<RPTGSKCellAll>("RPTGSKCell");
              expose::cell_state_etc<RPTGSKCellAll>("RPTGSK");// just one expose of state

        }

        static void
        models() {
            typedef shyft::core::region_model<r_pt_gs_k::cell_discharge_response_t, shyft::api::a_region_environment> RPTGSKOptModel;
            typedef shyft::core::region_model<r_pt_gs_k::cell_complete_response_t, shyft::api::a_region_environment> RPTGSKModel;
            expose::model<RPTGSKModel>("RPTGSKModel","RPTGSK");
            expose::model<RPTGSKOptModel>("RPTGSKOptModel","RPTGSK");
            def_clone_to_similar_model<RPTGSKModel, RPTGSKOptModel>("create_opt_model_clone");
            def_clone_to_similar_model<RPTGSKOptModel,RPTGSKModel>("create_full_model_clone");
        }


        static void
        model_calibrator() {
            expose::model_calibrator<shyft::core::region_model<r_pt_gs_k::cell_discharge_response_t,shyft::api::a_region_environment>>("RPTGSKOptimizer");
        }
    }
}


BOOST_PYTHON_MODULE(_r_pt_gs_k)
{

    boost::python::scope().attr("__doc__")="Shyft python api for the r_pt_gs_k model";
    boost::python::def("version", version);
	boost::python::docstring_options doc_options(true, true, false);// all except c++ signatures
    expose::r_pt_gs_k::parameter_state_response();
    expose::r_pt_gs_k::cells();
    expose::r_pt_gs_k::models();
    expose::r_pt_gs_k::collectors();
    expose::r_pt_gs_k::model_calibrator();
}
