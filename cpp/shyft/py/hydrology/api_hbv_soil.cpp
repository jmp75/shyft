/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/hbv_soil.h>

namespace expose {

	void hbv_soil() {
		using namespace shyft::core::hbv_soil;
        namespace py=boost::python;

		py::class_<parameter>("HbvSoilParameter")
			.def(py::init<py::optional<double, double>>(py::args("fc", "beta"), "create parameter object with specifed values"))
			.def_readwrite("fc", &parameter::fc, "mm, .. , default=300")
			.def_readwrite("beta", &parameter::beta, ",default=2.0")
			;

		py::class_<state>("HbvSoilState")
			.def(py::init<py::optional<double>>(py::args("sm"), "create a state with specified values"))
			.def_readwrite("sm", &state::sm, "Soil  moisture [mm]")
			;

		py::class_<response>("HbvSoilResponse")
			.def_readwrite("outflow", &response::outflow, "from Soil-routine in [mm]")
			;

		typedef  calculator<parameter> HbvSoilCalculator;
		py::class_<HbvSoilCalculator>("HbvSoilCalculator",
			"tobe done.. \n"
			"\n"
			"\n", py::no_init
			)
			.def(py::init<const parameter&>(py::args("parameter"), "creates a calculator with given parameter"))
			.def("step", &HbvSoilCalculator::step<response,state>, py::args("state", "response", "t0", "t1", "insoil", "actual_evap", "pot_evap"), 
				"steps the model forward from t0 to t1, updating state and response")
			;
	}
}
