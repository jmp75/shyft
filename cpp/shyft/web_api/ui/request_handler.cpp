
#include <shyft/web_api/ui/request_handler.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>
#include <shyft/web_api/generators/proxy_attr.h>

#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/energy_market/ui/srv/server.h>


namespace shyft::web_api::ui {
    using shyft::web_api::grammar::phrase_parser;
    using shyft::web_api::grammar::request_grammar;
    using shyft::web_api::generator::emit;
    using shyft::web_api::generator::emit_object;
    using shyft::web_api::generator::emit_vector_fx;

    bg_work_result request_handler::handle_request(const request & req) {
        if (req.keyword == "get_layouts") {
            return handle_get_layouts_request(req.request_data);
        } else if (req.keyword == "read_layout") {
            return handle_read_layout_request(req.request_data);
        }
        return bg_work_result("Unknown keyword '" + req.keyword + "'");
    }

    bg_work_result request_handler::do_the_work(const string& input) {
        // 1. Parse into a request
        // 2. Error handling: On success, do a switch on keyword
        // 3. Handle every case and emit a response string

        // 1. Parse the request
        request arequest;
        request_grammar<const char*> request_;
        bool ok_parse = false;
        bg_work_result response;
        try {
            ok_parse = phrase_parser(input.c_str(), request_, arequest);
            if (ok_parse) {
                response = handle_request(arequest);
            } else {
                response = bg_work_result{"not understood: " + input};
            }
        } catch (std::runtime_error const &re) {
            response = bg_work_result{string("request_parse: ") + re.what()};
        }
        return response;
    }

    bg_work_result request_handler::do_subscription_work(observer_base_ const&o) {
        // we want subscription on proxy-type expressions
        // so ts_expression_observer_ , url, might look different for the in-memory em server
        if(o->recalculate()) {
            // What to emit if a change is observed:
            return bg_work_result{};
        } else {
            return bg_work_result{};
        }
    }

    bg_work_result request_handler::handle_get_layouts_request(const json& data) {
        // Communicate with server:
        auto model_infos = srv->db.get_model_infos({});

        // Prepare response
        auto req_id = data.required<string>("request_id");
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        emit_vector_fx(sink, model_infos, [](auto oi, auto li) {
            emit_object<decltype(oi)> oo(oi);
            oo.def("layout_id", li.id)
                .def("name", li.name)
                ;
        });
        response += "}";
        return bg_work_result{response};
    }

    bg_work_result request_handler::handle_read_layout_request(const json& data) {
        // Required data:
        auto req_id = data.required<string>("request_id");
        auto lid = data.required<int>("layout_id");

        // Optional data:
        auto name = boost::get_optional_value_or(data.optional<string>("name"), "");
        auto args = boost::get_optional_value_or(data.optional<json>("args"), json{});
        auto store_layout = boost::get_optional_value_or(data.optional<bool>("store"), false);
        // Generate string from json args:
        string jargs;
        {
            auto sink = std::back_inserter(jargs);
            emit(sink, args);
        }
        // Read, or generate layout:
        auto li = srv->db.read_model_with_args(lid, name, jargs, store_layout);

        // Create response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        {
            auto sink = std::back_inserter(response);
            emit_object<decltype(sink)> oo(sink);
            oo.def("layout_id", li->id)
                .def("name", li->name)
                ;
            // For the json, we parse it to a json struct first.
            json layout_struct;
            shyft::web_api::grammar::json_grammar<const char*> json_;
            bool ok_parse = false;
            ok_parse = phrase_parser(li->json.c_str(), json_, layout_struct);
            if (ok_parse)
                oo.def("layout", layout_struct);
            else
                oo.def("layout", string("error in parsing json: '") + li->json + "'");
        }
        response += "}";
        return bg_work_result{response};
    }


}
