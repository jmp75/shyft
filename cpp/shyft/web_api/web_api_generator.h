/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/karma_string.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include <string_view>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/geo.h>

// -- we need some more stuff into the dd namespace to ease working with spirit::karma:
//
namespace shyft::time_series::dd {

    /** @brief point proxy
     *
     *  To help creating apoint_ts iterator that could allow modifying the
     * underlying time-series.
     *
     */
    struct point_proxy {
        ipoint_ts *ts; ///< not owning,
        size_t i;
        operator point() const {
            return point(ts->time(i),ts->value(i));
        }
        point_proxy& operator=(point const& p) {
            if(ts->time(i)!=p.t)
                throw runtime_error("not supported, change time in an interator of points");
            if(dynamic_cast<gpoint_ts*>(ts))
                dynamic_cast<gpoint_ts*>(ts)->set(i,p.v);
            return *this;
        }
        utctime time() const {return ts->time(i);}
        double value() const {return ts->value(i);}
    };

    struct apoint_ts_c {
        using value_type=point_proxy;
        apoint_ts ats;
        apoint_ts_c(apoint_ts const &ats):ats(ats) {}

        struct iterator
        : public boost::iterator_facade<
                iterator
            , point
            , boost::random_access_traversal_tag
            , point_proxy
            >
        {
            iterator(){}
            iterator(const apoint_ts&a,size_t i=0):ats{a},i{i} {}
            void increment() {++i;}
            void decrement() {--i;}
            void advance(difference_type n) {i=static_cast<size_t>(static_cast<difference_type>(i)+n);}

            bool equal(iterator const& other) const { return ats.ts == other.ats.ts && i==other.i;}
            difference_type distance_to(iterator const& other) const {return static_cast<difference_type>(other.i) - static_cast<difference_type>(i);}

            point_proxy dereference() const { return point_proxy{ const_cast<ipoint_ts*>(ats.ts.get()),i}; } //TODO: consider difference between const and non-const dereference

        private:
            apoint_ts ats;
            size_t i{0};
        };
        using const_iterator= iterator;
        iterator begin() const {return iterator(ats,0u);}
        iterator end() const {return iterator(ats,ats.size());}
    };

//    apoint_ts_c::iterator begin(apoint_ts const&ts) {return apoint_ts_c::iterator{ts};};
//    apoint_ts_c::iterator end(apoint_ts const&ts) {return apoint_ts_c::iterator{ts,ts.size()};};
}

namespace shyft::web_api::generator {
    namespace qi = boost::spirit::qi;
    namespace ka=boost::spirit::karma;
    namespace phx=boost::phoenix;
    using std::string;
    using qi::double_;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using geo_ts_id=shyft::dtss::geo::ts_id;
    using shyft::time_series::ts_point_fx;
    using shyft::time_series::point;
    using shyft::core::to_seconds;
    using shyft::dtss::ts_info;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::time_axis::fixed_dt;
    using shyft::time_axis::calendar_dt;
    using shyft::time_axis::point_dt;
    using shyft::time_axis::generic_dt;


    // ref to: https://www.boost.org/doc/libs/1_69_0/libs/spirit/doc/html/spirit/karma/reference/numeric/real_number.html
    template <typename T>
    struct time_policy : ka::real_policies<T> {
        using base_policy_type=ka::real_policies<T>;
        static unsigned int precision(T) { return 6; }
        static int floatfield(T) {return base_policy_type::fmtflags::fixed;}
    };

    /** @brief grammer for emitting a time,value point
    *
    */
    template<class OutputIterator>
    struct point_generator:ka::grammar<OutputIterator,point()> {
        point_generator();
        ka::rule<OutputIterator,point()> pg;
        ka::real_generator<double, time_policy<double> > time_;
    };

    /** @brief a time-series generator
    *
    * outputs:
    *   {pfx:(true|false),data:[[t,v],..]}
    * or if empty
    *   {pfx:null,data:null}
    */
    template<class OutputIterator>
    struct apoint_ts_generator:ka::grammar<OutputIterator,apoint_ts()> {

        apoint_ts_generator();
        ka::rule<OutputIterator,apoint_ts()> tsg;
        ka::rule<OutputIterator,shyft::time_series::dd::apoint_ts_c()> pts_;
        point_generator<OutputIterator> pt_;
    };

    /** @brief a time-series vector generator
    *
    * outputs:
    *   {pfx:(true|false),data:[[t,v],..]}
    * or if empty
    *   {pfx:true,data:[]}
    */
    template<class OutputIterator>
    struct atsv_generator:ka::grammar<OutputIterator,std::vector<apoint_ts>()> {

        atsv_generator():atsv_generator::base_type(tsg) {
            using ka::true_;
            using ka::bool_;
            using ka::omit;
            tsg = '[' << -( ats_ % ',') << ']'
            ;
            tsg.name("atsv");
        }
        ka::rule<OutputIterator,std::vector<apoint_ts>()> tsg;
        apoint_ts_generator<OutputIterator> ats_;
    };

    /** @brief grammar for emitting utctime
    *
    * generates double_|null
    */
    template<class OutputIterator>
    struct utctime_generator:ka::grammar<OutputIterator,utctime()> {

        utctime_generator();
        ka::rule<OutputIterator,utctime()> pg;
        ka::real_generator<double, time_policy<double> > time_;
    };

    /** @brief grammar for emitting utcperiod
    *
    * generates [ double_,double_ ] | null
    */
    template<class OutputIterator>
    struct utcperiod_generator:ka::grammar<OutputIterator,utcperiod()> {

        utcperiod_generator();
        ka::rule<OutputIterator,utcperiod()> pg;
        utctime_generator<OutputIterator> time_;
    };

    /** @brief grammar for emitting different types of time axes
     *
     */
    template<class OutputIterator>
    struct fixed_dt_generator: ka::grammar<OutputIterator, fixed_dt()> {

        fixed_dt_generator(): fixed_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"t\":") << time_[ka::_1=phx::bind(&fixed_dt::t, ka::_val)]
                << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&fixed_dt::dt, ka::_val)]
                << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&fixed_dt::n, ka::_val)]
                << ka::lit("}");
            pg.name("fixed_dt");
        }
        ka::rule<OutputIterator, fixed_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct calendar_dt_generator: ka::grammar<OutputIterator, calendar_dt()> {

        calendar_dt_generator(): calendar_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"tz\":") << "\"" << ka::string[ka::_1=phx::bind(&calendar_dt::get_tz_name,ka::_val)] << "\""
                << ka::lit(",\"t\":") << time_[ka::_1=phx::bind(&calendar_dt::t, ka::_val)]
                << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&calendar_dt::dt, ka::_val)]
                << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&calendar_dt::n, ka::_val)]
                << ka::lit("}");
            pg.name("calendar_dt");
        }
        ka::rule<OutputIterator, calendar_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct point_dt_generator: ka::grammar<OutputIterator, point_dt()> {

        point_dt_generator(): point_dt_generator::base_type(pg) {
            pg = ka::lit("{\"t\":[") << (t_ % ",")[ka::_1 = phx::bind(&point_dt::t, ka::_val)] << "]"
                << ka::lit(",\"t_end\":") << t_[ka::_1 = phx::bind(&point_dt::t_end, ka::_val)]
                << ka::lit("}");
            pg.name("point_dt");
        }
        ka::rule<OutputIterator, point_dt()> pg;
        utctime_generator<OutputIterator> t_;

    };

    template<class OutputIterator>
    struct generic_dt_generator: ka::grammar<OutputIterator, generic_dt()> {

        generic_dt_generator(): generic_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"gt\":")
                << ( (&int_(0)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] << "FIXED,\"f\":" << f_[ka::_1 = phx::bind(&generic_dt::f, ka::_val)])
                | (&int_(1)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] << "CALENDAR,\"c\":" << c_[ka::_1 = phx::bind(&generic_dt::c, ka::_val)])
                | (&int_(2)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] <<    "POINT,\"p\":" << p_[ka::_1 = phx::bind(&generic_dt::p, ka::_val)]))
                << ka::lit("}");
        }

        ka::rule<OutputIterator, generic_dt()> pg;
        fixed_dt_generator<OutputIterator> f_;
        calendar_dt_generator<OutputIterator> c_;
        point_dt_generator<OutputIterator> p_;
    };

    /** @brief generator for ts_info
    *
    * ts_info provides a minimal set of dtss time-series
    * and is the result-item of the find_ts_request.
    * @see find_ts_request
    */
    template<class OutputIterator>
    struct ts_info_generator:ka::grammar<OutputIterator,ts_info()> {
        ts_info_generator();
        ka::rule<OutputIterator,ts_info()> pg;
        utctime_generator<OutputIterator> time_;
        utcperiod_generator<OutputIterator> period_;
    };

    /** @brief a ts_info vector generator
    *
    * outputs:[ ts_infos ]
    * @see ts_info_generator
    */
    template<class OutputIterator>
    struct ts_info_vector_generator:ka::grammar<OutputIterator,std::vector<ts_info>()> {

        ts_info_vector_generator():ts_info_vector_generator::base_type(tsig) {
            tsig = '[' << -( tsi_ % ',') << ']'
            ;
            tsig.name("ts_info_vector");
        }
        ka::rule<OutputIterator,std::vector<ts_info>()> tsig;
        ts_info_generator<OutputIterator> tsi_;
    };

    /** geo://<geo_db>/v/g/e/t */
    template<class OutputIterator>
    struct geo_ts_url_generator: ka::grammar<OutputIterator, geo_ts_id() > {
        geo_ts_url_generator(std::string prefix="geo://"): geo_ts_url_generator::base_type(pg),prefix{prefix} {
            using ka::int_;
            using ka::long_;
            pg = ka::lit(this->prefix) << ka::string[ka::_1=phx::bind(&geo_ts_id::geo_db,ka::_val)] 
                << ka::lit("/") << int_[ka::_1=phx::bind(&geo_ts_id::v, ka::_val)]
                << ka::lit("/") << int_[ka::_1=phx::bind(&geo_ts_id::g, ka::_val)]
                << ka::lit("/") << int_[ka::_1=phx::bind(&geo_ts_id::e, ka::_val)]
                << ka::lit("/") << long_[ka::_1=phx::bind(&geo_ts_id::secs, ka::_val)]
                ;
            pg.name("geo_ts_url");
        }
        ka::rule<OutputIterator, geo_ts_id()> pg;
        string prefix;
    };

    using generator_output_iterator = std::back_insert_iterator<string>;
    extern template struct point_generator<generator_output_iterator>;
    extern template struct ts_info_generator<generator_output_iterator>;
    extern template struct utctime_generator<generator_output_iterator>;
    extern template struct utcperiod_generator<generator_output_iterator>;
    extern template struct apoint_ts_generator<generator_output_iterator>;
}

