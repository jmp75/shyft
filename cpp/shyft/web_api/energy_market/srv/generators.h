/** Ths file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
 * See file COPYING for more details.
 */

#pragma once
#include <shyft/web_api/web_api_generator.h>
#include <shyft/web_api/energy_market/generators.h>

#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>

namespace shyft::web_api::generator {
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;
    using shyft::energy_market::stm::srv::stm_run;
    using shyft::energy_market::stm::srv::stm_run_;
    using shyft::energy_market::stm::srv::stm_session;

    /** @brief generator for strings with added escape character at quoation marks
     *
     * @tparam OutputIterator
     */
    template <class OutputIterator>
    struct escaped_string_generator : ka::grammar<OutputIterator, std::string()>
    {
        escaped_string_generator()
            : escaped_string_generator::base_type(esc_str)
        {
            esc_char.add('\"', "\\\"");

            esc_str = *(esc_char | ka::print | "\\x" << ka::hex );
        }
        ka::rule<OutputIterator, std::string()> esc_str;
        ka::symbols<char, char const*> esc_char;
    };
    /** @brief generator for model_info **/
    template <class OutputIterator>
    struct model_info_generator: ka::grammar<OutputIterator, model_info()> {
        model_info_generator(): model_info_generator::base_type(pg) {
            using ka::int_;
            using ka::_1;
            using ka::_val;
            pg = ka::lit("{")
                << ka::lit("\"id\":") << int_[_1 = phx::bind(&model_info::id, _val)] << comma
                << ka::lit("\"name\":") << quote << ka::string[_1 = phx::bind(&model_info::name, _val)] << quote << comma
                << ka::lit("\"created\":") << time_[_1 = phx::bind(&model_info::created, _val)] << comma
                << ka::lit("\"json\":") << quote << esc_str_[_1 = phx::bind(&model_info::json, _val)] << quote
                << ka::lit("}");
        }

        ka::rule<OutputIterator, model_info()> pg;
        utctime_generator<OutputIterator> time_;
        escaped_string_generator<OutputIterator> esc_str_;
    };
    emit_type(model_info, model_info_generator);
    x_emit_vec(model_info);
    x_emit_shared_ptr(model_info);

    /** @brief generator for model_ref **/
    template<class OutputIterator>
    struct model_ref_generator: ka::grammar<OutputIterator, model_ref()> {
        model_ref_generator(): model_ref_generator::base_type(pg) {
            using ka::_1;
            using ka::_val;
            using ka::int_;

            pg = ka::lit("{")
                << ka::lit("\"host\":") << quote << ka::string[_1 = phx::bind(&model_ref::host, _val)] << quote << comma
                << ka::lit("\"port_num\":") << int_[_1 = phx::bind(&model_ref::port_num, _val)] << comma
                << ka::lit("\"api_port_num\":") << int_[_1 = phx::bind(&model_ref::api_port_num, _val)] << comma
                << ka::lit("\"model_key\":") << quote << ka::string[_1 = phx::bind(&model_ref::model_key, _val)] << quote
                << ka::lit("}");
        }

        ka::rule<OutputIterator, model_ref()> pg;
    };
    emit_type(model_ref, model_ref_generator);
    x_emit_shared_ptr(model_ref);

    /** @brief generator for stm_run **/
    template <class OutputIterator>
    struct stm_run_generator: ka::grammar<OutputIterator, stm_run()> {
        stm_run_generator(): stm_run_generator::base_type(pg) {
            using ka::_1;
            using ka::_val;
            using ka::int_;

            labels_ = ka::lit(arr_begin) << -((quote << ka::string << quote) % ',') << arr_end;
            mr_ptr_ = mr_[_1 = phx::bind([](auto m){return *m;}, _val)];
            pg = ka::lit("{")
                << ka::lit("\"id\":") << int_[_1 = phx::bind(&stm_run::id, _val)] << comma
                << ka::lit("\"name\":") << quote << ka::string[_1 = phx::bind(&stm_run::name, _val)] << quote << comma
                << ka::lit("\"created\":") << time_[_1 = phx::bind(&stm_run::created, _val)] << comma
                << ka::lit("\"json\":") << quote << esc_str_[_1 = phx::bind(&stm_run::json, _val)] << quote << comma
                << ka::lit("\"labels\":") << labels_[_1 = phx::bind(&stm_run::labels, _val)] << comma
                << ka::lit("\"model_refs\":")
                    << (arr_begin << -(mr_ptr_ % ',') << arr_end)[_1 = phx::bind(&stm_run::model_refs, _val)]
                << ka::lit("}");
        }

        ka::rule<OutputIterator, stm_run()> pg;
        model_ref_generator<OutputIterator> mr_;
        ka::rule<OutputIterator, model_ref_()> mr_ptr_;
        utctime_generator<OutputIterator> time_;
        escaped_string_generator<OutputIterator> esc_str_;
        ka::rule<OutputIterator, vector<std::string>> labels_;
    };
    emit_type(stm_run, stm_run_generator);
    x_emit_shared_ptr(stm_run);

    /** @brief generator for stm_session **/
    template<class OutputIterator>
    struct stm_session_generator : ka::grammar<OutputIterator, stm_session()> {
        stm_session_generator(): stm_session_generator::base_type(pg)
        {
            using ka::_1;
            using ka::_val;
            using ka::int_;

            labels_ = ka::lit(arr_begin) << -((quote << ka::string << quote) % ',') << arr_end;
            run_ptr_ = run_[_1 = phx::bind([](auto r){return *r;},_val)];
            runs_ = ka::lit(arr_begin) << -(run_ptr_% ',') << arr_end;

            pg = ka::lit("{")
                << ka::lit("\"id\":") << int_[_1 = phx::bind(&stm_session::id, _val)] << comma
                << ka::lit("\"name\":") << quote << ka::string[_1 = phx::bind(&stm_session::name, _val)] << quote << comma
                << ka::lit("\"created\":") << time_[_1 = phx::bind(&stm_session::created, _val)] << comma
                << ka::lit("\"json\":") << quote << esc_str_[_1 = phx::bind(&stm_session::json, _val)] << quote << comma
                << ka::lit("\"labels\":") << labels_[_1 = phx::bind(&stm_session::labels, _val)] << comma
                << ka::lit("\"runs\":") << runs_[_1 = phx::bind(&stm_session::runs, _val)] << comma
                << ka::lit("\"base_model\":") << mr_[_1 = phx::bind(&stm_session::base_mdl, _val)] << comma
                << ka::lit("\"task_name\":") << quote << ka::string[_1 = phx::bind(&stm_session::task_name, _val)] << quote
                << ka::lit("}");
        }

        ka::rule<OutputIterator, stm_session()> pg;
        model_ref_generator<OutputIterator> mr_;
        stm_run_generator<OutputIterator> run_;
        ka::rule<OutputIterator, stm_run_()> run_ptr_;
        utctime_generator<OutputIterator> time_;
        escaped_string_generator<OutputIterator> esc_str_;
        ka::rule<OutputIterator, vector<std::string>> labels_;
        ka::rule<OutputIterator, vector<stm_run_>> runs_;
    };
    emit_type(stm_session, stm_session_generator);
    x_emit_shared_ptr(stm_session);
}