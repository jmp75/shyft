/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>
#include <map>
#include <memory>

namespace shyft::web_api::grammar {
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;
    using shyft::energy_market::stm::srv::stm_run;
    using shyft::energy_market::stm::srv::stm_run_;
    using shyft::energy_market::stm::srv::stm_session;

    /** @brief Utility function to help extract value to a member of a class
     * when that member is optional in the input to parser.
     * NOTE: The reason for this helper function is that the phx::bind is unable
     * to resolve the return type of get_value_or lazily.
     *
     * Example:
     *      (-int_)[_val = phx::bind(get_value_or<int>(_1, 0))]
     *      If int_ parses succesfully to some integer, _val will be set to this,
     *      otherwise _val = 0, in this case.
     *
     * @tparam T: value_type of member variable
     * @param t: parsed value from optional section of parser
     * @param v: default value, if optional part was not present
     * @return equivalent to (t) ? t.get() : v;
     */
    template <typename T>
    T const& get_value_or(boost::optional<T> const& t, T const& v) {
        return t.get_value_or(v);
    }

    /** @brief Grammar for model_info **/
    template <class Iterator, class Skipper=qi::ascii::space_type>
    struct model_info_grammar : public qi::grammar<Iterator, model_info(), Skipper> {
        model_info_grammar();

        qi::rule<Iterator, model_info(), Skipper> start;
        utctime_grammar<Iterator> time_;
        quoted_string_grammar<Iterator, Skipper> string_;

        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief Grammar for model_ref **/
    template <class Iterator, class Skipper=qi::ascii::space_type>
    struct model_ref_grammar : public qi::grammar<Iterator, model_ref(), Skipper> {
        model_ref_grammar();

        qi::rule<Iterator, model_ref(), Skipper> start;
        quoted_string_grammar<Iterator, Skipper> string_;

        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief Grammar for stm_run **/
    template <class Iterator, class Skipper=qi::ascii::space_type>
    struct stm_run_grammar : public qi::grammar<Iterator, stm_run(), Skipper> {
        stm_run_grammar();

        qi::rule<Iterator, stm_run(), Skipper> start;
        qi::rule<Iterator, vector<string>(), Skipper> labels_;
        qi::rule<Iterator, vector<model_ref_>(), Skipper> mr_list_;
        model_ref_grammar<Iterator, Skipper> mr_;
        qi::rule<Iterator, model_ref_(), Skipper> mr_ptr_;
        quoted_string_grammar<Iterator, Skipper> string_;
        utctime_grammar<Iterator> time_;

        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief Grammar for stm_session */
    template<class Iterator, class Skipper=qi::ascii::space_type>
    struct stm_session_grammar : public qi::grammar<Iterator, stm_session(), Skipper> {
        stm_session_grammar();

        qi::rule<Iterator, stm_session(), Skipper> start;
        qi::rule<Iterator, vector<stm_run_>(), Skipper> runs_;
        stm_run_grammar<Iterator, Skipper> run_;
        qi::rule<Iterator, stm_run_(), Skipper> run_ptr_;
        utctime_grammar<Iterator> time_;
        quoted_string_grammar<Iterator, Skipper> string_;
        qi::rule<Iterator, vector<string>(), Skipper> labels_;
        model_ref_grammar<Iterator, Skipper> mr_;

        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Forward declare the templates we need
     */
    // From shyft/web_api/web_api_grammar.h
    // using request_iterator_t = const char*;
    // using request_skipper_t = qi::ascii::space_type;
    extern template struct model_info_grammar<request_iterator_t, request_skipper_t>;
    extern template struct model_ref_grammar<request_iterator_t, request_skipper_t>;
    extern template struct stm_run_grammar<request_iterator_t, request_skipper_t>;
    extern template struct stm_session_grammar<request_iterator_t, request_skipper_t>;
}
