#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

#include <shyft/energy_market/constraints.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>

#include <shyft/mp.h>
#include <shyft/energy_market/a_wrap.h>

namespace shyft::web_api::energy_market {
    using namespace shyft::web_api::generator;
    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    using shyft::energy_market::stm::srv::srv_shared_lock;// for read access
    using shyft::energy_market::stm::srv::srv_unique_lock;// for exclusive write access to models
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::stm_hps;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::run_parameters;
    
    using shyft::energy_market::core::constraint_base;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using shyft::energy_market::stm::subscription::proxy_attr_observer_;

    using shyft::web_api::grammar::phrase_parser;
    
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::abin_op_ts;
    using shyft::time_series::dd::ts_as;
    using shyft::energy_market::attr_traits::exists;
    
    map<string, model_info> request_handler::get_model_infos() { return srv->do_get_model_infos(); }
    
    bg_work_result request_handler::handle_request(const request &req) {
        if (req.keyword == "read_model") {
            return handle_read_model_request(req.request_data);
        } else if (req.keyword == "read_attributes") {
            return handle_read_attribute_request(req.request_data);
        } else if (req.keyword == "get_model_infos") {
            return handle_get_model_infos_request(req.request_data);
        } else if (req.keyword == "get_hydro_components") {
            return handle_get_hydro_components_request(req.request_data);
        } else if (req.keyword == "set_attributes") {
            return handle_set_attribute_request(req.request_data);
        } else if (req.keyword == "unsubscribe") {
            return handle_unsubscribe_request(req.request_data);
        } else if (req.keyword == "fx") {
            return handle_fx_request(req.request_data);
        } else if (req.keyword == "run_params") {
            return handle_run_params_request(req.request_data);
        }
        return bg_work_result("Unknown keyword: " + req.keyword);
    }

    /** called by the web-server timer serving the web-socket connections at regular intervals */
    bg_work_result request_handler::do_subscription_work(observer_base_ const&o) {
        // We support subscription on proxy-type expressions
        // so ts_expression_observer_ , url, looks different for the in-memory em server
        // instead of 'ts-url', we use request_id (assumed to be unique between clients, so that's a possible problem!),
        // ...a map o->request_id -> request_json, (from the original request)
        // and then we can simply do
        //  handle_read_attribute_request(request_json)
        //
        if(o->recalculate()) { // ok, is there a difference(version-number updated, ref. the last sent)
            auto proxy_observer = std::dynamic_pointer_cast<proxy_attr_observer>(o);
            return proxy_observer->re_emit_response();
        } else {
            return bg_work_result{};
        }
    }

    bg_work_result request_handler::do_the_work(const string &input) {
        // 1: Parse into a request
        // 2: Error handling: On success, do a switch on keyword
        // 3: Handle every case, and emit a response string

        bg_work_result b;
        // 1) Parse the request:
        request arequest;
        shyft::web_api::grammar::request_grammar<const char *> request_;
        bool ok_parse = false;
        bg_work_result response;
        try {
            ok_parse = phrase_parser(input.c_str(), request_, arequest);
            if (ok_parse) {
                response = handle_request(arequest);
            } else {
                response = bg_work_result{string("not understood: ") + input};
            }
        } catch (std::runtime_error const &re) {
            response = bg_work_result{string("request_parse:") + re.what()};
        }
        return response;
    }

    shared_ptr<stm_system_context> request_handler::get_system(const string &mid) {
        return srv->do_get_context(mid);
    }

    
    /** @brief visitor class for dispatching how to read attributes based on type
     * 
     */
    struct read_proxy_handler : public boost::static_visitor<attribute_value_type> {
        read_proxy_handler(const json& data, server *const srv, std::function<bg_work_result(json const&)>&& cb): srv{srv} {
            // Set up for subscription:
            auto osub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
            if (osub) {
                json sub_data = data;
                sub_data.m.erase("subscribe");
                subscription = std::make_shared<proxy_attr_observer>(srv, data.required<string>("request_id"), sub_data,
                    std::move(cb));
                subscription->recalculate();//important: since we emit version 0, set initial value of terminals to 0
            }
            
            // Setting up read_type, -period and time_axis;
            read_type = boost::get_optional_value_or(data.optional<string>("read_type"), "read");
            if (read_type == "percentiles")
                percentiles = data.required<vector<int>>("percentiles");
            ta = data.required<generic_dt>("time_axis");
            read_period = boost::get_optional_value_or(data.optional<utcperiod>("read_period"), ta.total_period());
        }
        
        attribute_value_type operator()(apoint_ts const& ts) {
            apoint_ts nts;
            if (ts.needs_bind()) { // Case 1: The series is unbound:
                if ( !srv || !(srv->dtss) )
                    throw std::runtime_error("Dtss has to be set to read unbound time series.");
                dtss::ts_vector_t tsv;
                tsv.emplace_back(ts.clone_expr());
                tsv = srv->dtss->do_evaluate_ts_vector(read_period, tsv, false, false, read_period);
                nts = tsv[0];
            } else {
                nts = ts;
            }
            return nts.average(ta);
        }
        
        
        template<typename V>
        attribute_value_type operator()(shared_ptr<map<utctime,V>> const& tv) {
            // We only read the values that are in the read_period:
            auto res = make_shared<map<utctime, V>>();
            for (auto const& kv : *tv) {
                if (read_period.contains(kv.first))
                    res->insert(res->end(),kv);
            }
            return res;
        }

        template<typename V>
        attribute_value_type operator()(V const& v) {
            return v;

        }
        
        proxy_attr_observer_ subscription = nullptr;
        string read_type;
        vector<int> percentiles;
        generic_dt ta;
        utcperiod read_period;
        server *const srv;

    };

    bool has_id(vector<string> const& vec, const char* id) {
        return std::find_if(vec.begin(), vec.end(),
            [id](auto e) {
               return !strcmp(e.c_str(), id);
            }) != vec.end();
    }

    template<class T>
    vector<json> get_proxy_attributes(
        T const & t,
        vector<string> const& attr_ids,
        read_proxy_handler& rph)
    {
        // Set up result:
        vector<json> attr_vals;

        // Iterate over each attribute:
        auto constexpr attribute_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(
            attribute_paths, // The sequence
            [&](auto m) {
                // 0. Check if current id is in the specified list:
                if (has_id(attr_ids, mp::leaf_accessor_id_str(m))) {
                    json attr_struct;
                    attr_struct["attribute_id"] = std::string(mp::leaf_accessor_id_str(m));
                    auto val = mp::leaf_access(t,m);
                    if (exists(val)) {
                        attr_struct["data"] = rph(mp::leaf_access(t,m));
                    } else {
                        attr_struct["data"] = string("not found");
                    }
                    if (rph.subscription) {
                        rph.subscription->add_subscription(t, m);
                    }
                    attr_vals.emplace_back(attr_struct);
                }
            }
        );
        return attr_vals;
    }


    /** @brief Reads a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     * @param rph: instance of handler, used in innermost loop to handle the different value types.
     * @return
     */
    template<class T, class Tc>
    vector<json> get_attribute_value_table(vector<shared_ptr<Tc>> const& vecvals, json const& data, read_proxy_handler& rph){
        vector<json> result;
        // Get out component_ids and attribute_ids:
        auto comp_ids = data.required<vector<int>>("component_ids");
        auto attr_ids = data.required<vector<string>>("attribute_ids");

        shared_ptr<T> comp;
        // For each attribute container:
        for (auto cid : comp_ids) {
            json comp_struct;
            comp_struct["component_id"] = cid;
            // Find container in vector:
            auto it = find_if(vecvals.begin(), vecvals.end(), [&cid](auto el) {
                return el->id == cid;
            });
            if (it == vecvals.end()) {
                comp_struct["component_data"] = string("Unable to find component");
            } else {
                comp = dynamic_pointer_cast<T>(*it);
                comp_struct["component_data"] = get_proxy_attributes(*comp,attr_ids, rph);
            }
            result.push_back(comp_struct);
        }
        return result;
    }

    /** @brief generate a response for a read_attributes request */
    bg_work_result request_handler::handle_read_attribute_request(const json &data) {
        // Get model and HPS:
        // we need to have a shared-lock on the model in order to read the model.
        auto mid = data.required<string>("model_key");
        auto hps_id = data.required<int>("hps_id");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab shared lock here
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading


        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;
        
        // Prepare response:
        auto req_id = data.required<string>("request_id");
        
        // We use a read_proxy_handler (visitor) to hold subscription and read data,
        // and pass that along to the innermost loop over components and attributes
        read_proxy_handler vis(data, srv,
            [this](json const& data) {
                return handle_read_attribute_request(data);
            }
        );
        
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
                .def("hps_id", hps->id);
            // Get reservoir attributes:
            auto comp_attrs = data.optional<json>("reservoirs");
            if (comp_attrs) oo.def("reservoirs",
                get_attribute_value_table<reservoir>(hps->reservoirs, *comp_attrs, vis));
            // Aggregates:
            comp_attrs = data.optional<json>("units");
            if (comp_attrs) oo.def("units", 
                get_attribute_value_table<unit>(hps->units, *comp_attrs, vis));
            // Power plants:
            comp_attrs = data.optional<json>("power_plants");
            if (comp_attrs) oo.def("power_plants",
                get_attribute_value_table<power_plant>(hps->power_plants, *comp_attrs, vis));
            // Waterways:
            comp_attrs = data.optional<json>("waterways");
            if (comp_attrs) oo.def("waterways",
                get_attribute_value_table<waterway>(hps->waterways, *comp_attrs, vis));
            // Catchments:
            comp_attrs = data.optional<json>("catchments");
            if (comp_attrs) oo.def("catchments",
                get_attribute_value_table<catchment>(hps->catchments, *comp_attrs, vis));
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    }

    /** @brief Helper function for disambiguating which array to put
     * subscription in, based on value type
     * @tparam T1
     * @tparam Ts
     * @return
     */
    template<class T1, class... Ts>
    constexpr bool is_one_of() noexcept {
        return (std::is_same_v<T1, Ts> || ...);
    }


    /** @brief handler class for setting attributes on a model.
     *
     */
    class set_attribute_handler {
            // Attributes:
            server* const srv; ///<Pointer to the server we need access to to handle time series correctly
            map<string, vector<string>> subs;///<Keep track of what attributes have been changed
            bool merge; ///<Whether to merge values by the "shotgun method", i.e. keep all values that don't directly overlap.
            bool recreate;///<Remove the old value entirely
            string mkey;///<Model key that will need to be prefixed to all subscription updates.
        public:
            set_attribute_handler(server* const srv, bool merge=false, bool recreate=false, const string& mid="")
                : srv{srv}, merge{merge}, recreate{recreate}, mkey{mid}
            {
                subs["time-series"] = {};
                subs["other"] = {};
            }

            /** @brief Need a override for attribute_value_type-variant, to safely unpack:
             *
             */
            template<class V>
            string apply(V& old_val, attribute_value_type const& new_val) {
                auto f = [this,&old_val](auto arg) { return this->apply(old_val, arg); };
                return boost::apply_visitor(f, new_val);
            }

            // Apply new_val to old_val based on the state of this, i.e. the values of merge, recreate and the server
            // to communicate with.
            template<class V>
            string apply(V& old_val, V const& new_val) {
                 // Update with new values:
                string res;
                if (exists(old_val)) {
                    res = merge_values(old_val, new_val);
                } else {
                    old_val = new_val;
                    res = "OK";
                }
                return res;
            }

            template<class V, class U>
            string apply(V& /*old_val*/, U const& /*new_val*/) {
                return "type mismatch";
            }


            // Notify accrued changes to the subscription managers:
            void notify_changes() {
                 // prep subscription identifiers to run notify on:
                bool sub_active= srv->sm && srv->sm->is_active();
                if(sub_active) { //propagate/notify changes due to write of attributes(e.g. time-series)
                    srv->sm->notify_change(subs["other"]);
                    srv->dtss->sm->notify_change(subs["time_series"]);// notify the dtss sm about time-series changes
                }
            }

            template <class Struct, class LeafAccessor>
            void add_sub_id(Struct const& t, LeafAccessor&& la) {
                // Set up result:
                string sub_id ="dstm://M" + mkey;
                sub_id.reserve(30);
                {
                    auto rbi = std::back_inserter(sub_id);
                    t.generate_url(rbi);
                    *rbi++ = '.';
                }
                sub_id += mp::leaf_accessor_id_str(la);
                // Get value type of leaf_accessor:
                //auto value_type = mp::leaf_accessor_type(la);
                using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
                // Send to correct subscription manager based on type:
                if constexpr (is_one_of<V, apoint_ts, absolute_constraint, penalty_constraint>()) {
                    subs["time_series"].push_back(sub_id);
                } else {
                    subs["other"].push_back(sub_id);
                }
            }

    private:
        string merge_values(apoint_ts& old_val, const apoint_ts& new_val) {
            if (ts_as<gpoint_ts>(old_val.ts)) { // Case 1: Treated as a local variable
                if (!recreate || merge) {
                    old_val.merge_points(new_val);
                } else
                    old_val = new_val;
            } else if (auto sts = ts_as<aref_ts>(old_val.ts)) { // Case 2: Reference to a time series
                // Case 2.0: In the case that the underlying time-series is unbounded:
                if (sts->needs_bind() && !(sts->id.rfind("dstm://", 0) == 0)) {
                    if (srv && srv->dtss) {
                        dtss::ts_vector_t tsv;
                        tsv.push_back(apoint_ts(old_val.id(), new_val));
                        if (merge) srv->dtss->do_merge_store_ts(tsv, false);
                        else srv->dtss->do_store_ts(tsv, recreate, false);
                        // NO need to update subs here. They are incremented in the do_store_ts function
                        return "stored to dtss";
                    } else {
                        return "Cannot set dtss time series without dtss.";
                    }
                } else {
                    if (!recreate || merge) {
                        old_val.merge_points(new_val);
                    } else {
                        old_val = new_val;
                    }
                }
            } else { // Case 3: The time series is an expression and should not be set
                return "Time series is an expression. Cannot be set.";
            }
            return "OK";
        }

        string merge_values(absolute_constraint& old_val, const absolute_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, old_val.flag);
            return "OK";
        }

        string merge_values(penalty_constraint& old_val, const penalty_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, new_val.flag);
            merge_values(old_val.cost, new_val.cost);
            merge_values(old_val.penalty, new_val.penalty);
            return "OK";
        }


        template <class T>
        string merge_values(shared_ptr<map<utctime, shared_ptr<T>>>& old_val, const shared_ptr<map<utctime, shared_ptr<T>>>& new_val) {
            // We want to overwrite, so insert doesn't do the trick:
            if (!recreate || merge) {
                for (auto const &kv : *new_val) {
                    (*old_val)[kv.first] = kv.second;
                }
            } else {
                old_val = new_val;
            }
            return "OK";
        }

        template <class T>
        string merge_values(T& old_val, T& new_val) {
            old_val = new_val;
            return "NOT OK";
        }
    };

    auto get_attr_struct(vector<json> const& attr_vals, string const& attr_id) {
        return std::find_if(attr_vals.begin(), attr_vals.end(),
            [&attr_id](auto const& element) -> bool {
                // Here we check if the json structure e contains the key "attribute_id",
                // and if so, if it compares equal to the provided attr_id.
                auto opt = ((json) element).optional<string>("attribute_id"); // Get compiler error: "invalid use of 'class std::optional<std::__cxx11::basic_string<char>>" if not cast
                if (opt) {
                    return *opt == attr_id;
                } else {
                    return false;
                }
            }
        );
    }
    /** @brief
     *
     * @tparam T : Type whose attributes we want to change. Assumed to be a hana-struct
     * @param t : The instance of type T whose attribute values we want to change
     * @param attr_vals : The values to set or merge in t's attributes. Each entry of the vector requires the
     *   following:
     *      * "attribute_id" : <string> (ID of the attribute to change)
     *      * "value" : The value to be inserted/merged in.
     * @return a list of json. Each entry is the attribute_id that has been processed together with a "status",
     *  stating whether the change was successful or if it  encountered an error.
     */
    template<class T>
    vector<json> set_attribute_values(T& t, vector<json> const& attr_vals, set_attribute_handler& handler) {
        // Initialize result:
        vector<json> result;
        result.reserve(attr_vals.size());

        // Go through each attribute:
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(attr_paths,
            [&t, &attr_vals, &result, &handler](auto m) {
                auto attr_id = string(mp::leaf_accessor_id_str(m));
                auto it = get_attr_struct(attr_vals, attr_id);
                if (it != attr_vals.end()) { // If we found the current attribute amongst the ones to change
                    // Initialize structure to be inserted into result
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    // Get out value to be inserted/merged:
                    auto value = it->required("value"); // value is now of type value_type -- a boost::variant
                    // Set value
                    auto f = [&handler, &t, &m](auto arg) {
                        auto& attr = mp::leaf_access(t,m);
                        return handler.apply(attr, arg);
                    };
                    // Alternatively, we could have used hana::partial like:
                    // auto f = hana::partial([&handler](auto& attr, auto arg) { return handler.apply(attr, arg); }, mp::leaf_access(t,m));
                    attr_res["status"] = boost::apply_visitor(f, value);
                    // Add subscription ID to notify change later:
                    handler.add_sub_id(t, m);
                    // Insert into result vector:
                    result.push_back(attr_res);
                }
            }
        );
        return result;
    }
    
    
    struct check_empty_visitor : public boost::static_visitor<bool> {
        template<typename T>
        bool operator()(const vector<T>& t) const {
            return t.size() == 0;
        }
        
        template<typename T>
        bool operator()(const T& ) const {
            return true;
        }
    };
    /** @brief Sets a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @param srv ref to server so that we can notify on subscription (server.sm subscription manager)
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     *          @key values: vector<attribute_value_type> of values to set for each component and attribute
     *              Must have length equal to |component_ids| * |attribute_ids|.
     * @param mid: The model key. Used as part of prefix to subscription id's to notify change on.
     * @param merge: Whether to merge new values with old values, if present.
     * @param recreate: Should old value be completely removed?
     * @return
     */
    template<class T, class Tc>
    vector<json> set_attribute_values_vector(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, set_attribute_handler& handler){
        vector<json> result;

        shared_ptr<T> comp;
        // Iterate over each element of the json-list:
        for (auto &comp_data : data) {
            // Each element of the vector is required to have:
            // int component_id
            // vector<json> attribute_data
            auto cid = comp_data.required<int>("component_id");
            auto temp = comp_data.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
            vector<json> attr_vals;
            if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                attr_vals = comp_data.required<vector<json>>("attribute_data");
            }
            // Initialize json to store results for current component:
            json comp_struct;
            comp_struct["component_id"] = cid;
            // Get out component:
            auto it = find_if(vecvals.begin(), vecvals.end(), [cid](auto e) { return e->id == cid; });
            if (it != vecvals.end()) {
                // We need to downcast the component because of inheritance.
                if ( (comp = dynamic_pointer_cast<T>(*it)) ) {
                    comp_struct["status"] = set_attribute_values(*comp, attr_vals, handler); // set_attribute_values(comp, attr_vals....)
                } else {
                    comp_struct["status"] = "Unable to cast hydro component";
                }
            } else {
                comp_struct["status"] = "Unable to find component";
            }
            // Add component's json to result list:
            result.push_back(comp_struct);
        }
        return result;
    }
        
    bg_work_result request_handler::handle_set_attribute_request(const json &data) {
        // Get model and HPS:
        auto mid = data.required<string>("model_key");
        auto ctx = get_system(mid);
        srv_unique_lock ul(ctx->mtx);// grab unique lock for modification here
        auto mdl=ctx->mdl;//for bw compat code below, and we have a mutable stm_system
        auto hps_id = data.required<int>("hps_id");
        
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");

        auto merge = boost::get_optional_value_or(data.optional<bool>("merge"), false);
        auto recreate = boost::get_optional_value_or(data.optional<bool>("recreate"), false);
        // Set up set_attribute_handler;
        set_attribute_handler handler(srv, merge, recreate, mid);

        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
                .def("hps_id", hps->id);
            // Get reservoir attributes:
            auto comp_attrs = data.optional<vector<json>>("reservoirs");
            if (comp_attrs) oo.def("reservoirs",
                set_attribute_values_vector<reservoir>(hps->reservoirs, *comp_attrs, handler));
            // Aggregates:
            comp_attrs = data.optional<vector<json>>("units");
            if (comp_attrs) oo.def("units",
                set_attribute_values_vector<unit>(hps->units, *comp_attrs, handler));
            // Power plants:
            comp_attrs = data.optional<vector<json>>("power_plants");
            if (comp_attrs) oo.def("power_plants",
                set_attribute_values_vector<power_plant>(hps->power_plants, *comp_attrs, handler));
            // Waterways:
            comp_attrs = data.optional<vector<json>>("waterways");
            if (comp_attrs) oo.def("waterways",
                set_attribute_values_vector<waterway>(hps->waterways, *comp_attrs, handler));
            // Catchments:
            comp_attrs = data.optional<vector<json>>("catchments");
            if (comp_attrs) oo.def("catchments",
                set_attribute_values_vector<catchment>(hps->catchments, *comp_attrs, handler));
        }
        handler.notify_changes();
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }

    template<class OutputIterator>
    void emit_system_hps(OutputIterator &oi, const stm_system &mdl) {
        emit_vector_fx(oi, mdl.hps, [](OutputIterator &oi, auto el) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", el->id)
                .def("name", el->name);
        });
    }

    /** @brief handle requests of the form:
     * 
     * @param data: {
     *          "model_key": <string>
     *      } 
     * @return 
     */
    bg_work_result request_handler::handle_read_model_request(const json &data) {
        // Get model:
        auto mid = data.required<string>("model_key");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab a shared lock while fiddling model with output
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        // Get request id:
        auto req_id = data.required<string>("request_id");
        std::string response = string("{\"request_id\":\"") + req_id + string("\",\"result\":");
        auto sink = std::back_inserter(response);
        // Emit bare-bones model structure:
		{
			emit_object<decltype(sink)> oo(sink);
			oo.def("model_key", mid)
			    .def("id", mdl->id)
				.def("name", mdl->name)
				.def("json", mdl->json)
				.def_fx("hps", [&mdl](decltype(sink) oi) {
				emit_system_hps(oi, *mdl);
					});
		}
        response += "}";
        return bg_work_result{response};
    }

    bg_work_result request_handler::handle_get_model_infos_request(const json &data) {
        // Communicate with server
        auto model_infos = get_model_infos();

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        emit_vector_fx(sink, model_infos, [](auto oi, auto mi) {
            emit_object<decltype(oi)> oo(oi);
            oo.def("model_key", mi.first)
                .def("id", mi.second.id)
                .def("name", mi.second.name);
        });
        response += "}";
        return bg_work_result{response};
    }


    /** @brief Get out a list of IDs for which attributes have values attached.
     *
     * @tparam T : Hana-struct type, e.g. reservoir, powerplant &c.
     * @param t  : instance to check which attributes are set for.
     * @return : vector of attribute-ID's in string format.
     */
    template<class T>
    vector<string> available_attributes(T const &t) {
        // Accessor functions for every attribute in the struct T (recursive, so gives attributes of nested structs as well)
        auto constexpr attr_paths = mp::leaf_accessors(hana::type_c<T>);

        vector<string> attr_ids{};
        // Iterate over every attribute:
        hana::for_each(attr_paths, // The sequence
            [&attr_ids, &t](auto m) {
                // m is here a pair (attr_id, attr_accessor_function);
                if (exists(mp::leaf_access(t, m))) {
                    attr_ids.push_back(string(mp::leaf_accessor_id_str(m)));
                }
            }
        );
        return attr_ids;
    }

    template<class OutputIterator>
    void emit_power_plant_skeleton(OutputIterator &oi, const power_plant &pp, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", pp.id)
            .def("name", pp.name)
            .def_fx("units", [&pp](auto oi) {
                *oi++ = arr_begin;
                for (auto it = pp.units.begin(); it != pp.units.end(); ++it) {
                    if (it != pp.units.begin()) *oi++ = comma;
                    emit(oi, (*it)->id);
                }
                *oi++ = arr_end;
            });
        if (get_data) oo.def("set_attrs", available_attributes(pp));
    }

    template<class OutputIterator>
    void emit_waterway_skeleton(OutputIterator &oi, const waterway &wr, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", wr.id)
            .def("name", wr.name)
            .def("upstreams", wr.upstreams)
            .def("downstreams", wr.downstreams);
        if (get_data) oo.def("set_attrs", available_attributes(wr));
    }


    template<class OutputIterator>
    void emit_hps_reservoirs(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.reservoirs, [&get_data](auto oi, auto res_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", res_->id)
                .def("name", res_->name);
            if (get_data) {
                oo.def("set_attrs", available_attributes(*dynamic_pointer_cast<reservoir>(res_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_units(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.units, [&get_data](auto oi, auto unit_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", unit_->id)
                .def("name", unit_->name);
            if (get_data) {
                oo.def("set_attrs", available_attributes(*dynamic_pointer_cast<unit>(unit_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_power_plants(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.power_plants, [&get_data](auto oi, auto pp_) {
            emit_power_plant_skeleton(oi, *dynamic_pointer_cast<power_plant>(pp_), get_data);
        });
    }

    template<class OutputIterator>
    void emit_hps_waterways(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.waterways, [&get_data](auto oi, auto wr_) {
            emit_waterway_skeleton(oi, *dynamic_pointer_cast<waterway>(wr_), get_data);
        });
    }

    bg_work_result request_handler::handle_get_hydro_components_request(const json &data) {
        // Get model ID and HPS ID:
        auto mid = data.required<string>("model_key");
        auto hps_id = data.required<int>("hps_id");

        // Find if whether to find available data as well:
        auto avail_data_kwarg = data.optional("available_data");
        bool avail_data = false;
        if (avail_data_kwarg) {
            avail_data = boost::get<bool>(*avail_data_kwarg);
        }
        // Get model and search for requested HPS in model:
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// shared-lock because we are reading.
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                               [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        { // Has to be in scope so destructor is called appropriately.
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
                .def("hps_id", hps->id)
                .def_fx("reservoirs", [&hps, &avail_data](auto oi) {
                    emit_hps_reservoirs(oi, *hps, avail_data);
                })
                .def_fx("units", [&hps, &avail_data](auto oi) {
                    emit_hps_units(oi, *hps, avail_data);
                })
                .def_fx("power_plants", [&hps, &avail_data](auto oi) {
                    emit_hps_power_plants(oi, *hps, avail_data);
                })
                .def_fx("waterways", [&hps, &avail_data](auto oi) {
                    emit_hps_waterways(oi, *hps, avail_data);
                });
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }
    
    bg_work_result request_handler::handle_unsubscribe_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto unsub_id = data.required<string>("subscription_id");
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("subscription_id", unsub_id)
                .def("diagnostics", string{});
        }
        
        return bg_work_result{response, unsub_id};
    }
    /** @brief handle fx(mid,fx_arg) request
     *
     * In the first approach, just call the server callback, 
     * doing no claim to the model/nor server.
     * Assume the callback will do proper claim on shared resources
     * when needed.
     */
    bg_work_result request_handler::handle_fx_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        auto fx_arg = data.required<string>("fx_arg");
        auto success= srv->fx_cb ? srv->fx_cb(model_key,fx_arg):false;
        
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("diagnostics", string{(success? "":"Failed")});
        }        
        return bg_work_result{response};
    }

    template<class T>
    vector<json> get_attribute_values(T const& t, read_proxy_handler& rph) {
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        vector<json> result{};
        result.reserve(mp::leaf_accessor_count(hana::type_c<T>));
        hana::for_each(
            attr_paths,
            [&result, &t, &rph](auto m) {
                json attr_struct;
                attr_struct["attribute_id"] = string(mp::leaf_accessor_id_str(m));
                attr_struct["data"] = mp::leaf_access(t,m);
                if (rph.subscription) {
                    rph.subscription->add_subscription(t, m);
                }
                result.push_back(attr_struct);
            }
        );

        return result;
    }
    /** @brief handle run_params request
     *
     */
    bg_work_result request_handler::handle_run_params_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        auto ctx = get_system(model_key);
        srv_shared_lock sl(ctx->mtx);
        // Generate result:
        json ndata = data;
        ndata["time_axis"] = generic_dt(); // Time_axis is required
        read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_run_params_request(data); });
        json result;
        result["model_key"] = model_key;
        result["values"] = get_attribute_values((ctx->mdl->run_params), vis);
        // Generate response:
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("result", result);
        }
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    };
}
