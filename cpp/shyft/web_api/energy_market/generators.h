/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/web_api_generator.h>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <boost/spirit/include/karma_real.hpp>

#include <algorithm>
#include <string_view>

namespace shyft::web_api::generator {
    namespace ka=boost::spirit::karma;
    namespace phx=boost::phoenix;

    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using xy_point=shyft::energy_market::hydro_power::point;
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_with_z;
    using shyft::energy_market::hydro_power::turbine_efficiency;
    using shyft::energy_market::hydro_power::turbine_description;
    using shyft::energy_market::stm::xyz_point_curve_list;
    using shyft::energy_market::stm::xy_point_curve_;
    using shyft::energy_market::stm::xyz_point_curve_;
    using shyft::energy_market::stm::xyz_point_curve_list_;
    using shyft::energy_market::stm::turbine_description_;

    using std::vector;
    using std::string;
    using std::string_view;
    using std::pair;

    using boost::spirit::karma::generate;

    using shyft::time_series::ts_point_fx;
    using shyft::time_series::point;

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;

    using namespace shyft::core;

    using std::string_view;
    using ka::int_;
    using ka::double_;
    using ka::bool_;
    namespace stm=shyft::energy_market::stm;
    constexpr char quote='"';
    constexpr char colon=':';
    constexpr char obj_begin='{';
    constexpr char obj_end='}';
    constexpr char arr_begin='[';
    constexpr char arr_end=']';
    constexpr char comma=',';
    constexpr char esc = '\\';

    template<class OutputIterator>
    void generate_url_level(OutputIterator& oi, string_view type_str, int id) {
        std::copy(begin(type_str), end(type_str), oi);
        generate(oi, int_, id);
    }
    /** @brief grammar for emitting a x,y point
    *   struct xy_point {
     *      double x,y;
     *   }
    */
    template<class OutputIterator>
    struct xy_generator:ka::grammar<OutputIterator,xy_point()> {
        xy_generator():xy_generator::base_type(pg) {
            using ka::double_;
            using ka::_1;
            using ka::_val; //        _1 = _val.x                               _1 = _val.y
            pg = '('<< double_[_1=phx::bind(&xy_point::x,_val)]<<','<<double_[_1=phx::bind(&xy_point::y,_val)]<< ')' ;
            pg.name("xy-point");
        }
        ka::rule<OutputIterator,xy_point()> pg;
    };

    /** @brief grammar for emitting a x,y point curve
     */
    template<class OutputIterator>
    struct xy_point_curve_generator:ka::grammar<OutputIterator,xy_point_curve()> {
        xy_point_curve_generator():xy_point_curve_generator::base_type(pg) {
            pg = ('['<< -(xy_%',') << ']')[ka::_1=phx::bind(&xy_point_curve::points,ka::_val)];
            pg.name("xy_point_curve");
        }
        ka::rule<OutputIterator,xy_point_curve()> pg;
        xy_generator<OutputIterator> xy_;
    };

    /** @brief grammar for emitting a x,y point curve with z
     */
    template<class OutputIterator>
    struct xy_point_curve_with_z_generator:ka::grammar<OutputIterator,xy_point_curve_with_z()> {
        xy_point_curve_with_z_generator():xy_point_curve_with_z_generator::base_type(pg) {
            pg = ka::lit("{\"z\":")<<double_[ka::_1=phx::bind(&xy_point_curve_with_z::z,ka::_val)]<<
                                   ka::lit(",\"points\":")<< xy_points_[ka::_1=phx::bind(&xy_point_curve_with_z::xy_curve,ka::_val)]<<'}';
            pg.name("xy_point_curve_with_z");
        }
        ka::rule<OutputIterator,xy_point_curve_with_z()> pg;
        xy_point_curve_generator<OutputIterator> xy_points_;
    };

    /** @brief grammar for emitting a list of x,y point curves with z
     */
    template<class OutputIterator>
    struct xyz_point_curve_list_generator:ka::grammar<OutputIterator,xyz_point_curve_list()> {
        xyz_point_curve_list_generator():xyz_point_curve_list_generator::base_type(pg) {
            pg = ('['<< -(xyz_points_%',') << ']');
            pg.name("xyz_point_curve_list");
        }
        ka::rule<OutputIterator,xyz_point_curve_list()> pg;
        xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
    };

    /** @brief grammar for emitting a turbine efficiency
     */
    template<class OutputIterator>
    struct turbine_efficiency_generator:ka::grammar<OutputIterator,turbine_efficiency()> {
        turbine_efficiency_generator():turbine_efficiency_generator::base_type(pg) {
            pg = ka::lit("{\"production_min\":")<<double_[ka::_1=phx::bind(&turbine_efficiency::production_min,ka::_val)]<<
                                          ka::lit(",\"production_max\":")<<double_[ka::_1=phx::bind(&turbine_efficiency::production_max,ka::_val)]<<
                                          ka::lit(",\"efficiency_curves\":[")<<(-(xyz_points_ %',')) [ka::_1=phx::bind(&turbine_efficiency::efficiency_curves,ka::_val)]<<ka::lit("]}");
            pg.name("turbine_effiency");
        }
        ka::rule<OutputIterator,turbine_efficiency()> pg;
        xy_point_curve_with_z_generator<OutputIterator> xyz_points_;
    };

    /** @brief grammar for emitting a turbine description.
     */
    template<class OutputIterator>
    struct turbine_description_generator:ka::grammar<OutputIterator,turbine_description()> {
        turbine_description_generator():turbine_description_generator::base_type(pg) {
            pg =
                ka::lit("{\"turbine_effiencies\":[")<<(-(t_eff_ %',')) [ka::_1=phx::bind(&turbine_description::efficiencies,ka::_val)]<<ka::lit("]}");
            pg.name("turbine_description");
        }
        ka::rule<OutputIterator,turbine_description()> pg;
        turbine_efficiency_generator<OutputIterator> t_eff_;
    };

    /** @brief grammar for emitting a message (pair<utctime, string>)
     */
    template<typename OutputIterator>
    struct t_str_generator : ka::grammar<OutputIterator, pair<utctime, string>()> {
        t_str_generator(): t_str_generator::base_type(pg) {
            pg =
                ka::lit("(") << time_ << comma << "\"" << ka::string << "\")";
        }

        ka::rule<OutputIterator, pair<utctime, string>()> pg;
        utctime_generator<OutputIterator> time_;
    };

    /** @brief grammar for emitting an absolute constraint */
    template <typename OutputIterator>
    struct absolute_constraint_generator : ka::grammar<OutputIterator, absolute_constraint()> {
        absolute_constraint_generator(): absolute_constraint_generator::base_type(pg) {
            pg = ka::lit("{")
                << ka::lit("\"limit\":") << ts_[ka::_1 = phx::bind(&absolute_constraint::limit, ka::_val)] << ","
                << ka::lit("\"flag\":") << ts_[ka::_1 = phx::bind(&absolute_constraint::flag, ka::_val)]
                << ka::lit("}");
        }
        ka::rule<OutputIterator, absolute_constraint()> pg;
        apoint_ts_generator<OutputIterator> ts_;
    };



    /** @brief grammar for emitting a penalty constraint */
    template <typename OutputIterator>
    struct penalty_constraint_generator : ka::grammar<OutputIterator, penalty_constraint()> {
        penalty_constraint_generator(): penalty_constraint_generator::base_type(pg) {
            pg = ka::lit("{")
                << ka::lit("\"limit\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::limit, ka::_val)] << ","
                << ka::lit("\"flag\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::flag, ka::_val)] << ","
                << ka::lit("\"cost\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::cost, ka::_val)] << ","
                << ka::lit("\"penalty\":") << ts_[ka::_1 = phx::bind(&penalty_constraint::cost, ka::_val)]
                << ka::lit("}");
        }

        ka::rule<OutputIterator, penalty_constraint()> pg;
        apoint_ts_generator<OutputIterator> ts_;
    };


/** Workbench notes:
 * What we try to achieve
 *
 * 1. speed/efficiency:
 *     hpc          : re-using boost::spirit::karma generators for time,period,time-series etc. since they do carry the
 *                    volume/performance critical parts
 *     zero-copy    : do not copy strings,etc, to emit (consider friend
 *
 * 2. ease of use/extension
 *     few lines    : we would like to have like one line code for each attribute exposed (for a given context).
 *     composition  : compose aggregates
 *     easy reading : direct code,
 *     parameterize : send parameters to emitter to control form/amount of emitted code.
 *     testing      : by composition
 *     !intrusive   : should be possible to use existing classes with no mods (maybe a friend class access, is allowed in case we start with private members).
 *
 * 3. what we do not yet need
 *     general framework : unless it satisfies the above requirements, including reusing boost::spirit::karma for the hpc parts.
 *
 */
    //--
    template <class OutputIterator> void emit_null(OutputIterator& oi) {*oi++='n';*oi++='u';*oi++='l';*oi++='l';}
    /**@brief Base class for emit functionality that will be overloaded
     * using partial template specialization
     * @tparam OutputIterator: Iterator for stream type to emit to.
     * @tparam T: Class to emit
     */
    template<class OutputIterator, class T>
    struct emit {
        /** @brief Constructor, so it can be called as emit(oi,t);
         *
         * @param oi: Output iterator to write to.
         * @param t: instance of T to emit to stream.
         */
        emit<OutputIterator, T>(OutputIterator& oi, const T& t) {
            auto value = string("ERROR: ") + typeid(t).name();
            copy(begin(value), end(value), oi);
            //throw std::runtime_error(typeid(T).name());
        }
    };

    /**@brief Helper class for partial specialization to handle shared_ptr<T>
     *
     * @tparam OutputIterator
     * @tparam T
     */
    template<class OutputIterator, class T>
    void emit_shared_ptr(OutputIterator& oi, std::shared_ptr<T> const &t) {
        if(t==nullptr) emit_null(oi);
        else emit<OutputIterator,T>(oi,*t); // Here we assume that emit<OutputIterator,T> has been specialized
    }
    #define x_emit_shared_ptr(T) template<class OutputIterator> struct emit<OutputIterator, std::shared_ptr<T>> { emit(OutputIterator& oi, std::shared_ptr<T> const& t){emit_shared_ptr(oi,t);}}

    /** emit a vector of values
    *   Could be upgraded to emit a range/sequence instead
    */
    template<class OutputIterator, class T>
    void emit_vector(OutputIterator& oi, std::vector<T> const& t){
        *oi++=arr_begin;
        bool first=true;
        for(auto const&v : t) {
            if(!first) *oi++=comma; else first=false;
            emit<OutputIterator, T>(oi,v);
        }
        *oi++=arr_end;
    }
    #define x_emit_vec(T) template<class OutputIterator> struct emit<OutputIterator, std::vector<T>> {emit(OutputIterator& oi, std::vector<T> const& t) {emit_vector(oi,t);}}

    /**@brief emit a vector of values, where what do generate at each
     * entry is provided through a provided function.
     *
     * @tparam OutputIterator: Type of iterator to write to
     * @tparam T: Type vector contains
     * @tparam Fx: Callback function type. Should be equivalent to void(*fx)(OutputIterator&, T const&)
     *
     * @param oi: Iterator to generate output to.
     * @param t: Vector of instances to generate output from
     * @param fx: Callback function to apply to each entry.
     *
     * If t is a vector with elements t1,t2,...,tN
     * emit_vector_fx writes to oi a list like
     *   [fx(oi,t1),fx(oi,t2),...,fx(oi,tN)]
     */
    template<class OutputIterator, typename T, class Fx>
    void emit_vector_fx(OutputIterator& oi, T const& t, Fx&& fx) {
        *oi++ = arr_begin;
        for(auto it = t.begin(); it != t.end(); it++) {
            if (it != t.begin()) *oi++ = comma;
            fx(oi, *it);
        }
        *oi++ = arr_end;
    }
    /** emit a map<Key,T> value
     *
     * This resolves typical time-map (time-dependent attributes required in the stm9
     *
     * as [ [key,value],...] sequence
     * assuming that emit(oi,utctime) and emit(oi,T) exists.
     */
    template<class OutputIterator, class Key, class T>
    void emit_map(OutputIterator&oi,std::map<Key,T> const &value) {
        *oi++=obj_begin;
        bool first=true;
        for(auto const&kv:value) {
            if(!first) *oi++=comma; else first=false;
            *oi++=arr_begin;
            emit(oi,kv.first);
            *oi++=colon;
            emit(oi,kv.second);
            *oi++=arr_end;
        }
        *oi++=obj_end;
    }
    #define x_emit_map(K,T) template<class OutputIterator> struct emit<OutputIterator, std::map<K,T>> {emit(OutputIterator& oi, std::map<K,T> const& t){emit_map(oi, t);}};

    /** @brief Visitor for emitting a boost::variant.
     * Usage:
     * typedef boost::variant<type1, type2,...,typeN> mytypes;
     *
     * // Partial template specialization of emit for mytypes:
     * template<class OutputIterator>
     * struct emit<OutputIterator, mytypes> {
     *     emit(OutputIterator& oi, mytypes const& m) {
     *          boost::apply_visitor(emit_visitor(oi),m);
     *     }
     * }
     *
     * @tparam OutputIterator
     */
    template<class OutputIterator>
    class emit_visitor: public boost::static_visitor<> {
        OutputIterator *oi{nullptr};
    public:
        explicit emit_visitor(OutputIterator* a_oi): oi(a_oi) {}

        template<class T>
        void operator()(T const& t) const{
            emit<OutputIterator, T>(*oi, t);
        }
    };
    //-- HERE WE BEGIN PARTIAL SPECIALIZATION FOR VARIOUS BASIS TYPES:
    // First, a helper directive
    #define emit_type(T,T_) template<class OutputIterator> struct emit<OutputIterator,T> {emit(OutputIterator& oi, T const & t) {static T_<OutputIterator> t_; generate(oi,t_,t);}}

    // We'll not use scientific notation when emitting doubles (since E-notation can truncate values)
    template <class Num>
    struct decimal_policy : ka::real_policies<Num> {
        static int floatfield(Num /*n*/) { return ka::real_policies<Num>::fmtflags::fixed; }

        static unsigned precision(Num) {
            return std::numeric_limits<Num>::digits10;
        }
    };

    template<class OutputIterator>
    struct emit<OutputIterator, double> {
        emit(OutputIterator& oi, double d) {
            generate(oi, fixed_double_, d);
        }

    private:
        ka::real_generator<double, decimal_policy<double>> fixed_double_;
    };

    template<class OutputIterator>
    struct emit<OutputIterator, bool> {
        emit(OutputIterator& oi, bool b) {
            generate(oi, bool_, b);
        }
    };

    template<class OutputIterator>
    struct emit<OutputIterator, int64_t> {
        emit(OutputIterator& oi, int64_t i) {
            generate(oi, int_, i);
        }
    };
    x_emit_vec(int64_t);

    template<class OutputIterator>
    struct emit<OutputIterator, int> {
        emit(OutputIterator& oi, int i){
            generate(oi, int_, i);
        }
    };
    x_emit_vec(int);

    template<class OutputIterator>
    inline void output_json_char(OutputIterator& oi, const char& c) {
        // Append a single character of a string to an output iterator, handling reserved characters in json.
        switch (c) {
        case '\n': *oi++ = esc; *oi++ = 'n'; return;
        case '\r': *oi++ = esc; *oi++ = 'r'; return;
        case '\t': *oi++ = esc; *oi++ = 't'; return;
        case '\f': *oi++ = esc; *oi++ = 'f'; return;
        case '\b': *oi++ = esc; *oi++ = 'b'; return;
        case quote: case esc: *oi++ = esc;
        }
        *oi++ = c;
    }

    template<class OutputIterator>
    struct emit<OutputIterator, const char*> {
        emit(OutputIterator& oi, const char* t) {
            *oi++ = quote;
            while (t && *t)
                output_json_char(oi, *t++);
            *oi++ = quote;
        }
    };

    template<class OutputIterator>
    struct emit<OutputIterator, char> {
        emit(OutputIterator& oi, const char& t) {
            *oi++ = quote;
            output_json_char(oi, t);
            *oi++ = quote;
        }
    };

    emit_type(utctime,utctime_generator);
    emit_type(utcperiod, utcperiod_generator);
    emit_type(apoint_ts, apoint_ts_generator);
    emit_type(generic_dt, generic_dt_generator);
    x_emit_vec(apoint_ts);
    //-- xy-type curve-descriptions, karma-generators,  considered basic types in this context:
    emit_type(xy_point_curve, xy_point_curve_generator);
    x_emit_vec(xy_point_curve);
    emit_type(xy_point_curve_with_z, xy_point_curve_with_z_generator);
    x_emit_vec(xy_point_curve_with_z);
    emit_type(turbine_description, turbine_description_generator);
    emit_type(absolute_constraint, absolute_constraint_generator);
    emit_type(penalty_constraint, penalty_constraint_generator);

    using message = pair<utctime, string>;
    emit_type(message, t_str_generator);
    x_emit_vec(message);

    template<typename T> using tm = std::map<utctime,T>;
    x_emit_map(utctime, double);
    x_emit_shared_ptr(tm<double>);

    x_emit_map(utctime, xy_point_curve_);
    x_emit_shared_ptr(tm<xy_point_curve_>);

    x_emit_map(utctime, xyz_point_curve_);
    x_emit_shared_ptr(tm<xyz_point_curve_>);

    x_emit_map(utctime, xyz_point_curve_list_);

    x_emit_map(utctime, xyz_point_curve_list);
    x_emit_shared_ptr(tm<xyz_point_curve_list_>);

    x_emit_map(utctime, turbine_description_);
    x_emit_shared_ptr(tm<turbine_description_>);

    //template <class OutputIterator> void emit(OutputIterator&oi,std::string const& value) {*oi++=quote;copy(begin(value),end(value),oi);*oi++=quote;}
    //TODO: for the string handling, ensure we deal with proper quoting/encoding
    template<class OutputIterator>
    struct emit<OutputIterator, string_view> {
        static constexpr const char* reserved = "\"\\\n\r\t\f\b"; // Reserved characters in json (see output_json_char)
        emit(OutputIterator& oi, string_view value) {
            *oi++ = quote;
            auto ita = begin(value);
            const auto itend = end(value);
            while (ita != itend) {
                auto itb = std::find_if(ita, itend, [](const auto& c){return strchr(reserved,c)?true:false;});
                copy(ita, itb, oi);
                if (itb != itend) {
                    output_json_char(oi, *itb);
                    ++itb;
                }
                ita = itb;
            }
            *oi++=quote;
        }
    };

    template<class OutputIterator>
    struct emit<OutputIterator, string> {
        emit(OutputIterator& oi, string const & t) {
            emit<OutputIterator, string_view>(oi, t);
        }
    };
    x_emit_vec(string);


    /** json object emitter
     *
     *  needs context to fix the comma sep
     */
    template <class OutputIterator>
    struct emit_object {
        OutputIterator&oi;
        bool first{true};
        explicit emit_object(OutputIterator&oi):oi{oi}{*oi++=obj_begin;}
        inline ~emit_object() {
            *oi++=obj_end;
        }

        inline void sep() {if(first) first=false;else *oi++=comma;}

        /** emit name-value pair
         *
         * note that we have to defer the trivial def of it until _after_
         * the vector-def. is seen, so that compiler succeed resolving
         * emit( vector<T>..)
         *
         * TO CONSIDER: Do better decomposition of the template library tools
         *
         */
        template <class Value>
        emit_object& def(std::string_view name,Value const& value) {
            sep();
            emit(oi,name);
            *oi++ = colon;
            emit<OutputIterator,Value>(oi,value); // value here could be a vector<T>, so with the current approach it must have been seen at the point of template def (no ADL/scope that can help us.).
            return *this;
        }


        template <class Fx>
        emit_object& def_fx(std::string_view name,Fx&& fx) {
            sep();
            emit(oi,name);
            *oi++ = colon;
            fx(oi);
            return *this;
        }

        emit_object(const emit_object&)=delete;
        emit_object(emit_object&&)=delete;
        emit_object & operator=(emit_object const&) =delete;
        emit_object & operator=(emit_object &&) =delete;
    };

    /** x_emit_shared_ptr
     *
     *  utility macro to create null or object emitter,
     *  we use a lot of shared-ptr, so we would like null or object almost everywhere
     */
    x_emit_shared_ptr(xy_point_curve);
    x_emit_shared_ptr(xy_point_curve_with_z); // alias xyz_point_curve
    x_emit_shared_ptr(xyz_point_curve_list);
    x_emit_shared_ptr(turbine_description);



    /** fwd declare the templates we need for request_iterator_t
     *
     * Current approach is to use a std::back_insert_iterator<string> as output_iterator type,
     *
     * Using extern template ensures that the templates are only
     * expanded once, in their in respective compilation units.
     *
     * Basically, we tell the c++ compiler that somewhere, there
     * is an instantiation of the template with response_iterator.
     * And then we take care in the grammar.cpp files to ensure that
     * we at least instantiate the templates for the reqest_iterator type.
     */
    using em_output_iterator=std::back_insert_iterator<std::string>;
    extern template struct emit_object<em_output_iterator>;
}
