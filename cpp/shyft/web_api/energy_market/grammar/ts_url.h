/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>

namespace shyft::web_api::grammar {
    using shyft::energy_market::stm::srv::server;

    /** @brief grammar for reading a ts_url of type dstm://model_id/hps_id/component_id/attribute_id
     * and bind to the requested attribute
     */
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct dstm_ts_url_grammar : public qi::grammar<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> {
        dstm_ts_url_grammar(server *const srv);

        qi::rule<Iterator, std::string(), Skipper> mid_;
        qi::rule<Iterator, std::string(), Skipper> attr_id_;
        qi::rule<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> ts_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    private:
        server *const srv;
    };

    extern template struct dstm_ts_url_grammar<request_iterator_em, request_skipper_em>;
}