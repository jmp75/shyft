#include <shyft/web_api/energy_market/grammar/ts_url.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>

#include <shyft/mp.h>
#include <boost/hana.hpp>

namespace shyft::web_api::grammar {

    using shyft::time_series::dd::apoint_ts;

    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::catchment;
    //using shyft::energy_market::stm::srv::srv_shared_lock;
    using shyft::energy_market::stm::stm_system;

    /** @brief get attribute value from t and provided attr_id.
     *
     * @tparam T : Hana struct to find attribute in
     * @param t : Attribute value to set
     * @param attr_id : attribute id to get
     * @return : If T::attr_id is valid, then it will return t.attr_id.
     */
    template<class T>
    attribute_value_type get_attr(T const& t, const std::string& attr_id) {
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        attribute_value_type result;
        const auto aid = attr_id.c_str();
        hana::for_each(attr_paths,
            [&result, &aid, &t](auto m) {
                // Compare attr_id with boost::hana::attr_id.
                if (!strcmp(aid, mp::leaf_accessor_id_str(m))) {
                    result = mp::leaf_access(t,m);
                }
            }
        );
        return result;
    }

    apoint_ts get_time_series(server *const srv, const string& mid, int hps_id, char comp_type, int comp_id, const string& attr_id) {
        if (!srv) {
            throw std::runtime_error("server must be set to successfully parse a ts_url");
        }
        // Get context and then model, 
        // notice: that we already do have a lock on the model, 
        //         since the code-path for this (currently) goes through web-api, or subscription.
        //         thus no model-lock required,
        auto ctx= srv->do_get_context(mid);// this will require a lock on srv, hard to avoid.
        // srv_shared_lock sl(ctx->mtx); // this one is not needed, we *should* already have, we will enforce that later.
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading

        // Get hps:
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw std::runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;
        // Get component:
        attribute_value_type attr;
        if (comp_type == 'R') {
            if (auto comp = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(comp_id))) {
                attr = get_attr(*comp, attr_id);
            }
        } else if (comp_type == 'U') {
            if (auto comp = std::dynamic_pointer_cast<unit>(hps->find_unit_by_id(comp_id))) {
                attr  = get_attr(*comp, attr_id);
            }
        } else if (comp_type == 'W') {
            if (auto comp = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_id(comp_id))) {
                attr = get_attr(*comp, attr_id);
            }
        } else if (comp_type == 'P') {
            if (auto comp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(comp_id))) {
                attr = get_attr(*comp, attr_id);
            }
        } else if (comp_type == 'C') {
            if (auto comp = std::dynamic_pointer_cast<catchment>(hps->find_catchment_by_id(comp_id))) {
                attr = get_attr(*comp, attr_id);
            }
        } else {
            throw std::runtime_error(string("Invalid component type ") + comp_type + ". Valid options are R|U|W|P|C.");
        }
        // Get attribute and return
        try {
            return boost::get<apoint_ts>(attr);
        } catch (boost::bad_get const& ) {
            throw std::runtime_error("Error when parsing dstm ts_url. Attribute " + std::string(attr_id) + " of component type " + comp_type + " is not a time series.");
        }
    }
    
    template<typename Iterator, typename Skipper>
    dstm_ts_url_grammar<Iterator, Skipper>::dstm_ts_url_grammar(server *const srv):
    dstm_ts_url_grammar::base_type(ts_, "dstm_ts_url"), srv{srv}
    {
        mid_ = lexeme[+(char_- "/")];
        attr_id_ = lexeme[+(char_)];
        ts_ = (lit("dstm://M") > mid_ 
                > "/HPS" > int_ 
                > "/" > char_("RUWPCGM") > int_
                > "." >> attr_id_)[_val = phx::bind(get_time_series, srv, _1, _2, _3, _4, _5)];

        on_error<fail>(ts_, error_handler(_4, _3, _2));
    }

    template struct dstm_ts_url_grammar<request_iterator_t, request_skipper_t>;
}
