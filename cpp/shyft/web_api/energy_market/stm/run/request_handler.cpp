#include <shyft/web_api/energy_market/stm/run/request_handler.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>

namespace shyft::web_api::energy_market::stm::run {

    using shyft::energy_market::stm::srv::stm_run_;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;
    using shyft::web_api::generator::emit;
    namespace ka = boost::spirit::karma;

    bool request_handler::handle_request(const request& req, bg_work_result& resp) {
        if(!super::handle_request(req, resp)) {
            // Handling of additional request types goes here.
            if (req.keyword == "add_run") {
                return handle_add_run_request(req.request_data, resp);
            } else if (req.keyword == "remove_run") {
                return handle_remove_run_request(req.request_data, resp);
            } else if (req.keyword == "get_run") {
                return handle_get_run_request(req.request_data, resp);
            } else if (req.keyword == "add_model_ref") {
                return handle_add_model_ref_request(req.request_data, resp);
            } else if (req.keyword == "remove_model_ref") {
                return handle_remove_model_ref_request(req.request_data, resp);
            } else if (req.keyword == "get_model_ref") {
                return handle_get_model_ref_request(req.request_data, resp);
            }
            return false;
        }
        return true;
    }

    bool request_handler::handle_add_run_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto run = std::make_shared<stm_run>(data.required<stm_run>("run"));
        // Store run in session:
        auto session = srv->db.read_model(mid);
        session->add_run(run);
        model_info mi;
        if (!srv->db.try_get_info_item(mid, mi)) {
            mi = model_info(session->id, session->name, session->created, session->json);
        }

        // As a result, we generate whether we can find the run in the provided session.
        srv->db.store_model(session, mi);
        session = srv->db.read_model(mid);
        bool result = std::any_of(session->runs.begin(), session->runs.end(),
            [&run](auto const& r) {
                if (!run && r) return true;
                if (!run || !r) return false;
                return *run == *r;
            }
        );
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_remove_run_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto rid = data.optional<int>("rid");
        auto rname = data.optional<string>("rname");

        bool result = false;
        auto session = srv->db.read_model(mid);
        // Attempt to remove run:
        if (rid) {
            result = session->remove_run(*rid);
        } else if (rname) {
            result = session->remove_run(*rname);
        }
        // Update server is run was removed:
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
            // TODO: Notify change to subscription manager:
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_get_run_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto rid = data.optional<int>("rid");
        auto rname = data.optional<string>("rname");

        stm_run_ result = nullptr;
        // Get result:
        auto session = srv->db.read_model(mid);
        if (rid) {
            result = session->get_run(*rid);
        } else if (rname) {
            result = session->get_run(*rname);
        }
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            emit(sink, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_add_model_ref_request(const json& data, bg_work_result& resp) {
        // Retrieve request data:
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto rid = data.required<int>("rid");
        auto mr = std::make_shared<model_ref>(data.required<model_ref>("model_ref"));

        bool result = false;
        auto session = srv->db.read_model(mid);
        auto run = session->get_run(rid);
        if (run) {
            auto presize = run->model_refs.size();
            run->add_model_ref(mr);
            if (run->model_refs.size() > presize) {
                result = true;
            }
        }
        // Update session on server if model reference was added successfully.
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
        }
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_remove_model_ref_request(const json& data, bg_work_result& resp) {
        // retrieve request data
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto rid = data.required<int>("rid");
        auto mkey = data.required<string>("model_key");

        // Do server action:
        bool result = false;
        auto session = srv->db.read_model(mid);
        auto run = session->get_run(rid);
        if (run) {
            result = run->remove_model_ref(mkey);
        }

        // Update session on server if model reference was successfully removed
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_get_model_ref_request(const json& data, bg_work_result& resp) {
        // Retrieve request data:
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto rid = data.required<int>("rid");
        auto mkey = data.required<string>("model_key");

        // Do server action:
        model_ref_ mr = nullptr;
        auto session = srv->db.read_model(mid);
        auto run = session->get_run(rid);
        if (run) {
            mr = run->get_model_ref(mkey);
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            emit(sink, mr);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }
}
