/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <map>
#include <iosfwd>

#include <shyft/energy_market/stm/srv/run/server.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>
#include <shyft/web_api/energy_market/srv/request_handler.h>

namespace shyft::web_api::energy_market::stm::run {
    using shyft::web_api::energy_market::request;
    using shyft::web_api::energy_market::json;
    using shyft::web_api::bg_work_result;

    using shyft::energy_market::stm::srv::run::server;
    /** @brief extension of functionality laid out in shyft::web_api::energy_market::srv::request_handler<Server>.
     *
     */
    struct request_handler : shyft::web_api::energy_market::srv::request_handler<server> {
        using super = shyft::web_api::energy_market::srv::request_handler<server>;
    protected:
        virtual bool handle_request(const request& req, bg_work_result& resp);
    private:
        // Specific handlers for each type of request
        bool handle_add_run_request(const json& data, bg_work_result& resp);
        bool handle_remove_run_request(const json& data, bg_work_result& resp);
        bool handle_get_run_request(const  json& data, bg_work_result& resp);
        bool handle_add_model_ref_request(const json& data, bg_work_result& resp);
        bool handle_remove_model_ref_request(const json& data, bg_work_result& resp);
        bool handle_get_model_ref_request(const json& data, bg_work_result& resp);
    };
}