#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
    
    template<typename Iterator,typename Skipper>
    web_request_grammar<Iterator,Skipper>::web_request_grammar() : web_request_grammar::base_type(start,"web_request") {
        start = ( find_ts_ | read_ts_ | average_ts_ | percentile_ts_| store_ts_ | info_ |unsubscribe_); // one of these constructs are accepted
        start.name("web_request");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct web_request_grammar<request_iterator_t,request_skipper_t>;

}
