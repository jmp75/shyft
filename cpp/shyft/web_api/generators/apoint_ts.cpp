/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

/** @brief adapter for apoint_ts, suited for boost.spirit.karma
 *
 * members of the tuple are:
 *  bool: true if the ts is non-empty
 *  bool: true if stair-case start of step (otherwise linear between points)
 *  apoint_ts_container: a boost.spirit.traits enabled container that provide the points
 */
BOOST_FUSION_ADAPT_ADT(
    shyft::time_series::dd::apoint_ts,
    (bool,bool, obj.size()>0,val)
    (bool,bool, obj.size() && obj.point_interpretation()==shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,val)
    (shyft::time_series::dd::apoint_ts_c,shyft::time_series::dd::apoint_ts_c, shyft::time_series::dd::apoint_ts_c(obj),val)
)

namespace shyft::web_api::generator {
    template<class OutputIterator>
    apoint_ts_generator<OutputIterator>::apoint_ts_generator(): apoint_ts_generator::base_type(tsg) {
        using ka::true_;
        using ka::false_;
        using ka::bool_;
        using ka::omit;
        using ka::eps;
        using ka::lit;
        pts_ =  '['<< -( pt_ % ',') <<']';
        tsg =
            (&true_ << "{\"pfx\":" << bool_ << ",\"data\":" << pts_ <<'}')
                |
            ( omit[bool_]<< "{\"pfx\":"<< bool_<< lit(",\"data\":")<<pts_<<'}')//<<omit[pts_])

        ;
        tsg.name("apoint_ts");
        //debug(tsg);
    }

    template struct apoint_ts_generator<generator_output_iterator>;
}
