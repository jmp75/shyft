/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

/** @brief point adapter for boost spirit  context
 *
 * members of the tuple are:
 * bool: true if value is finite
 * double: time in si-units, ref. utctime.
 * double: value
 */
BOOST_FUSION_ADAPT_ADT(
    shyft::time_series::point,
    (bool,bool,std::isfinite(obj.v),val)
    (double,double,shyft::core::to_seconds(obj.t),val)
    (double,double,obj.v,val)
)

namespace shyft::web_api::generator {
    template<class OutputIterator>
    point_generator<OutputIterator>::point_generator() : point_generator::base_type(pg) {
        using ka::true_;
        using ka::bool_;
        using ka::omit;
        using ka::real_generator;

        pg =
             &true_     << ( '[' <<  time_<< ',' << double_ << ']')
                |          // notice that we emit null here instead of NaN, we can adjust to fit!
             omit[bool_]<< ( '[' <<  time_<< ',' << "null" << ']')<<omit[double_]

        ;
        pg.name("point");
    }

    template struct point_generator<generator_output_iterator>;
}

