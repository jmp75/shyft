/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>


BOOST_FUSION_ADAPT_ADT(shyft::core::utctime,
    (bool,bool,obj!=shyft::core::no_utctime,obj=shyft::core::no_utctime)
    (double,double,shyft::core::to_seconds(obj),obj=shyft::core::from_seconds(val))
)

namespace shyft::web_api::generator {

    template<class OutputIterator>
    utctime_generator<OutputIterator>::utctime_generator()
        : utctime_generator::base_type(pg)
    {
        using ka::true_;
        using ka::bool_;
        using ka::omit;
        using ka::real_generator;

        pg =
              &true_ << time_
                       |
              omit[bool_] <<"null"<<omit[time_]

        ;
        pg.name("utctime");
    }

    template struct utctime_generator<generator_output_iterator>;
}