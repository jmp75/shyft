/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

BOOST_FUSION_ADAPT_ADT(shyft::core::utcperiod,
    (bool,bool,obj.valid(),obj=shyft::core::utcperiod{})
    (shyft::core::utctime,shyft::core::utctime,obj.start,val)
    (shyft::core::utctime,shyft::core::utctime,obj.end,val)
)

namespace shyft::web_api::generator {

    template<class OutputIterator>
    utcperiod_generator<OutputIterator>::utcperiod_generator()
        : utcperiod_generator::base_type(pg)
    {
        using ka::true_;
        using ka::bool_;
        using ka::omit;
        using ka::real_generator;

        pg =
              &true_ << '['<< time_<<','<<time_<<']'
                       |
              omit[bool_] <<"null"<<omit[time_]<<omit[time_]

        ;
        pg.name("utcperiod");
    }

    template struct utcperiod_generator<generator_output_iterator>;
}
