#pragma once
#include <boost/beast/core/flat_buffer.hpp>
#include <shyft/core/subscription.h>

namespace shyft::web_api {
    using expression_subscription_=std::shared_ptr<shyft::core::subscription::observer_base>;
    using std::string;
    using std::forward;
    
    /** @brief the background worker result structure
     *
     * Usually just a ready formatted buffer to emit,
     * but also other results, that the front-end io-worker could need
     * in order to maintain something that should span the socket state,
     * like subscription mechnanism.
     * 
     * Considered: instead of letting the bg produce the response, delegate that to be done
     *             in the front-end fiber. Yes, could be efficient, producing 
     *             output response as a multi-buffer construct-write chain,
     *             but also more involved, since it require a shared_ptr to the
     *             structure (with safe consistent const access during write),
     *             -- so we do simple approach: do this in the bgw, and
     *                   we get a consistent result at exec time, detached from
     *                   data-structures.
     * 
     */
    struct bg_work_result {
        boost::beast::flat_buffer response; ///< the response to emit to the client
        expression_subscription_ subscription;///< null or the subscription to add
        string unsubscribe_id;///< empty or the sub-id to unsubscribe.
        inline void copy_response(string const&srep) {
            auto n=boost::asio::buffer_copy(response.prepare(srep.size()),boost::asio::buffer(srep));
            response.commit(n);
        }
        
        //-- useful constructs
        bg_work_result()=default;
        /** just a plain response */
        explicit bg_work_result(string const&srep){
            copy_response(srep);
        }
        
        /** plain response, plus unsubscribe_id */
        bg_work_result(string const& srep, string const& unsub_id):unsubscribe_id{unsub_id} {
            copy_response(srep);
        }
        
        
        /** plain response, plus a subscription to maintain */
        bg_work_result(string const& srep, expression_subscription_ s):subscription{std::move(s)} {
            copy_response(srep);
        }
    };
}
