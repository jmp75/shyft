/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif
#include <shyft/hydrology/api/api_pch.h>

/**
 serializiation implemented using boost,
  see reference: http://www.boost.org/doc/libs/1_62_0/libs/serialization/doc/
 */
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>

//
// 1. first include std stuff and the headers for
//    files with serializeation support
//
#include  <shyft/core/core_serialization.h>
#include  <shyft/core/core_archive.h>
#include <shyft/hydrology/api/api_state.h>
#include <shyft/hydrology/api/api.h>
// then include stuff you need like vector,shared, base_obj,nvp etc.

//
// 2. Then implement each class serialization support
//

using namespace boost::serialization;
using namespace shyft::core;

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

template<class Archive>
void shyft::api::cell_state_id::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("cid", cid)
    & core_nvp("x", x)
    & core_nvp("y", y)
    & core_nvp("a", area)
    ;
}
template <class CS>
template <class Archive>
void shyft::api::cell_state_with_id<CS>::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("id", id)
    & core_nvp("state", state)
    ;
}
template <class Archive>
void shyft::api::GeoPointSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("mid_point_",mid_point_)
    & core_nvp("ts", ts)
    & core_nvp("uid",uid)
    ;
}

template <class Archive>
void shyft::api::TemperatureSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("geopoint_ts",base_object<shyft::api::GeoPointSource>(*this))
    ;
}

template <class Archive>
void shyft::api::PrecipitationSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("geopoint_ts",base_object<shyft::api::GeoPointSource>(*this))
    ;
}

template <class Archive>
void shyft::api::WindSpeedSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("geopoint_ts",base_object<shyft::api::GeoPointSource>(*this))
    ;
}

template <class Archive>
void shyft::api::RelHumSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("geopoint_ts",base_object<shyft::api::GeoPointSource>(*this))
    ;
}

template <class Archive>
void shyft::api::RadiationSource::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("geopoint_ts",base_object<shyft::api::GeoPointSource>(*this))
    ;
}

template <class Archive>
void shyft::api::a_region_environment::serialize(Archive&ar, const unsigned int file_version) {
    ar
    & core_nvp("temperature",temperature)
    & core_nvp("precipitation",precipitation)
    & core_nvp("radiation",radiation)
    & core_nvp("wind_speed",wind_speed)
    & core_nvp("rel_hum",rel_hum)
    ;
}

//
// 3. force impl. of pointers etc.
//
x_serialize_implement(shyft::api::cell_state_id);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::hbv_stack::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::pt_st_k::state>);
x_serialize_implement(shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state>);
x_serialize_implement(shyft::api::GeoPointSource);
x_serialize_implement(shyft::api::TemperatureSource);
x_serialize_implement(shyft::api::PrecipitationSource);
x_serialize_implement(shyft::api::WindSpeedSource);
x_serialize_implement(shyft::api::RelHumSource);
x_serialize_implement(shyft::api::RadiationSource);
x_serialize_implement(shyft::api::a_region_environment);
//
// 4. Then include the archive supported
//

x_arch(shyft::api::cell_state_id);
x_arch(shyft::api::cell_state_with_id<shyft::core::hbv_stack::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::pt_gs_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::r_pm_gs_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::r_pt_gs_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::pt_ss_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::pt_hs_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::pt_st_k::state>);
x_arch(shyft::api::cell_state_with_id<shyft::core::pt_hps_k::state>);
x_arch(shyft::api::GeoPointSource);
x_arch(shyft::api::TemperatureSource);
x_arch(shyft::api::PrecipitationSource);
x_arch(shyft::api::WindSpeedSource);
x_arch(shyft::api::RelHumSource);
x_arch(shyft::api::RadiationSource);
x_arch(shyft::api::a_region_environment);


namespace shyft {
    namespace api {
        //-serialization of state to byte-array in python support
        template <class CS>
        std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<CS>>& states) {
            std::ostringstream xmls;
            core_oarchive oa(xmls,core_arch_flags);
            oa << core_nvp("states",states);
            xmls.flush();
            auto s = xmls.str();
            return std::vector<char>(s.begin(), s.end());
        }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::hbv_stack::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_gs_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pm_gs_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pt_gs_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_ss_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hs_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_k::state>>>& states);// { return serialize_to_bytes_impl(states); }
        template std::vector<char> serialize_to_bytes(const std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hps_k::state>>>& states);// { return serialize_to_bytes_impl(states); }

        template <class CS>
        void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<CS>>&states) {
            std::string str_bin(bytes.begin(), bytes.end());
            std::istringstream xmli(str_bin);
            core_iarchive ia(xmli,core_arch_flags);
            ia >> core_nvp("states",states);
        }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::hbv_stack::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_gs_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pm_gs_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::r_pt_gs_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hs_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_st_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_ss_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
        template void deserialize_from_bytes(const std::vector<char>& bytes, std::shared_ptr<std::vector<cell_state_with_id<shyft::core::pt_hps_k::state>>>&states);// { deserialize_from_bytes_impl(bytes, states); }
    
        std::vector<char> a_region_environment::serialize_to_bytes() const {
            std::ostringstream xmls;
            core_oarchive oa(xmls,core_arch_flags);
            oa << core_nvp("a_region_environment",*this);
            xmls.flush();
            auto s = xmls.str();
            return std::vector<char>(s.begin(), s.end());
        }
        
        a_region_environment a_region_environment::deserialize_from_bytes(const std::vector<char>&ss) {
            std::string str_bin(ss.begin(), ss.end());
            std::istringstream xmli(str_bin);
            core_iarchive ia(xmli,core_arch_flags);
            a_region_environment r;
            ia >> core_nvp("a_region_environment",r);
            return r;
        }
        
    }
}
