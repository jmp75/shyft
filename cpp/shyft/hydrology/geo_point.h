/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <cmath>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/srs/epsg.hpp>
#include <boost/geometry/srs/projection.hpp>

#include <shyft/core/core_serialization.h>

namespace shyft::core {

    
    
    /** @brief GeoPoint commonly used in the shyft::core for
     *  representing a 3D point in the terrain model.
     *  Primary usage is in the interpolation routines
     *
     *  Absolutely a primitive point model aiming for efficiency and simplicity.
     *
     *  Units of x,y,z are metric, z positive upwards to sky, represents elevation
     *  x is east-west axis
     *  y is south-north axis
     */
    struct geo_point {
        double x, y, z;
        geo_point() : x(0), y(0), z(0) {}
        geo_point(double x, double y=0.0, double z=0.0) : x(x), y(y), z(z) {}

        bool operator==(const geo_point &o) const {
            const double accuracy=1e-3;//mm
            return distance2(*this,o)<accuracy;
        }
        bool operator!=(const geo_point&o) const {return !operator==(o);}
        
        /** @brief Squared eucledian distance
         *
         * @return the 3D distance^2 between supplied arguments a and b
         */
        static inline double distance2(const geo_point& a, const geo_point& b) {
            return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z);
        }

        /** @brief Distance measure on the form sum(abs(a.x - b.x)^p + abs(a.y - b.y)^p + (zscale * abs(a.z - b.z))^p)
         *
         * @return the 3D distance measure between supplied arguments a and b, by using an optional z scale factor
         */
        static inline double distance_measure(const geo_point& a, const geo_point& b, double p, double zscale) {
            const double d=(a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale;
            return fabs(p-2.0)<1e-8 ? d : std::pow(d, p/2.0);
        }

        /** @brief Z scaled, non-eucledian distance between points a and b.
         *
         * @return sqrt( (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale)
         */
        static inline double zscaled_distance(const geo_point& a, const geo_point& b, double zscale) {
            return std::sqrt( (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale);
        }

                    /** @brief Z scaled, non-eucledian distance between points a and b.
         *
         * @return  (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale
         */
        static inline double zscaled_distance2(const geo_point& a, const geo_point& b, double zscale) {
            return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) + (a.z - b.z)*(a.z - b.z)*zscale*zscale;
        }

        /** @brief Euclidian distance between points a and b, first projected onto x-y plane.
         *
         * return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y))
         */
        static inline double xy_distance(const geo_point& a, const geo_point& b) {
            return std::sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
        }
        /** @brief Euclidian distance^2 between points a and b, first projected onto x-y plane.
        *
        * return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y)
        */
        static inline double xy_distance2(const geo_point& a, const geo_point& b) {
            return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y);
        }
        static inline double z_distance2(const geo_point& a, const geo_point& b) {
            return (a.z - b.z)*(a.z - b.z);
        }

        /** @brief Difference between points a and b, i.e. a - b. Should perhaps return a
         * GeoDifference instead for mathematical correctness.
         *
         * @return GeoPoint(a.x - b.x, a.y - b.y, a.z - b.z)
         */
        static inline geo_point difference(const geo_point& a, const geo_point& b) {
            return geo_point(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        x_serialize_decl();
    };
    
    /** @brief plain struct to represent longitude, latitude pairs */
    struct geo_point_ll {
      double longitude{0.0};
      double latitude{0.0};
    };
    
    namespace detail {
        namespace bg= boost::geometry;
        using bg::degree;
        using bg::cs::cartesian;
        using bg::cs::geographic;
        using bg::distance;
        using bg::srs::projection;
        using bg::srs::proj4;
        using bg::srs::epsg;
        using point_ll=bg::model::point<double, 2,geographic<degree> >; ///< point (latitude,longitude),  epsg 4326 ,wgs 84
        using point_xy=bg::model::point<double, 2,cartesian>; /// < x and y of geo_point, in some metric epsg zone, utm.. etc.
        
     
        inline geo_point_ll impl_to_longitude_latitude(geo_point const&p,int geo_epsg_id) {
            point_xy p_xy(p.x,p.y);
            point_ll p_ll;
            projection<> prj{epsg{geo_epsg_id}};
            prj.inverse(p_xy,p_ll);
            return geo_point_ll{bg::get<0>(p_ll),bg::get<1>(p_ll)};
        }
        template<class pxy>
        inline pxy epsg_map(pxy a,int from_epsg,int to_epsg) {
            if(from_epsg==to_epsg) {
                return a;
            } else {
                projection<> a_prj{epsg{from_epsg}};
                point_ll a_ll;
                a_prj.inverse(a,a_ll);
                projection<> r_prj{epsg{to_epsg}};
                pxy r;
                r_prj.forward(a_ll,r);
                return r;
            }
        }
    }
    
    /** Given geo_point and it's epsg_id, provide the longitude, latitude coord of the points */
    inline geo_point_ll convert_to_longitude_latitude(geo_point const &p,int geo_epsg_id) {return detail::impl_to_longitude_latitude(p,geo_epsg_id);}

    /** Given a geo_point, from_epsg to_epsg, do the transform of the x-y part */
    inline geo_point epsg_map(geo_point a_,int from_epsg,int to_epsg) {
        if(from_epsg==to_epsg) {
            return a_;
        } else {
            auto a=detail::epsg_map(detail::point_xy(a_.x,a_.y),from_epsg,to_epsg);
            return geo_point(detail::bg::get<0>(a),detail::bg::get<1>(a),a_.z);
        }
    }

}


//-- serialization support shyft
x_serialize_export_key(shyft::core::geo_point);
