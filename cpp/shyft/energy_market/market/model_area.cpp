/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

namespace shyft::energy_market::market {


    bool model_area::operator==(const model_area &o)const {
        return id == o.id && name == o.name && equal_structure(o); 
    }


    bool model_area::equal_structure(const model_area& b)const {
        const model_area& a = *this;
        if ( id_base::operator!=(b))
            return false;
        if (!equal_map_content(a.power_modules,b.power_modules))
            return false;
        if ((a.detailed_hydro == nullptr) != (b.detailed_hydro == nullptr))
            return false;
        if (a.detailed_hydro && !a.detailed_hydro->equal_structure(*b.detailed_hydro))
            return false;
        return true;
    }

}
