
set(cpps 
    serialization_impl.cpp 
    market/model.cpp 
    market/model_area.cpp
    market/power_line.cpp 
    market/power_module.cpp 
    hydro_power/hydro_power_system.cpp
    hydro_power/hydro_component.cpp
    hydro_power/hydro_operations.cpp
    hydro_power/power_plant.cpp
    hydro_power/reservoir.cpp
    hydro_power/catchment.cpp
    hydro_power/waterway.cpp
    hydro_power/xy_point_curve.cpp)


if (MSVC)
    add_library(em_model_core STATIC ${cpps} )
    set_target_properties(em_model_core PROPERTIES
        DEBUG_POSTFIX "_debug"
        COMPILE_PDB_NAME "em_model_core"
        COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    )
else()
    add_library(em_model_core SHARED ${cpps} )
    set_target_properties(em_model_core PROPERTIES INSTALL_RPATH "$ORIGIN")
    install(TARGETS em_model_core DESTINATION ${CMAKE_SOURCE_DIR}/shyft/lib)
endif()
target_link_libraries(em_model_core shyft_core ${boost_link_libraries} dlib::dlib ${arma_libs} ${LIBS})

install(TARGETS em_model_core DESTINATION ${inst_root}lib)

if (MSVC AND CMAKE_BUILD_TYPE STREQUAL "Debug")
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/em_model_core.pdb DESTINATION ${inst_root}lib)
endif()
