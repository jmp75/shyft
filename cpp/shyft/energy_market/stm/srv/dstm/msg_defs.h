#pragma once
#include <shyft/core/dlib_utils.h>

namespace shyft::energy_market::stm::srv {

	/** @brief dems message-types
	 *
	 * The message types used for the wire-communication of dems
	 *
	 */
	struct message_type {
		using type = uint8_t;
		static constexpr type SERVER_EXCEPTION = 0;
		static constexpr type VERSION_INFO = 1;
		static constexpr type CREATE_MODEL = 2;
		static constexpr type ADD_MODEL = 3;
		static constexpr type REMOVE_MODEL = 4;
		static constexpr type RENAME_MODEL = 5;
		static constexpr type CLONE_MODEL = 6;
		static constexpr type GET_MODEL_IDS = 7;
		static constexpr type GET_MODEL_INFOS = 8;
		static constexpr type GET_MODEL = 9;
		static constexpr type OPTIMIZE = 10;
		static constexpr type GET_STATE = 11;
		static constexpr type GET_LOG = 12;
		static constexpr type FX = 13;
		static constexpr type EVALUATE_MODEL = 14;
		// SET_INPUT
		// GET_INPUT
		// START_OPTIMIZATION
		// GET_RESULTS
	};

	/** @brief adapt low-level and message-type handling from the core/dblib_utils.h */
	using msg=shyft::core::msg_util<message_type>; 
}
