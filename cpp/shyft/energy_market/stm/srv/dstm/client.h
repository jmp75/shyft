#pragma once

#include <string>
#include <cstdint>
#include <exception>

#include <dlib/iosockstream.h>

#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

#ifdef SHYFT_WITH_SHOP
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#endif

namespace shyft::energy_market::stm::srv {
	using std::vector;
    using std::map;
	using std::string;
	using std::string_view;
	using std::to_string;
	using std::runtime_error;
	using shyft::core::srv_connection;
	using shyft::core::scoped_connect;
	using shyft::core::do_io_with_repair_and_retry;
	using shyft::core::utctime;
	using shyft::core::no_utctime;
	using shyft::energy_market::srv::model_info;
#ifdef SHYFT_WITH_SHOP
    using shyft::energy_market::stm::shop::shop_command;
#endif
	/** @brief a client
	*
	*
	* This class take care of message exchange to the remote server,
	* using the supplied connection parameters.
	*
	* It implements the message protocol of the server, sending
	* message-prefix, arguments, waiting for response
	* deserialize the response and handle it back to the user.
	*
	* @see server
	*
	*/
	struct client {
		srv_connection c;
		//client()=delete;
		client(string host_port,int timeout_ms=1000);

		string version_info() ;
		bool create_model(string const& mid);
		bool add_model(string const& mid, stm_system_ mdl);
		bool remove_model(string const&mid) ;
		bool rename_model(string const& old_mid, string const& new_mid);
		bool clone_model(string const& old_mid, string const& new_mid);
		vector<string> get_model_ids() ;
		map<string, model_info> get_model_infos() ;
		stm_system_ get_model(string const& mid) ;

#ifdef SHYFT_WITH_SHOP
		bool optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd);
#endif
        /** @brief get state of a model:
         */
        model_state get_state(const string& mid);
        
        
        /** @brief get SHOP log for model.
         */
        string get_log(const string& mid);

         /** @brief exeute fx(mid,fx_arg) on the server side
         */
        bool fx(const string& mid, const string& fx_arg);

        /** @brief evaluate any unbound time series attributes in a model.
         */
        bool evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period=utcperiod{});
        
		/** @brief close, until needed again, the server connection
		*
		*/
		void close();
	};
}
