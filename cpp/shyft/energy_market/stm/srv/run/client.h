#pragma once
#include <memory>
#include <shyft/energy_market/stm/srv/run/msg_defs.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>
#include <shyft/energy_market/srv/client.h>
#include <shyft/core/core_archive.h>

namespace shyft::energy_market::stm::srv::run {
    using std::shared_ptr;
    using std::string;
    using std::to_string;

    using shyft::energy_market::stm::srv::stm_session;
    using shyft::energy_market::stm::srv::stm_run_;
    using shyft::energy_market::stm::srv::model_ref_;

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;
    using shyft::core::srv_connection;
    using shyft::core::scoped_connect;
    using shyft::core::do_io_with_repair_and_retry;

    struct client : shyft::energy_market::srv::client<stm_session> {
        using super = shyft::energy_market::srv::client<stm_session>;
        client() = delete;
        client(string host_port, int timeout_ms=1000) : super(host_port, timeout_ms) {}

        /** @brief Add a run to an existing session
         *
         * @param mid: Session ID.
         * @param run: stm_run to add to session
         */
        void add_run(int64_t mid, const stm_run_& run) {
            scoped_connect sc(c);
            do_io_with_repair_and_retry(c, [this, &mid, &run](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::ADD_RUN, io);
                    oa << mid << run;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::ADD_RUN) {
                    return;
                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
        }

        /** @brief remove run from an existing session
         *
         * @param mid: session ID.
         * @param rid: run id to remove
         */
        bool remove_run(int64_t mid, int64_t rid) {
            scoped_connect sc(c);
            bool result = false;
            do_io_with_repair_and_retry(c, [this, &result, mid, rid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::REMOVE_RUN_ID, io);
                    oa << mid << rid;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REMOVE_RUN_ID) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> result;

                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
            return result;
        }

        /** @brief remove run from an existing session
         *
         * @param mid: session ID.
         * @param rid: run name to remove
         */
        bool remove_run(int64_t mid, const string& rname ) {
            scoped_connect sc(c);
            bool result = false;
            do_io_with_repair_and_retry(c, [this, &result, mid, &rname](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::REMOVE_RUN_NAME, io);
                    oa << mid << rname;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REMOVE_RUN_NAME) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> result;

                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
            return result;
        }

        /** @brief get run based on ID.
         *
         * @param mid: session ID.
         * @param rid: run ID.
         * @returns run: requested run, or nullptr if not found
         */
        stm_run_ get_run(int64_t mid, int64_t rid) {
            scoped_connect sc(c);
            stm_run_ r;
            do_io_with_repair_and_retry(c, [this, &r, mid, rid](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::GET_RUN_ID, io);
                    oa << mid << rid;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::GET_RUN_ID) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
            return r;
        }

        /** @brief get run based on name.
         *
         * @param mid: session ID.
         * @param rname: run name.
         * @returns run: requested run, or nullptr if not found
         */
        stm_run_ get_run(int64_t mid, const string& rname) {
            scoped_connect sc(c);
            stm_run_ r;
            do_io_with_repair_and_retry(c, [this, &r, mid, rname](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::GET_RUN_NAME, io);
                    oa << mid << rname;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::GET_RUN_NAME) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
            return r;
        }

        /** @brief add a model_ref to a run
         *
         * @param mid: Session ID
         * @param rid: Run ID
         * @param mr: The model reference to add
         */
        void add_model_ref(int64_t mid, int64_t rid, const model_ref_& mr) {
            scoped_connect sc(c);
            do_io_with_repair_and_retry(c, [this, mid, rid, &mr](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::ADD_MODEL_REF, io);
                    oa << mid << rid << mr;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::ADD_MODEL_REF) {
                    return;
                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
        }

        /** @brief remove a model_ref from a run
         *
         * @param mid: Session ID
         * @param rid: run ID
         * @param mkey: Model reference key
         *
         * @returns success: true if model ref was found and removed, false otherwise.
         */
        bool remove_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_connect sc(c);
            bool r = false;
            do_io_with_repair_and_retry(c, [this, mid, rid, &r, &mkey](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::REMOVE_MODEL_REF, io);
                    oa << mid << rid << mkey;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REMOVE_MODEL_REF) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                }
            });
            return r;
        }

        /** @brief get model_reference
         *
         * @param mid: Session ID
         * @param rid: Run ID
         * @param mkey: Key to model reference in
         */
        model_ref_ get_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_connect sc(c);
            model_ref_ mr = nullptr;
            do_io_with_repair_and_retry(c, [this, mid, rid, &mkey, &mr](srv_connection&c) {
                dlib::iosockstream& io = *c.io;
                {
                    core_oarchive oa(io, core_arch_flags);
                    msg::write_type(message_type::GET_MODEL_REF, io);
                    oa << mid << rid << mkey;
                }
                auto response_type = msg::read_type(io);
                 if (response_type == message_type::SERVER_EXCEPTION) {
                     auto re = msg::read_exception(io);
                     throw re;
                 } else if (response_type == message_type::GET_MODEL_REF) {
                     core_iarchive ia(io, core_arch_flags);
                     ia >> mr;
                 } else {
                     throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
                 }
            });
            return mr;
        }
    };
}
