/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
 * See file COPYING for more details. **/

#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <shyft/core/utctime_utilities.h>
#include <shyft/energy_market/srv/run.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::stm::srv {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::core::utctime;
    using shyft::core::no_utctime;
    using shyft::energy_market::srv::run_state;

    template <class T>
    bool vector_compare(const vector<T>& lhs, const vector<T>& rhs) {
        if (lhs.size() != rhs.size()) return false;
        return std::is_permutation(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(),
            [](const auto& t1, const auto& t2)->bool { return t1 == t2; });
    }

    template<class T>
    bool vector_compare(const vector<shared_ptr<T>>& lhs, const vector<shared_ptr<T>>& rhs) {
        if (lhs.size() != rhs.size()) return false;
        return std::is_permutation(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(),
            [](const auto& t1, const auto& t2)-> bool {
                // If both are null ptr:
                if (!t1 && !t2) return true;
                // If one, but not the other:
                if (!t1 || !t2) return false;
                // If both are non-null pointers:
                return *t1 == *t2;
            }
        );
    }

    /** @brief model_ref. Keeps info about a specific model,
     * and on what server to find the model.
     */
    struct model_ref {
        string host; ///< Host where server with model is running.
        int port_num{-1}; ///< Port-number to interface with server.
        int api_port_num{-1}; ///< Api-port-number to access model via
        string model_key; ///< Under what model-key the model is stored on the server.
        vector<string> labels{};

        // For python expose:
        model_ref() = default;
        model_ref(const string& host, int port_num, int api_port_num, const string& mk):
            host{host}, port_num{port_num}, api_port_num{api_port_num}, model_key{mk} {}

        bool operator==(model_ref const& o) const noexcept {
            return host == o.host && port_num == o.port_num && model_key == o.model_key
                && api_port_num == o.api_port_num
                && vector_compare(labels, o.labels)
                ;
        }
        bool operator!=(model_ref const& o) const noexcept { return !operator==(o); }
        x_serialize_decl();
    };
    using model_ref_ = shared_ptr<model_ref>;

    /** @brief stm_run. A collection of runs, related e.g. by a task or process.
     *
     */
    struct stm_run {
        int64_t id{0};
        string name;
        string json;
        utctime created{no_utctime};///< or modified.
        vector<string> labels{}; ///<Optional, tags to make a run more easily searchable.
        vector<model_ref_> model_refs{}; ///<Collection of model_refs making up the stm_run.

        stm_run() = default;
        stm_run(const stm_run& arun) = default;
        stm_run& operator=(const stm_run& arun) = default;
        stm_run(int64_t id, string const& name, utctime created, string json=string{},
            vector<string> labels = vector<string>{},
            vector<model_ref_> model_refs=vector<model_ref_>{}):
            id{id}, name{name}, json{json}, created{created}, labels{labels}, model_refs{model_refs} {}

        bool operator==(stm_run const& o) const noexcept {
            bool eq = id == o.id && name == o.name && json == o.json && created == o.created;
            if (!eq) return false;
            if (!vector_compare(labels, o.labels)) return false;
            if (!vector_compare(model_refs, o.model_refs)) return false;
            return true;
        }

        bool operator!=(stm_run const& o) const noexcept {
            return !operator==(o);
        }

        void add_model_ref(const model_ref_& amr) {
            // Check for uniqueness:
            if (!amr || std::any_of(model_refs.begin(), model_refs.end(), [&amr](auto const& mr)->bool {
                 return *mr == *amr;
            })) {
                throw std::runtime_error("Run " + name + "[" + std::to_string(id) + "] already contains provided model_ref.");
            }
            model_refs.push_back(amr);
        }

        /** @brief remove a model reference from the run based on model-key
         *
         *  @param mkey: Model key of reference to remove
         *  @note: If more than one reference has the same model key it removes the first occurence
         */
        bool remove_model_ref(const string& mkey) {
            auto it = std::find_if(model_refs.begin(), model_refs.end(), [&mkey](auto const& r) -> bool {
                return r && r->model_key == mkey;
            });
            if (it != model_refs.end()) {
                model_refs.erase(it);
                return true;
            }
            return false;

        }

        /** @brief get a model reference based on model_key
         *
         * @param mkey : model key to reference you want.
         * @return model_reference. nullptr if not found
         */
        model_ref_ get_model_ref(const string& mkey) {
            auto it = std::find_if(model_refs.begin(), model_refs.end(), [&mkey](auto const& r) -> bool {
                return r && r->model_key == mkey;
            });
            return (it != model_refs.end()) ? *it : nullptr;
        }

        x_serialize_decl();
    };
    using stm_run_ = std::shared_ptr<stm_run>;


    /** @brief stm_session. A collection of stm_runs.
     *
     */
    struct stm_session {
        int64_t id{0};
        string name;
        string json;
        utctime created{no_utctime}; ///< or modified.
        vector<string> labels{}; ///< Optional, labels for sessions be more easily searchable, &c.
        vector<stm_run_> runs{}; ///< Colllection of runs associated with the model and task
        model_ref base_mdl; ///< Reference to base model used in session
        string task_name; ///< Name of task connected to session.

        stm_session() = default;
        stm_session(int64_t id, const string& name, utctime created, string json=string{},
            vector<string> labels=vector<string>{},
            vector<stm_run_> runs=vector<stm_run_>{},
            model_ref base_mdl = model_ref{},
            string task_name = string())
            : id{id}, name{name}, json{json},created{created}, labels{labels},
            runs{runs}, base_mdl{base_mdl}, task_name{task_name} {}

        bool operator==(stm_session const& o) const noexcept {
            bool eq = id == o.id && name == o.name && json == o.json && created == o.created;
            if (!eq) return false;
            if (!vector_compare(labels, o.labels)) return false;
            if (!vector_compare(runs, o.runs)) return false;
            return base_mdl == o.base_mdl && task_name == o.task_name;
        }

        bool operator!=(stm_session const& o) const noexcept {
            return !operator==(o);
        }

        void add_run(const stm_run_& arun) {
            // We want ID and name to be unique within a session:
            if(std::any_of(runs.begin(), runs.end(),
                [&arun](const auto& r)->bool { return arun->id == r->id || arun->name == r->name; }))
            {
                throw std::runtime_error("Provided name or ID of run was not unique within session " + std::to_string(id));
            }
            runs.push_back(arun);
        }

        /** @brief remove a run based on ID
         * returns true if the run with the given id was removed.
         * return false if no such run could be found.*/
        bool remove_run(int64_t aid) {
            // See if we can find run with given ID:
            auto it = std::find_if(runs.begin(), runs.end(), [aid](auto const& r)->bool {
               return r->id == aid;
            });
            if (it != runs.end()) {
                runs.erase(it);
                return true;
            }
            return false;
        }
        /** @brief remove a run based on name */
        bool remove_run(const string& aname) {
            auto it = std::find_if(runs.begin(), runs.end(), [&aname](auto const& r)-> bool {
                return r->name == aname;
            });
            if (it != runs.end()) {
                runs.erase(it);
                return true;
            }
            return false;
        }

        /** @brief get run from session based on id
         *
         * @param id: run id of run to get
         * @return shared pointer to requested run. nullptr if not found
         */
        stm_run_ get_run(int64_t aid) {
            auto it = std::find_if(runs.begin(), runs.end(), [aid](auto const& r) -> bool {
                return r->id == aid;
            });
            return (it != runs.end()) ? *it : nullptr;
        }

        /** @brief get run from session based on name
         *
         * @param rname: name of run to get
         * @return shared pointer to requested run. nullptr if not found
         */
        stm_run_ get_run(const string& rname) {
            auto it = std::find_if(runs.begin(), runs.end(), [&rname](auto const& r) -> bool {
                return r->name == rname;
            });
            return (it != runs.end())  ? *it : nullptr;
        }


        x_serialize_decl();
    };
}

x_serialize_export_key(shyft::energy_market::stm::srv::model_ref);
x_serialize_export_key(shyft::energy_market::stm::srv::stm_run);
x_serialize_export_key(shyft::energy_market::stm::srv::stm_session);
