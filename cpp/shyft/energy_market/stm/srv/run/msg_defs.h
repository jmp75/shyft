#pragma once
#include <shyft/energy_market/srv/msg_defs.h>

namespace shyft::energy_market::stm::srv::run {
    /** @brief extension of message types from shyft::energy_market::srv
     */
    struct message_type : shyft::energy_market::srv::message_type {
        static constexpr type ADD_RUN = 8;
        static constexpr type REMOVE_RUN_ID = 9;
        static constexpr type REMOVE_RUN_NAME = 10;
        static constexpr type GET_RUN_ID = 11;
        static constexpr type GET_RUN_NAME = 12;
        static constexpr type ADD_MODEL_REF = 13;
        static constexpr type REMOVE_MODEL_REF = 14;
        static constexpr type GET_MODEL_REF = 15;
    };

    using msg=shyft::core::msg_util<message_type>; ///< so that we get low level functions adapted to our enum type
}