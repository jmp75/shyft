#pragma once
#include <shyft/energy_market/srv/server.h>
#include <shyft/energy_market/srv/db.h>
#include <shyft/energy_market/stm/srv/run/msg_defs.h>
#include <shyft/core/core_archive.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>

namespace shyft::energy_market::stm::srv::run {
    using std::string;

    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::srv::stm_session;
    using shyft::energy_market::stm::srv::stm_run;
    using shyft::energy_market::stm::srv::stm_run_;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;

    using db_t = shyft::energy_market::srv::db<stm_session>;

    struct server : shyft::energy_market::srv::server<db_t> {
        using super = shyft::energy_market::srv::server<db_t>;

        server(string root_dir) : super(root_dir) {}
        server(server&&)=delete;
        server(const server&)=delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server()=default;

    protected:
        virtual bool message_dispatch(
            std::istream & in,
            std::ostream & out,
            message_type::type msg_type
            )
        {
            using shyft::core::core_iarchive;
            using shyft::core::core_oarchive;
            using shyft::core::core_arch_flags;

            if (!super::message_dispatch(in, out, msg_type)) {
                core_iarchive ia(in, core_arch_flags);
                core_oarchive oa(out, core_arch_flags);
                switch(msg_type) {
                    case message_type::ADD_RUN: {
                        stm_run_ run;
                        int64_t mid;
                        ia >> mid >> run;
                        auto session = db.read_model(mid);
                        session->add_run(run);
                        model_info mi;
                        if (!db.try_get_info_item(mid, mi)) {
                            mi = model_info(session->id, session->name, session->created, session->json);
                        }
                        db.store_model(session, mi);
                        msg::write_type(message_type::ADD_RUN, out);
                    } break;
                    case message_type::REMOVE_RUN_ID: {
                        int64_t mid;
                        int64_t rid;
                        ia >> mid >> rid;
                        auto session = db.read_model(mid);
                        auto result = session->remove_run(rid);
                        model_info mi;
                        if (!db.try_get_info_item(mid, mi)) {
                            mi = model_info(session->id, session->name, session->created, session->json);
                        }
                        db.store_model(session, mi);
                        msg::write_type(message_type::REMOVE_RUN_ID, out);
                        oa << result;
                    } break;
                    case message_type::REMOVE_RUN_NAME: {
                        int64_t mid;
                        string rname;
                        ia >> mid >> rname;
                        auto session = db.read_model(mid);
                        auto result = session->remove_run(rname);
                        model_info mi;
                        if (!db.try_get_info_item(mid, mi)) {
                            mi = model_info(session->id, session->name, session->created, session->json);
                        }
                        db.store_model(session, mi);
                        msg::write_type(message_type::REMOVE_RUN_NAME, out);
                        oa << result;
                    } break;
                    case message_type::GET_RUN_ID: {
                        int64_t mid;
                        int64_t rid;
                        ia >> mid >> rid;
                        auto session = db.read_model(mid);
                        auto result = session->get_run(rid);
                        msg::write_type(message_type::GET_RUN_ID, out);
                        oa << result;
                    } break;
                    case message_type::GET_RUN_NAME: {
                        int64_t mid;
                        string rname;
                        ia >> mid >> rname;
                        auto session = db.read_model(mid);
                        auto result = session->get_run(rname);
                        msg::write_type(message_type::GET_RUN_NAME, out);
                        oa << result;
                    } break;
                    case message_type::ADD_MODEL_REF: {
                        int64_t mid;
                        int64_t rid;
                        model_ref_ mr;
                        ia >> mid >> rid >> mr;
                        auto session = db.read_model(mid);
                        auto run = session->get_run(rid);
                        if (run) { // If we found the run, we can add to it
                            run->add_model_ref(mr);
                            model_info mi;
                            if (!db.try_get_info_item(mid, mi)) {
                                mi = model_info(session->id, session->name, session->created, session->json);
                            }
                            db.store_model(session, mi);
                        }
                        msg::write_type(message_type::ADD_MODEL_REF, out);
                    } break;
                    case message_type::REMOVE_MODEL_REF: {
                        int64_t mid;
                        int64_t rid;
                        string mkey;
                        bool result = false;
                        ia >> mid >> rid >> mkey;
                        auto session = db.read_model(mid);
                        auto run = session->get_run(rid);
                        if (run) result = run->remove_model_ref(mkey);
                        if (result) { // If model ref was successully removed, we must update session
                            model_info mi;
                            if (!db.try_get_info_item(mid, mi)) {
                                mi = model_info(session->id, session->name, session->created, session->json);
                            }
                            db.store_model(session, mi);
                        }
                        msg::write_type(message_type::REMOVE_MODEL_REF, out);
                        oa << result;
                    } break;
                    case message_type::GET_MODEL_REF: {
                        int64_t mid;
                        int64_t rid;
                        string mkey;
                        model_ref_ mr = nullptr;
                        ia >> mid >> rid >> mkey;
                        auto session = db.read_model(mid);
                        auto run = session->get_run(rid);
                        if (run) mr = run->get_model_ref(mkey);
                        msg::write_type(message_type::GET_MODEL_REF, out);
                        oa << mr;
                    } break;
                    // other
                    default:
                        return false;
                }
            }
            return true;
        }
    };
}