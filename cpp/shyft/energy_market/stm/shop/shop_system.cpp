#include <shyft/energy_market/stm/shop/shop_system.h>
#include <memory>
#include <cstdint>
#include <cmath>
#ifndef _WIN32
#include <unistd.h> // environ
#endif
#include <cstdlib>
#include <string>
#include <stdexcept>
//#include <boost/dll/runtime_symbol_info.hpp> // program_location
#include <filesystem> // current_path
//#include <iostream> // cout/cerr
//#include <mutex>
//namespace shop {
//    std::recursive_mutex api_mx;
//}
namespace shyft::energy_market::stm::shop {

#ifndef WIN32
static inline int strcpy_s(char *dst,size_t n,char const*src) {
    if(dst==nullptr) return -1;//not allowd
    if(strlen(src)>=n) {dst[0]=0;return -2;}//to large
    if(dst+n < src || dst >=src+n) {//no overlap
        strcpy(dst,src);
    }
    dst[0]=0;
    return -3;//overlaps
}
#endif

shop_system::shop_system(utcperiod period, utctimespan t_step, stm_system* stm, const string& prefix)
	: time_axis{}, api{}, adapter{ api, period }, emitter{ api, adapter, period }, commander{ api } {
	if (!period.valid())
		throw runtime_error("period is not valid");
	if (t_step <= utctimespan())
		throw runtime_error("t_step is not valid");
	auto n_steps{ period.timespan() / t_step };
	if (n_steps <= 0)
		throw runtime_error("n_steps <= 0");
	vector<utctime> t_points;
	t_points.reserve(n_steps);
	for (auto t = period.start; t <= period.end; t += t_step)
		t_points.push_back(t);
	time_axis = shyft::time_axis::generic_dt{ t_points };
	set_time_axis(api, time_axis);
	if (!std::getenv("ICC_COMMAND_PATH")) {
		// Shop API v0.3.0 needs to be told path to solver interface library from either API function ShopAddDllPath
		// or env.var. ICC_COMMAND_PATH (but core solver library can be in PATH, application directory, working directory etc).
		// Any license file must be either in working directory or path given by env.var. ICC_COMMAND_PATH (but ShopAddDllPath
		// is not being considered).
		// Here we default to load solver interface library from current working directory, when ICC_COMMAND_PATH is not set,
		// so that solver interface library file and the license file both must be in path given by either of these.
		api.set_library_path(std::filesystem::current_path().string().c_str());
		//std::cerr << "Using current path: " << std::filesystem::current_path().string() << std::endl;
	} else {
		//std::cerr << "Using ICC_COMMAND_PATH: " << std::getenv("ICC_COMMAND_PATH") << std::endl;
	}
	if (stm) {
		vis = std::make_shared<shop_visitor>(*stm, prefix);
		vis->update_run_time_axis(time_axis);
	}
}
shop_system::shop_system(const shyft::time_axis::generic_dt& time_axis, stm_system* stm, const string& prefix)
	: time_axis{ time_axis }, api{}, adapter{ api, time_axis.total_period() }, emitter{ api, adapter, time_axis.total_period() }, commander{ api } {
	if (time_axis.size() == 0)
		throw runtime_error("time_axis.size() == 0");
	set_time_axis(api, time_axis);
	if (!std::getenv("ICC_COMMAND_PATH")) {
		// Shop API v0.3.0 needs to be told path to solver interface library from either API function ShopAddDllPath
		// or env.var. ICC_COMMAND_PATH (but core solver library can be in PATH, application directory, working directory etc).
		// Any license file must be either in working directory or path given by env.var. ICC_COMMAND_PATH (but ShopAddDllPath
		// is not being considered).
		// Here we default to load solver interface library from current working directory, when ICC_COMMAND_PATH is not set,
		// so that solver interface library file and the license file both must be in path given by either of these.
		api.set_library_path(std::filesystem::current_path().string().c_str());
		//std::cerr << "Using current path: " << std::filesystem::current_path().string() << std::endl;
	} else {
		//std::cerr << "Using ICC_COMMAND_PATH: " << std::getenv("ICC_COMMAND_PATH") << std::endl;
	}
	if (stm) {
		vis = std::make_shared<shop_visitor>(*stm, prefix);
		vis->update_run_time_axis(time_axis);
	}
}


shop_system::~shop_system() {
	uninstall_logger();
}

void shop_system::set_time_axis(shop_api& api, const utcperiod& period, const utctimespan& t_step) {
	api.set_time_axis(to_seconds64(period.start), to_seconds64(period.end), to_seconds64(t_step));
}

void shop_system::set_time_axis(shop_api& api, const shyft::time_axis::generic_dt& time_axis) {
	vector<time_t> t_axis; t_axis.reserve(time_axis.size());
	if (time_axis.gt == shyft::time_axis::generic_dt::POINT) {
		std::transform(std::cbegin(time_axis.p.t), std::cend(time_axis.p.t), std::back_inserter(t_axis), to_seconds64);
		t_axis.push_back(to_seconds64(time_axis.p.t_end));
	} else {
		auto point_axis = shyft::time_axis::convert_to_point_dt(time_axis);
		std::transform(std::cbegin(point_axis.t), std::cend(point_axis.t), std::back_inserter(t_axis), to_seconds64);
		t_axis.push_back(to_seconds64(point_axis.t_end));
	}
	api.set_time_axis(t_axis);
}

void shop_system::environment(std::ostream& out) {
	for (char** the_environ = environ; *the_environ; the_environ++)
		out << *the_environ << std::endl;
}

void shop_system::install_logger(std::shared_ptr<shop_logger> alogger) {
	logger = std::move(alogger);
	shop_logger_hook::info = std::bind(&shop_logger::info, logger, std::placeholders::_1);
	shop_logger_hook::warning = std::bind(&shop_logger::warning, logger, std::placeholders::_1);
	shop_logger_hook::error = std::bind(&shop_logger::error, logger, std::placeholders::_1);
	shop_logger_hook::exit = std::bind(&shop_logger::exit, logger);
}
void shop_system::uninstall_logger() {
	shop_logger_hook::info = nullptr;
	shop_logger_hook::warning = nullptr;
	shop_logger_hook::error = nullptr;
	shop_logger_hook::exit = nullptr;
	logger.reset();
}

}
