#
# CMake build script for a stand-alone application that generates C++ API around
# Sintef's C API for Shop.
#
# Re-using relevant configuration from the main shyft build script.
#
# Requires a specific version of shop_api cmake package in configured shyft dependencies
# directory (but none of the other dependencies).
#
cmake_minimum_required(VERSION 3.14.0)
project(shop_cxx_api_generator CXX)
set(CMAKE_CXX_STANDARD 17)

option(EXECUTE "Run executable to automatically generate headers after build" OFF)

# Build type
set(SHYFT_DEFAULT_BUILD_TYPE "Release")
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
    message(STATUS "No build type specified. Defaulting to '${SHYFT_DEFAULT_BUILD_TYPE}'.")
    set(CMAKE_BUILD_TYPE ${SHYFT_DEFAULT_BUILD_TYPE} CACHE STRING "Choose the type of build." FORCE)
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif()

# The dependencies directory (search path for shop_api package)
if(NOT DEFINED SHYFT_DEPENDENCIES_DIR)
    if(DEFINED ENV{SHYFT_DEPENDENCIES_DIR})
        set(SHYFT_DEPENDENCIES_DIR $ENV{SHYFT_DEPENDENCIES_DIR})
    else()
        set(SHYFT_DEPENDENCIES_DIR "${PROJECT_SOURCE_DIR}/../shyft_dependencies")
    endif()
endif()
get_filename_component(SHYFT_DEPENDENCIES ${SHYFT_DEPENDENCIES_DIR} ABSOLUTE)
message(STATUS "SHYFT_DEPENDENCIES directory: " ${SHYFT_DEPENDENCIES})
message(STATUS "  You can override the above via the SHYFT_DEPENDENCIES_DIR environment variable")
set(CMAKE_PREFIX_PATH "${SHYFT_DEPENDENCIES}" ${CMAKE_PREFIX_PATH})

# Shop API package
set(SHOP_API_VERSION 13.2.1.5 CACHE STRING "The version of the package shop_api")
find_package(shop_api ${SHOP_API_VERSION} EXACT CONFIG REQUIRED)
message(STATUS "Using shop_api version ${shop_api_SHOP_VERSION}")
add_compile_definitions(SHOP_API_VERSION=${shop_api_SHOP_VERSION})

# Executable tool
set(target shop_cxx_api_generator)
add_executable(${target} main.cpp)
target_link_libraries(${target} shop_api)

# OLD: License does not seem to be needed to do this!
#if(DEFINED shop_api_LICENSE_FILE)
#    # Copy license file to application folder to be able to run without prerequisites
#    # (cplex runtime library not needed in this case).
#    add_custom_command(TARGET ${target} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different
#        ${shop_api_LICENSE_FILE} $<TARGET_FILE_DIR:${target}>/SHOP_license.dat)
#else()
#    message(WARNING "Shop license file not found")
#endif()

if(EXECUTE)
    # Custom command to execute the executable and generate headers with pre-defined paths.
    message(STATUS "Will run executable post build to generate default headers")
    message(STATUS "  ${CMAKE_CURRENT_LIST_DIR}/shop_enums.h")
    message(STATUS "  ${CMAKE_CURRENT_LIST_DIR}/shop_api.h")
    add_custom_command(TARGET ${target} POST_BUILD
        COMMAND ${target} "\"${CMAKE_CURRENT_LIST_DIR}/shop_enums.h\"" "\"${CMAKE_CURRENT_LIST_DIR}/shop_api.h\""
        COMMENT "Running executable post build to generate default headers")
endif()
