#pragma once
// auto genereated files based on active version of Sintef SHOP api dll
#include "shop_proxy.h"
namespace shop {

using proxy::obj;
using shop::proxy::rw;
using shop::proxy::ro;

template<class A>
struct reservoir:obj<A,0> {
    using super=obj<A,0>;
    reservoir()=default;
    reservoir(A* s,int oid):super(s, oid) {}
    reservoir(const reservoir& o):super(o) {}
    reservoir(reservoir&& o):super(std::move(o)) {}
    reservoir& operator=(const reservoir& o) {
        super::operator=(o);
        return *this;
    }
    reservoir& operator=(reservoir&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<reservoir,4,int> cut_time{this}; // x[no_unit],y[no_unit]
    rw<reservoir,5,double> start_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,6,double> start_head{this}; // x[meter],y[meter]
    rw<reservoir,7,double> max_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,8,double> lrl{this}; // x[meter],y[meter]
    rw<reservoir,9,double> hrl{this}; // x[meter],y[meter]
    ro<reservoir,10,double> endpoint_penalty{this}; // x[nok],y[nok]
    rw<reservoir,12,typename A::_xy> vol_head{this}; // x[mm3],y[meter]
    rw<reservoir,13,typename A::_xy> flow_descr{this}; // x[meter],y[m3_per_s]
    rw<reservoir,14,typename A::_xy> endpoint_desc_nok_mm3{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,15,typename A::_xy> endpoint_desc_nok_mwh{this}; // x[mm3],y[nok_per_mwh]
    rw<reservoir,16,typename A::_xy> watervalue_desc_nok_mm3{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,18,vector<typename A::_xy>> reservoir_cut_coeffs{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,19,vector<typename A::_xy>> inflow_cut_coeffs{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,20,typename A::_txy> inflow{this}; // x[no_unit],y[m3_per_s]
    rw<reservoir,21,typename A::_txy> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    rw<reservoir,22,typename A::_txy> inflow_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,23,typename A::_txy> min_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,24,typename A::_txy> min_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,25,typename A::_txy> max_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,26,typename A::_txy> max_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,27,typename A::_txy> tactical_cost_min{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,28,typename A::_txy> tactical_cost_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,29,typename A::_txy> tactical_cost_max{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,30,typename A::_txy> tactical_cost_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,31,typename A::_txy> tactical_limit_min{this}; // x[no_unit],y[mm3]
    rw<reservoir,32,typename A::_txy> tactical_limit_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,33,typename A::_txy> tactical_limit_max{this}; // x[no_unit],y[mm3]
    rw<reservoir,34,typename A::_txy> tactical_limit_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,35,typename A::_txy> overflow_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,36,typename A::_txy> schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,37,typename A::_txy> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,38,typename A::_txy> volume_schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,39,typename A::_txy> level_schedule{this}; // x[no_unit],y[meter]
    rw<reservoir,40,typename A::_txy> upper_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,41,typename A::_txy> lower_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,42,typename A::_txy> elevation_adjustment{this}; // x[no_unit],y[meter]
    ro<reservoir,43,typename A::_txy> storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,44,typename A::_txy> sim_storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,45,typename A::_txy> head{this}; // x[no_unit],y[meter]
    ro<reservoir,46,typename A::_txy> sim_head{this}; // x[no_unit],y[meter]
    ro<reservoir,47,typename A::_txy> global_incr_cost_nok_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,48,typename A::_txy> local_incr_cost_nok_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,49,typename A::_txy> local_incr_cost_nok_mwh{this}; // x[no_unit],y[nok_per_mwh]
    ro<reservoir,50,typename A::_txy> penalty{this}; // x[no_unit],y[mm3]
    ro<reservoir,51,typename A::_txy> tactical_penalty_up{this}; // x[no_unit],y[nok]
    ro<reservoir,52,typename A::_txy> tactical_penalty_down{this}; // x[no_unit],y[nok]
    ro<reservoir,53,vector<typename A::_xy>> cut_output_coeffs_mm3{this}; // x[percent],y[nok_per_mm3]
    rw<reservoir,54,vector<typename A::_xy>> cut_input_coeffs_mm3{this}; // x[percent],y[nok_per_mm3]
    ro<reservoir,55,typename A::_txy> end_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,56,typename A::_txy> penalty_nok{this}; // x[no_unit],y[nok]
    ro<reservoir,57,typename A::_txy> vow_in_transit{this}; // x[no_unit],y[nok]
    ro<reservoir,58,typename A::_txy> tactical_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,59,typename A::_txy> end_value{this}; // x[no_unit],y[nok]
    ro<reservoir,60,typename A::_txy> change_in_end_value{this}; // x[no_unit],y[nok]
    rw<reservoir,61,double> latitude{this}; // x[no_unit],y[no_unit]
    rw<reservoir,62,double> longitude{this}; // x[no_unit],y[no_unit]
    rw<reservoir,63,int> head_opt{this}; // x[no_unit],y[no_unit]
    rw<reservoir,64,typename A::_txy> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<reservoir,65,typename A::_txy> overflow_cost_flag{this}; // x[no_unit],y[nok_per_mm3]
};
template<class A>
struct power_station:obj<A,1> {
    using super=obj<A,1>;
    power_station()=default;
    power_station(A* s,int oid):super(s, oid) {}
    power_station(const power_station& o):super(o) {}
    power_station(power_station&& o):super(std::move(o)) {}
    power_station& operator=(const power_station& o) {
        super::operator=(o);
        return *this;
    }
    power_station& operator=(power_station&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<power_station,69,int> time_delay{this}; // x[no_unit],y[no_unit]
    ro<power_station,71,int> num_gen{this}; // x[no_unit],y[no_unit]
    ro<power_station,72,int> num_pump{this}; // x[no_unit],y[no_unit]
    rw<power_station,76,double> outlet_line{this}; // x[no_unit],y[meter]
    ro<power_station,77,double> prod_factor{this}; // x[no_unit],y[kwh_per_mm3]
    rw<power_station,80,vector<double>> main_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_station,81,vector<double>> penstock_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    //--TODO: ro<power_station,82,xyt> best_profit_q{this}; // x[mw],y[m3_per_s]
    //--TODO: ro<power_station,83,xyt> best_profit_mc{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<power_station,84,xyt> best_profit_ac{this}; // x[mw],y[nok_per_mw]
    rw<power_station,85,vector<typename A::_xy>> tailrace_loss{this}; // x[m3_per_s],y[meter]
    rw<power_station,86,vector<typename A::_xy>> intake_loss{this}; // x[m3_per_s],y[meter]
    rw<power_station,87,typename A::_xy> shape_discharge{this}; // x[hour],y[no_unit]
    rw<power_station,88,typename A::_xy> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<power_station,89,typename A::_txy> linear_startup_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,90,typename A::_txy> mip_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,91,typename A::_txy> tailrace_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,92,typename A::_txy> intake_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,93,typename A::_xy> max_q_limit_rsv_up{this}; // x[meter],y[m3_per_s]
    rw<power_station,94,typename A::_xy> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<power_station,95,typename A::_txy> min_p_constr{this}; // x[no_unit],y[mw]
    rw<power_station,96,typename A::_txy> max_p_constr{this}; // x[no_unit],y[mw]
    rw<power_station,97,typename A::_txy> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_station,98,typename A::_txy> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_station,99,typename A::_txy> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,100,typename A::_txy> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,101,typename A::_txy> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,102,typename A::_txy> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,103,typename A::_txy> intake_line{this}; // x[no_unit],y[meter]
    rw<power_station,104,typename A::_txy> frr_up_min{this}; // x[no_unit],y[mw]
    rw<power_station,105,typename A::_txy> frr_up_max{this}; // x[no_unit],y[mw]
    rw<power_station,106,typename A::_txy> frr_down_min{this}; // x[no_unit],y[mw]
    rw<power_station,107,typename A::_txy> frr_down_max{this}; // x[no_unit],y[mw]
    rw<power_station,108,typename A::_txy> rr_up_min{this}; // x[no_unit],y[mw]
    rw<power_station,109,typename A::_txy> frr_symmetric_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,110,typename A::_txy> distribute_production{this}; // x[no_unit],y[no_unit]
    rw<power_station,111,typename A::_txy> discharge_group{this}; // x[no_unit],y[no_unit]
    rw<power_station,112,typename A::_txy> bid_group{this}; // x[no_unit],y[no_unit]
    rw<power_station,113,typename A::_txy> prod_area{this}; // x[no_unit],y[no_unit]
    rw<power_station,114,typename A::_txy> prod_area_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,115,typename A::_txy> tides{this}; // x[no_unit],y[delta_meter]
    rw<power_station,116,typename A::_txy> block_merge_tolerance{this}; // x[no_unit],y[mw]
    rw<power_station,117,typename A::_txy> block_generation_mwh{this}; // x[no_unit],y[hour]
    rw<power_station,118,typename A::_txy> block_generation_m3s{this}; // x[no_unit],y[hour]
    rw<power_station,119,typename A::_txy> production_schedule{this}; // x[no_unit],y[mw]
    rw<power_station,120,typename A::_txy> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_station,121,typename A::_txy> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<power_station,122,typename A::_txy> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_station,123,typename A::_txy> feeding_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_station,124,typename A::_txy> production_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_station,125,typename A::_txy> consumption_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_station,126,typename A::_txy> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_station,127,typename A::_txy> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,128,typename A::_txy> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,129,typename A::_txy> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,130,typename A::_txy> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,131,typename A::_txy> feeding_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,132,typename A::_txy> production_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,133,typename A::_txy> consumption_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,134,typename A::_txy> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,136,typename A::_txy> mc_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,137,typename A::_txy> mip_length{this}; // x[no_unit],y[no_unit]
    rw<power_station,138,typename A::_txy> mip_length_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,139,typename A::_txy> min_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,140,typename A::_txy> max_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,141,typename A::_txy> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,142,typename A::_txy> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    ro<power_station,143,typename A::_txy> min_p_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,144,typename A::_txy> max_p_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,145,typename A::_txy> min_q_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,146,typename A::_txy> max_q_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,147,typename A::_txy> production{this}; // x[no_unit],y[mw]
    ro<power_station,148,typename A::_txy> sim_production{this}; // x[no_unit],y[mw]
    ro<power_station,149,typename A::_txy> consumption{this}; // x[no_unit],y[mw]
    ro<power_station,150,typename A::_txy> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_station,151,typename A::_txy> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_station,152,typename A::_txy> eff_head{this}; // x[no_unit],y[meter]
    ro<power_station,153,typename A::_txy> head_loss{this}; // x[no_unit],y[meter]
    ro<power_station,154,typename A::_txy> prod_unbalance{this}; // x[no_unit],y[mw]
    ro<power_station,155,typename A::_txy> cons_unbalance{this}; // x[no_unit],y[mw]
    ro<power_station,156,typename A::_txy> schedule_up_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,157,typename A::_txy> schedule_down_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,159,typename A::_txy> p_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,160,typename A::_txy> q_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_station,161,typename A::_txy> schedule_penalty{this}; // x[no_unit],y[nok]
    rw<power_station,162,double> latitude{this}; // x[no_unit],y[no_unit]
    rw<power_station,163,double> longitude{this}; // x[no_unit],y[no_unit]
    rw<power_station,164,typename A::_txy> power_ramping_up{this}; // x[no_unit],y[mw]
    rw<power_station,165,typename A::_txy> power_ramping_down{this}; // x[no_unit],y[mw]
    rw<power_station,166,typename A::_txy> discharge_ramping_up{this}; // x[no_unit],y[mw]
    rw<power_station,167,typename A::_txy> discharge_ramping_down{this}; // x[no_unit],y[mw]
    rw<power_station,168,typename A::_txy> bp_dyn_wv_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,169,typename A::_txy> production_group{this}; // x[no_unit],y[no_unit]
    ro<power_station,170,typename A::_txy> ref_prod{this}; // x[no_unit],y[mw]
    rw<power_station,173,int> equal_distribution{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<power_station,174,int_array> gen_priority{this}; // x[no_unit],y[no_unit]
    rw<power_station,175,double> less_distribution_eps{this}; // x[no_unit],y[no_unit]
    rw<power_station,176,double> ownership{this}; // x[no_unit],y[no_unit]
    rw<power_station,177,double> sched_penalty_cost_down{this}; // x[no_unit],y[nok]
    rw<power_station,178,double> sched_penalty_cost_up{this}; // x[no_unit],y[nok]
    rw<power_station,179,typename A::_txy> n_marg_points{this}; // x[no_unit],y[no_unit]
    rw<power_station,180,typename A::_txy> n_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_station,181,typename A::_txy> n_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_station,182,typename A::_txy> n_mip_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_station,183,typename A::_txy> n_mip_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_station,184,typename A::_txy> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,185,typename A::_txy> min_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_station,186,typename A::_txy> max_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_station,187,typename A::_txy> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_station,188,typename A::_txy> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_station,189,typename A::_txy> min_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,190,typename A::_txy> max_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,191,typename A::_txy> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_station,192,typename A::_txy> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct aggregate:obj<A,2> {
    using super=obj<A,2>;
    aggregate()=default;
    aggregate(A* s,int oid):super(s, oid) {}
    aggregate(const aggregate& o):super(o) {}
    aggregate(aggregate&& o):super(std::move(o)) {}
    aggregate& operator=(const aggregate& o) {
        super::operator=(o);
        return *this;
    }
    aggregate& operator=(aggregate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<aggregate,201,int> type{this}; // x[no_unit],y[no_unit]
    ro<aggregate,202,int> num_needle_comb{this}; // x[no_unit],y[no_unit]
    rw<aggregate,205,int> initial_state{this}; // x[no_unit],y[no_unit]
    rw<aggregate,206,int> penstock{this}; // x[no_unit],y[no_unit]
    rw<aggregate,207,double> p_min{this}; // x[no_unit],y[mw]
    rw<aggregate,208,double> p_max{this}; // x[no_unit],y[mw]
    rw<aggregate,209,double> p_nom{this}; // x[no_unit],y[mw]
    //--TODO: ro<aggregate,210,xyt> best_profit_q{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<aggregate,211,xyt> best_profit_p{this}; // x[mw],y[nok_per_mw]
    rw<aggregate,212,typename A::_xy> gen_eff_curve{this}; // x[mw],y[percent]
    rw<aggregate,213,vector<typename A::_xy>> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<aggregate,214,typename A::_xy> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<aggregate,215,vector<typename A::_xy>> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<aggregate,216,typename A::_txy> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,217,typename A::_txy> production_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,218,typename A::_txy> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,219,typename A::_txy> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<aggregate,220,typename A::_txy> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    ro<aggregate,221,typename A::_txy> production_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,222,typename A::_txy> discharge_schedule_penalty{this}; // x[no_unit],y[mm3]
    rw<aggregate,223,typename A::_txy> schedule_deviation_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,224,typename A::_txy> committed_in{this}; // x[no_unit],y[no_unit]
    rw<aggregate,225,typename A::_txy> committed_flag{this}; // x[no_unit],y[no_unit]
    ro<aggregate,226,typename A::_txy> committed_out{this}; // x[no_unit],y[no_unit]
    rw<aggregate,227,typename A::_txy> ref_production{this}; // x[no_unit],y[mw]
    rw<aggregate,228,typename A::_txy> min_p_constr{this}; // x[no_unit],y[mw]
    rw<aggregate,229,typename A::_txy> max_p_constr{this}; // x[no_unit],y[mw]
    rw<aggregate,230,typename A::_txy> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<aggregate,231,typename A::_txy> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<aggregate,232,typename A::_txy> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,233,typename A::_txy> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,234,typename A::_txy> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,235,typename A::_txy> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    ro<aggregate,236,typename A::_txy> production{this}; // x[no_unit],y[mw]
    ro<aggregate,237,typename A::_txy> lp_production{this}; // x[no_unit],y[mw]
    ro<aggregate,238,typename A::_txy> sim_production{this}; // x[no_unit],y[mw]
    ro<aggregate,239,typename A::_txy> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<aggregate,240,typename A::_txy> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<aggregate,241,typename A::_txy> startcost{this}; // x[no_unit],y[nok]
    rw<aggregate,242,typename A::_txy> stopcost{this}; // x[no_unit],y[nok]
    rw<aggregate,243,typename A::_txy> reserve_ramping_cost_up{this}; // x[no_unit],y[nok]
    rw<aggregate,244,typename A::_txy> reserve_ramping_cost_down{this}; // x[no_unit],y[nok]
    rw<aggregate,245,typename A::_txy> upstream_min{this}; // x[no_unit],y[meter]
    rw<aggregate,246,typename A::_txy> downstream_max{this}; // x[no_unit],y[meter]
    rw<aggregate,247,typename A::_txy> commit_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,249,typename A::_txy> prod_area{this}; // x[no_unit],y[no_unit]
    rw<aggregate,251,typename A::_txy> priority{this}; // x[no_unit],y[no_unit]
    rw<aggregate,252,typename A::_txy> mc_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,253,typename A::_txy> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,255,typename A::_txy> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<aggregate,256,typename A::_txy> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<aggregate,257,typename A::_txy> p_rr_min{this}; // x[no_unit],y[mw]
    rw<aggregate,258,typename A::_txy> frr_up_min{this}; // x[no_unit],y[mw]
    rw<aggregate,259,typename A::_txy> frr_up_max{this}; // x[no_unit],y[mw]
    rw<aggregate,260,typename A::_txy> frr_down_min{this}; // x[no_unit],y[mw]
    rw<aggregate,261,typename A::_txy> frr_down_max{this}; // x[no_unit],y[mw]
    rw<aggregate,262,typename A::_txy> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<aggregate,263,typename A::_txy> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<aggregate,264,typename A::_txy> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<aggregate,265,typename A::_txy> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<aggregate,266,typename A::_txy> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<aggregate,267,typename A::_txy> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<aggregate,268,typename A::_txy> rr_up_min{this}; // x[no_unit],y[mw]
    rw<aggregate,269,typename A::_txy> rr_up_max{this}; // x[no_unit],y[mw]
    rw<aggregate,270,typename A::_txy> rr_down_min{this}; // x[no_unit],y[mw]
    rw<aggregate,271,typename A::_txy> rr_down_max{this}; // x[no_unit],y[mw]
    rw<aggregate,272,typename A::_txy> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,273,typename A::_txy> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,274,typename A::_txy> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,275,typename A::_txy> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,276,typename A::_txy> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,277,typename A::_txy> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,278,typename A::_txy> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<aggregate,279,typename A::_txy> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,280,typename A::_txy> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,281,typename A::_txy> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,282,typename A::_txy> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,283,typename A::_txy> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,284,typename A::_txy> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,285,typename A::_txy> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,286,typename A::_txy> fcr_n_up_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,287,typename A::_txy> fcr_n_down_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,288,typename A::_txy> fcr_d_up_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,289,typename A::_txy> frr_up_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,290,typename A::_txy> frr_down_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,291,typename A::_txy> rr_up_group{this}; // x[no_unit],y[no_unit]
    rw<aggregate,292,typename A::_txy> rr_down_group{this}; // x[no_unit],y[no_unit]
    ro<aggregate,293,typename A::_txy> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,294,typename A::_txy> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,295,typename A::_txy> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,296,typename A::_txy> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,297,typename A::_txy> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,298,typename A::_txy> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,299,typename A::_txy> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<aggregate,300,typename A::_txy> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,301,typename A::_txy> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,302,typename A::_txy> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,303,typename A::_txy> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,304,typename A::_txy> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,305,typename A::_txy> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<aggregate,306,typename A::_txy> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    rw<aggregate,307,typename A::_txy> droop_cost{this}; // x[no_unit],y[nok]
    rw<aggregate,308,typename A::_txy> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<aggregate,309,typename A::_txy> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,310,typename A::_txy> droop_min{this}; // x[no_unit],y[no_unit]
    rw<aggregate,311,typename A::_txy> droop_max{this}; // x[no_unit],y[no_unit]
    ro<aggregate,312,typename A::_txy> droop_result{this}; // x[no_unit],y[no_unit]
    ro<aggregate,321,typename A::_txy> startup_cost_mip_objective{this}; // x[no_unit],y[nok]
    ro<aggregate,322,typename A::_txy> startup_cost_total_objective{this}; // x[no_unit],y[nok]
    ro<aggregate,323,typename A::_txy> schedule_penalty{this}; // x[no_unit],y[nok]
    ro<aggregate,324,typename A::_txy> discharge_fee_objective{this}; // x[no_unit],y[nok]
    ro<aggregate,325,typename A::_txy> feeding_fee_objective{this}; // x[no_unit],y[nok]
    ro<aggregate,326,typename A::_txy> market_income{this}; // x[no_unit],y[nok]
    rw<aggregate,327,int> affinity_eq_flag{this}; // x[no_unit],y[no_unit]
    rw<aggregate,333,typename A::_txy> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<aggregate,339,xyt> pq_curve{this}; // x[mw],y[nok_per_mw]
};
template<class A>
struct needle_combination:obj<A,3> {
    using super=obj<A,3>;
    needle_combination()=default;
    needle_combination(A* s,int oid):super(s, oid) {}
    needle_combination(const needle_combination& o):super(o) {}
    needle_combination(needle_combination&& o):super(std::move(o)) {}
    needle_combination& operator=(const needle_combination& o) {
        super::operator=(o);
        return *this;
    }
    needle_combination& operator=(needle_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<needle_combination,343,double> p_max{this}; // x[no_unit],y[mw]
    rw<needle_combination,344,double> p_min{this}; // x[no_unit],y[mw]
    rw<needle_combination,345,double> p_nom{this}; // x[no_unit],y[mw]
    rw<needle_combination,346,vector<typename A::_xy>> turb_eff_curves{this}; // x[m3_per_s],y[percent]
};
template<class A>
struct pump:obj<A,4> {
    using super=obj<A,4>;
    pump()=default;
    pump(A* s,int oid):super(s, oid) {}
    pump(const pump& o):super(o) {}
    pump(pump&& o):super(std::move(o)) {}
    pump& operator=(const pump& o) {
        super::operator=(o);
        return *this;
    }
    pump& operator=(pump&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<pump,352,int> penstock{this}; // x[no_unit],y[no_unit]
    rw<pump,353,int> initial_state{this}; // x[no_unit],y[no_unit]
    rw<pump,355,double> p_max{this}; // x[no_unit],y[mw]
    rw<pump,356,double> p_min{this}; // x[no_unit],y[mw]
    rw<pump,357,double> p_nom{this}; // x[no_unit],y[mw]
    rw<pump,358,typename A::_xy> gen_eff_curve{this}; // x[mw],y[percent]
    rw<pump,359,vector<typename A::_xy>> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<pump,360,typename A::_txy> committed_in{this}; // x[no_unit],y[no_unit]
    rw<pump,361,typename A::_txy> committed_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,362,typename A::_txy> committed_out{this}; // x[no_unit],y[no_unit]
    ro<pump,363,typename A::_txy> consumption{this}; // x[no_unit],y[mw]
    ro<pump,364,typename A::_txy> upflow{this}; // x[no_unit],y[m3_per_s]
    rw<pump,365,typename A::_txy> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,366,typename A::_txy> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<pump,367,typename A::_txy> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,368,typename A::_txy> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<pump,369,typename A::_txy> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,370,typename A::_txy> consumption_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,371,typename A::_txy> upflow_schedule_penalty{this}; // x[no_unit],y[mm3]
    rw<pump,372,typename A::_txy> upstream_max{this}; // x[no_unit],y[meter]
    rw<pump,373,typename A::_txy> upstream_max_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,374,typename A::_txy> downstream_min{this}; // x[no_unit],y[meter]
    rw<pump,375,typename A::_txy> downstream_min_flag{this}; // x[no_unit],y[meter]
    rw<pump,376,typename A::_txy> startcost{this}; // x[no_unit],y[nok]
    rw<pump,377,typename A::_txy> stopcost{this}; // x[no_unit],y[nok]
    rw<pump,378,typename A::_txy> commit_group{this}; // x[no_unit],y[no_unit]
    rw<pump,379,typename A::_txy> droop_cost{this}; // x[no_unit],y[nok]
    rw<pump,380,typename A::_txy> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<pump,381,typename A::_txy> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    ro<pump,382,typename A::_txy> droop_result{this}; // x[no_unit],y[no_unit]
    rw<pump,383,typename A::_txy> droop_min{this}; // x[no_unit],y[no_unit]
    rw<pump,384,typename A::_txy> droop_max{this}; // x[no_unit],y[no_unit]
    rw<pump,385,typename A::_txy> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<pump,386,typename A::_txy> p_rr_min{this}; // x[no_unit],y[mw]
    rw<pump,387,typename A::_txy> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<pump,388,typename A::_txy> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,389,typename A::_txy> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,390,typename A::_txy> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,391,typename A::_txy> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,392,typename A::_txy> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,393,typename A::_txy> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,394,typename A::_txy> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,395,typename A::_txy> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,396,typename A::_txy> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,397,typename A::_txy> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,398,typename A::_txy> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,399,typename A::_txy> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,400,typename A::_txy> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,401,typename A::_txy> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,402,typename A::_txy> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,403,typename A::_txy> fcr_n_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,404,typename A::_txy> fcr_n_down_group{this}; // x[no_unit],y[no_unit]
    rw<pump,405,typename A::_txy> fcr_d_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,406,typename A::_txy> frr_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,407,typename A::_txy> frr_down_group{this}; // x[no_unit],y[no_unit]
    rw<pump,408,typename A::_txy> rr_up_group{this}; // x[no_unit],y[no_unit]
    rw<pump,409,typename A::_txy> rr_down_group{this}; // x[no_unit],y[no_unit]
    ro<pump,410,typename A::_txy> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,411,typename A::_txy> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,412,typename A::_txy> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,413,typename A::_txy> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,414,typename A::_txy> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,415,typename A::_txy> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,416,typename A::_txy> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,417,typename A::_txy> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,418,typename A::_txy> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,419,typename A::_txy> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,420,typename A::_txy> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,421,typename A::_txy> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,422,typename A::_txy> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,423,typename A::_txy> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    rw<pump,424,typename A::_txy> reserve_ramping_cost_up{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,425,typename A::_txy> reserve_ramping_cost_down{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,426,typename A::_txy> frr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,427,typename A::_txy> frr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,428,typename A::_txy> frr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,429,typename A::_txy> frr_down_max{this}; // x[no_unit],y[mw]
    rw<pump,430,typename A::_txy> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<pump,431,typename A::_txy> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<pump,432,typename A::_txy> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<pump,433,typename A::_txy> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<pump,434,typename A::_txy> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<pump,435,typename A::_txy> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<pump,436,typename A::_txy> rr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,437,typename A::_txy> rr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,438,typename A::_txy> rr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,439,typename A::_txy> rr_down_max{this}; // x[no_unit],y[mw]
};
template<class A>
struct gate:obj<A,5> {
    using super=obj<A,5>;
    gate()=default;
    gate(A* s,int oid):super(s, oid) {}
    gate(const gate& o):super(o) {}
    gate(gate&& o):super(std::move(o)) {}
    gate& operator=(const gate& o) {
        super::operator=(o);
        return *this;
    }
    gate& operator=(gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<gate,442,int> type{this}; // x[no_unit],y[no_unit]
    rw<gate,444,int> time_delay{this}; // x[no_unit],y[no_unit]
    rw<gate,445,int> add_slack{this}; // x[no_unit],y[no_unit]
    rw<gate,446,double> max_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<gate,449,typename A::_xy> shape_discharge{this}; // x[hour],y[no_unit]
    rw<gate,450,vector<typename A::_xy>> functions_meter_m3s{this}; // x[meter],y[m3_per_s]
    rw<gate,451,vector<typename A::_xy>> functions_deltameter_m3s{this}; // x[delta_meter],y[m3_per_s]
    rw<gate,452,typename A::_txy> min_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,453,typename A::_txy> min_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,454,typename A::_txy> max_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,455,typename A::_txy> max_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,456,typename A::_txy> schedule_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<gate,457,typename A::_txy> schedule_percent{this}; // x[no_unit],y[percent]
    rw<gate,458,typename A::_txy> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,459,typename A::_txy> setting{this}; // x[no_unit],y[no_unit]
    rw<gate,460,typename A::_txy> setting_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,461,typename A::_txy> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,462,typename A::_txy> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,463,typename A::_txy> block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<gate,464,typename A::_txy> discharge_group{this}; // x[no_unit],y[no_unit]
    rw<gate,465,typename A::_txy> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,466,typename A::_txy> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    ro<gate,467,typename A::_txy> min_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,468,typename A::_txy> max_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,469,typename A::_txy> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<gate,470,typename A::_txy> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    rw<gate,471,double> lin_rel_a{this}; // x[no_unit],y[no_unit]
    rw<gate,472,double> lin_rel_b{this}; // x[no_unit],y[no_unit]
    rw<gate,473,typename A::_txy> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,474,typename A::_txy> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,475,typename A::_txy> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,476,typename A::_txy> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct thermal:obj<A,6> {
    using super=obj<A,6>;
    thermal()=default;
    thermal(A* s,int oid):super(s, oid) {}
    thermal(const thermal& o):super(o) {}
    thermal(thermal&& o):super(std::move(o)) {}
    thermal& operator=(const thermal& o) {
        super::operator=(o);
        return *this;
    }
    thermal& operator=(thermal&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct junction:obj<A,7> {
    using super=obj<A,7>;
    junction()=default;
    junction(A* s,int oid):super(s, oid) {}
    junction(const junction& o):super(o) {}
    junction(junction&& o):super(std::move(o)) {}
    junction& operator=(const junction& o) {
        super::operator=(o);
        return *this;
    }
    junction& operator=(junction&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction,492,int> junc_slack{this}; // x[no_unit],y[no_unit]
    rw<junction,493,double> altitude{this}; // x[no_unit],y[no_unit]
    ro<junction,494,typename A::_txy> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction,495,typename A::_txy> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    rw<junction,496,double> loss_factor_1{this}; // x[no_unit],y[s2_per_m5]
    rw<junction,497,double> loss_factor_2{this}; // x[no_unit],y[s2_per_m5]
    rw<junction,498,typename A::_txy> min_pressure{this}; // x[no_unit],y[meter]
    ro<junction,499,typename A::_txy> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction,500,typename A::_txy> incr_cost{this}; // x[no_unit],y[nok]
    ro<junction,501,typename A::_txy> local_incr_cost{this}; // x[no_unit],y[nok]
};
template<class A>
struct junction_gate:obj<A,8> {
    using super=obj<A,8>;
    junction_gate()=default;
    junction_gate(A* s,int oid):super(s, oid) {}
    junction_gate(const junction_gate& o):super(o) {}
    junction_gate(junction_gate&& o):super(std::move(o)) {}
    junction_gate& operator=(const junction_gate& o) {
        super::operator=(o);
        return *this;
    }
    junction_gate& operator=(junction_gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction_gate,479,int> add_slack{this}; // x[no_unit],y[no_unit]
    rw<junction_gate,480,double> height_1{this}; // x[no_unit],y[meter]
    rw<junction_gate,481,double> loss_factor_1{this}; // x[no_unit],y[s2_per_m5]
    rw<junction_gate,482,double> loss_factor_2{this}; // x[no_unit],y[s2_per_m5]
    rw<junction_gate,483,typename A::_txy> schedule{this}; // x[no_unit],y[no_unit]
    ro<junction_gate,484,typename A::_txy> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction_gate,485,typename A::_txy> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,486,typename A::_txy> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,487,typename A::_txy> tunnel_loss_1{this}; // x[no_unit],y[meter]
    ro<junction_gate,488,typename A::_txy> tunnel_loss_2{this}; // x[no_unit],y[meter]
};
template<class A>
struct creek_intake:obj<A,9> {
    using super=obj<A,9>;
    creek_intake()=default;
    creek_intake(A* s,int oid):super(s, oid) {}
    creek_intake(const creek_intake& o):super(o) {}
    creek_intake(creek_intake&& o):super(std::move(o)) {}
    creek_intake& operator=(const creek_intake& o) {
        super::operator=(o);
        return *this;
    }
    creek_intake& operator=(creek_intake&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<creek_intake,504,double> net_head{this}; // x[no_unit],y[meter]
    rw<creek_intake,505,double> max_inflow{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,506,typename A::_txy> max_inflow_dynamic{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,507,typename A::_txy> inflow{this}; // x[no_unit],y[m3_per_s]
    ro<creek_intake,508,typename A::_txy> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,509,typename A::_txy> inflow_percentage{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,510,typename A::_txy> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<creek_intake,511,typename A::_txy> non_physical_overflow_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct contract:obj<A,10> {
    using super=obj<A,10>;
    contract()=default;
    contract(A* s,int oid):super(s, oid) {}
    contract(const contract& o):super(o) {}
    contract(contract&& o):super(std::move(o)) {}
    contract& operator=(const contract& o) {
        super::operator=(o);
        return *this;
    }
    contract& operator=(contract&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct network:obj<A,11> {
    using super=obj<A,11>;
    network()=default;
    network(A* s,int oid):super(s, oid) {}
    network(const network& o):super(o) {}
    network(network&& o):super(std::move(o)) {}
    network& operator=(const network& o) {
        super::operator=(o);
        return *this;
    }
    network& operator=(network&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct market:obj<A,12> {
    using super=obj<A,12>;
    market()=default;
    market(A* s,int oid):super(s, oid) {}
    market(const market& o):super(o) {}
    market(market&& o):super(std::move(o)) {}
    market& operator=(const market& o) {
        super::operator=(o);
        return *this;
    }
    market& operator=(market&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<market,513,int> prod_area{this}; // x[no_unit],y[no_unit]
    rw<market,514,int> reserve_group{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<market,515,string> market_type{this}; // x[no_unit],y[no_unit]
    ro<market,516,typename A::_txy> buy{this}; // x[no_unit],y[mw]
    ro<market,517,typename A::_txy> sale{this}; // x[no_unit],y[mw]
    ro<market,518,typename A::_txy> sim_sale{this}; // x[no_unit],y[mw]
    rw<market,519,typename A::_txy> load{this}; // x[no_unit],y[mw]
    rw<market,520,typename A::_txy> max_buy{this}; // x[no_unit],y[mw]
    rw<market,521,typename A::_txy> max_sale{this}; // x[no_unit],y[mw]
    rw<market,522,typename A::_txy> buy_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,523,typename A::_txy> sale_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,524,typename A::_txy> buy_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,525,typename A::_txy> sale_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,526,typename A::_txy> bid_flag{this}; // x[no_unit],y[no_unit]
    rw<market,527,typename A::_txy> common_scenario{this}; // x[no_unit],y[no_unit]
    ro<market,528,typename A::_txy> reserve_obligation_penalty{this}; // x[no_unit],y[no_unit]
    ro<market,529,typename A::_txy> load_penalty{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct optimization:obj<A,13> {
    using super=obj<A,13>;
    optimization()=default;
    optimization(A* s,int oid):super(s, oid) {}
    optimization(const optimization& o):super(o) {}
    optimization(optimization&& o):super(std::move(o)) {}
    optimization& operator=(const optimization& o) {
        super::operator=(o);
        return *this;
    }
    optimization& operator=(optimization&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct reserve_group:obj<A,14> {
    using super=obj<A,14>;
    reserve_group()=default;
    reserve_group(A* s,int oid):super(s, oid) {}
    reserve_group(const reserve_group& o):super(o) {}
    reserve_group(reserve_group&& o):super(std::move(o)) {}
    reserve_group& operator=(const reserve_group& o) {
        super::operator=(o);
        return *this;
    }
    reserve_group& operator=(reserve_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<reserve_group,530,int> group_id{this}; // x[no_unit],y[no_unit]
    rw<reserve_group,531,typename A::_txy> fcr_n_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,532,typename A::_txy> fcr_n_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,533,typename A::_txy> fcr_d_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,534,typename A::_txy> frr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,535,typename A::_txy> frr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,536,typename A::_txy> rr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,537,typename A::_txy> rr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,538,typename A::_txy> fcr_n_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,539,typename A::_txy> fcr_d_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,540,typename A::_txy> frr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,541,typename A::_txy> rr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    ro<reserve_group,542,typename A::_txy> fcr_n_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,543,typename A::_txy> fcr_n_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,544,typename A::_txy> fcr_d_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,545,typename A::_txy> frr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,546,typename A::_txy> frr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,547,typename A::_txy> rr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,548,typename A::_txy> rr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,549,typename A::_txy> fcr_n_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,550,typename A::_txy> fcr_n_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,551,typename A::_txy> fcr_d_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,552,typename A::_txy> frr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,553,typename A::_txy> frr_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,554,typename A::_txy> rr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,555,typename A::_txy> rr_down_violation{this}; // x[no_unit],y[mw]
};
template<class A>
struct cut:obj<A,15> {
    using super=obj<A,15>;
    cut()=default;
    cut(A* s,int oid):super(s, oid) {}
    cut(const cut& o):super(o) {}
    cut(cut&& o):super(std::move(o)) {}
    cut& operator=(const cut& o) {
        super::operator=(o);
        return *this;
    }
    cut& operator=(cut&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct commit_group:obj<A,16> {
    using super=obj<A,16>;
    commit_group()=default;
    commit_group(A* s,int oid):super(s, oid) {}
    commit_group(const commit_group& o):super(o) {}
    commit_group(commit_group&& o):super(std::move(o)) {}
    commit_group& operator=(const commit_group& o) {
        super::operator=(o);
        return *this;
    }
    commit_group& operator=(commit_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<commit_group,571,int> group_id{this}; // x[no_unit],y[no_unit]
    rw<commit_group,572,typename A::_txy> exclude_group_when_committed{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct discharge_group:obj<A,17> {
    using super=obj<A,17>;
    discharge_group()=default;
    discharge_group(A* s,int oid):super(s, oid) {}
    discharge_group(const discharge_group& o):super(o) {}
    discharge_group(discharge_group&& o):super(std::move(o)) {}
    discharge_group& operator=(const discharge_group& o) {
        super::operator=(o);
        return *this;
    }
    discharge_group& operator=(discharge_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<discharge_group,556,int> group_id{this}; // x[no_unit],y[no_unit]
    rw<discharge_group,557,double> initial_deviation_mm3{this}; // x[mm3],y[mm3]
    rw<discharge_group,558,typename A::_txy> max_accumulated_deviation_mm3_up{this}; // x[no_unit],y[mm3]
    rw<discharge_group,559,typename A::_txy> max_accumulated_deviation_mm3_down{this}; // x[no_unit],y[mm3]
    rw<discharge_group,560,typename A::_txy> weighted_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group,561,typename A::_txy> penalty_cost_up_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    rw<discharge_group,562,typename A::_txy> penalty_cost_down_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    ro<discharge_group,563,typename A::_txy> actual_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    ro<discharge_group,564,typename A::_txy> accumulated_deviation_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,565,typename A::_txy> upper_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,566,typename A::_txy> lower_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,567,typename A::_txy> upper_slack_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,568,typename A::_txy> lower_slack_mm3{this}; // x[no_unit],y[mm3]
};
template<class A>
struct production_group:obj<A,18> {
    using super=obj<A,18>;
    production_group()=default;
    production_group(A* s,int oid):super(s, oid) {}
    production_group(const production_group& o):super(o) {}
    production_group(production_group&& o):super(std::move(o)) {}
    production_group& operator=(const production_group& o) {
        super::operator=(o);
        return *this;
    }
    production_group& operator=(production_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<production_group,569,double> period_sum{this}; // x[mwh],y[mwh]
    rw<production_group,570,typename A::_txy> period_flag{this}; // x[no_unit],y[mwh]
};
template<class A>
struct scenario:obj<A,19> {
    using super=obj<A,19>;
    scenario()=default;
    scenario(A* s,int oid):super(s, oid) {}
    scenario(const scenario& o):super(o) {}
    scenario(scenario&& o):super(std::move(o)) {}
    scenario& operator=(const scenario& o) {
        super::operator=(o);
        return *this;
    }
    scenario& operator=(scenario&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<scenario,574,int> scenario_id{this}; // x[no_unit],y[no_unit]
    rw<scenario,575,typename A::_txy> probability{this}; // x[no_unit],y[no_unit]
    rw<scenario,576,typename A::_txy> common_scenario{this}; // x[no_unit],y[no_unit]
    rw<scenario,577,typename A::_txy> common_history{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct objective:obj<A,20> {
    using super=obj<A,20>;
    objective()=default;
    objective(A* s,int oid):super(s, oid) {}
    objective(const objective& o):super(o) {}
    objective(objective&& o):super(std::move(o)) {}
    objective& operator=(const objective& o) {
        super::operator=(o);
        return *this;
    }
    objective& operator=(objective&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: ro<objective,578,string> solver_status{this}; // x[no_unit],y[no_unit]
    ro<objective,579,double> grand_total{this}; // x[nok],y[nok]
    ro<objective,580,double> sim_grand_total{this}; // x[nok],y[nok]
    ro<objective,581,double> total{this}; // x[nok],y[nok]
    ro<objective,582,double> rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,583,double> sim_rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,584,double> rsv_end_value_relative{this}; // x[nok],y[nok]
    ro<objective,585,double> market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,586,double> sim_market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,587,double> load_value{this}; // x[nok],y[nok]
    ro<objective,588,double> reserve_sale_buy{this}; // x[nok],y[nok]
    ro<objective,589,double> reserve_oblig_value{this}; // x[nok],y[nok]
    ro<objective,590,double> contract_value{this}; // x[nok],y[nok]
    ro<objective,591,double> startup_costs{this}; // x[nok],y[nok]
    ro<objective,592,double> sim_startup_costs{this}; // x[nok],y[nok]
    ro<objective,593,double> sum_penalties{this}; // x[nok],y[nok]
    ro<objective,594,double> minor_penalties{this}; // x[nok],y[nok]
    ro<objective,595,double> major_penalties{this}; // x[nok],y[nok]
    ro<objective,596,double> vow_in_transit{this}; // x[nok],y[nok]
    ro<objective,597,double> sum_feeding_fee{this}; // x[nok],y[nok]
    ro<objective,598,double> sum_discharge_fee{this}; // x[nok],y[nok]
    ro<objective,599,double> rsv_end_penalty{this}; // x[nok],y[nok]
    ro<objective,600,double> rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,601,double> sim_rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,602,double> load_penalty{this}; // x[nok],y[nok]
    ro<objective,603,double> group_time_period_penalty{this}; // x[nok],y[nok]
    ro<objective,604,double> group_time_step_penalty{this}; // x[nok],y[nok]
    ro<objective,605,double> sum_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,606,double> plant_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,607,double> rsv_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,608,double> gate_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,609,double> contract_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,610,double> group_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,611,double> discharge_group_penalty{this}; // x[nok],y[nok]
    ro<objective,612,double> common_decision_penalty{this}; // x[nok],y[nok]
    ro<objective,613,double> bidding_penalty{this}; // x[nok],y[nok]
    ro<objective,614,double> safe_mode_universal_penalty{this}; // x[nok],y[nok]
    ro<objective,615,double> gate_discharge_cost{this}; // x[nok],y[nok]
    ro<objective,616,double> bypass_cost{this}; // x[nok],y[nok]
    ro<objective,617,double> gate_spill_cost{this}; // x[nok],y[nok]
    ro<objective,618,double> physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,619,double> nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,620,double> creek_spill_cost{this}; // x[nok],y[nok]
    ro<objective,621,double> creek_physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,622,double> creek_nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,623,double> gate_slack_cost{this}; // x[nok],y[nok]
    ro<objective,624,double> junction_slack_cost{this}; // x[nok],y[nok]
    ro<objective,625,double> rsv_tactical_penalty{this}; // x[nok],y[nok]
    ro<objective,626,double> plant_p_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,627,double> plant_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,628,double> gate_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,629,double> plant_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,630,double> gen_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,631,double> pump_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,632,double> reserve_violation_penalty{this}; // x[nok],y[nok]
    ro<objective,633,double> reserve_slack_cost{this}; // x[nok],y[nok]
};
template<class A>
struct bid_group:obj<A,21> {
    using super=obj<A,21>;
    bid_group()=default;
    bid_group(A* s,int oid):super(s, oid) {}
    bid_group(const bid_group& o):super(o) {}
    bid_group(bid_group&& o):super(std::move(o)) {}
    bid_group& operator=(const bid_group& o) {
        super::operator=(o);
        return *this;
    }
    bid_group& operator=(bid_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<bid_group,634,int> group_id{this}; // x[no_unit],y[no_unit]
    ro<bid_group,635,int> num_plants{this}; // x[no_unit],y[no_unit]
    ro<bid_group,637,int> price_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,638,int> time_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,639,int> bid_start_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,640,int> bid_end_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,644,double> reduction_cost{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<bid_group,646,xyt> bid_curves{this}; // x[nok_per_mwh],y[mwh]
    ro<bid_group,647,typename A::_txy> bid_penalty{this}; // x[nok_per_mwh],y[mwh]
};
template<class A>
struct cut_group:obj<A,22> {
    using super=obj<A,22>;
    cut_group()=default;
    cut_group(A* s,int oid):super(s, oid) {}
    cut_group(const cut_group& o):super(o) {}
    cut_group(cut_group&& o):super(std::move(o)) {}
    cut_group& operator=(const cut_group& o) {
        super::operator=(o);
        return *this;
    }
    cut_group& operator=(cut_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<cut_group,648,vector<typename A::_xy>> cut_rhs{this}; // x[nok],y[nok]
};
template<class A>
struct system:obj<A,23> {
    using super=obj<A,23>;
    system()=default;
    system(A* s,int oid):super(s, oid) {}
    system(const system& o):super(o) {}
    system(system&& o):super(std::move(o)) {}
    system& operator=(const system& o) {
        super::operator=(o);
        return *this;
    }
    system& operator=(system&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<system,649,vector<typename A::_xy>> cut_output_rhs{this}; // x[nok],y[nok]
    rw<system,650,vector<double>> cut_input_rhs{this}; // x[nok],y[nok]
    rw<system,651,int> cut_output_time{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct unit_combination:obj<A,24> {
    using super=obj<A,24>;
    unit_combination()=default;
    unit_combination(A* s,int oid):super(s, oid) {}
    unit_combination(const unit_combination& o):super(o) {}
    unit_combination(unit_combination&& o):super(std::move(o)) {}
    unit_combination& operator=(const unit_combination& o) {
        super::operator=(o);
        return *this;
    }
    unit_combination& operator=(unit_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: ro<unit_combination,652,xyt> discharge{this}; // x[mw],y[m3_per_s]
    //--TODO: ro<unit_combination,653,xyt> marginal_cost{this}; // x[mw],y[nok_per_mw]
    //--TODO: ro<unit_combination,654,xyt> average_cost{this}; // x[m3_per_s],y[nok_per_mw]
};

}
