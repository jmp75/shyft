#include "shop_cxx_api_generator.h"
#include <shop_lib_interface.h> // External Shop library
#include <string>
#include <fstream>
#include <memory>

#define STRINGIZE1(x) #x
#define STRINGIZE2(x) STRINGIZE1(x)

// Externals for Shop library
void SHOP_log_error(char*) {}
void SHOP_log_info(char*) {}
void SHOP_log_warning(char*) {}
void SHOP_exit(void*) {}

using namespace std;
using namespace string_literals;
using namespace shop_cxx_api_generator;

int main(int argc, char* argv[])
{
	cout << "Shop C++ API generator for version " << STRINGIZE2(SHOP_API_VERSION) << endl;
	string enum_file_name = "shop_enums.h";
	string api_file_name = "shop_api.h";
	if (argc < 2) {
		cout << "Defaulting to working directory and file names " << enum_file_name << " and " << api_file_name << endl;
	} else if (argc < 3) {
		enum_file_name = argv[1];
		cout << "Writing to enum file: " << enum_file_name << endl;
		cout << "Writing default api file in working directory: " << api_file_name << endl;
	} else if (argc < 4) {
		enum_file_name = argv[1];
		api_file_name = argv[2];
		cout << "Writing to enum file: " << enum_file_name << endl;
		cout << "Writing to api file: " << api_file_name << endl;
	} else {
		cerr << "Invalid usage: Maximum 2 arguments supported - output enum file and api file!" << endl;
	}
	
	ofstream ofs(enum_file_name, ofstream::out);
	if (!ofs) {
		cerr<< "Failed to open enum file \"" << enum_file_name << "\" for writing" << endl;
		return 2;
	}
	ofstream pofs(api_file_name, ofstream::out);
	if (!pofs) {
		cerr<< "Failed to open api file \"" << api_file_name << "\" for writing" << endl;
		return 2;
	}

	unique_ptr<ShopSystem, bool(*)(ShopSystem*)> shop_safe(ShopInit(),ShopFree);
	ShopSystem* shop = shop_safe.get();// for convinience calling the next functions
	ShopSetSilentConsole(shop, true);
	ofs << "#pragma once" << endl
		<< "#include <tuple>" << endl
		<< "namespace shop::enums {"
		<< endl;
	print_object_types(shop, ofs);
	ofs << endl;
	print_attribute_types(shop, ofs, pofs);
	ofs << endl;
	print_relation_types(shop, ofs);
	ofs << endl;
	print_command_types(shop, ofs);
	ofs << endl;
	print_other(shop, ofs);
	ofs << "\n}" << endl;
}