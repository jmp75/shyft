#pragma once
#include <iostream>
#include <shop_lib_interface.h>

namespace shyft::energy_market::stm::shop {

struct shop_export {
	static void export_data(ShopSystem* api, bool all, std::ostream& out = std::cout);
	static void export_topology(ShopSystem* api, bool all, bool raw, std::ostream& out = std::cout);
};

}