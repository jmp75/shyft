#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <cstdint>
#include <cmath>
#include <string>

namespace shyft::energy_market::stm::shop {

using std::size_t;
using std::string;
using std::string_view;
using std::vector;
using std::runtime_error;
using std::dynamic_pointer_cast;
using qac_ts = shyft::time_series::dd::qac_ts;
using qac_parameter = shyft::time_series::dd::qac_parameter;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using hydro_power::xy_point_curve;
using hydro_power::xy_point_curve_with_z;
using hydro_power::hydro_component_;

static hydro_component_ downstream(const hydro_component_& from, bool traverse_water_routes) {
	if (from && from->downstreams.size()) {
		const auto& to = from->downstreams.front().target_();
		if (traverse_water_routes && dynamic_pointer_cast<waterway>(to))
			return downstream(to, traverse_water_routes);
		return to;
	}
	return nullptr;
}
template<class T> static shared_ptr<T> downstream_of_type(const hydro_component_& from, bool traverse_water_routes) {
	return dynamic_pointer_cast<T>(downstream(from, traverse_water_routes));
}
template<class T> static shared_ptr<T> upstream(const hydro_component_& from) {
	return from->upstreams.size() ? dynamic_pointer_cast<T>(from->upstreams.front().target_()) : nullptr;
}
template<class T> static void upstreams_of_type(const hydro_component_& from, vector<shared_ptr<T>> &to, bool traverse_water_routes) {
	if (from && from->upstreams.size()) {
		for (const auto& con : from->upstreams) {
			if (auto& obj = dynamic_pointer_cast<T>(con.target_())) {
				to.push_back(obj);
			}
			else if (traverse_water_routes) {
				if (const auto& wtr = dynamic_pointer_cast<waterway>(con.target_()))
					return upstreams_of_type(wtr, to, true);
			}
		}
	}
}

double shop_emitter::get_tunnel_loss_coeff(const waterway& wtr) const {
	return adapter.valid_temporal(wtr.head_loss_coeff) ? adapter.get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
}

waterway_ shop_emitter::get_penstock(const unit& agg, bool always_inlet) {
	// Assuming each aggregate have a dedicated waterroute upstream, which is either its penstock
	// or an inlet tunnel leading to a penstock (possibly) shared with other aggregates.
	// There can only be one waterroute segment of any inlet and penstock.
	if (always_inlet) {
		// Mode 1: Assuming there is always an inlet tunnel connecting an aggregate to its penstock.
		// This is easy, the penstock is always the second waterroute segment above the aggregate!
		if (const auto& inlet = dynamic_pointer_cast<waterway>(agg.upstream())) { // Assuming only one input to each aggreagate, which is always an inlet tunnel out from a penstock.
			if (const auto& penstock = inlet->upstreams.size() ? dynamic_pointer_cast<waterway>(inlet->upstreams.front().target_()) : nullptr) { // Assuming only one input to the inlet, which is always the penstock.
				return penstock;
			}
		}
	} else {
		// Mode 2: Assuming the inlet tunnel is optional, and only present if the penstock is shared.
		// This is a bit harder, since we might have combinations of branches with shared penstocks and inlets,
		// and branches with individual penstocks and no inlets. But with the two assumptions, inlet only
		// present if necessary (shared penstocks), and only one waterroute segment of any penstock and inlet,
		// it is rather easy to unambiguously find out: We know that we just have to consider at most two waterroute
		// segments up, so by moving up three segments from the aggregate, we have the entire penstock-inlet-aggregate
		// tree structure below, and we also know that a penstock can only have other penstocks as sibling segments,
		// and inlets can only have other inlets as siblings.
		// It is considered an error to include inlet tunnel for an aggregate with a penstock that is not
		// shared, because it is not possible to know where the penstock is in all cases. E.g. with a stream
		// aggregate-inlet-penstock-main-rsv and another stream aggregate-penstock-main1-main2-rsv we cannot detect
		// the penstock waterroute consistently in both cases (we would pick the inlet as the penstock in the first case).
		if (const auto& first = dynamic_pointer_cast<waterway>(agg.upstream())) { // Assuming only one input to each aggreagate, which is either the penstock or an inlet out from a shared penstock.
			if (const auto& second = first->upstreams.size() ? dynamic_pointer_cast<waterway>(first->upstreams.front().target_()) : nullptr) { // Assuming only one input to the tunnel (no junction), which is either a shared penstock or the main tunnel.
				size_t siblings = 0; // Count sibling segments of first (sibling penstocks if first is a penstock, sibling inlets if first is an inlet)
				for (const auto& con : second->downstreams) {
					if (const auto& sibling_of_first = dynamic_pointer_cast<waterway>(con.target_())) {
						if (sibling_of_first != first) {
							++siblings;
							for (const auto& con : sibling_of_first->downstreams) {
								if (const auto& target = con.target_()) {
									if (dynamic_pointer_cast<waterway>(target)) {
										// The segment sibling to the first upstream from the aggregate leads into a deeper structure,
										// and then it is safe to assume it is a penstock connected by inlets into multiple aggregates.
										// This means the "first" segment we found initially cannot cannot be an inlet, as penstocks and
										// inlets cannot be siblings, so this means it must be the penstock we are looking for!
										return first;
									} else if (dynamic_pointer_cast<unit>(target)) {
										// The segment sibling to the first upstream from the aggregate leads into another aggregate,
										// and then we do not know if the two segments are inlets from a shared penstock, or if these
										// aggregates have separate penstocks without inlets. To find out we must go to level three above
										// the aggregate, to see if there are other branches down from there. If there is, then we know that
										// our current branch is with a shared penstock - with an inlet segment first and penstock second.
										if (const auto& third = second->upstreams.size() ? dynamic_pointer_cast<waterway>(second->upstreams.front().target_()) : nullptr) {
											for (const auto& con : third->downstreams) {
												if (const auto& sibling_of_second = dynamic_pointer_cast<waterway>(con.target_())) {
													if (sibling_of_second != second) {
														return second;
													}
												}
											}
										}
										return first;
									} else {
										throw runtime_error(string("Unexpected element in plant topology: ") + target->name + " [" + std::to_string(target->id) + "]");
									}
								}
							}
						}
					}
				}
				if (siblings < 2) {
					// There were no siblings of the first segment, which means it cannot be an inlet and it must be the penstock
					// (from the assumption that inlets are only modelled when connecting multiple aggregates to a shared penstock).
					return first;
				}
			}
		}
	}
	return {};
}
waterway_ shop_emitter::get_tailrace(const unit& agg) {
	if (const auto& draft_tube = dynamic_pointer_cast<waterway>(agg.downstream())) { // Assuming only one output from each aggreagate, which is always a draft tube.
		if (const auto& tailrace = draft_tube->downstreams.size() ? dynamic_pointer_cast<waterway>(draft_tube->downstreams.front().target_()) : nullptr) { // Assuming only one output from the draft tube, which is always the tailrace.
			return tailrace;
		}
	}
	return {};
}

#ifdef SHOP_API_v0_2
std::pair<shop_object_id,shop_object_id> shop_emitter::handle_plant_input(const waterway_& main) {
	// Handle main input tunnel to (upstream from) a plant, including any creek intakes and junctions.
	// Executes recursivively, in case of multiple water route segments on a single stream, multiple
	// branches with creek intakes and or multiple reservoirs (junctions), etc.
	// Returning a pair of shop ids:
	// - The first value is the Shop object that the tunnel ended in. This is the main result,
	//   and at the end of the recursion this is the shop id to connect as input to the plant.
	// - The second value is used only for the creek input handling during recursion. This is the
	//   Shop object that the tunnel started with, when it contained a creek intake along the way.
	//   With Shop objects the reservoir object will be (from upstream) connected into the creek
	//   (as if it were downstream on the same tunnel). During recursion the handling of such a
	//   branch will end with the creek object, returned as the first (main) result, and then
	//   to we identify the reservoir as the source of the branch in the second result value so that
	//   we can detect that this is not a simple creek insert but actually an entire branch.
	//   Think of .first as the source of the branch and .second as the sink.
	const size_t n_upstreams = main->upstreams.size();
	if (n_upstreams < 1) { // Nothing upstream: Invalid!
		throw runtime_error(string("No upstreams from water route ") + main->name + " [" + std::to_string(main->id) + "]");
	} else if (n_upstreams < 2) { // Single upstream: Must be a reservoir (regular or creek).
		if (auto input = dynamic_pointer_cast<reservoir>(main->upstreams.front().target_())) {
			// Reached the end of this stream (and recursion): Upstream tunnel segment ended in reservoir.
			// Assuming reservoirs have already been handled, we just return it's shop id.
			// Note that the reservoir might be represented as a creek_intake in Shop, instead of a regular reservoir!
			if (auto id = objects.id_of(input))
				return { id, {} };
			throw runtime_error("Reached an unknown input reservoir");
		} else if (auto input = dynamic_pointer_cast<waterway>(main->upstreams.front().target_())) {
			return handle_plant_input(input); // Additional water route segments are just skipped.
		} else {
			throw runtime_error("Not implemented: Input tunnel from anything other than reservoir!"); // Only reservoirs (and creeks) are allowed upstrema from plant!?
		}
	} else { // Multiple upstreams: Junction in STM - but not necessarily in Shop!
		vector<waterway_> branches;
		vector<int> junction_input_ids;
		vector<int> creek_intake_ids;
		bool gate_controlled{ false };
		apoint_ts junction_schedule{ shyft::time_axis::point_dt{ { adapter.period.start, adapter.period.end } }, 0.0 };
		int branch_number = 0;
		for (const auto& up : main->upstreams) {
			if (const auto& branch_wtr = dynamic_pointer_cast<waterway>(up.target_())) {
				auto[branch_sink_id, branch_source_id] = handle_plant_input(branch_wtr);
				if (!branch_source_id && branch_sink_id.t_id == shop_creek_intake::t_id) {
					// The recursive handling of the branch found that this was a plain creek input.
					// The !input_source_id condition means there was no reservoir upstream from it,
					// because if it was we must treet it like a proper branch and not a creek input.
					creek_intake_ids.push_back(branch_sink_id.id);
				} else {
					branches.push_back(branch_wtr);
					junction_input_ids.push_back(branch_sink_id.id);
					++branch_number;
					if (branch_wtr->gates.size()) {
						apoint_ts input_schedule{ shyft::time_axis::point_dt{ { adapter.period.start, adapter.period.end } }, 0.0 };
						for (const auto& g : branch_wtr->gates) {
							if (const auto& gt = dynamic_pointer_cast<gate>(g)) {
								if (exists(gt->discharge.schedule)) {
									input_schedule = input_schedule + gt->discharge.schedule;
									gate_controlled = true;
								}
							}
						}
						// Calculate combined junction gate schedule, selecting which single branch to run from, based on a gate schedule for each of the input branches.
						junction_schedule = junction_schedule + input_schedule.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, (double)branch_number, 0.0);
					}
				}
			}
		}
		shop_object_id sink_id, source_id;
		if (branches.size() > 1) {
			if (gate_controlled) {
				auto& shop_junction_object = objects.add(main, adapter.to_shop<shop_junction_gate>(main, branches));
				// The combined junction gate schedule contains values between 1 and n (2) indicating which single branch
				// has open gate(s). Shop treats value 0 or >n as all are closed, and with licensed (password protected)
				// functionality "Junction gate as junction" (SHOP_JUNCG_TWO_WAY) a special value -2 will make the junction
				// gate be treated as an ordinary junction.
				auto junction_schedule_two_way = make_shared<qac_ts>(junction_schedule, qac_parameter{ utctimespan::zero(), 0.0, branch_number+0.5, utctimespan::zero(), 0.0, -2.0 });
				adapter.set(shop_junction_object.schedule, (apoint_ts)junction_schedule_two_way);
				sink_id = shop_junction_object;
			} else {
				auto& shop_junction_object = objects.add(main, adapter.to_shop<shop_junction>(main, branches));
				sink_id = shop_junction_object;
			}
			// Connect inputs (source of each branch) to junction
			for (const auto& id : junction_input_ids) {
				api._connect_objects(id, ::shop::connection::main, sink_id.id);
			}
		} else if (branches.size() == 1) {
			// Only one real branch left, presumably all other branches reduced to creek intakes into this one (which means creek_input_ids should have size()>0)...
			sink_id = { shop_reservoir::t_id, junction_input_ids.front() };
		} else {
			if (creek_intake_ids.size() > 1) { // Since we are in the n_upstreams>=2 case this should be true (unless there are nullptr entries messing with our n_inputs count?)
				throw runtime_error("Not supported: Junction between creeks!");
			} else if (creek_intake_ids.size() == 1) {
				throw runtime_error("Unexpected: What seemed to be a junction reduced to only a creek!");
			} else {
				throw runtime_error("Unexpected: What seemed to be a junction reduced to nothing!");
			}
		}
		// Connect any creek intakes
		if (sink_id) {
			if (creek_intake_ids.size() > 0) {
				source_id = sink_id;
				for (const auto& id : creek_intake_ids) {
					api._connect_objects(sink_id.id, ::shop::connection::main, id);
					sink_id = { shop_creek_intake::t_id, id };
				}
			}
		}
		return {sink_id,source_id};
	}
}
#else
std::pair<shop_object_id,shop_object_id> shop_emitter::handle_plant_input(const waterway& main) {
	// Handle main input tunnel to (upstream from) a plant, including any creek intakes and junctions.
	// Executes recursivively, in case of multiple water route segments on a single stream, multiple
	// branches with creek intakes and/or multiple reservoirs (junctions), etc.
	// Returning a pair of shop ids:
	// - The first value is the Shop object that the tunnel ended in. This is the main result,
	//   and at the end of the recursion this is the shop id to connect as input to the plant.
	// - The second value is used only for the creek input handling during recursion. This is the
	//   Shop object that the tunnel started with, when it contained a creek intake along the way.
	//   With Shop objects the reservoir object will be (from upstream) connected into the creek
	//   (as if it were downstream on the same tunnel). During recursion the handling of such a
	//   branch will end with the creek object, returned as the first (main) result, and then
	//   to we identify the reservoir as the source of the branch in the second result value so that
	//   we can detect that this is not a simple creek insert but actually an entire branch.
	//   Think of .first as the source of the branch and .second as the sink.
	const size_t n_upstreams = main.upstreams.size();
	if (n_upstreams < 1) { // Nothing upstream: Invalid!
		throw runtime_error(string("No upstreams from water route ") + main.name + " [" + std::to_string(main.id) + "]");
	} else if (n_upstreams < 2) { // Single upstream: Must be a reservoir (regular or creek).
		if (auto input = dynamic_pointer_cast<reservoir>(main.upstreams.front().target_())) {
			// Reached the end of this stream (and recursion): Upstream tunnel segment ended in reservoir.
			// Assuming reservoirs have already been handled, we just return it's shop id.
			// Note that the reservoir might be represented as a creek_intake in Shop, instead of a regular reservoir!
			if (auto id = objects.id_of(*input))
				return { id, {} };
			throw runtime_error("Reached an unknown input reservoir");
		} else if (auto input = dynamic_pointer_cast<waterway>(main.upstreams.front().target_())) {
			return handle_plant_input(*input); // Additional water route segments are just skipped.
		} else {
			throw runtime_error("Not implemented: Input tunnel from anything other than reservoir!"); // Only reservoirs (and creeks) are allowed upstrema from plant!?
		}
	} else { // Multiple upstreams: Junction in STM - but not necessarily in Shop!
		vector<waterway_> branches; branches.reserve(n_upstreams);
		vector<shop_object_id> junction_input_ids; junction_input_ids.reserve(n_upstreams);
		bool gate_controlled{ false };
		apoint_ts junction_schedule{ shyft::time_axis::point_dt{ { adapter.period.start, adapter.period.end } }, 0.0 };
		int branch_number = 0;
		for (const auto& up : main.upstreams) {
			if (const auto& branch_wtr = dynamic_pointer_cast<waterway>(up.target_())) {
				auto[branch_sink_id, branch_source_id] = handle_plant_input(*branch_wtr);
				if (branches.size() && branch_sink_id.t_id == shop_reservoir::t_id) {
					// Temporary (?) workaround for limitation in Shop API (as of version 0.3.0) that reservoirs must be connected before creeks in junctions.
					size_t i = junction_input_ids.size();
					while (i > 0 && junction_input_ids[i - 1].t_id != shop_reservoir::t_id)
						--i;
					junction_input_ids.insert(junction_input_ids.begin() + i, branch_sink_id);
					branches.insert(branches.begin() + i, branch_wtr);
				} else {
					branches.push_back(branch_wtr);
					junction_input_ids.push_back(branch_sink_id);
				}
				++branch_number;
				if (branch_wtr->gates.size()) {
					apoint_ts input_schedule{ shyft::time_axis::point_dt{ { adapter.period.start, adapter.period.end } }, 0.0 };
					for (const auto& g : branch_wtr->gates) {
						if (const auto& gt = dynamic_pointer_cast<gate>(g)) {
							if (adapter.exists(gt->discharge.schedule)) {
								input_schedule = input_schedule + gt->discharge.schedule;
								gate_controlled = true;
							}
						}
					}
					// Calculate combined junction gate schedule, selecting which single branch to run from, based on a gate schedule for each of the input branches.
					junction_schedule = junction_schedule + input_schedule.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, (double)branch_number, 0.0);
				}
			}
		}
		shop_object_id sink_id, source_id;
		if (branches.size() > 1) {
			if (gate_controlled) {
				auto& shop_junction_object = objects.add(main, adapter.to_shop<shop_junction_gate>(main, branches));
				// The combined junction gate schedule contains values between 1 and n (2) indicating which single branch
				// has open gate(s). Shop treats value 0 or >n as all are closed, and with licensed (password protected)
				// functionality "Junction gate as junction" (SHOP_JUNCG_TWO_WAY) a special value -2 will make the junction
				// gate be treated as an ordinary junction.
				auto junction_schedule_two_way = make_shared<const qac_ts>(junction_schedule, qac_parameter{ utctimespan::zero(), 0.0, branch_number+0.5, utctimespan::zero(), 0.0, -2.0 });
				adapter.set(shop_junction_object.schedule, apoint_ts{junction_schedule_two_way});
				sink_id = shop_junction_object;
			} else {
				auto& shop_junction_object = objects.add(main, adapter.to_shop<shop_junction>(main, branches));
				sink_id = shop_junction_object;
			}
			// Connect branches to junction
			for (const auto& j : junction_input_ids) {
				api._connect_objects(j.id, ::shop::connection::main, sink_id.id);
			}
		} else if (branches.size() == 1) {
			// Only one real branch left, since we are in the n_upstreams>=2 case this means there are nullptr entries messing with our n_upstreams count?
			sink_id = junction_input_ids.front();
		} else {
			// No real branch left, since we are in the n_upstreams>=2 case this means there are nullptr entries messing with our n_upstreams count?
			throw runtime_error("Unexpected: What seemed to be a junction reduced to nothing!");
		}
		return {sink_id,source_id};
	}
}
#endif

void shop_emitter::handle_reservoir_output(const reservoir& rsv) {
	// Handle output connections from a reservoir, but only those ending
	// in other reservoirs. Connections from reservoirs to aggregates are handled
	// by the upstream tunnel handling for power plants.
	// NOTE: Since we currently allow bypass and main waterroutes out from reservoir
	// which are not connected to anything downstream, implicitely to the sea,
	// we must process the connections _out_ and not _in_ from the reservoirs!
	for (const auto& out : rsv.downstreams) {
		switch (out.role) {
		case hydro_power::main:
		case hydro_power::bypass:
		case hydro_power::flood:
			if (const auto& wtr = dynamic_pointer_cast<waterway>(out.target_())) {
				// Find downstream object (if any), and then:
				// - If a downstream object was found, and it is a reservoir, create a gate
				//   object and connect the two reservoirs via it. The stm model may or may
				//   not have actual gates, it could be waterway with delta-meter function,
				//   but in any case it must be modelled as gate in shop.
				// - If the downstream object was anything else, skip - aggregates will be
				//   handled elsewhere by the upstream tunnel handling for power plants.
				// - If there was no downstream object then add a gate connected upstream only,
				//   effectively just letting it drain out into the sea. This is often used for
				//   bypass and flood, but could also be main waterway out from a reservoir
				//   at the end of the topology (ref: Trollheim).
				const auto& down = downstream(wtr, true);
				int rsv2_id = -1;
				if (const auto& rsv2 = dynamic_pointer_cast<reservoir>(down)) {
					rsv2_id = objects.id_of<shop_reservoir>(*rsv2).id;
				}
				if (rsv2_id >= 0 || !down) {
					// This qualifies for a gate object in Shop (from one reservoir into another, or from reservoir into nothing/sea).
					// If there are no actual gates in stm, send the waterroute as a gate,
					// since Shop has no notion of water routes but represent all as gates.
					// Note: We only use the first (upstream) stm waterroute (with its gates) to create the Shop gate,
					//       if there are multiple water route segments then we ignore any gate on following segments.
					// TODO: We send all gates on the water route (parallel gates) to Shop, but it is not
					//       currently supporting more than one gate on the flood connection!
					auto rsv1_id = objects.id_of(rsv).id; // could be shop_reservoir or shop_creek_intake
					if (!rsv1_id)
						throw runtime_error(string("Failed to create gate: Unable to find shop object for stm reservoir ") + rsv.name + " [" + std::to_string(rsv.id) + "]");
					auto git = wtr->gates.cbegin();
					auto gt = git != wtr->gates.cend() ? std::dynamic_pointer_cast<gate>(*git) : nullptr;
					auto sg_role = out.role == hydro_power::bypass ? ::shop::connection::bypass : out.role == hydro_power::flood ? ::shop::connection::flood : ::shop::connection::main;
					do {
						auto& sg = objects.add(gt?stm_object_id{*gt}:stm_object_id{*wtr}, adapter.to_shop_gate(*wtr, gt.get()));
						// Connect required upstream reservoir/creek to the gate
						api.connect_reservoir_gate(rsv1_id, sg_role, sg.id);
						// Connect optional dowstream reservoir to the gate
						if (rsv2_id >= 0)
							api.connect_reservoir_gate(sg.id, ::shop::connection::main, rsv2_id);
						if (gt)
							gt = ++git != wtr->gates.cend() ? std::dynamic_pointer_cast<gate>(*git) : nullptr;
					} while (gt);
				}
			}
			break;
		case hydro_power::input:
			break;
		}
	}
}
void shop_emitter::to_shop(const hydro_power::reservoir& rsv) {
	auto const& rs = dynamic_cast<const reservoir &>(rsv);
    if (adapter.exists(rs.volume_level_mapping)) { // If storage_description then assume regular reservoir, else creek.
        auto& sr = objects.add(rs, adapter.to_shop(rs));
        // Custom adaption of start reservoir from historical time series
        if (adapter.exists(rs.level.realised)) {
            auto ts = (apoint_ts)rs.level.realised;
            if (!!ts) {
                const utctime t_startres = adapter.period.start; // OLD: adapter.period.start - utctimespan(1); // OLDER: t_begin-1*t_step;
                adapter.set(sr.start_head, ts(t_startres));
            } // else: attribute exists but empty ts
        }
        // Custom adaption of endpoint description, from time series in STM to XY in Shop (where x value is not used)
        if (adapter.exists(rs.endpoint_desc)) {
            auto ts = (apoint_ts)rs.endpoint_desc;
            if (!!ts) {
                //hydro_power::xy_point_curve_with_z xy{ {{{0.0,ts(t_end-1*t_step)}}}, 0.0 };
                xy_point_curve_with_z xy{ xy_point_curve{{{0.0,ts(adapter.period.end-utctimespan(1))}}}, 0.0 };
                adapter.set(sr.endpoint_desc_nok_mwh, xy);
            } // else: attribute exists but empty ts
        }
    } else {
        (void) objects.add(rs, adapter.to_shop_creek(rs));
    }
}

void shop_emitter::to_shop(const hydro_power::power_plant& plant) {
	// Migrate power plant, with surrounding topology (aggregates and tunnels),
	// and connect it to upstream reservoirs, possibly via junctions and/or creek intakes, as well
	// as downstream reservoir (if any).
	// Current assumptions:
	// - Only a single main tunnel from intake reservoirs (not supporting parallel intakes).
	// - Each aggregate have a dedicated waterroute upstream, which is either its penstock or an inlet
	//   tunnel leading from a penstock shared with other aggregates.
	// - There can only be one waterroute segment of any inlet and penstock.
	// - Penstock loss is the headloss coefficient on individual penstocks, where aggregates can have
	//   separate or shared penstocks which must be indicated by their penstock attribute.
	// - Tailrace loss is the headloss coefficient on the shared (accross aggregates) tailrace.
	//   Assuming always one output tunnel from each aggreagate, which is always a draft tube,
	//   and then always one output tunnel from the draft tube, which is always the tailrace.
	// - Plant main loss is the headloss in the shared (accross all penstocks and aggregates)
	//   main intake tunnel, between the reservoir and the penstocks, and can be described in two ways:
	//     - Head loss coefficients: An array of double values representing loss factors. Normally there
	//       is only one coefficient, but there may be multiple values if there is a need to differentate
	//       segments of the main tunnel. Currently we only consider loss from a single segment, and then
	//       pick the most downstream main tunnel segment (immediately upstream from penstocks).
	//     - Loss function: An array of XYZ descriptions, one for each head. When this is used, one
	//       normally does not specify head loss coefficients, nor penstock loss coefficients, as the loss
	//       function describes the complete input loss in detail. We only look for loss function on the
	//       most downstream main tunnel segment (immediately upstream from penstocks).
	auto const& pl = dynamic_cast<const power_plant&>(plant);
    // Create basic Shop representation of the power station
    auto sp = adapter.to_shop(pl);
    // Handle aggreagates, with immediately surrounding topology (penstocks and tailrace)
    vector<waterway_> penstocks;
    waterway_ tailrace;
    for (const auto& g : pl.units) {
        if (const auto& ag = dynamic_pointer_cast<unit>(g)) {
            auto& sg = objects.add(*ag, adapter.to_shop(*ag));
            api.connect_plant_generator(sp.id, sg.id);
            if (const auto& penstock = get_penstock(*ag)) { // Find penstock of the aggregate
                int penstock_number;
                if (auto it = std::find_if(std::cbegin(penstocks), std::cend(penstocks), [&penstock](const auto& v) { return penstock->id == v->id; }); it != std::cend(penstocks)) {
                    penstock_number = (int) std::distance(std::cbegin(penstocks), it) + 1;
                } else {
                    penstocks.push_back(penstock);
                    penstock_number = penstocks.size();
                }
                adapter.set(sg.penstock, penstock_number);
            } else {
                throw runtime_error(string("Unable to find penstock for plant ") + pl.name + " [" + std::to_string(ag->id) + "] aggregate " + ag->name + " [" + std::to_string(ag->id) + "]");
            }
            if (const auto& tr = get_tailrace(*ag)) { // Find tailrace of the aggregate
                if (!tailrace) {
                    tailrace = tr;
                } else if (tr->id != tailrace->id) {
                    throw runtime_error(string("More than one tailrace for plant ") + pl.name);
                }
            } else {
                // Accept missing tailrace for one aggregate? If another aggregate do have a tailrace, then this will be used to represent entire plant!
                //throw runtime_error(string("Unable to find tailrace for plant ") + ps->name + " [" + std::to_string(ag->id) + "] aggregate " + ag->name + " [" + std::to_string(ag->id) + "]");
            }
        }
    }
    // Set penstock and tailrace head loss attributes.
    vector<double> penstock_loss;
    std::transform(std::cbegin(penstocks), std::cend(penstocks), std::back_inserter(penstock_loss), [this](const auto& penstock) {return get_tunnel_loss_coeff(*penstock);});
    adapter.set(sp.penstock_loss, penstock_loss);
    if (tailrace && adapter.exists(tailrace->head_loss_func)) { // Accept missing tailrace for plant?
        adapter.set(sp.tailrace_loss, tailrace->head_loss_func);
    }
    // Continue with topology upstream from penstocks: Connect plant and upstream objects, and set main tunnel head loss attributes.
    vector<double> main_loss; // Array with loss in each segment of the main tunnel.
    if (auto main = penstocks.front()->upstreams.size() ? dynamic_pointer_cast<waterway>(penstocks.front()->upstreams.front().target_()) : nullptr) { // Assuming only one input to the tunnel (no junction), which is either a shared penstock or the main tunnel.
        auto[input_sink_id, input_source_id] = handle_plant_input(*main);
        main_loss.push_back(get_tunnel_loss_coeff(*main)); // Segmented head loss coefficients
        if (adapter.exists(main->head_loss_func)) // Head loss function (XYZ array)
            adapter.set(sp.intake_loss, main->head_loss_func);
        api._connect_objects(input_sink_id.id, ::shop::connection::main, sp.id);
    }
    adapter.set(sp.main_loss, main_loss);
    // Continue with topology downstream from trailrace: Connect plant output to downstream reservoir - if any.
    if (const auto& ag = pl.units.size() ? dynamic_pointer_cast<unit>(pl.units.front()) : nullptr) {
        if (const auto& rsv = downstream_of_type<reservoir>(ag, true)) {
            if (auto rsv_id = objects.id_of<shop_reservoir>(*rsv)) {
                api._connect_objects(sp.id, ::shop::connection::main, rsv_id.id);
            }
        }
    }
}

void shop_emitter::to_shop(const stm_hps& hps) {
	to_shop(hps.reservoirs);
	// Go through all reservoirs again to handle any gate connections between them
	for (const auto& r : hps.reservoirs) {
		if (const auto & rsv = dynamic_pointer_cast<reservoir>(r)) {
			handle_reservoir_output(*rsv);
		}
	}
	to_shop(hps.power_plants);
}

void shop_emitter::to_shop(const stm_system& stm) {
	to_shop(stm.market);
	to_shop(stm.hps);
}

void shop_emitter::from_shop(stm_system& stm) {
	for (const auto& m : stm.market) {
		if (const auto& mkt = dynamic_pointer_cast<energy_market_area>(m)) {
			if (const auto it = objects.find<shop_market>(*mkt); it != objects.end<shop_market>()) {
				adapter.from_shop(*mkt, it->second);
			}
		}
	}
	for (const auto& hps : stm.hps) {
		for (const auto& r : hps->reservoirs) {
			if (const auto& rsv = dynamic_pointer_cast<reservoir>(r)) {
				if (const auto it = objects.find<shop_reservoir>(*rsv); it != objects.end<shop_reservoir>()) {
					adapter.from_shop(*rsv, it->second);
				}
				if (const auto it = objects.find<shop_creek_intake>(*rsv); it != objects.end<shop_creek_intake>()) {
					adapter.from_shop(*rsv, it->second);
				}
			}
		}
		for (const auto& a : hps->units) {
			if (const auto& agg = dynamic_pointer_cast<unit>(a)) {
				if (const auto it = objects.find<shop_aggregate>(*agg); it != objects.end<shop_aggregate>()) {
					adapter.from_shop(*agg, it->second);
				}
			}
		}
		for (const auto& w : hps->waterways) {
			if (const auto& wtr = dynamic_pointer_cast<waterway>(w)) {
				if (const auto it = objects.find<shop_gate>(*wtr); it != objects.end<shop_gate>()) {
					adapter.from_shop(*wtr, it->second);
				}
			}
		}
	}
}

}
