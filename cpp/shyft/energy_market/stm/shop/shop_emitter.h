/*
 * Integration of stm with shop api via proxy.
 */
#pragma once
#include <vector>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_data.h>

namespace shyft::energy_market::stm::shop {

using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using shyft::core::to_seconds64;

//
// Utility class for uniqely identifying a Shop object (any of the shop::proxy::obj specializations).
// All object ids are only unique within same object type, so we must include the object type id as well.
//
struct shop_object_id {
	using t_id_type = int;//decltype(shop_reservoir::t_id);
	using id_type = int;// decltype(shop_reservoir::id);
	t_id_type t_id; // shop object type id
	id_type id; // shop object index, as reported when object is created, unique within same object type
	shop_object_id() : t_id{ -1 }, id{ -1 } {}
	shop_object_id(t_id_type t_id, id_type id) : t_id{ t_id }, id{ id } {}
	template<int T> shop_object_id(const shop_object<T>& o) : t_id{ o.t_id }, id{ o.id } {}
	explicit operator bool() const { return t_id >= 0 && id >= 0; }
	bool operator <(const shop_object_id &o) const { return t_id < o.t_id || (t_id == o.t_id && id < o.id); }
};

//
// Utility class for uniqely identifying a STM object.
// All hydro_component ids are only unique within same hydro_power_system, so we must include the id of the hydro_power_system owner.
//
struct stm_object_id {
	using hps_id_type = decltype(hydro_power::hydro_power_system::id);
	using id_type = decltype(id_base::id);
	hps_id_type hps_id{-1}; // id of a hydro_power_system
	id_type id{-1}; // id of a hydro_component, unique within same hydro_power_system
	//-- constructors, pay attention to priority resolution order here, best match (simplest = best)
	stm_object_id() =default;
	stm_object_id(hps_id_type hps_id, id_type id) : hps_id{ hps_id }, id{ id } {}
#if 0  // does not compile RHEL toolkit-7  that we have : linux, gcc 7.3, lacks if constexpr feature
	template<class T> stm_object_id(const std::shared_ptr<T>& o) {
		if constexpr (std::is_base_of<hydro_power::hydro_component, T>::value) {
			if (auto h = o->hps_()) { //if (const auto& c = std::dynamic_pointer_cast<hydro_power::hydro_component>(o); auto h = c->hps_()) {
				hps_id = h->id;
				id = o->id;
			} else {
				hps_id = id = -1; // Considering a "free" hydro_component (no hydro_power_system owner), as invalid.
			}
		} else if constexpr (std::is_base_of<id_base, T>::value) {
			hps_id = -1; // Setting hps_id to -1 for components which are not a hydro_component, but derived from id_base as a hydro_component is, e.g. energy_market_area.
			id = o->id;
		} else static_assert(false, "Type must be at least id_base");
	}
#else
	//precise on how to extract a useful stm_object_id from a given shared_ptr<O> 
	explicit stm_object_id(waterway const&o) {if(o.hps_()){id=o.id;hps_id= o.hps_()->id;}}
	explicit stm_object_id(reservoir const&o) {if(o.hps_()){id=o.id;hps_id= o.hps_()->id;}}
	explicit stm_object_id(unit const&o) {if(o.hps_()){id=o.id;hps_id= o.hps_()->id;}}
	explicit stm_object_id(power_plant const&o) {if(o.hps_()){id=o.id;hps_id= o.hps_()->id;}}
	explicit stm_object_id(gate const&o) {if(o.wtr_() && o.wtr_()->hps_()){id=o.id;hps_id=o.wtr_()->hps_()->id;}}
	
	
	// remaining objects, are not attached to a hps.
	explicit stm_object_id(id_base const&o) {id=o.id;hps_id=-1;}
	//explicit stm_object_id(shared_ptr<power_station> const&o) {id=o->id;hps_id= o->hps_()?o->hps_()->id:-1;}
	//explicit stm_object_id(shared_ptr<power_station> const&o) {id=o->id;hps_id= o->hps_()?o->hps_()->id:-1;}

#endif
	explicit operator bool() const { return id >= 0; } // Note that hps_id will be -1 for components that are not hydro_component, such as energy_market_area
	bool operator <(const stm_object_id &o) const { return hps_id < o.hps_id || (hps_id == o.hps_id && id < o.id); }
};

//
// Utility class for keeping Shop objects and their connection to STM objects.
// Filled when converting an STM model before running Shop, and then used for putting results back from Shop objects into STM.
//
struct shop_objects {
	// Main storage structure: Tuple with one map for each shop object type.
	template<class T> using map_type = std::map<stm_object_id, T>; // Mapping from stm id to shop object
	//using key_type = stm_object_id; //template<class T> using key_type = map_type<T>::key_type;
	std::tuple<
		map_type<shop_market>,
		map_type<shop_reservoir>,
		map_type<shop_creek_intake>,
		map_type<shop_aggregate>,
		map_type<shop_gate>,
		map_type<shop_junction>,
		map_type<shop_junction_gate>
	> maps;
	// Basic access functions, similar to std::map.
	// Since users of this object probably never reference it as a const, there are explicit
	// const variants of everything, similar to what std has for cbegin and cend (only).
	template<class T> using iterator = typename map_type<T>::iterator;
	template<class T> using const_iterator = typename map_type<T>::const_iterator;
	template<class T> map_type<T>& get() { return std::get<map_type<T>>(maps); }
	template<class T> const map_type<T>& cget() const { return std::get<map_type<T>>(maps); }
	template<class T> typename map_type<T>::size_type size() const { return cget<T>().size(); }
	template<class T> iterator<T> begin() { return get<T>().begin(); }
	template<class T> const_iterator<T> cbegin() const { return cget<T>().cbegin(); }
	template<class T> iterator<T> end() { return get<T>().end(); }
	template<class T> const_iterator<T> cend() const { return cget<T>().cend(); }
	template<class T> iterator<T> find(stm_object_id id) { return get<T>().find(id); }
	template<class T> const_iterator<T> cfind(stm_object_id id) const { return cget<T>().find(id); }
	template<class T_shop, class T_stm> iterator<T_shop> find(const T_stm& o) { return find<T_shop>(stm_object_id{o}); }
	template<class T_shop, class T_stm> const_iterator<T_shop> cfind(const T_stm& o) const { return cfind<T_shop>(stm_object_id{o}); }
	// Getter and setter for shop object of a given type corresponding to a given stm object.
	template<class T_shop> T_shop& get(stm_object_id id) {
		if (const auto it = find<T_shop>(id); it != end<T_shop>())
			return *it;
		throw std::out_of_range("shop object of specified type not found for given stm object id");
	}
	template<class T_shop> const T_shop& cget(stm_object_id id) const {
		if (const auto it = cfind<T_shop>(id); it != cend<T_shop>())
			return *it;
		throw std::out_of_range("shop object of specified type not found for given stm object id");
	}
	template<class T> T& operator()(stm_object_id id) { return get<T>(id); }
	template<class T> const T& operator()(stm_object_id id) const { return cget<T>(id); }
	template<class T> T& add(stm_object_id id, T&& o_shop) {
		auto p = get<T>().emplace(id, std::forward<T>(o_shop));
		if (p.second)
			return p.first->second;
		throw std::out_of_range("a shop object of specified type already exists for given stm object id");
	}
	template<class T> T& set(stm_object_id id, T&& o_shop) {
		return get<T>().insert_or_assign(id, std::forward<T>(o_shop)).first->second;
	}
	template<class T_stm, class T_shop> T_shop& add(const T_stm& o_stm, T_shop&& o_shop) {
		return add(stm_object_id{o_stm}, std::forward<T_shop>(o_shop));
	}
	template<class T_stm, class T_shop> T_shop& set(const T_stm& o_stm, T_shop&& o_shop) {
		return set(stm_object_id{o_stm}, std::forward<T_shop>(o_shop));
	}

	// Find id of shop object of a given type corresponding to a given stm object.
	template<class T_shop, class T_stm> shop_object_id id_of(const T_stm& o) const {
		if (const auto it = cfind<T_shop>(o); it != cend<T_shop>())
			return it->second;
		return {};
	}
	// Find id of shop object corresponding to a given stm object, which in some cases can be one of
	// several shop object types (e.g. an stm reservoir can be a shop_reservoir or shop_creek_intake).
	shop_object_id id_of(const energy_market_area& o) const { return id_of<shop_market>(o); }
	shop_object_id id_of(const reservoir& o) const {
		auto id = id_of<shop_reservoir>(o);
		return id ? id : id_of<shop_creek_intake>(o);
	}
	shop_object_id id_of(const unit& o) const { return id_of<shop_aggregate>(o); }
	shop_object_id id_of(const waterway& o) const {
		auto id = id_of<shop_gate>(o);
		if (!id) id = id_of<shop_junction>(o);
		if (!id) id = id_of<shop_junction_gate>(o);
		return id;
	}
	shop_object_id id_of(const gate& o) const {
		auto id = id_of<shop_gate>(o);
        return id;
	}
#if 0
	// Find id of shop object of a given type corresponding to a given stm object.
	// Convenience methods for calling id_of with template argument.
	shop_object_id id_of_reservoir(const reservoir_& o) const { return id_of<shop_reservoir>(o); }
	shop_object_id id_of_creek(const reservoir_& o) const { return id_of<shop_creek_intake>(o); }
	shop_object_id id_of_aggregate(const unit_& o) const { return id_of<shop_aggregate>(o); }
	shop_object_id id_of_gate(const waterway_& o) const { return id_of<shop_gate>(o); }
	shop_object_id id_of_junction(const waterway_& o) const { return id_of<shop_junction>(o); }
	shop_object_id id_of_junction_gate(const waterway_& o) const { return id_of<shop_junction_gate>(o); }
#endif
};

struct shop_emitter {

	shop_api& api;
	shop_adapter& adapter;
	utcperiod period;

	shop_objects objects; // storage for all shop objects and their connection to STM objects

	shop_emitter(shop_api& api, shop_adapter& adapter, const utcperiod& period) : api{api}, adapter{adapter}, period{period} {
		if (!period.valid())
			throw runtime_error("period is not valid");
	}

    shop_emitter(shop_emitter&& o) = default;

    shop_emitter& operator=(shop_emitter&& o) {
        api = std::move(o.api);
        adapter = std::move(o.adapter);
        period = std::move(o.period);
        objects = std::move(o.objects);
        return *this;
    }

	// Top level to/from shop
	void to_shop(const stm_system& stm);
	void from_shop(stm_system& stm);

	// Lower level to/from shop called from top level
	//template<class T> void to_shop(const vector<T>& v) { for (const auto& i : v) to_shop(i); }
	template<class T> void to_shop(const vector<shared_ptr<T>>& v) { for (const auto& i : v) to_shop(*i); }
	template<class T> void to_shop(const T& v) { objects.add(v, adapter.to_shop(v)); }
	void to_shop(const stm_hps& hps);
	void to_shop(const hydro_power::reservoir& reservoir);
	void to_shop(const hydro_power::power_plant& plant);

	// Utility functions
	void handle_reservoir_output(const reservoir& rsv);
	std::pair<shop_object_id,shop_object_id> handle_plant_input(const waterway& main);
	double get_tunnel_loss_coeff(const waterway& wtr) const;
	static waterway_ get_penstock(const unit& agg, bool always_inlet = false);
	static waterway_ get_tailrace(const unit& agg);
};

}
