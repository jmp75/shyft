#include <boost/format.hpp>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/em_utils.h>

namespace shyft::energy_market::stm {
    using std::make_shared;
    using std::any_of;
    using std::begin;
    using std::end;
    using boost::format;

    stm_system::stm_system(){
        // as empty as possible.. serialization etc.
        mk_url_fx(this);
        run_params.mdl=this;
    }

    stm_system::stm_system(int id,string name,string json):id_base{id,name,json,{}} {
        mk_url_fx(this);
        run_params.mdl=this;
    }
    
    shared_ptr<stm_system> stm_system::clone_stm_system(const shared_ptr<stm_system>& s) {
        auto blob = to_blob(s);
        return from_blob(blob);
    }

    static void _generate_url(std::string const& prefix, std::string const& id_expr,int id,std::back_insert_iterator<string>& rbi, int /*levels*/, int template_levels) {
        if (!template_levels) {
            const auto a = prefix + id_expr;
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            const auto idstr=prefix+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }    
    }
    void stm_system::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const  {
        _generate_url("/M","${mdl_id}",id,rbi,levels,template_levels);
    }
    stm_hps::stm_hps() {
        // as empty as possible.. serialization etc.
    }

    stm_hps::stm_hps(int id, const string&name) :super(id, name) {
    }

    void stm_hps::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        _generate_url("/HPS","${hps_id}",id,rbi,levels,template_levels);
    }


    bool stm_hps::operator==(const stm_hps& other) const {

        return id == other.id && name == other.name  // hydro_power_system::operator== is missing
            && equal_vector_ptr_content<reservoir>(reservoirs, other.reservoirs)
            && equal_vector_ptr_content<unit>(units, other.units)
            && equal_vector_ptr_content<waterway>(waterways, other.waterways)
            && equal_vector_ptr_content<power_plant>(power_plants, other.power_plants);
    }


    template <class T,class CT>
    static void ensure_unique_id_and_name(stm_hps_& /*sys*/,const string &tp_name,CT& c,int id, const string& name,const string& /*json*/) {
        if (any_of(begin(c), end(c), [&name](const auto&w)->bool {return w->name == name;}))
            throw stm_rule_exception((format("%2% name must be unique within a HydroPowerSystem,  name' %1%' already exists")% name%tp_name).str());
        if (any_of(begin(c), end(c), [&id](const auto&w)->bool {return w->id == id;}))
            throw stm_rule_exception((format("%2% id must be unique within a HydroPowerSystem, id %1% already exists")% id%tp_name).str());
    }

    template <class T,class CT>
    static shared_ptr<T> add_ensure_unique_id_and_name(stm_hps_&sys,const string &tp_name,CT& c,int id, const string& name,const string& json) {
        ensure_unique_id_and_name<T>(sys,tp_name,c,id,name,json);
        auto o=make_shared<T>(id,name,json,sys);
        c.push_back(o);
        return o;
    }
    
    reservoir_ stm_hps_builder::create_reservoir(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<reservoir>(s,"Reservoir",s->reservoirs,id,name,json);
    }
    unit_ stm_hps_builder::create_unit(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<unit>(s,"Unit",s->units,id,name,json);
    }
    power_plant_ stm_hps_builder::create_power_plant(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<power_plant>(s,"PowerPlant",s->power_plants,id,name,json);
    }
    waterway_ stm_hps_builder::create_waterway(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<waterway>(s,"Waterway",s->waterways,id,name,json);
    }
    catchment_ stm_hps_builder::create_catchment(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<catchment>(s,"Catchment",s->catchments,id,name,json);
    }
    gate_ stm_hps_builder::create_gate(int id,const string&name,const string &json) {
        auto gts=s->gates();
        ensure_unique_id_and_name<gate>(s,"Gate",gts,id,name,json);
        return make_shared<gate>(id,name,json);
    }
}

