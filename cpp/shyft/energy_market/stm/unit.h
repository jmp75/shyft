#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>

#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using std::map;
	using shyft::core::utctime;
	using shyft::time_series::dd::apoint_ts;

    
	struct unit : hydro_power::unit {
		using super = hydro_power::unit;

		/** @brief generate an almost unique, url-like string for a unit.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
         void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;

        unit();
        unit(int id, const string& name, const string&json, const stm_hps_ &hps);

        bool operator==(const unit& other) const;
        bool operator!=(const unit& other) const { return !( *this == other); };
        
        
        struct production_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts,min), ///< W
                    (apoint_ts,max)  ///< W
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            BOOST_HANA_DEFINE_STRUCT(production_, // units are W, watt
                (apoint_ts,schedule),    ///< W
                (apoint_ts,realised),    ///< W  as in historical fact
                (apoint_ts,static_min),  ///< W
                (apoint_ts,static_max),  ///< W
                (constraint_,constraint),
                (apoint_ts,result)       ///< W
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        //-- definition of nested composition groups
        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts,min), ///< m3/s
                    (apoint_ts,max)  ///< m3/s
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts,result), ///< m3/s
                (apoint_ts,schedule), ///< m3/s
                (constraint_,constraint)
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };
        
        struct cost_ {
            BOOST_HANA_DEFINE_STRUCT(cost_,
                (apoint_ts,start),  ///< money/#start
                (apoint_ts,stop)    ///< money/#stop
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };
        
        //-- class descriptive members

        BOOST_HANA_DEFINE_STRUCT(unit,
            (t_xy_, generator_description),
            (t_turbine_description_, turbine_description),
            (apoint_ts, unavailability),   ///< no-unit, 1== unavailable, (0,nan)-> available?
            (production_, production),
            (discharge_, discharge),
            (cost_, cost)
        );

        x_serialize_decl();
    };
    using unit_ = shared_ptr<unit>;
}

x_serialize_export_key(shyft::energy_market::stm::unit);
