#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::shared_ptr;
    using std::dynamic_pointer_cast;
    using std::map;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;

    struct power_plant : hydro_power::power_plant {
        using super = hydro_power::power_plant;

        /** @brief generate an almost unique, url-like string for a power station.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels=-1, int template_levels = -1) const;
        
        power_plant(int id, const string& name, const string&json, const stm_hps_ &hps);
        power_plant();

        static void add_unit(const power_plant_&ps,const unit_&a);

        void remove_unit(const unit_&a);
        
        bool operator==(const power_plant& other) const;
        bool operator!=(const power_plant& other) const { return !( *this == other); };


        struct production_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(production_,
                (apoint_ts, constraint_min),  ///< W
                (apoint_ts, constraint_max),  ///< W
                (apoint_ts, schedule)         ///< W
            );
        };// production{ *this };

        struct discharge_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, constraint_min),
                (apoint_ts, constraint_max),
                (apoint_ts, schedule)
            );
        };

        BOOST_HANA_DEFINE_STRUCT(power_plant,
            (apoint_ts,outlet_level), ///< masl, (input, or result ?)
            (apoint_ts, mip),         ///< bool, opt. with mip
            (apoint_ts, unavailability),///< bool, .. 
            (production_, production),
            (discharge_,discharge)
        );

        x_serialize_decl();
    };
    using power_plant_ = shared_ptr<power_plant>;

}

x_serialize_export_key(shyft::energy_market::stm::power_plant);

