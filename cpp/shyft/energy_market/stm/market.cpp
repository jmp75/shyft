#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

    energy_market_area::energy_market_area(){};
    
    energy_market_area::energy_market_area(int id, const string& name, const string&json, const stm_system_& sys) 
        : super{ id, name,json,{}}, sys{ sys }  {}
        
    void energy_market_area::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = sys_();
            if (tmp) tmp->generate_url(rbi, levels, template_levels ? template_levels - 1 : template_levels);
        }

        if (!template_levels) {
            constexpr std::string_view a = "/M${market_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto a="/M"+std::to_string(id);
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }

}

