#include <shyft/energy_market/stm/waterway.h>


namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;
    
    using std::static_pointer_cast;
    using std::dynamic_pointer_cast;
    using std::make_shared;
    using std::runtime_error;


    gate::gate(int id, const string& name, const string& json) : super{id, name, json} {mk_url_fx(this);}
    gate::gate(){mk_url_fx(this);}

    void waterway::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }

        if (!template_levels) {
            constexpr std::string_view a = "/W${wtr_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/W"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }

    bool waterway::operator==(const waterway& other) const {
        auto equal_attributes= hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<waterway>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        );

        auto equal_gate_attributes = [this, &other] () {
            return (gates.size() == other.gates.size())
                && std::equal(gates.begin(), gates.end(), other.gates.begin(),
                    [] (const auto& a, const auto& b) { return a==b || *dynamic_pointer_cast<gate>(a)==*dynamic_pointer_cast<gate>(b); });
        };

        return equal_attributes && equal_gate_attributes();
    }
    
    void gate::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels ) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/G${gate_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/G"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }

    bool gate::operator==(const gate& other) const {
        return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<gate>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    }
}
