#pragma once
#include <string>
#include <vector>
#include <boost/hana.hpp>
#include <shyft/time_series/time_axis.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::ui {
namespace hana=boost::hana;

using std::string;
using std::vector;
using std::shared_ptr;
using shyft::time_axis::generic_dt;
using shyft::energy_market::id_base;


/** @intro layout for user-interface
 *
 * The base-class, specifies only the name, or identifier for the
 * layout. We guess that grid is the popular one, but more can
 * be added, e.g:
 *
 * @ref https://developer.android.com/guide/topics/ui/declaring-layout.html
 *
 * Overall strategy for the implementation in ui_core:
 *
 * (1) keep it simple
 * (2) it's going to be exposed to python, so we can use
 *     builder-class (the one users get to build/modify structure)
 *     to ensure user can not create something wrong.
 * (3) we need to serialize it, store it, even versioning, so we use boost for this
 * (4) we use inheritance/composition as suited.
 * (5) it's going to be rendered and forwareded to front end
 *     as json, using boost.karma, and the json-emitter framework on the
 *     web-api. So use types, not enums cleverly.
 *
 * Current approach: Utilize Qt and PyQt to build sufficient UI-wireframe with data-bindings
 *   challenges:
 *       (1) serialization : replace with cached instances of Qt key-value store
 *       (2) databinding   : use proxy widget in e.g.Button in GridView to represent time-series
 */

    /** @brief layout_info
     * The class to be stored on server. Still need to figure out exactly what it should contain.
     */
    struct layout_info: id_base {
        using super = id_base;
        layout_info() = default;
        // Copy semantics is deleted because of id_base::em_handle for storing PyObjects.
        layout_info(const layout_info&) = delete;
        layout_info& operator=(const layout_info&) = delete;

        layout_info(layout_info&&) = default;
        layout_info& operator=(layout_info&&) = default;

        layout_info(int id, const string& name, const string& json=""): super{id, name, json,{}} {}

        bool operator==(const layout_info& o) const {
            return super::operator==(o);
        }
        bool operator!=(const layout_info& o) const {
            return !operator==(o);
        }
        x_serialize_decl();
    };
}

x_serialize_export_key(shyft::energy_market::ui::layout_info);

