include_directories(
    ${CMAKE_SOURCE_DIR}/cpp
    ${SHYFT_DEPENDENCIES}/include
)
find_package(Qt5 COMPONENTS Widgets Charts REQUIRED)

set(em_ui "em_ui")
set(sources
    ui_core.cpp
    ui_serialization.cpp
    ../../web_api/ui/request_handler.cpp
)

if (MSVC)
    add_library(${em_ui} STATIC ${sources})
    set_target_properties(${em_ui} PROPERTIES
        DEBUG_POSTFIX "_debug"
        COMPILE_PDB_NAME "${em_ui}"
        COMPILE_PDB_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    )
else()
    add_library(${em_ui} SHARED ${sources})
    set_target_properties(${em_ui} PROPERTIES
        INSTALL_RPATH "$ORIGIN"
        VISIBILITY_INLINES_HIDDEN TRUE)
    install(TARGETS ${em_ui} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/lib)
endif()
target_link_libraries(${em_ui}
    shyft_core
    em_model_core
    stm_core
    Qt5::Widgets
    Qt5::Charts
)
target_compile_definitions(${em_ui} PUBLIC QT_NO_EMIT)

install(TARGETS ${em_ui} DESTINATION ${inst_root}lib)

if (MSVC AND CMAKE_BUILD_TYPE STREQUAL "Debug")
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${em_ui}.pdb DESTINATION ${inst_root}lib)
endif()
