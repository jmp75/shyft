#include "test_pch.h"
#include <memory>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/energy_market/ui/srv/server.h>

#include <shyft/web_api/ui/request_handler.h>
#include <test/test_utils.h>

#include <csignal>
#include <iostream>

using std::make_shared;
using std::vector;
using std::string;
using std::string_view;
using std::to_string;

using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;

namespace test {
    string generate_json_api(const string& layout_name, const string& ) {
        if (layout_name == "test_ok") {
            return R"_({"key1": "value1", "key2": [4,5,6] })_";
        } else if (layout_name == "test_not_ok") {
            return "invalid json string";
        } else
            return "";
    }
}

namespace shyft::energy_market::ui::test {
    using shyft::energy_market::ui::srv::config_server;

    struct test_server: config_server {
        shyft::web_api::ui::request_handler bg_server;
        std::future<int> web_srv;

        explicit test_server(const string& root_dir): config_server(root_dir) {
            bg_server.srv = this;
        }

        void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads) {
            if (!web_srv.valid()) {
                web_srv = std::async(
                    std::launch::async,
                    [this, host_ip, port, doc_root, fg_threads, bg_threads]()->int {
                        return shyft::web_api::run_web_server(
                            bg_server,
                            host_ip,
                            static_cast<unsigned short>(port),
                            make_shared<string>(doc_root),
                            fg_threads,
                            bg_threads);
                    }
                );
            }
        }

        bool web_api_running() const { return web_srv.valid(); }

        void stop_web_api() {
            if(web_srv.valid()) {
                std::raise(SIGINT);
                (void) web_srv.get();
            }
        }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;
    namespace websocket = boost::beast::websocket;
    using boost::system::error_code;

    class session : public std::enable_shared_from_this<session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string text_;
        string response_;
        string fail_;
        std::function<string(string const&)> report_response;

        // Report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }

        #define fail_on_error(ec, diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit session(boost::asio::io_context& ioc)
            : resolver_(ioc)
            , ws_(ioc) {}


        string response() const { return response_; }
        string diagnostics() const { return fail_; }

        // Start the asynchronous operation
        template <class Fx>
        void run(string_view host, int port, string_view text, Fx&& rep_response) {
            // Save these for later
            host_ = host;
            text_ = text;
            port_ = std::to_string(port);
            report_response = rep_response;
            resolver_.async_resolve(host_, port_, // Look up the domain name
                [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                    me->on_resolve(ec, results);
                }
            );
        }

        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                std::bind(&session::on_connect, shared_from_this(), std::placeholders::_1)
            );
        }

        void on_connect(error_code ec) {
            fail_on_error(ec, "connect")
            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->on_handshake(ec);
                }
            );
        }

        void on_handshake(error_code ec) {
            fail_on_error(ec, "handshake")
            if (text_.size()) {
                ws_.async_write(
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec, size_t bytest_transferred) {
                        me->on_write(ec, bytest_transferred);
                    }
                );
            } else {
                ws_.async_close(websocket::close_code::normal,
                    [me=shared_from_this()](error_code ec) {
                        me->on_close(ec);
                    }
                );
            }
        }

        void on_write(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "write")

            ws_.async_read(buffer_, //Read a message into our buffer
                [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                    me->on_read(ec, bytes_transferred);
                }
            );
        }

        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            response_ = boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());
            text_ = report_response(response_);
            if(text_.size()) {
                ws_.async_write(
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_write(ec, bytes_transferred);
                    }
                );
            } else {
                ws_.async_close(websocket::close_code::normal,
                    [me=shared_from_this()](error_code ec) {
                        me->on_close(ec);
                    }
                );
            }
        }

        void on_close(error_code ec) {
            fail_on_error(ec, "close")
        }
        #undef fail_on_error
    };

    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return acceptor.local_endpoint().port();
    }
}

TEST_SUITE("ui_web_api") {
    using namespace shyft::energy_market::ui;
    using namespace shyft::energy_market::ui::test;
    using shyft::energy_market::srv::model_info;
    TEST_CASE("basic_requests") {
        auto dirname = "ui.web_api.test." + to_string(to_seconds64(utctime_now()));
        ::test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();

        // Set up test server
        using namespace shyft::energy_market::ui::test;
        test_server a(tmpdir.string());
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.set_read_cb(::test::generate_json_api);
        a.start_server();
        // Store some models:
        auto li1 = make_shared<layout_info>(1, "li1", R"_({"a": "some info", "b": [1,2,3]})_");
        model_info mi1(li1->id, li1->name, utctime_now(), "");
        a.db.store_model(li1, mi1);

        auto li2 = make_shared<layout_info>(2, "li2", R"_({"c": "layoutson", "d": false})_");
        model_info mi2(li2->id, li2->name, utctime_now(), "");
        a.db.store_model(li2, mi2);

        int port = get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true, a.web_api_running());
        vector<string> requests{
            R"_(get_layouts {"request_id":"1"})_",
            R"_(read_layout {"request_id":"2", "layout_id": 1})_",
            R"_(read_layout {"request_id":"3", "layout_id": 0, "name": "test_not_ok"})_",
            R"_(read_layout {"request_id":"4", "layout_id": 0, "name": "test_ok", "store": true})_",
            R"_(get_layouts {"request_id":"5"})_",
        };

        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],
                [&responses, &r, &requests](string const& web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size() ? string("") : requests[r];
                }
            );
            ioc.run();
            s1.reset();
        }
        //-- take some care: the get list responses could appear in any order, so 1st and last response
        //   checking needs to consider alternatives
        vector<string> expected_1st_alt{
            R"_({"request_id":"1","result":[{"layout_id":2,"name":"li2"},{"layout_id":1,"name":"li1"}]})_",
            R"_({"request_id":"1","result":[{"layout_id":1,"name":"li1"},{"layout_id":2,"name":"li2"}]})_",
        };
        vector<string> expected{
            R"_({"request_id":"2","result":{"layout_id":1,"name":"li1","layout":{"a":"some info","b":[1,2,3]}}})_",
            R"_({"request_id":"3","result":{"layout_id":-1,"name":"test_not_ok","layout":"error in parsing json: 'invalid json string'"}})_",
            R"_({"request_id":"4","result":{"layout_id":3,"name":"test_ok","layout":{"key1":"value1","key2":[4,5,6]}}})_",
        };
        vector<string> expected_layout_alt{
            R"_({"request_id":"5","result":[{"layout_id":3,"name":"test_ok"},{"layout_id":2,"name":"li2"},{"layout_id":1,"name":"li1"}]})_",
            R"_({"request_id":"5","result":[{"layout_id":3,"name":"test_ok"},{"layout_id":1,"name":"li1"},{"layout_id":2,"name":"li2"}]})_",
            R"_({"request_id":"5","result":[{"layout_id":1,"name":"li1"},{"layout_id":2,"name":"li2"},{"layout_id":3,"name":"test_ok"}]})_",
        };
        REQUIRE_EQ(expected.size()+2, responses.size());

        for (size_t i=0; i<expected.size(); i++) {
            CHECK_EQ(responses[i+1], expected[i]);
        }
        
        bool any_ok{false};
        for(auto const&e:expected_1st_alt) {
            any_ok= any_ok || (e==responses.front());
        }
        if(!any_ok) {
            CHECK_EQ(responses.front(), expected_1st_alt[0]);
        }
        any_ok=false;
        for(auto const&e:expected_layout_alt) {
            any_ok= any_ok || (e==responses.back());
        }
        if(!any_ok) {
            CHECK_EQ(responses.back(), expected_layout_alt[0]);
        }
        
            
        a.stop_web_api();
    }
}
