#include <build_test_model.h>
#include <doctest/doctest.h>
#include <iostream>
#include <vector>
#include <limits>
#include <memory>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace shyft::energy_market::hydro_power;
using traits = boost::graph_traits<hydro_power_system>;

namespace test {

    class edge_weight_map: public boost::put_get_helper<int, edge_weight_map> {
    public:
        using category = boost::readable_property_map_tag;
        using value_type = int;
        using reference = int;
        using key_type = boost::graph_traits<hydro_power_system>::edge_descriptor;

        edge_weight_map() = default;

        value_type operator[](const key_type& e) const {
            // We use the connection_role to filter out bypass and flood.
            // If water_route is the source we need to check the role of the connection
            // target -> source
            auto conn = e.second;
            auto source = std::dynamic_pointer_cast<waterway>(e.first);
            auto target = conn.target;
            connection_role role;
            if (source) {
                // We find the corresponding edge target -> source:
                traits::out_edge_iterator ei, ei_end;
                boost::tie(ei, ei_end) = out_edges(target, *(target->hps_()));
                auto it = std::find_if(ei, ei_end,
                    [&source](auto el) { return el.second.target->id == source->id; });
                role = it->second.role;

            } else role = conn.role;
            if (role == connection_role::bypass || role == connection_role::flood) {
                //return std::numeric_limits<int>::max();
                return 1000; // To not get overflow.
            } else
                return 1;
        }
    };

    template<class PredecessorMap>
    class record_predecessor : public boost::dijkstra_visitor<> {
    public:
        record_predecessor(PredecessorMap p): m_predecessor(p) { }

        template<class Edge, class Graph>
            void edge_relaxed(Edge e, Graph& g) {
            put(m_predecessor, target(e, g), source(e,g));
        }
    private:
        PredecessorMap m_predecessor;
    };

    template<class PredecessorMap>
    record_predecessor<PredecessorMap> make_predecessor_recorder(PredecessorMap p) {
        return record_predecessor<PredecessorMap>(p);
    }
}
TEST_SUITE("hps_graph") {
    using std::make_unique;
    
    auto hps = test::build_hps("test_hps");
    auto index_map = boost::get(boost::vertex_index, *hps);
    auto name_map = boost::get(boost::vertex_name, *hps);
    auto weight_map = test::edge_weight_map();

    TEST_CASE("vertex_iterator") {
        auto hps_vi = make_shared<hydro_power_system>("vertex_iterator_tester");
        auto builder = make_unique<hydro_power_system_builder>(hps_vi);
        traits::vertex_iterator vi, vib, vi_end;
        // Check when empty:
        CHECK_THROWS_AS(vertices(*hps_vi), std::runtime_error);
        // Add a reservoir
        auto r1 = builder->create_reservoir(1, "r1", "misc data");
        CHECK_EQ(num_vertices(*hps_vi), 1);
        boost::tie(vi, vi_end) = vertices(*hps_vi);
        CHECK_EQ(*(*vi), *r1);
        
        // Add unit:
        auto u1 = builder->create_unit(1, "u1", "misc data");
        CHECK_EQ(num_vertices(*hps_vi), 2);
        // Remove reservoir to check that vertex_iterator sets correct beginning:
        hps_vi->reservoirs.erase(hps_vi->reservoirs.begin());
        CHECK_EQ(num_vertices(*hps_vi), 1);
        boost::tie(vi, vi_end) = vertices(*hps_vi);
        CHECK_EQ(*(*vi), *u1);
        
        // Add water_route
        auto wtr1 = builder->create_waterway(1, "wtr1", "misc data");
        // Remove unit to check that vertex_iterator sets correct beginning:
        hps_vi->units.erase(hps_vi->units.begin());
        boost::tie(vi, vi_end) = vertices(*hps_vi);
        CHECK_EQ(*(*vi), *wtr1);
        
        // Add back reservoir and unit:
        r1 = builder->create_reservoir(1, "r1", "misc data");
        u1 = builder->create_unit(1, "u1", "misc data");
        boost::tie(vi, vi_end) = vertices(*hps_vi);
        vib = vi;
        CHECK_EQ(vib, vi);
        // Increment vi:
        vi++;
        CHECK_NE(vib, vi);
        CHECK_EQ(*(*vi), *u1);
        vi++;
        CHECK_EQ(*(*vi), *wtr1);
        vi++;
        CHECK_EQ(vi, vi_end);
        // Decrement:
        vi--;
        CHECK_EQ(*(*vi), *wtr1);
        vi--;
        CHECK_EQ(*(*vi), *u1);
        vi--;
        CHECK_EQ(*(*vi), *r1);
        CHECK_EQ(vib, vi);
    }
    
    TEST_CASE("out_edge_iterator"){
        auto hps_ei = make_shared<hydro_power_system>("out_edge_iterator_tester");
        auto builder = make_unique<hydro_power_system_builder>(hps_ei);
        // Add hydro_components:
        auto r1 = builder->create_reservoir(1, "r1", "misc data");
        auto u1 = builder->create_unit(1, "u1", "misc data");
        auto wtr1 = builder->create_waterway(1, "wtr1", "misc data");
        
        traits::out_edge_iterator ei, eib, ei_end;
        // When nothing's connected:
        CHECK_EQ(out_degree(r1, *hps), 0);
        CHECK_EQ(in_degree(r1, *hps), degree(r1, *hps));
        boost::tie(ei, ei_end) = out_edges(r1, *hps_ei);
        CHECK_THROWS_AS(*ei, std::runtime_error);
        
        // Make connections, and test iterators:
        connect(wtr1).input_from(r1).output_to(u1);
        CHECK_EQ(out_degree(r1, *hps), 1);
        CHECK_EQ(out_degree(wtr1, *hps), 2);
        CHECK_EQ(out_degree(u1, *hps), 1);
        // For the reservoir
        boost::tie(ei, ei_end) = out_edges(r1, *hps);
        CHECK_EQ((*ei).second, r1->downstreams[0]);
        CHECK_EQ(*(*ei).first, *r1);
        ++ei;
        CHECK_EQ(ei, ei_end);
        // For the waterway:
        boost::tie(ei, ei_end) = out_edges(wtr1, *hps);
        CHECK_EQ((*ei).second, wtr1->downstreams[0]);
        ++ei;
        CHECK_EQ((*ei).second, wtr1->upstreams[0]);
        ++ei;
        CHECK_EQ(ei ,ei_end);
        --ei;
        CHECK_EQ((*ei).second, wtr1->upstreams[0]);
        --ei;
        CHECK_EQ((*ei).second, wtr1->downstreams[0]);
        
        // For the unit (doesn't have downstreams):
        boost::tie(ei, ei_end) = out_edges(u1, *hps);
        CHECK_EQ((*ei).second, u1->upstreams[0]);
        ++ei;
        CHECK_EQ(ei, ei_end);
        
    }
    
    TEST_CASE("core_functionality") {
        auto num_nodes = hps->reservoirs.size() + hps->units.size() + hps->waterways.size();
        CHECK_EQ(num_vertices(*hps), num_nodes);

        // 1. Check VertexListGraph stuff:
        traits::vertex_iterator vi, vi_end;
        traits::vertex_descriptor v;
        int counter = 0;
        for(boost::tie(vi, vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            v = *vi;
            reservoir_ rsv = std::dynamic_pointer_cast<reservoir>(v);
            if (rsv) REQUIRE(hps->find_reservoir_by_id(rsv->id));
            unit_ u = std::dynamic_pointer_cast<unit>(v);
            if (u) REQUIRE(hps->find_unit_by_id(u->id));
            waterway_ wtr = std::dynamic_pointer_cast<waterway>(v);
            if (wtr) REQUIRE(hps->find_waterway_by_id(wtr->id));

            // Also check that the property maps for vertices are properly set:
            CHECK_EQ(index_map[v], counter);
            CHECK_EQ(name_map[v], v->name);
            ++counter;
        }
         // And in reverse:
         boost::tie(vi, vi_end) = vertices(*hps);

        // Check IncidenceGraph stuff:
        traits::out_edge_iterator ei, ei_end;
        for(boost::tie(vi, vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            v = *vi;
            CHECK_EQ(v->downstreams.size() + v->upstreams.size(), out_degree(v, *hps));
            int oe_index = 0;
            int offset = v->downstreams.size();
            for (boost::tie(ei, ei_end) = out_edges(v, *hps); ei != ei_end; ++ei) {
                traits::edge_descriptor e = *ei;
                CHECK_EQ(*source(e,*hps), *v);
                if (oe_index < offset)
                    CHECK_EQ(*target(e,*hps), *(v->downstreams[oe_index].target));
                else
                    CHECK_EQ(*target(e, *hps), *(v->upstreams[oe_index-offset].target));
                oe_index++;
            }
        }
    }

    TEST_CASE("distance_to_ocean") {
        // Set source node:
        traits::vertex_descriptor src = hps->find_reservoir_by_name("ocean");
        // Set up vector to store distances:
        std::vector<int> d(num_vertices(*hps));
        std::vector<int> expected = {4, 6, 4, 0, // reservoirs
                                     2, 2,       // units
                                     3, 5, 1, 3, 1, 1, 1}; // waterways
        // Distance property map:
        auto distances = boost::make_iterator_property_map(d.begin(),
                                                           index_map);
        // Invoke Dijkstra's algorithm:
        boost::dijkstra_shortest_paths(*hps, src,
            boost::distance_map(distances)
            .weight_map(weight_map));
        // Check all vertex distances:
        traits::vertex_iterator vi, vi_end;
        for (boost::tie(vi ,vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            CHECK_EQ(distances[*vi], expected[index_map[*vi]]);
        }
    }

    TEST_CASE("path_to_ocean") {
        // Set source node:
        traits::vertex_descriptor src = hps->find_reservoir_by_name("ocean");
        // Set vector to store distances:
        std::vector<int> d(num_vertices(*hps));
        auto distances = boost::make_iterator_property_map(d.begin(),
                                                           index_map);
        // Set up predecessor recorder:
        std::vector<traits::vertex_descriptor> p(num_vertices(*hps), nullptr);
        auto predecessors = boost::make_iterator_property_map(p.begin(), index_map);
        // Do Dijkstra's:
        boost::dijkstra_shortest_paths(*hps, src,
            boost::distance_map(distances)
            .weight_map(weight_map)
            .visitor(test::make_predecessor_recorder(predecessors)));

        // We get the path from r2:
        std::vector<int> expected = {1, 7, 0, 6, 4, 8, 3}; // ID are based in index_map
        int counter = 0;
        traits::vertex_descriptor v = hps->find_reservoir_by_name("r2");
        while (v) {
            CHECK_EQ(index_map[v], expected[counter]);
            counter++;
            // Get predecessor:
            v = predecessors[v];
        }
        CHECK_EQ(counter, expected.size());
    }
}
