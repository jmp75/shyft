#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/core/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>

#ifdef _DEBUG
// Set these to true for improved debugging (in case it gets included in commit we should keep default false for release builds)
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#endif

TEST_SUITE("stm_shop_adapter_basics")
{

TEST_CASE("getters")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utcperiod;

	const shyft::core::calendar calendar("Europe/Oslo");
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto make_constant_ts = [&t_begin, &t_end](double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_begin, t_end - t_begin, 1), value, POINT_AVERAGE_VALUE); };

	SUBCASE("basic")
	{
		const double v_dbl = 1.23;
		CHECK(shop_adapter::exists(v_dbl));
		CHECK(shop_adapter::valid(v_dbl));
		CHECK(shop_adapter::get(v_dbl) == v_dbl);
		const int v_int = 123;
		CHECK(shop_adapter::exists(v_int));
		CHECK(shop_adapter::valid(v_int));
		CHECK(shop_adapter::get(v_int) == v_int);
		const utctime v_time = shyft::core::utctime_now();
		CHECK(shop_adapter::exists(v_time));
		CHECK(shop_adapter::valid(v_time));
		CHECK(shop_adapter::get(v_time) == v_time);
		const auto v_ts = make_constant_ts(100.0);
		CHECK(shop_adapter::exists(v_ts));
		CHECK(shop_adapter::valid(v_ts));
		CHECK(shop_adapter::get(v_ts) == v_ts);
	}
	SUBCASE("temporal ts")
	{
		const double ts_value = 100.0;
		const auto ts_empty = apoint_ts();
		CHECK(shop_adapter::exists(ts_empty) == false);
		CHECK(shop_adapter::valid(ts_empty) == false); // Note: Treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::valid_temporal(ts_empty, t_begin) == false); // Temporal value not valid at specified time
		CHECK_THROWS(shop_adapter::get(ts_empty, t_begin)); // Attribute is empty
		const auto ts_const = make_constant_ts(ts_value);
		CHECK(shop_adapter::exists(ts_const) == true);
		CHECK(shop_adapter::valid(ts_const) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::valid_temporal(ts_const, t_begin) == true);
		CHECK(shop_adapter::get(ts_const, t_begin) == ts_value);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(ts_const, t) == true);
		CHECK(shop_adapter::get(ts_const, t) == ts_value);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(ts_const, t) == false);
		CHECK_THROWS(shop_adapter::get(ts_const, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(ts_const, t_end) == false);
		CHECK_THROWS(shop_adapter::get(ts_const, t_end)); // Attribute is not valid at t
	}
	SUBCASE("temporal xy")
	{
		const auto txy = make_shared<map<utctime, xy_point_curve_>>();
		CHECK(shop_adapter::exists(txy) == false);
		CHECK(shop_adapter::valid_temporal(txy, t_begin) == false);
		CHECK_THROWS(shop_adapter::get(txy, t_begin)); // Attribute is empty
		const auto xy = make_shared<hydro_power::xy_point_curve>(
			std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
			std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
		txy.get()->emplace(t_begin, xy);
		CHECK(shop_adapter::exists(txy) == true);
		CHECK(shop_adapter::valid_temporal(txy, t_begin) == true);
		CHECK(shop_adapter::get(txy, t_begin) == xy);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(txy, t) == true);
		CHECK(shop_adapter::get(txy, t) == xy);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(txy, t) == false);
		CHECK_THROWS(shop_adapter::get(txy, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(txy, t_end) == true);
		CHECK(shop_adapter::get(txy, t_end) == xy); // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
	}
	SUBCASE("stm attribute")
	{
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);

		// stm ts attribute
		CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == false);
		CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == false); // Note: Treated as a regular non-temporal ts value in this case
		const double ts_value = 100.0;
		const auto ts_empty = apoint_ts();
		rsv_stm->inflow.schedule = ts_empty;
		CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == false);
		CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == false); // Note: Treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::get(rsv_stm->inflow.schedule) == ts_empty);
		const auto ts_const = make_constant_ts(ts_value);
		rsv_stm->inflow.schedule = ts_const;
		CHECK(shop_adapter::exists(rsv_stm->inflow.schedule) == true);
		CHECK(shop_adapter::valid(rsv_stm->inflow.schedule) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::get(rsv_stm->inflow.schedule) == ts_const);

		// stm temporal ts attribute
		rsv_stm->level.regulation_max = ts_empty;
		CHECK(shop_adapter::get(rsv_stm->level.regulation_max) == ts_empty);
		CHECK(shop_adapter::exists(rsv_stm->level.regulation_max) == false);
		CHECK(shop_adapter::valid(rsv_stm->level.regulation_max) == false); // Note: Treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_begin) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t_begin)); // Attribute is empty
		rsv_stm->level.regulation_max = ts_const;
		CHECK(shop_adapter::exists(rsv_stm->level.regulation_max) == true);
		CHECK(shop_adapter::valid(rsv_stm->level.regulation_max) == true); // Note: Valid, because it is treated as a regular non-temporal ts value in this case
		CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_begin) == true);
		CHECK(shop_adapter::get(rsv_stm->level.regulation_max, t_begin) == ts_value);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t) == true);
		CHECK(shop_adapter::get(rsv_stm->level.regulation_max, t) == ts_value);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(rsv_stm->level.regulation_max, t_end) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->level.regulation_max, t_end)); // Attribute is not valid at t

		// stm temporal xy attribute
		rsv_stm->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
		CHECK_THROWS(shop_adapter::get(rsv_stm->volume_level_mapping, t_begin)); // Attribute is empty
		const auto xy = make_shared<hydro_power::xy_point_curve>(
			std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
			std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
		rsv_stm->volume_level_mapping.get()->emplace(t_begin, xy);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t_begin) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t_begin) == xy);
		t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t) == xy);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->volume_level_mapping, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_level_mapping, t_end) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_level_mapping, t_end) == xy);  // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
	}
}

TEST_CASE("setters")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utcperiod;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;


	const shyft::core::calendar calendar("Europe/Oslo");
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const utcperiod t_period{ t_begin, t_end };
	const auto make_constant_ts = [](utcperiod t_period, double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_period.start, t_period.timespan(), 1), value, POINT_AVERAGE_VALUE); };
	const auto make_ts = [&t_period, &make_constant_ts](double value) { return make_constant_ts(t_period, value); };
	const auto t_step = shyft::core::deltahours(1);

	int id = 1;

	stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
	stm_hps_builder builder(hps);
	reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);
	shop_system api{ t_period, t_step };
	api.set_logging_to_stdstreams(shop_console_output);
	api.set_logging_to_files(shop_log_files);
	//auto rsv_shop = api.api.create<shop_reservoir>(rsv_stm->name);

	SUBCASE("basic")
	{
		const double v_dbl_src = 1.23;
		double v_dbl_dst = -999.0;
		CHECK(api.adapter.valid_to_set(v_dbl_dst, v_dbl_src));
		CHECK(api.adapter.set(v_dbl_dst, v_dbl_src) == v_dbl_dst);
		CHECK(v_dbl_dst == v_dbl_src);
		const int v_int_src = 123;
		int v_int_dst = -999;
		CHECK(api.adapter.valid_to_set(v_int_dst, v_int_src));
		CHECK(api.adapter.set(v_int_dst, v_int_src) == v_int_dst);
		CHECK(v_int_dst == v_int_src);
		const utctime v_time_src = shyft::core::utctime_now();
		utctime v_time_dst = shyft::core::no_utctime;
		CHECK(api.adapter.valid_to_set(v_time_dst, v_time_src));
		CHECK(api.adapter.set(v_time_dst, v_time_src) == v_time_dst);
		CHECK(v_time_dst == v_time_src);
		const double ts_value = 100.0;
		const auto v_ts_src = make_ts(ts_value);
		auto v_ts_dst = apoint_ts();
		CHECK(api.adapter.valid_to_set(v_ts_dst, v_ts_src));
		CHECK(api.adapter.set(v_ts_dst, v_ts_src) == v_ts_dst);
		CHECK(v_ts_dst == v_ts_src);
		v_dbl_dst = -999.0;
		CHECK(api.adapter.valid_to_set(v_dbl_dst, v_ts_src));
		CHECK(api.adapter.set(v_dbl_dst, v_ts_src) == v_dbl_dst);
		CHECK(v_dbl_dst == ts_value);
	}

	SUBCASE("basic stm attribute not exists")
	{
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == false);
		CHECK_THROWS(api.adapter.set(v, rsv_stm->level.regulation_min)); // Throws: Attempt to read not-yet-set attribute for object
		api.adapter.set_if(v, rsv_stm->level.regulation_min); // Does nothing, and not throwing, because attribute does not exist
		CHECK(v == v_init); // Still unchanged
		CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets default because attribute does not exist
		CHECK(v == 1.23);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v); // Does not throw, does nothing, because attribute does not exist
		CHECK(v == v_init); // Still unchanged
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets default value because attribute does not exist
		CHECK(v == 1.23); // New default value set
	}

	SUBCASE("stm attribute exists and valid")
	{
		const double ts_value = 100.0;
		rsv_stm->level.regulation_min = make_ts(ts_value);
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == true);
		CHECK(api.adapter.set(v, rsv_stm->level.regulation_min) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_if(v, rsv_stm->level.regulation_min) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min) == v);
		CHECK(v == ts_value);
		v = v_init;
	}

	SUBCASE("stm attribute exists but not valid")
	{
		const double ts_value = 100.0;
		rsv_stm->level.regulation_min = make_constant_ts({t_end, calendar.add(t_end, calendar.DAY, 1)}, ts_value); // Temporal value that is valid only after the api period
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == false);
		CHECK_THROWS(api.adapter.set(v, rsv_stm->level.regulation_min)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		CHECK_THROWS(api.adapter.set_if(v, rsv_stm->level.regulation_min)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_if(v, rsv_stm->level.regulation_min, 1.23) == v); // Set 1.23 since there is no valid value (not throwing since attribute exists)
		CHECK(v == 1.23);
		v = v_init;
		CHECK_THROWS(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
		CHECK(v == 4.56);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min) == v); // Does nothing since attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23) == v); // Does nothing since attribute is not valid at t (not setting 1.23 since attribute exists)
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
		CHECK(v == 4.56);
		v = v_init;
		CHECK_THROWS(api.adapter.set_required(v, rsv_stm->level.regulation_min));  // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23) == v); // Sets 1.23 since attribute exists but is not valid at t, and no additional default is given
		CHECK(v == 1.23);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23, 4.56) == v); // Sets 4.56 since attribute exists but is not valid at t, not setting 1.23 since this is now only for the not exists case
		CHECK(v == 4.56);
		v = v_init;
	}

	SUBCASE("stm attribute exists valid nan")
	{
		const double ts_value = shyft::nan;
		rsv_stm->level.regulation_min = make_ts(ts_value);
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->level.regulation_min) == true);
		CHECK(api.adapter.set_or(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->level.regulation_min, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
	}

} // TEST_CASE
} // TEST_SUITE

TEST_SUITE("stm_shop_adapter_to_shop")
{
    // Aggregation of tests checking that 'adapter::to_shop' functions creates SHOP objects that
    // has SHOP attributes set as expected.

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_axis::fixed_dt;
    using shyft::time_series::POINT_AVERAGE_VALUE;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;

    using shyft::energy_market::hydro_power::point;
    using xyz_list = std::vector<xy_point_curve_with_z>;
    using t_xyz_list = std::map<utctime, std::shared_ptr<xyz_list>>;


    const auto t_step = shyft::core::deltahours(1);
	const utcperiod t_period{shyft::core::create_from_iso8601_string("2020-01-01T00:00:00Z"),
                             shyft::core::create_from_iso8601_string("2020-01-02T00:00:00Z")};

    auto create_constant_t_double = [] (double val) -> apoint_ts {
        auto dt = t_period.end - t_period.start + shyft::core::deltahours(1);
        auto ta = fixed_dt(t_period.start, dt, 1);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_ts = [] (double val) -> apoint_ts {
        int num_step = (t_period.end - t_period.start) / t_step;
        auto ta = gta_t(t_period.start, t_step, num_step);
        return apoint_ts(ta, val, POINT_AVERAGE_VALUE);
    };

    auto create_constant_t_xyz = [] (double x, double y, double z) -> t_xyz_list_ {
        auto m = make_shared<t_xyz_list_::element_type>();
        xy_point_curve xy {{point{x, y}}};
        auto xyz = std::make_shared<std::vector<xy_point_curve_with_z>>();
        xyz->emplace_back(xy, z);
        m->emplace(t_period.start, xyz);
        return m;
    };

    auto create_shop_system = [] (stm_hps_ hps) -> shared_ptr<shop_system> {
        // wrap the hps in a dummy stm system
        auto mdl = make_shared<stm_system>(0, "dummy_stm_system"s, ""s);
        auto mkt = make_shared<energy_market_area>(0, "dummy_stm_market"s, ""s, mdl);
        mdl->hps.push_back(hps);
        mdl->market.push_back(mkt);

        // creating the shop system calls ShopInit
        auto s = make_shared<shop_system>(t_period, t_step, mdl.get());
        return s;
    };

    TEST_CASE("shop_gate")
    {
        // NOTE: It seems that SHOP api fails does not correcctly evaluate ShopAttributeExists
        // for these attributes:
        //   - schedule_m3s
        //   - schedule_percent
        //   - functions_meter_m3s
        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_basic_attributes")
        {
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            wtr->discharge.static_max = create_constant_t_double(10.0);
            gt->flow_description = create_constant_t_xyz(10.0, 20.0, 30.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.max_discharge.exists());
            CHECK(sg.functions_meter_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_optimise_sets_discharge_schedule")
        {
            // Check that if both 'discharge.schedule' and 'opening.schedule' STM attributes
            // are set, then the 'schedule_m3s' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->discharge.schedule = create_constant_ts(10.0);
            gt->opening.schedule = create_constant_ts(100.0);  // should be ignored by adapter, discharge takes precedence

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_opening_schedule")
        {
            // Check that if only 'opening.schedule' STM attribute is set, then
            // the 'schedule_percent' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->opening.schedule = create_constant_ts(100.0);
            gt->discharge.realised = create_constant_ts(10.0);  // should be ignored by adapter, schedule takes precedence

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_percent.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_optimise_sets_discharge_realised")
        {
            // Check that if both 'discharge.realised' and 'opening.realised' STM attributes
            // are set, then the 'schedule_m3s' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->discharge.realised = create_constant_ts(10.0);
            gt->opening.realised = create_constant_ts(100.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_m3s.get().size());  // Check exist does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_with_gate_sets_opening_realised")
        {
            // Check that if only 'opening.realised' STM attribute is set, then
            // the 'schedule_percent' SHOP attribute is set on the SHOP gate.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(2, "gate name", ""s);
            waterway::add_gate(wtr, gt);

            gt->opening.realised = create_constant_ts(100.0);

            auto shop_sys = create_shop_system(hps);
            shop_gate sg = shop_sys->adapter.to_shop_gate(*wtr, gt.get());

            CHECK(sg.schedule_percent.get().size());  // exists does not work
        }

        SUBCASE("shop_gate_from_stm_waterway_and_gate_not_connected_throws")
        {
            // Check that 'to_shop_gate' throws if the gate does not belong to the waterway.
            auto hps = std::make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto wtr = builder.create_waterway(1, "waterway_name"s, ""s);
            auto gt = builder.create_gate(1, "gate name", ""s);  // not linked to waterway

            auto shop_sys = create_shop_system(hps);
            CHECK_THROWS(shop_sys->adapter.to_shop_gate(*wtr, gt.get()));
        }
    } // TESTCASE

    TEST_CASE("shop_reservoir")
    {
        SUBCASE("shop_reservoir_gets_spill_flow_description_from_gate")
        {
            // Check that spill flow description ('flow_descr' in SHOP)  is set correctly
            // on the SHOP reservoir created by the adapter.
            auto hps = make_shared<stm_hps>(0, "test_hps"s);
            auto builder = stm_hps_builder(hps);
            auto rsv = builder.create_reservoir(0, "reservoir"s, ""s);

            // spillway information is on waterway/gate in stm
            auto spill_wtr = builder.create_waterway(1, "spillway"s, ""s);
            connect(spill_wtr).input_from(rsv, connection_role::flood);
            auto spill_gt = builder.create_gate(2, "spillway gate"s, ""s);
            waterway::add_gate(spill_wtr, spill_gt);
            spill_gt->flow_description = create_constant_t_xyz(10.0, 20.0, 0.0);

            auto shop_sys = create_shop_system(hps);
            shop_reservoir sr = shop_sys->adapter.to_shop(*rsv);

            CHECK(sr.flow_descr.exists());
        }
    } // TEST_CASE

} // TEST_SUITE

