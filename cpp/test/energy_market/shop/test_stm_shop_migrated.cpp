#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/core/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>
#include <fstream>
#include <streambuf>
#include <shyft/core/fs_compat.h>

using std::string;
using std::vector;
using std::ifstream;
using namespace std::string_literals;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using namespace shyft::energy_market::stm::shop;
using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::to_seconds64;
using shyft::time_axis::point_dt;

stm_system_ read_model_from_file(fs::path path)
{
	REQUIRE(fs::is_regular_file(path));
	MESSAGE("Reading file " << path);
	// Read entire file into blob string
	ifstream f(path.c_str(), std::ios::ate | std::ios::binary);
	string blob;
	blob.reserve(f.tellg());
	f.seekg(0, std::ios::beg);
	auto it = std::istreambuf_iterator<char>{ f };
	blob.assign(it, {});
	// Check status
	CHECK(it == std::istreambuf_iterator<char>{});
	CHECK(f.good());
	CHECK(f.tellg() == blob.size());
	// Deserialize blob into an stm system
	return stm_system::from_blob(blob);
}
vector<stm_system_> read_models_from_path(fs::path path)
{
	vector<stm_system_> models;
	if (fs::exists(path)) {
		if (fs::is_directory(path)) {
			MESSAGE("Iterating directory "s << path);
			for (const auto& item : fs::directory_iterator(path)) {
				if (fs::is_regular_file(item)) {
					models.push_back(read_model_from_file(item));
				}
			}
		} else if (fs::is_regular_file(path)) {
			models.push_back(read_model_from_file(path));
		} else {
			FAIL("Path "s << path << " is not a file or directory"s);
		}
	} else {
		FAIL("Path "s << path << " does not exist"s);
	}
	return models;
}

void test_model_file(fs::path path, bool optimize = false)
{
	REQUIRE(fs::is_regular_file(path));
	
	// Perform import
	auto model = read_model_from_file(path);
	CHECK(model);

	// Run tests on the imported model
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const shyft::core::utcperiod t_period{ t_begin, t_end };
	const auto t_step = shyft::core::deltahours(1);
	// Some basic tests
	{
		shop_system api{ t_period, t_step };
		for (const auto& hps : model->hps) {
			for (const auto& r : hps->reservoirs) {
				if (const auto& rsv = std::dynamic_pointer_cast<reservoir>(r)) {
					if (api.emitter.adapter.exists(rsv->volume_level_mapping)) { // If storage_description then assume regular reservoir, else creek.
						api.emitter.objects.add(*rsv, std::move(api.adapter.to_shop(*rsv)));
					} else {
						api.emitter.objects.add(*rsv, std::move(api.adapter.to_shop_creek(*rsv)));
					}
				} else {
					FAIL("Reservoir "s << rsv->name << " is not an stm::reservoir"s);
				}
			}
			for (const auto& r : hps->reservoirs) {
				if (const auto& rsv = std::dynamic_pointer_cast<reservoir>(r)) {
					api.emitter.handle_reservoir_output(*rsv);
				}
			}
		}
		for (const auto& hps : model->hps) {
			for (const auto& ps : hps->power_plants) {
				if (const auto& station = std::dynamic_pointer_cast<power_plant>(ps)) {
					waterway_ penstock;
					for (const auto& a : station->units) {
						if (const auto& ag = std::dynamic_pointer_cast<unit>(a)) {
							penstock = shop_emitter::get_penstock(*ag, false);
							CHECK(penstock);
						} else {
							FAIL("Aggregate "s << ag->name << " is not an stm::unit"s);
						}
					}
					if (penstock->upstreams.size() > 0) {
						if (auto input = dynamic_pointer_cast<waterway>(penstock->upstreams.front().target_())) {
							auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(*input);
							CHECK(input_sink_id);
						} else {
							FAIL("Penstock "s << penstock->name << " is not an stm::waterway"s);
						}
					} else {
						FAIL("Penstock "s << penstock->name << " does not have anything upstream"s);
					}
				} else {
					FAIL("Power station "s << station->name << " is not an stm::power_plant"s);
				}
			}
		}
	}
	// Optionally perform a full optimization
	if (optimize) {
		shop_system::optimize(*model, t_begin, t_end, t_step, {
			shop_command::set_method_primal(),
			shop_command::set_code_full(),
			shop_command::start_sim(3),
			shop_command::set_code_incremental(),
			shop_command::start_sim(3)
		}, true, true);
	}
}

TEST_SUITE("stm_shop_migrated") {


TEST_CASE("all migrated models"
	* doctest::description("reading model from file and running optimization on it")
	* doctest::skip(true))
{
	//
	// Manual test:
	// 1) Perform full migration by executing the following from the binary directory of the test application:
	//      > python internal\stm\migration\models\perform_full_migration.py"
	// 2) Run test executable with arguments to run select this test only and ignore the skip decorator:
	//      > test_energy_market_core.exe --test-suite=stm_shop_migrated --no-skip
	//
	const bool run_optimization = false;
	fs::path path;
	if (const char* env = std::getenv("STM_SYSTEMS")) {
		path = env;
	} else {
		path = R"(C:\Dev\Projects\STM-Python\Migration\stm_systems\stm_system_100004_Barduelva.blob)"; // "stm_systems";
	}
	if (fs::exists(path)) {
		if (fs::is_directory(path)) {
			MESSAGE("Iterating directory "s << path);
			std::size_t counter{ 0 };
			for (const auto& item : fs::directory_iterator(path)) {
				if (fs::is_regular_file(item)) {
					test_model_file(item, run_optimization);
					++counter;
				}
			}
			CHECK(counter > 0);
			MESSAGE("Processed "s << counter << " files"s);
		} else if (fs::is_regular_file(path)) {
			test_model_file(path, run_optimization);
		} else {
			FAIL("Path "s << path << " is not a file or directory"s);
		}
	} else {
		FAIL("Path "s << path << " does not exist"s);
	}
}

}
