#include <doctest/doctest.h>

#include <string>

#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/stm_system.h>

#include <test/energy_market/stm/utilities.h>

using namespace std;
using namespace shyft::energy_market::stm;
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE("stm_unit") {

    TEST_CASE("stm_unit_equality") {
        // Test that stm::plant::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto unit_1 = builder(hps_1).create_unit(14, "test", "");
        auto unit_2 = builder(hps_2).create_unit(14, "test", "");


        // All attributes left at default, objects should be equal
        CHECK_EQ(*unit_1, *unit_2);

        // Add some attributes to first object, no longer equal
        unit_1->production.static_max = test::create_t_double(1, 10.0);
        unit_1->turbine_description = test::create_t_turbine_description(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_NE(*unit_1, *unit_2);

        // Add attributes to other object, now equal again
        unit_2->production.static_max = test::create_t_double(1, 10.0);
        unit_2->turbine_description = test::create_t_turbine_description(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_EQ(*unit_1, *unit_2);
    }

    TEST_CASE("stm_power_plant_equality") {
        // Test that stm::plant::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto plant_1 = builder(hps_1).create_power_plant(11, "test", "");
        auto plant_2 = builder(hps_2).create_power_plant(11, "test", "");

        auto unit_1 = builder(hps_1).create_unit(14, "test", "");
        auto unit_2 = builder(hps_2).create_unit(14, "test", "");

        // All attributes left at default, objects should be equal
        CHECK_EQ(*plant_1, *plant_2);

        // Add unit to one power_plant, no longer equal
        power_plant::add_unit(plant_1, unit_1);
        CHECK_NE(*plant_1, *plant_2);

        // Add unit to the other power_plant, equal again
        power_plant::add_unit(plant_2, unit_2);
        CHECK_EQ(*plant_1, *plant_2);

        // Add some attributes to first object, no longer equal
        plant_1->production.constraint_max = test::create_t_double(1, 0.01);
        CHECK_NE(*plant_1, *plant_2);

        // Add attributes to other object, now equal again
        plant_2->production.constraint_max = test::create_t_double(1, 0.01);
        CHECK_EQ(*plant_1, *plant_2);

        // Add unit attribute to first object, no longer equal
        unit_1->production.static_max = test::create_t_double(1, 10.0);
        CHECK_NE(*plant_1, *plant_2);

        // Add unit attribute to other object, now equal again
        unit_2->production.static_max = test::create_t_double(1, 10.0);
        CHECK_EQ(*plant_1, *plant_2);
    }
}
