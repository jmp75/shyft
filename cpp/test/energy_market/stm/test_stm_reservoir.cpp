#include <string>
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <boost/format.hpp>
#include <shyft/energy_market/a_wrap.h>

#include <test/energy_market/stm/utilities.h>

using namespace std;
using namespace shyft::energy_market::stm;
using boost::format;
namespace dont_use_this_now {
    using shyft::energy_market::url_fx_t;
    // for testing out the a_wrap class without the py dependency.
    template <typename A>
    struct a_wrap {
        url_fx_t url_fx; ///< the callable url_fx, provided by the python attribute expose of the a_wrap<T>.. 
        std::string a_name;///< the visible attribute name in python.
        A& a;///< attribute object reference, to location within the  owning object(might be nested aggregate in e.g. unit,)
        
        a_wrap()=delete;///< don't allow default construct.
        a_wrap(url_fx_t &&url_fx,std::string a_name,A& a):url_fx{url_fx},a_name{a_name},a{a} {}
        a_wrap(const a_wrap &c):url_fx{c.url_fx},a_name{c.a_name},a{c.a} {}
        a_wrap ( a_wrap&&c):url_fx{std::move(c.url_fx)},a_name{c.a_name},a{c.a} {}
        
        
        /** @brief generate an almost unique, url-like string for a proxy exposed attribute.
         * 
         * @param prefix: What the resulting string will start with
         * @param levels: How many levels of the url to include.
         *      levels== 0 includes only this level. (Use levels < 0 to get all levels)
         * @param placeholders: The last element of the vector states whether to use the attribute ID or place_holder
         *  "${attr_id}". The remaining vector will be used in subsequent levels of the url.
         *  If the vector is empty, the function defaults to not using placeholder in the url
         */
        std::string url(std::string prefix="",int levels=-1,int template_levels=-1) const {
            std::string s;
            auto sbi=std::back_inserter(s);
            std::copy(begin(prefix),end(prefix),sbi);
            url_fx(sbi,levels,template_levels,"");
            std::string attr_part= template_levels==0?std::string("${attr_id}"):a_name;
            return (format("%1%.%2%")%s%attr_part).str(); 
        }

        /** @brief exits returns true if the attribute is kind of non-null, false otherwise */
        bool exists() const {
            if constexpr(std::is_same<A,apoint_ts>::value) {
                return a.ts.get()?true:false;
            } else { // just assume it's a shared-ptr, compiler will tell you if not
              return a.get()?true:false;  
            }
        }
        /** @brief remove() nullfies the attribute, so that after call .exists() ==false */
        void remove() {
            if constexpr(std::is_same<A,apoint_ts>::value) {
                a=apoint_ts();
            } else {
                a.reset(); // just assume it's a shared-ptr, compiler will tell you if not
            }
        }
        
        /** @brief equal operator, propagate to the attribute type */
        bool operator==( a_wrap const&b) const { 
            if constexpr(std::is_same<A,apoint_ts>::value) {
                return a==b.a;
            } else {
                return (!a && !b.a) || (a&&b.a && *a == *b.a);// ... we could utilize stm::equal.. here.. 
            }
        }
        bool operator!=( a_wrap const&other) const {return !operator==(other);};
    };
}

using shyft::time_series::dd::apoint_ts;
using shyft::energy_market::sbi_t;
using shyft::energy_market::a_wrap;
using namespace shyft::energy_market::stm;// enable equal operatos

TEST_SUITE("stm_reservoir"){
	TEST_CASE("reservoir basics") {
        const auto name{ "res_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto r = b.create_reservoir(1, name,json_string);
        CHECK_EQ(r->id, 1);
        CHECK_EQ(r->name, name);
        CHECK_EQ(r->json,json_string);
        CHECK_THROWS_AS(b.create_reservoir(1,name+"a",""),stm_rule_exception);
        CHECK_THROWS_AS(b.create_reservoir(2,name,""),stm_rule_exception);
        string s;
        sbi_t sbi(s);
        r->inflow.url_fx(sbi,-1,-1,".schedule");
        CHECK_EQ(s,"/HPS1/R1.inflow.schedule");
        
        // verify a_wrap at primary and secondary-nested levels
        a_wrap<apoint_ts> ris(
            [o=&r->inflow](sbi_t& so,int l, int tl,string_view sv)->void {
               o->url_fx(so,l,tl,sv); 
            },
            "schedule",
            r->inflow.schedule
        );

        CHECK_EQ(ris.url("",0,-1),".inflow.schedule");
        CHECK_EQ(ris.url("",0,0),".${attr_id}");
        CHECK_EQ(ris.url("",0,1),".inflow.schedule");

        CHECK_EQ(ris.url("",1,-1),"/R1.inflow.schedule");
        CHECK_EQ(ris.url("",1,0),"/R${rsv_id}.${attr_id}");
        CHECK_EQ(ris.url("",1,1),"/R${rsv_id}.inflow.schedule");
        CHECK_EQ(ris.url("",1,2),"/R1.inflow.schedule");
        
        CHECK_EQ(ris.url("",2,-1),"/HPS1/R1.inflow.schedule");
        CHECK_EQ(ris.url("",2,0),"/HPS${hps_id}/R${rsv_id}.${attr_id}");
        CHECK_EQ(ris.url("",2,1),"/HPS${hps_id}/R${rsv_id}.inflow.schedule");
        CHECK_EQ(ris.url("",2,2),"/HPS${hps_id}/R1.inflow.schedule");
        CHECK_EQ(ris.url("",2,3),"/HPS1/R1.inflow.schedule");
        string a_name="endpoint_desc";
        string a_id="${attr_id}";
        a_wrap<apoint_ts> res(
            [o=r,a_name,a_id](sbi_t& so,int l, int tl,string_view /*sv*/)->void {
                if(l)
                    o->generate_url(so,l-1,tl>0?tl-1:tl); 
            },
            a_name,
            r->endpoint_desc
        );
        CHECK_EQ(res.url("",0,-1),".endpoint_desc");
        CHECK_EQ(res.url("",0,0),".${attr_id}");
        CHECK_EQ(res.url("",0,1),".endpoint_desc");

        CHECK_EQ(res.url("",1,-1),"/R1.endpoint_desc");
        CHECK_EQ(res.url("",1,0),"/R${rsv_id}.${attr_id}");
        CHECK_EQ(res.url("",1,1),"/R${rsv_id}.endpoint_desc");
        CHECK_EQ(res.url("",1,2),"/R1.endpoint_desc");
        r->volume_level_mapping=make_shared<t_xy_::element_type>();
        auto level_volume = make_shared<xy_point_curve_::element_type>();
        using shyft::energy_market::hydro_power::point;
        using shyft::core::from_seconds;
        (*level_volume).points.push_back(point(1000,0));
        (*level_volume).points.push_back(point(1010,10));
        (*r->volume_level_mapping)[from_seconds(0)]= level_volume;
        auto blob=stm_hps::to_blob(shp);
        auto o=stm_hps::from_blob(blob);
        auto r2=std::dynamic_pointer_cast<reservoir>(o->reservoirs[0]);
        CHECK_EQ(r2->id,r->id);
        CHECK_EQ(true,r2->volume_level_mapping.get()!=nullptr);
        auto const& m1=*r2->volume_level_mapping;
        auto const& m2=*r->volume_level_mapping;
        CHECK_EQ(m1.size(),m2.size());
        for(auto const&kv:m2) {
            auto f=m1.find(kv.first);
            if(f==m1.end()) {
                MESSAGE("Failed to find key");
                CHECK(f==m1.end());
                continue;
            }
            if( *(*f).second != (*kv.second) ) {
                MESSAGE("Different values..");
                CHECK(false);//
            }
        }
    }

    TEST_CASE("stm_reservoir_equality") {
        
        // Test that stm::reservoir::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto rsv_1 = make_shared<reservoir>(11, "test", "", hps_1);
        auto rsv_2 = make_shared<reservoir>(11, "test", "", hps_2);

        using shyft::energy_market::hydro_power::xy_point_curve;

        // All attributes left at default, objects should be equal
        CHECK_EQ(*rsv_1, *rsv_2);

        // Add some attributes to first object, no longer equal
        rsv_1->level.regulation_min = test::create_t_double(1, 101.0);
        rsv_1->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
        CHECK_NE(*rsv_1, *rsv_2);

        // Add attributes to other object, now equal again
        rsv_2->level.regulation_min = test::create_t_double(1, 101.0);
        rsv_2->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
        CHECK_EQ(*rsv_1, *rsv_2);
    }
}
