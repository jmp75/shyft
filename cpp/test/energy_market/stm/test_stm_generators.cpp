#include <test/test_pch.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/json_struct.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/grammar.h>

#include <test/web_api/test_parser.h>

#include <shyft/energy_market/srv/model_info.h>

using shyft::energy_market::srv::model_info;
using shyft::core::utctime;
using shyft::core::no_utctime;
using shyft::core::from_seconds;

using shyft::web_api::grammar::stm_run_grammar;
using shyft::web_api::grammar::json_grammar;

using shyft::web_api::energy_market::json;

using std::string;
using std::vector;
using std::make_shared;

using namespace shyft::web_api::generator;

TEST_SUITE("stm_generators") {
    TEST_CASE("model_info") {
       // Using generate:
       string ps;
       auto sink = std::back_inserter(ps);
       model_info mi(1, "mi1",  from_seconds(10));
       CHECK_EQ(true, generate(sink, model_info_generator<decltype(sink)>(), mi));
       CHECK_EQ(ps, R"_({"id":1,"name":"mi1","created":10.0,"json":""})_");

       // With emit:
       ps.clear();
       CHECK_EQ(ps.size(), 0);
       emit(sink, mi);
       CHECK_EQ(ps, R"_({"id":1,"name":"mi1","created":10.0,"json":""})_");
    }

    TEST_CASE("model_ref") {
       // Using generate:
       string ps;
       auto sink = std::back_inserter(ps);
       model_ref ri("testhost", 123, 456,"mkey");

       CHECK_EQ(true, generate(sink, model_ref_generator<decltype(sink)>(), ri));
       CHECK_EQ(ps, R"_({"host":"testhost","port_num":123,"api_port_num":456,"model_key":"mkey"})_");

       // Using emit:
       ps.clear();
       CHECK_EQ(ps.size(), 0);
       emit(sink, ri);
       CHECK_EQ(ps, R"_({"host":"testhost","port_num":123,"api_port_num":456,"model_key":"mkey"})_");
    }

    TEST_CASE("stm_run") {
       // Using generate:
       string ps;
       auto sink = std::back_inserter(ps);
       stm_run arun(1, "testrun", from_seconds(15));

       CHECK_EQ(true, generate(sink, stm_run_generator<decltype(sink)>(), arun));
       CHECK_EQ(ps, R"_({"id":1,"name":"testrun","created":15.0,"json":"","labels":[],"model_refs":[]})_");

       // Using emit
       ps.clear();
       CHECK_EQ(ps.size(),0);
       emit(sink, arun);
       CHECK_EQ(ps, R"_({"id":1,"name":"testrun","created":15.0,"json":"","labels":[],"model_refs":[]})_");

       // A more complicated example:
       stm_run brun(2, "testrun2", from_seconds(20), "{misc.}", {"test", "testlabel"},
           {std::make_shared<model_ref>("testhost", 123, 456, "mkey")});
       ps.clear();
       CHECK_EQ(ps.size(),0);
       emit(sink, brun);
       CHECK_EQ(ps, R"_({"id":2,"name":"testrun2","created":20.0,"json":"{misc.}","labels":["test","testlabel"],"model_refs":[{"host":"testhost","port_num":123,"api_port_num":456,"model_key":"mkey"}]})_");

       // Generating for shared pointer:
       auto crun = make_shared<stm_run>(3, "testrun3", from_seconds(5));
       ps.clear();
       CHECK_EQ(ps.size(), 0);
       emit(sink, crun);
       CHECK_EQ(ps, R"_({"id":3,"name":"testrun3","created":5.0,"json":"","labels":[],"model_refs":[]})_");

       ps.clear();
       crun = nullptr;
       emit(sink, crun);
       CHECK_EQ(ps, "null");
    }

    TEST_CASE("escaped_string") {
        string ps;
        auto sink = std::back_inserter(ps);
        escaped_string_generator<decltype(sink)> g;
        // 0. Normal case
        string normal = "A string";
        CHECK_EQ(true, generate(sink, g, normal));
        CHECK_EQ(ps, "A string");
        ps.clear();
        // 1. String with escaped characters:
        string esc_str = "A string with \"quotation\" characters in it.";
        CHECK_EQ(true, generate(sink, g, esc_str));
        CHECK_EQ(ps, R"_(A string with \"quotation\" characters in it.)_");
    }

    TEST_CASE("valid_json_string") {
       // Test data:
       stm_run run(1, "testrun", from_seconds(10), "{\"a\": \"value\"}");
       string ps;
       auto sink = std::back_inserter(ps);
       stm_run_grammar<const char*> stm_run_;
       json_grammar<const char*> json_;
       // 0. Generate string from stm_run:
       CHECK_EQ(true, generate(sink, stm_run_generator<decltype(sink)>(), run));
       CHECK_EQ(ps, "{\"id\":1,\"name\":\"testrun\",\"created\":10.0,\"json\":\"{\\\"a\\\": \\\"value\\\"}\",\"labels\":[],\"model_refs\":[]}");
       // 1. Parse to get a new run:
       stm_run prun;
       CHECK_EQ(test::phrase_parser(ps.c_str(), stm_run_, prun), true);
       CHECK_EQ(run, prun);
       // 2. Parse the run's json attribute:
       json data;
       CHECK_EQ(test::phrase_parser(prun.json.c_str(), json_, data), true);
       CHECK_EQ(data.required<string>("a"), "value");
    }

    TEST_CASE("stm_session") {
        // Test data:
        string ps;
        auto sink = std::back_inserter(ps);
        stm_session_generator<decltype(sink)> session_;
        // 0. from base_type:
        auto r1 = std::make_shared<stm_run>(2, "testrun", from_seconds(5));
        stm_session session(1, "testsession", from_seconds(20), "{misc.}",
            {"label1", "label2"}, {r1},
            model_ref("testhost", 12, 34, "mdlk"), "atask");
        CHECK_EQ(true, generate(sink, session_, session));
        CHECK_EQ(ps, R"_({"id":1,"name":"testsession","created":20.0,"json":"{misc.}","labels":["label1","label2"],"runs":[{"id":2,"name":"testrun","created":5.0,"json":"","labels":[],"model_refs":[]}],"base_model":{"host":"testhost","port_num":12,"api_port_num":34,"model_key":"mdlk"},"task_name":"atask"})_");

        // 1. Using emit:
        string pse;
        auto sinke = std::back_inserter(pse);
        emit(sinke, session);
        CHECK_EQ(pse, ps);

        // 2. Emitting shared pointer:
        pse.clear();
        auto session_ptr = make_shared<stm_session>(session);
        emit(sinke, session_ptr);
        CHECK_EQ(pse, ps);

    }
}