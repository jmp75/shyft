#include <doctest/doctest.h>
#include "../serialize_loop.h"
#include <shyft/energy_market/stm/srv/run/stm_run.h>


TEST_SUITE("stm_run") {
    using namespace shyft::energy_market::stm::srv;

    TEST_CASE("model_ref") {
        model_ref r1{"host", 123, 456, "key"};
        CHECK_EQ(r1, model_ref{"host", 123, 456, "key"});
        CHECK_EQ(model_ref{}, model_ref{});

        CHECK_NE(r1, model_ref{"guest", 123, 456, "key"});
        CHECK_NE(r1, model_ref{"host", 321, 456, "key"});
        CHECK_NE(r1, model_ref{"host", 123, 456, "lock"});
        CHECK_NE(r1, model_ref{"host", 123, 654, "key"});
    }

    TEST_CASE("run") {
        stm_run r2(1, "testrun", no_utctime);
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime});
        CHECK_NE(r2, stm_run{2, "testrun", no_utctime});
        CHECK_NE(r2, stm_run{1, "testrun", shyft::core::from_seconds(1)});

        // Adding json:
        r2.json = "misc.";
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime, "misc."});
        CHECK_NE(r2, stm_run{1, "testrun", no_utctime, "central."});

        // Adding labels:
        r2.labels.push_back("test");
        r2.labels.push_back("test2");
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime, "misc.", {"test", "test2"}});
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime, "misc.", {"test2", "test"}});
        CHECK_NE(r2, stm_run{1, "testrun", no_utctime, "misc.", {"production"}});

        // Adding model_refs:
        auto mr = std::make_shared<model_ref>("host", 123, 456, "key");
        auto mr2 = std::make_shared<model_ref>();
        r2.add_model_ref(mr);
        r2.add_model_ref(std::make_shared<model_ref>(model_ref()));
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime, "misc.",
                             {"test", "test2"}, {mr, mr2}});
        CHECK_EQ(r2, stm_run{1, "testrun", no_utctime, "misc.",
                             {"test", "test2"}, {mr2, mr}});
        CHECK_NE(r2, stm_run{1, "testrun", no_utctime, "misc.",
                             {"test", "test2"}, {mr2}});
        CHECK_EQ(mr, r2.model_refs[0]);
        CHECK_EQ(r2.model_refs.size(), 2);

        // Removing model references:
        CHECK(r2.remove_model_ref("key"));
        CHECK_EQ(r2.model_refs.size(), 1);
        CHECK_FALSE(r2.remove_model_ref("nokey"));
        CHECK(r2.remove_model_ref(""));
        CHECK_EQ(r2.model_refs.size(), 0);
    }

    TEST_CASE("stm_session") {
        // 0. Basic setup of stm_session:
        stm_session s1(1, "test-session", no_utctime);
        CHECK_EQ(s1, stm_session{1, "test-session", no_utctime});

        // 1. Add a run:
        auto r1 = std::make_shared<stm_run>(1, "testrun", no_utctime);
        auto r2 = std::make_shared<stm_run>(1, "testrun2", no_utctime);
        auto r3 = std::make_shared<stm_run>(2, "testrun", no_utctime);
        auto r4 = std::make_shared<stm_run>(2, "testrun2", no_utctime);

        s1.add_run(r1);
        CHECK_EQ(s1.runs.size(), 1);
        CHECK_EQ(*(s1.runs[0]), *r1);
        CHECK_THROWS(s1.add_run(r2));
        CHECK_THROWS(s1.add_run(r3));
        s1.add_run(r4);
        CHECK_EQ(s1.runs.size(), 2);
        CHECK_EQ(*(s1.runs[1]), *r4);

        // 2. Get runs:
        CHECK_EQ(nullptr, s1.get_run(3));
        CHECK_EQ(*r1, *(s1.get_run(1)));
        CHECK_EQ(nullptr, s1.get_run("norun"));
        CHECK_EQ(*r4, *(s1.get_run("testrun2")));
        // 3. remove runs:
        CHECK_FALSE(s1.remove_run(3)); // Trying to remove run with non existent ID
        CHECK_FALSE(s1.remove_run("norun")); // Trying to remove run by non existent name.

        CHECK(s1.remove_run(1));
        CHECK_EQ(s1.runs.size(), 1);
        CHECK(s1.remove_run("testrun2"));
        CHECK_EQ(s1.runs.size(),0);
    }

    TEST_CASE("run_serialization") {
        auto ri = std::make_shared<model_ref>("host", 123, 456, "key");
        auto ri2 = std::make_shared<model_ref>();
        stm_run run(1, "testrun", no_utctime, "misc.",
                   {"test", "test2"}, {ri, ri2});

        auto ri3 = test::serialize_loop(*ri);
        CHECK_EQ(*ri, ri3);
        auto run2 = test::serialize_loop(run);
        CHECK_EQ(run, run2);
    }
}
