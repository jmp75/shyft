#include <doctest/doctest.h>
#include <shyft/energy_market/stm/srv/run/server.h>
#include <shyft/energy_market/stm/srv/run/client.h>
#include <shyft/energy_market/stm/srv/run/stm_run.h>
#include <test/test_utils.h>

using std::to_string;
using std::string;
using std::vector;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::no_utctime;
using shyft::core::to_seconds64;
using shyft::core::from_seconds;
using shyft::core::utcperiod;

TEST_SUITE("run_server") {
    using shyft::energy_market::stm::srv::run::server;
    using shyft::energy_market::stm::srv::run::client;
    using shyft::energy_market::stm::srv::stm_session;
    using shyft::energy_market::stm::srv::stm_run;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::srv::model_info;

    //-- keep test directory unique and with auto-cleanup.
    auto run_dir = "stm.run.server.test." + to_string(to_seconds64(utctime_now()));
    test::utils::temp_dir srvdir(run_dir.c_str());

    TEST_CASE("server") {
        // 0. Arrange server and a client running on localhost, tempdir:
        server s0(srvdir.string());
        string host_ip = "127.0.0.1";
        s0.set_listening_ip(host_ip);
        auto s_port = s0.start_server();
        CHECK_GE(s_port, 1024);

        client c{host_ip + ":" + to_string(s_port)};
        vector<int64_t> mids;
        auto mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 0);
        // 1. Create one stm_session:
        auto session = std::make_shared<stm_session>(1, "testsession", from_seconds(3600));
        session->labels.push_back("test");

        auto r = std::make_shared<stm_run>(1, "testrun", from_seconds(3600));
        r->model_refs.push_back(std::make_shared<model_ref>("host", 123, 456, "key"));
        model_info mi(session->id, session->name, session->created, session->json);
        session->add_run(r);
        // 2. store it to server:
        auto rid = c.store_model(session, mi);
        CHECK_EQ(rid, session->id);

        // 3. verify model info and run:
        auto session2 = c.read_model(session->id);
        CHECK_EQ(*session, *session2);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 1u);
        CHECK_EQ(mi, mis[0]);

        // 4. Filtered get_model_infos search:
        auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
        CHECK_EQ(mis2.size(), 0u);
        mis2 = c.get_model_infos(mids, utcperiod(2000, 4000));
        CHECK_EQ(mis2.size(), 1u);
        CHECK_EQ(mi, mis2[0]);

        c.close(); // To force auto-open on next call
        // 5. Check that we can update model info of existing run
        mi.json = string("{\"updated\": true}");
        c.update_model_info(mi.id, mi);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 1u);
        CHECK_EQ(mi, mis[0]);

        // 6. Add run to a session:
        auto r2 = std::make_shared<stm_run>(2, "testrun2", from_seconds(10));
        c.add_run(session->id, r2);
        session2 = c.read_model(session->id);
        CHECK_EQ(session2->runs.size(), 2);
        CHECK_THROWS(c.add_run(1, r2));

        // 7. Get run from a session:
        CHECK_EQ(*r, *(c.get_run(session->id, r->id)));
        CHECK_EQ(nullptr, c.get_run(session->id, 4));
        CHECK_THROWS(c.get_run(-1, r->id));

        CHECK_EQ(*r2, *(c.get_run(session->id, r2->name)));
        CHECK_EQ(nullptr, c.get_run(session->id, "norun"));

        // 8. Add a model_reference to a run:
        auto mr = std::make_shared<model_ref>("host", 12, 34, "testkey");
        c.add_model_ref(session->id, r->id, mr);
        CHECK_EQ(*mr, *(c.get_model_ref(session->id, r->id, "testkey")));
        CHECK_EQ(nullptr, c.get_model_ref(session->id, r->id, "nonkey"));
        CHECK_EQ(nullptr, c.get_model_ref(session->id, -1, "testkey"));
        CHECK_THROWS(c.get_model_ref(-1, r->id, "testkey"));

        // Some invalid inputs:
        CHECK_EQ(c.get_run(session->id, r->id)->model_refs.size(), 2);
        c.add_model_ref(session->id, -1, mr); // Invalid run ID, so should not be added.
        CHECK_EQ(c.get_run(session->id, r->id)->model_refs.size(), 2);

        // 9. Remove model reference from a run:
        CHECK(c.remove_model_ref(session->id, r->id, "testkey"));
        CHECK_EQ(c.get_run(session->id, r->id)->model_refs.size(), 1);
        CHECK_THROWS(c.remove_model_ref(-1, r->id, "testkey"));
        CHECK_FALSE(c.remove_model_ref(session->id, -1, "testkey"));
        CHECK_FALSE(c.remove_model_ref(session->id, r->id, "nonkey"));

        // 10. Remove runs from a session:
        CHECK_FALSE(c.remove_run(session->id, 3));
        CHECK(c.remove_run(session->id, 2));
        session2 = c.read_model(session->id);
        CHECK_EQ(session2->runs.size(), 1);
        CHECK_FALSE(c.remove_run(session->id, 2));

        CHECK_FALSE(c.remove_run(session->id, "norun"));
        CHECK(c.remove_run(session->id, "testrun"));
        session2 = c.read_model(session->id);
        CHECK_EQ(session2->runs.size(), 0);
        CHECK_FALSE(c.remove_run(session->id, "testrun"));
        // Finally, remove the model, and verify we're are back to zero:
        c.remove_model(session->id);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 0u);
        c.close();
        s0.clear();

    }

}
