#include <doctest/doctest.h>

#include <string>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/stm_system.h>

#include <test/energy_market/stm/utilities.h>

using namespace std;
using namespace shyft::energy_market::stm;
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE("stm_waterway") {

    TEST_CASE("stm_gate_equality") {
        // Test that stm::wtr::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto gate_1 = builder(hps_1).create_gate(14, "test", "");
        auto gate_2 = builder(hps_2).create_gate(14, "test", "");


        // All attributes left at default, objects should be equal
        CHECK_EQ(*gate_1, *gate_2);

        // Add some attributes to first object, no longer equal
        gate_1->discharge.static_max = test::create_t_double(1, 10.0);
        gate_1->flow_description = test::create_t_xyz(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_NE(*gate_1, *gate_2);

        // Add attributes to other object, now equal again
        gate_2->discharge.static_max = test::create_t_double(1, 10.0);
        gate_2->flow_description = test::create_t_xyz(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_EQ(*gate_1, *gate_2);
    }

    TEST_CASE("stm_waterway_equality") {
        // Test that stm::wtr::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto wtr_1 = builder(hps_1).create_waterway(11, "test", "");
        auto wtr_2 = builder(hps_2).create_waterway(11, "test", "");

        auto gate_1 = builder(hps_1).create_gate(14, "test", "");
        auto gate_2 = builder(hps_2).create_gate(14, "test", "");

        // All attributes left at default, objects should be equal
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add gate to one waterway, no longer equal
        waterway::add_gate(wtr_1, gate_1);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add gate to the other waterway, equal again
        waterway::add_gate(wtr_2, gate_2);
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add some attributes to first object, no longer equal
        wtr_1->head_loss_coeff = test::create_t_double(1, 0.01);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add attributes to other object, now equal again
        wtr_2->head_loss_coeff = test::create_t_double(1, 0.01);
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add gate attribute to first object, no longer equal
        gate_1->discharge.static_max = test::create_t_double(1, 10.0);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add gate attribute to other object, now equal again
        gate_2->discharge.static_max = test::create_t_double(1, 10.0);
        CHECK_EQ(*wtr_1, *wtr_2);

    }
}
