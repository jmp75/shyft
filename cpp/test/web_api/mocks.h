#pragma once
#include <shyft/core/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/time_series_merge.h>
#include <shyft/time_series/dd/apoint_ts.h>



namespace mocks {
    using std::string;
    using std::vector;
    using namespace shyft;
    using namespace shyft::core;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_series::ts_point_fx;

    static inline shyft::core::utctime _t(int64_t t1970s) {return shyft::core::utctime{shyft::core::seconds(t1970s)};}

    
    inline vector<apoint_ts> mk_expression(const string & ts_url_base, utctime t, utctimespan dt, int n) {
        auto  aa = apoint_ts(ts_url_base + "aa", apoint_ts(gta_t(t, dt, n), -double(n) / 2.0, ts_point_fx::POINT_AVERAGE_VALUE));
        auto a = apoint_ts(ts_url_base,aa*3.0 + aa);
        return {aa, a};
    }
    
    inline vector<apoint_ts> mk_expressions(const string& cname) {
        calendar utc;
        auto t = utc.time(1970, 1, 1);
        auto dt = deltahours(1);
        auto dt24 = deltahours(24);
        int n = 240;
        time_axis::fixed_dt ta(t, dt, n);
        // Create some time series:
        std::vector<apoint_ts> tsl;
        size_t kb = 4;
        tsl.reserve((16-kb));
        for ( ; kb < 16 ; kb += 2) {
            auto ts_url=string("shyft://" + cname + "/ts") + std::to_string(kb);
            auto tsv = mk_expression(ts_url, t, dt, kb * 1000);
            tsl.insert(tsl.end(), tsv.begin(), tsv.end());
        }
        return tsl;
    }
    
}
